plugins {
    kotlin("jvm") version "1.6.0"
    id("kotlinx-serialization") version "1.6.0"
    id("maven-publish")
    id("org.jlleitschuh.gradle.ktlint") version "10.2.0"
    id("org.jetbrains.kotlinx.kover") version "0.4.2"
    id("io.github.gradle-nexus.publish-plugin") version "1.1.0"
    id("org.jetbrains.dokka") version "1.6.10"
    signing
}

val sourcesJar by tasks.creating(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets.getByName("main").allSource)
}

val testJar by tasks.registering(Jar::class) {
    archiveClassifier.set("tests")
    from(sourceSets.test.get().output)
    exclude("*.txt")
    exclude(".lsp/")
    exclude("sample_projects/")
    exclude("**/*UnitTest*.class")
}

val javadocJar by tasks.creating(Jar::class) {
    archiveClassifier.set("javadoc")
    from("$buildDir/dokka/javadoc")
}
tasks.getByName("javadocJar").dependsOn("dokkaJavadoc")
tasks.getByName("build").dependsOn("dokkaJavadoc")

group = Publish.GROUP
version = "0.9.15" // Upgrade to Kotlinx serialization version 1.3.2; do not sign the local maven deploys; use a new
// publishing script (publish.sh, which is not committed) to deploy to Maven Central.

val artifactVersion = version as String

signing {
    isRequired = project.property("signing.keyId") != "NoSuchKey"
    if (isRequired) sign(publishing.publications)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            artifactId = rootProject.name
            from(components["kotlin"])
            artifact(javadocJar)
            artifact(sourcesJar)
            artifact(testJar)
            Publish.populatePom(pom)
        }
    }

    repositories {
        maven {
            url = uri(Publish.getRepositoryUrl(artifactVersion))
            credentials {
                username = project.property("SONATYPE_NEXUS_USERNAME") as String
                password = project.property("SONATYPE_NEXUS_PASSWORD") as String
            }
        }
    }
}
tasks.getByName("build").dependsOn("publishToMavenLocal")

nexusPublishing {
    repositories {
        sonatype {
            nexusUrl.set(uri("https://s01.oss.sonatype.org/service/local/"))
            snapshotRepositoryUrl.set(uri("https://s01.oss.sonatype.org/content/repositories/snapshots/"))
            username.set(project.property("SONATYPE_NEXUS_USERNAME") as String)
            password.set(project.property("SONATYPE_NEXUS_PASSWORD") as String)
        }
    }
}

ktlint {
    verbose.set(true)
    outputToConsole.set(true)
    coloredOutput.set(true)
    debug.set(false)
    android.set(false)
    outputColorName.set("RED")
    ignoreFailures.set(false)
    enableExperimentalRules.set(false)
    reporters {
        reporter(org.jlleitschuh.gradle.ktlint.reporter.ReporterType.CHECKSTYLE)
        reporter(org.jlleitschuh.gradle.ktlint.reporter.ReporterType.JSON)
        reporter(org.jlleitschuh.gradle.ktlint.reporter.ReporterType.HTML)
    }
    filter {
        exclude("**/style-violations.kt")
        exclude("**/generated/**")
        include("src/**/*.kt")
    }
}

kover {
    isEnabled = true
    coverageEngine.set(kotlinx.kover.api.CoverageEngine.INTELLIJ)
    intellijEngineVersion.set("1.0.622")
    generateReportOnCheck.set(true)
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

repositories {
    mavenCentral()
    mavenLocal()
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.2")
    implementation("org.junit.jupiter:junit-jupiter:5.8.2")

    testImplementation("org.jetbrains.kotlin:kotlin-test:1.5.21")
}

tasks {
    compileKotlin { kotlinOptions.jvmTarget = "11" }
    compileTestKotlin { kotlinOptions.jvmTarget = "11" }
    test {
        useJUnitPlatform()
        extensions.configure(kotlinx.kover.api.KoverTaskExtension::class) {
            isEnabled = true
            binaryReportFile.set(file("$buildDir/custom/result.bin"))
            includes = listOf("""com.pajato..*""")
        }
    }
    koverVerify {
        rule {
            name = "Covered Snippet Percentage"
            bound { minValue = 100 }
        }
    }
    koverHtmlReport { isEnabled = true }
    koverXmlReport { isEnabled = true }
}
