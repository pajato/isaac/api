# api

A Kotlin representation and implementation of the [Language Server Protocol](https://microsoft.github.io/language-server-protocol/specifications/specification-current/) (LSP) specification interfaces, i.e. an SDK to support developing LSP clients, servers and tools.

## Overview

The component is based on LSP specification version 3.16.x. Each Typescript interface in that spec has a Kotlin definition in this SDK.

The Kotlin definition is generally an interface and is found in a file named using the LSP/Typescript interface name. Along with the Kotlin interface, the file contains a constructor (convenience) function, a concrete Kotlin implementation for the interface and serialization support.

For example, the LSP interface Position and associated artifacts are represented in Kotlin as:

```kotlin
package com.pajato.lsp4k.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface Position {
    val line: Int
    val character: Int
}

fun Position(line: Int = 0, character: Int = 0): Position = Lsp4kPosition(line, character)

@Serializable class Lsp4kPosition(override val line: Int, override val character: Int) : Position

object PositionSerializer : JsonContentPolymorphicSerializer<Position>(Position::class) {
    override fun selectDeserializer(element: JsonElement) = Lsp4kPosition.serializer()
}
```
## Serialization

LSP does not support the use of type keys (discriminants) in client server request and response processing so Lsp4k uses Kotlin content and transforming serialization support to bridge the gap between Kotlin and Typescript interface syntax.

Content (```JsonContentPolymorphicSerializer<T>```) is the default and preferred means for serializing without a type discriminant.

Transforming (```JsonTransformingPolymprphicSerializer```) adds the ability to transform the JSON representation of the interface as necessary (object to/from primitive, for example).

## Use cases

This component can be used by LSP client and server developers building projects using Kotlin. It is the lowest level component in that it has no dependencies apart from the Kotlin language and its standard libraries.

## Structure

The component provides a single package containing (basically) a single Kotlin file for each interface defined in the LSP spec. There are on the order of two hundred such files. Each file defines an interface and contains a public creation function, a private implementation class and one or more serialization objects to support serializing and de-serializing LSP objects between clients and servers.

## Building and deploying

As of version 1.0, every executable snippet of source code is covered by at least one unit test, there are no warnings and no todo items.

The component is deployed to MavenCentral (groupId == "com.pajato.isaac", artifactId == "api", version == "1.0.0") using Gradle publishing support.




## Support

If you encounter a bug, feel like an important feature is missing or generally have an observation to make, please file an issue on this repo.


## Roadmap

tbd

## Contributing

See the separate doc: [CONTRIBUTING.md](CONTRIBUTING.md)

## Authors and acknowledgment

tbd

## License

This project is licensed using the GPL. See the separate document [LICENSE](LICENSE) for details.

## Project status

Active and early stages of development.
