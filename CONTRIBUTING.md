wip:

For a PR to be accepted (merged) the following will be evaluated and considered:

Is the source code clean? See the article [Clean Code with Kotlin](https://magdamiu.com/2021/08/23/clean-code-with-kotlin-2/) for some excellent details and examples.
- All spurious and unnecessary comments have been removed.
- Functions are small, approximately four (or less) lines of executable code.
- Block structure (embedded or nested functions) is employed (see Structure and Interpretation fo Computer Programs - SICP, by Abelson & Sussman, MIT Press)
- The source code screams "Language Server Protocol".
- Names reveal intent.
- The code reads left to right like well written prose. (Grady Booch, Hal Abelson)
- All executable source code snippets are covered by at least one test.
- All bad code smells have been removed.

Does the PR address a bug, issue, etc.?
