import java.io.File

object Signing {
    private const val keyId = "1242F6ED"
    private const val password = "Blue Foundation Award"
    private val secretKeyRingFile = File("""${System.getProperty("user.home")}/.gnupg/isaac/secrets/secring.gpg""")

    fun printSecrets() {
        println("""The secrets are: "$keyId", "$password", and "${secretKeyRingFile.absolutePath}".""")
    }
}
