package com.pajato.isaac.generalclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.GeneralClientCapabilities
import com.pajato.isaac.api.GeneralClientCapabilitiesSerializer
import com.pajato.isaac.api.RegularExpressionsClientCapabilities
import com.pajato.isaac.api.SerializerTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class RegularExpressionsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing with a regular expressions client capabilities property, verify a correct result`() {
        val engine = "Some Engine"
        val version = "12.4.5"
        val prop = RegularExpressionsClientCapabilities(engine, version)
        val data = GeneralClientCapabilities(regularExpressions = prop)
        val json = """{"regularExpressions":{"engine":"$engine","version":"$version"}}"""

        testEncode(json, GeneralClientCapabilitiesSerializer, data)
        testDecode(json, GeneralClientCapabilitiesSerializer) { actualObject ->
            val regularExpressions = actualObject.regularExpressions

            assertTrue(regularExpressions is RegularExpressionsClientCapabilities)
            assertEquals(engine, regularExpressions.engine)
            assertEquals(version, regularExpressions.version)
        }
    }
}
