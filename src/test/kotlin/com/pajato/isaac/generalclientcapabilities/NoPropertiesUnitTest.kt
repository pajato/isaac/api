package com.pajato.isaac.generalclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.GeneralClientCapabilities
import com.pajato.isaac.api.GeneralClientCapabilitiesSerializer
import com.pajato.isaac.api.SerializerTest
import kotlin.test.Test
import kotlin.test.assertEquals

class NoPropertiesUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a default general client capabilities object, verify a correct result`() {
        val data = GeneralClientCapabilities()
        val json = """{}"""

        testEncode(json, GeneralClientCapabilitiesSerializer, data)
        testDecode(json, GeneralClientCapabilitiesSerializer) { actualObject ->
            assertEquals(null, actualObject.regularExpressions)
            assertEquals(null, actualObject.markdown)
        }
    }
}
