package com.pajato.isaac.generalclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.GeneralClientCapabilities
import com.pajato.isaac.api.GeneralClientCapabilitiesSerializer
import com.pajato.isaac.api.IsaacGeneralClientCapabilities
import com.pajato.isaac.api.IsaacMarkdownClientCapabilities
import com.pajato.isaac.api.MarkdownClientCapabilities
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class MarkdownUnitTest : ApiTestProfiler() {
    private val parser = "Some Parser"
    private val version = "12.4.5"
    private val prop: MarkdownClientCapabilities = IsaacMarkdownClientCapabilities(parser, version)
    private val capabilities: GeneralClientCapabilities = IsaacGeneralClientCapabilities(markdown = prop)
    private val json = """{"markdown":{"parser":"$parser","version":"12.4.5"}}"""

    @Test
    fun `When encoding a general client capabilities object with a regular expressions property, verify()`() {
        val actualJson = Json.encodeToString(GeneralClientCapabilitiesSerializer, capabilities)

        assertEquals(json, actualJson)
    }

    @Test
    fun `When decoding a fully empty client capabilities object, verify the result()`() {
        val actualObject = Json.decodeFromString(GeneralClientCapabilitiesSerializer, json)
        val actualProp = actualObject.markdown

        assertTrue(actualObject is IsaacGeneralClientCapabilities)
        assertTrue(actualProp is IsaacMarkdownClientCapabilities)
        assertEquals(parser, actualProp.parser)
        assertEquals(version, actualProp.version)
    }
}
