package com.pajato.isaac.workspaceclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.WorkspaceClientCapabilities
import com.pajato.isaac.api.WorkspaceClientSerializer
import com.pajato.isaac.api.WorkspaceSymbolClientCapabilities
import kotlin.test.Test
import kotlin.test.assertEquals

class WorkspaceSymbolUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing with a default workspace symbol client capabilities object, verify result`() {
        val prop = WorkspaceSymbolClientCapabilities()
        val data = WorkspaceClientCapabilities(symbol = prop)
        val json = """{"symbol":{}}"""

        testEncode(json, WorkspaceClientSerializer, data)
        testDecode(json, WorkspaceClientSerializer) {
            assertEquals(null, it.applyEdit)
            assertEquals(null, it.workspaceEdit)
            assertEquals(null, it.didChangeConfiguration)
            assertEquals(null, it.didChangeWatchedFiles)
            assertEquals(null, it.symbol!!.dynamicRegistration)
            assertEquals(null, it.executeCommand)
            assertEquals(null, it.workspaceFolders)
            assertEquals(null, it.configuration)
            assertEquals(null, it.semanticTokens)
            assertEquals(null, it.codeLens)
            assertEquals(null, it.fileOperations)
        }
    }

    @Test fun `When serializing with a workspace symbol property, verify a correct result`() {
        val prop = WorkspaceSymbolClientCapabilities(dynamicRegistration = true)
        val data = WorkspaceClientCapabilities(symbol = prop)
        val json = """{"symbol":{"dynamicRegistration":true}}"""

        testEncode(json, WorkspaceClientSerializer, data)
        testDecode(json, WorkspaceClientSerializer) {
            assertEquals(null, it.applyEdit)
            assertEquals(null, it.workspaceEdit)
            assertEquals(null, it.didChangeConfiguration)
            assertEquals(null, it.didChangeWatchedFiles)
            assertEquals(true, it.symbol!!.dynamicRegistration)
            assertEquals(null, it.executeCommand)
            assertEquals(null, it.workspaceFolders)
            assertEquals(null, it.configuration)
            assertEquals(null, it.semanticTokens)
            assertEquals(null, it.codeLens)
            assertEquals(null, it.fileOperations)
        }
    }
}
