package com.pajato.isaac.workspaceclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.DidChangeWatchedFilesClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.WorkspaceClientCapabilities
import com.pajato.isaac.api.WorkspaceClientSerializer
import kotlin.test.Test
import kotlin.test.assertEquals

class DidChangeWatchedFilesUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing with a did change watched files property, verify a correct result`() {
        val prop = DidChangeWatchedFilesClientCapabilities(true)
        val data = WorkspaceClientCapabilities(didChangeWatchedFiles = prop)
        val json = """{"didChangeWatchedFiles":{"dynamicRegistration":true}}"""

        testEncode(json, WorkspaceClientSerializer, data)
        testDecode(json, WorkspaceClientSerializer) {
            assertEquals(null, it.applyEdit)
            assertEquals(null, it.workspaceEdit)
            assertEquals(null, it.didChangeConfiguration)
            assertEquals(true, it.didChangeWatchedFiles!!.dynamicRegistration)
            assertEquals(null, it.symbol)
            assertEquals(null, it.executeCommand)
            assertEquals(null, it.workspaceFolders)
            assertEquals(null, it.configuration)
            assertEquals(null, it.semanticTokens)
            assertEquals(null, it.codeLens)
            assertEquals(null, it.fileOperations)
        }
    }
}
