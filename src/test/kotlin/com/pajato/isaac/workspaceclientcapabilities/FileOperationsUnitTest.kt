package com.pajato.isaac.workspaceclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.FileOperations
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.WorkspaceClientCapabilities
import com.pajato.isaac.api.WorkspaceClientSerializer
import kotlin.test.Test
import kotlin.test.assertEquals

class FileOperationsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing with a file operations property, verify a correct result`() {
        val prop = FileOperations(dynamicRegistration = true)
        val data = WorkspaceClientCapabilities(fileOperations = prop)
        val json = """{"fileOperations":{"dynamicRegistration":true}}"""

        testEncode(json, WorkspaceClientSerializer, data)
        testDecode(json, WorkspaceClientSerializer) {
            assertEquals(null, it.applyEdit)
            assertEquals(null, it.workspaceEdit)
            assertEquals(null, it.didChangeConfiguration)
            assertEquals(null, it.didChangeWatchedFiles)
            assertEquals(null, it.symbol)
            assertEquals(null, it.executeCommand)
            assertEquals(null, it.workspaceFolders)
            assertEquals(null, it.configuration)
            assertEquals(null, it.semanticTokens)
            assertEquals(null, it.codeLens)
            assertEquals(true, it.fileOperations!!.dynamicRegistration)
        }
    }
}
