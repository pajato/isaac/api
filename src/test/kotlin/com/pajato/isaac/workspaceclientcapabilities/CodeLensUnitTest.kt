package com.pajato.isaac.workspaceclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.CodeLensWorkspaceClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.WorkspaceClientCapabilities
import com.pajato.isaac.api.WorkspaceClientSerializer
import kotlin.test.Test
import kotlin.test.assertEquals

class CodeLensUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a workspace client capabilities object with a semantic tokens property, verify()`() {
        val prop = CodeLensWorkspaceClientCapabilities(refreshSupport = true)
        val data = WorkspaceClientCapabilities(codeLens = prop)
        val json = """{"codeLens":{"refreshSupport":true}}"""

        testEncode(json, WorkspaceClientSerializer, data)
        testDecode(json, WorkspaceClientSerializer) {
            assertEquals(null, it.applyEdit)
            assertEquals(null, it.workspaceEdit)
            assertEquals(null, it.didChangeConfiguration)
            assertEquals(null, it.didChangeWatchedFiles)
            assertEquals(null, it.symbol)
            assertEquals(null, it.executeCommand)
            assertEquals(null, it.workspaceFolders)
            assertEquals(null, it.configuration)
            assertEquals(null, it.semanticTokens)
            assertEquals(true, it.codeLens!!.refreshSupport)
            assertEquals(null, it.fileOperations)
        }
    }
}
