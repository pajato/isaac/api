package com.pajato.isaac.workspaceclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.SemanticTokensWorkspaceClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.WorkspaceClientCapabilities
import com.pajato.isaac.api.WorkspaceClientSerializer
import kotlin.test.Test
import kotlin.test.assertEquals

class SemanticTokensUnitTest : SerializerTest, ApiTestProfiler() {
    private val prop = SemanticTokensWorkspaceClientCapabilities(refreshSupport = true)
    private val data = WorkspaceClientCapabilities(semanticTokens = prop)
    private val json = """{"semanticTokens":{"refreshSupport":true}}"""

    @Test fun `When serializing a workspace client capabilities object with a semantic tokens property, verify()`() {
        testEncode(json, WorkspaceClientSerializer, data)
        testDecode(json, WorkspaceClientSerializer) {
            assertEquals(null, it.applyEdit)
            assertEquals(null, it.workspaceEdit)
            assertEquals(null, it.didChangeConfiguration)
            assertEquals(null, it.didChangeWatchedFiles)
            assertEquals(null, it.symbol)
            assertEquals(null, it.executeCommand)
            assertEquals(null, it.workspaceFolders)
            assertEquals(null, it.configuration)
            assertEquals(true, it.semanticTokens!!.refreshSupport)
            assertEquals(null, it.codeLens)
            assertEquals(null, it.fileOperations)
        }
    }
}
