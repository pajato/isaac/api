package com.pajato.isaac.workspaceclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.WorkspaceClientCapabilities
import com.pajato.isaac.api.WorkspaceClientSerializer
import kotlin.test.Test
import kotlin.test.assertEquals

class ApplyEditUnitTest : SerializerTest, ApiTestProfiler() {
    private val data = WorkspaceClientCapabilities(applyEdit = true)
    private val json = """{"applyEdit":true}"""

    @Test fun `When serializing a workspace client capabilities object with an apply edit property, verify the result()`() {
        testEncode(json, WorkspaceClientSerializer, data)
        testDecode(json, WorkspaceClientSerializer) {
            assertEquals(true, it.applyEdit)
            assertEquals(null, it.workspaceEdit)
            assertEquals(null, it.didChangeConfiguration)
            assertEquals(null, it.didChangeWatchedFiles)
            assertEquals(null, it.symbol)
            assertEquals(null, it.executeCommand)
            assertEquals(null, it.workspaceFolders)
            assertEquals(null, it.configuration)
            assertEquals(null, it.semanticTokens)
            assertEquals(null, it.codeLens)
            assertEquals(null, it.fileOperations)
        }
    }
}
