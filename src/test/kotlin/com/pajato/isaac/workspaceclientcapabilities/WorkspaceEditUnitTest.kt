package com.pajato.isaac.workspaceclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.WorkspaceClientCapabilities
import com.pajato.isaac.api.WorkspaceClientSerializer
import com.pajato.isaac.api.WorkspaceEditClientCapabilities
import kotlin.test.Test
import kotlin.test.assertEquals

class WorkspaceEditUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a workspace client capabilities object with a workspace edit property, verify()`() {
        val prop = WorkspaceEditClientCapabilities(documentChanges = true)
        val data = WorkspaceClientCapabilities(workspaceEdit = prop)
        val json = """{"workspaceEdit":{"documentChanges":true}}"""

        testEncode(json, WorkspaceClientSerializer, data)
        testDecode(json, WorkspaceClientSerializer) {
            assertEquals(null, it.applyEdit)
            assertEquals(true, it.workspaceEdit!!.documentChanges)
            assertEquals(null, it.didChangeConfiguration)
            assertEquals(null, it.didChangeWatchedFiles)
            assertEquals(null, it.symbol)
            assertEquals(null, it.executeCommand)
            assertEquals(null, it.workspaceFolders)
            assertEquals(null, it.configuration)
            assertEquals(null, it.semanticTokens)
            assertEquals(null, it.codeLens)
            assertEquals(null, it.fileOperations)
        }
    }
}
