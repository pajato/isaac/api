package com.pajato.isaac.workspaceclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.ExecuteCommandClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.WorkspaceClientCapabilities
import com.pajato.isaac.api.WorkspaceClientSerializer
import kotlin.test.Test
import kotlin.test.assertEquals

class ExecuteCommandUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing with an apply edit property, verify the result`() {
        val prop = ExecuteCommandClientCapabilities(dynamicRegistration = true)
        val data = WorkspaceClientCapabilities(executeCommand = prop)
        val json = """{"executeCommand":{"dynamicRegistration":true}}"""

        testEncode(json, WorkspaceClientSerializer, data)
        testDecode(json, WorkspaceClientSerializer) {
            assertEquals(null, it.applyEdit)
            assertEquals(null, it.workspaceEdit)
            assertEquals(null, it.didChangeConfiguration)
            assertEquals(null, it.didChangeWatchedFiles)
            assertEquals(null, it.symbol)
            assertEquals(true, it.executeCommand!!.dynamicRegistration)
            assertEquals(null, it.workspaceFolders)
            assertEquals(null, it.configuration)
            assertEquals(null, it.semanticTokens)
            assertEquals(null, it.codeLens)
            assertEquals(null, it.fileOperations)
        }
    }
}
