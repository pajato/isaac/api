package com.pajato.isaac.workspaceclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.DidChangeConfigurationClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.WorkspaceClientCapabilities
import com.pajato.isaac.api.WorkspaceClientSerializer
import kotlin.test.Test
import kotlin.test.assertEquals

class WorkspaceClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing with a did change configuration property, verify a correct result`() {
        val prop = DidChangeConfigurationClientCapabilities(true)
        val data = WorkspaceClientCapabilities(didChangeConfiguration = prop)
        val json = """{"didChangeConfiguration":{"dynamicRegistration":true}}"""

        testEncode(json, WorkspaceClientSerializer, data)
        testDecode(json, WorkspaceClientSerializer) {
            assertEquals(null, it.applyEdit)
            assertEquals(null, it.workspaceEdit)
            assertEquals(true, it.didChangeConfiguration!!.dynamicRegistration)
            assertEquals(null, it.didChangeWatchedFiles)
            assertEquals(null, it.symbol)
            assertEquals(null, it.executeCommand)
            assertEquals(null, it.workspaceFolders)
            assertEquals(null, it.configuration)
            assertEquals(null, it.semanticTokens)
            assertEquals(null, it.codeLens)
            assertEquals(null, it.fileOperations)
        }
    }
}
