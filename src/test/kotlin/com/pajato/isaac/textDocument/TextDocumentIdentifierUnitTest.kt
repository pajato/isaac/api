package com.pajato.isaac.textDocument

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.IsaacTextDocumentIdentifier
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentIdentifierSerializer
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

const val mainPath = "sample_projects/kotlin_gradle/hello_world/src/main/kotlin/sample/main.kt"

class TextDocumentIdentifierUnitTest : SerializerTest, ApiTestProfiler() {
    @Test fun `When serializing a text document identifier, verify correct results`() {
        val classLoader: ClassLoader = this::class.java.classLoader
        val uri = classLoader.getResource(mainPath)!!.toString()
        val data = IsaacTextDocumentIdentifier(uri)
        val json = """{"uri":"$uri"}"""

        testEncode(json, TextDocumentIdentifierSerializer, data)
        testDecode(json, TextDocumentIdentifierSerializer) { actual ->
            assertTrue(actual is IsaacTextDocumentIdentifier, getDiagnostic("type"))
            assertEquals(actual.uri, uri, getDiagnostic("document uri"))
        }
    }
}
