package com.pajato.isaac.textDocument

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanDocumentSymbolProvider
import com.pajato.isaac.api.DocumentSymbolProvider
import com.pajato.isaac.api.DocumentSymbolProviderSerializer
import com.pajato.isaac.api.IsaacDocumentSymbolOptions
import com.pajato.isaac.api.OptionsDocumentSymbolProvider
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class DocumentSymbolProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a boolean document symbol provider object, verify a correct result`() {
        val provider: DocumentSymbolProvider = BooleanDocumentSymbolProvider(true)
        val actualJson = Json.encodeToString(DocumentSymbolProviderSerializer, provider)
        val expectedJson = """{"state":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a boolean document symbol provider object, verify a correct result`() {
        val incomingJson = """{"state":true}"""
        val actualObject = Json.decodeFromString(DocumentSymbolProviderSerializer, incomingJson)

        assertTrue(actualObject is BooleanDocumentSymbolProvider)
        assertEquals(true, actualObject.state)
    }

    @Test
    fun `When encoding an options document symbol provider object, verify a correct result`() {
        val provider: DocumentSymbolProvider =
            OptionsDocumentSymbolProvider(options = IsaacDocumentSymbolOptions(true))
        val actualJson = Json.encodeToString(DocumentSymbolProviderSerializer, provider)
        val expectedJson = """{"options":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding an options document symbol provider object, verify a correct result`() {
        val incomingJson = """{"options":{"workDoneProgress":true}}"""
        val actualObject = Json.decodeFromString(DocumentSymbolProviderSerializer, incomingJson)

        assertTrue(actualObject is OptionsDocumentSymbolProvider)
        assertEquals(true, actualObject.options.workDoneProgress)
    }

    @Test
    fun `When invalid JSON is used, verify an illegal state exception is thrown`() {
        val incomingJson = """{"xyzzy":true}"""

        assertFailsWith<IllegalStateException> {
            Json.decodeFromString(DocumentSymbolProviderSerializer, incomingJson)
        }
    }
}
