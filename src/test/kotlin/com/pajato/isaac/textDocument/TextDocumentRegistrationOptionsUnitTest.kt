package com.pajato.isaac.textDocument

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.DocumentFilter
import com.pajato.isaac.api.DocumentSelector
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentRegistrationOptions
import com.pajato.isaac.api.TextDocumentRegistrationOptionsSerializer
import com.pajato.isaac.assertOptions
import kotlin.test.Test

class TextDocumentRegistrationOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertAll(expected: TextDocumentRegistrationOptions, actual: TextDocumentRegistrationOptions) {
        assertOptions(expected, actual, assertId = false, assertWorkDoneProgress = false)
    }

    @Test fun `When serializing a text document registration default object, verify result`() {
        val data = TextDocumentRegistrationOptions()
        val json = """{}"""

        testEncode(json, TextDocumentRegistrationOptionsSerializer, data)
        testDecode(json, TextDocumentRegistrationOptionsSerializer) { actual -> assertAll(data, actual) }
    }

    @Test fun `When decoding a text document registration non-default object, verify result`() {
        val filter = DocumentFilter(
            language = "Kotlin",
            scheme = "a scheme",
            pattern = "a pattern",
        )
        val selector: DocumentSelector = listOf(filter)
        val data = TextDocumentRegistrationOptions(selector)
        val json = """{"documentSelector":[{"language":"Kotlin","scheme":"a scheme","pattern":"a pattern"}]}"""

        testEncode(json, TextDocumentRegistrationOptionsSerializer, data)
        testDecode(json, TextDocumentRegistrationOptionsSerializer) { actual -> assertAll(data, actual) }
    }
}
