package com.pajato.isaac.textDocument

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.ContentTextDocumentSyncSerializer
import com.pajato.isaac.api.IsaacSaveOptions
import com.pajato.isaac.api.IsaacTextDocumentSyncOptions
import com.pajato.isaac.api.KindTextDocumentSync
import com.pajato.isaac.api.OptionsSave
import com.pajato.isaac.api.OptionsTextDocumentSync
import com.pajato.isaac.api.TextDocumentSyncKind.Full
import com.pajato.isaac.api.TextDocumentSyncKind.Incremental
import com.pajato.isaac.api.TextDocumentSyncKind.None
import com.pajato.isaac.api.TextDocumentSyncOptions
import com.pajato.isaac.api.TransformingTextDocumentSyncSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class TextDocumentSyncUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a text document sync kind object, verify a correct result`() {
        val expectedJson = """{"kind":$Incremental}"""
        val actualJson = Json.encodeToString(ContentTextDocumentSyncSerializer, KindTextDocumentSync(Incremental))

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a text document sync kind object, verify a correct result`() {
        val incomingJson = """{"kind":$Full}"""
        val actualObject = Json.decodeFromString(ContentTextDocumentSyncSerializer, incomingJson)

        assertTrue(actualObject is KindTextDocumentSync)
        assertEquals(Full, actualObject.kind)
    }

    @Test
    fun `When encoding a text document sync options object, verify a correct result`() {
        val options: TextDocumentSyncOptions = IsaacTextDocumentSyncOptions(
            openClose = true,
            change = None,
            willSave = true,
            willSaveWaitUntil = true,
            save = OptionsSave(IsaacSaveOptions(false)),
        )
        val expectedJson = """{"options":{"openClose":true,"change":0,"willSave":true,"willSaveWaitUntil":true,""" +
            """"save":{"includeText":false}}}"""
        val actualJson = Json.encodeToString(ContentTextDocumentSyncSerializer, OptionsTextDocumentSync(options))

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a text document sync options object, verify a correct result`() {
        val incomingJson = """{"options":{"openClose":true,"change":0,"willSave":true,"willSaveWaitUntil":true,""" +
            """"save":{"includeText":false}}}"""
        val actualObject = Json.decodeFromString(ContentTextDocumentSyncSerializer, incomingJson)

        assertTrue(actualObject is OptionsTextDocumentSync)
        assertEquals(true, actualObject.options.openClose)
        assertEquals(None, actualObject.options.change)
        assertEquals(true, actualObject.options.willSave)
        assertEquals(true, actualObject.options.willSaveWaitUntil)
        assertTrue(actualObject.options.save is OptionsSave, "Did not get an OptionsSave type!")
        assertEquals(false, (actualObject.options.save as OptionsSave).value.includeText)
    }

    @Test
    fun `When an invalid element is passed to the serializers, verify an illegal state exception is thrown`() {
        val incomingJson = """["xyzzy"]"""

        assertFailsWith<IllegalStateException> {
            Json.decodeFromString(ContentTextDocumentSyncSerializer, incomingJson)
        }
        assertFailsWith<IllegalStateException> {
            Json.decodeFromString(TransformingTextDocumentSyncSerializer, incomingJson)
        }
    }
}
