package com.pajato.isaac.textDocument

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.TextDocumentSyncKind
import kotlin.test.Test
import kotlin.test.assertEquals

class TextDocumentSyncKindUnitTest : ApiTestProfiler() {

    @Test
    fun `When the text document sync kind constants are accessed directly, verify correct results`() {
        assertEquals(0, TextDocumentSyncKind.None)
        assertEquals(1, TextDocumentSyncKind.Full)
        assertEquals(2, TextDocumentSyncKind.Incremental)
    }

    @Test
    fun `When the text document sync kind object is referenced indirectly, verify correct results`() {
        val foo = TextDocumentSyncKind

        assertEquals(0, foo.None)
    }
}
