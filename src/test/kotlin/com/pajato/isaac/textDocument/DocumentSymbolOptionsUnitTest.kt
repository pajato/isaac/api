package com.pajato.isaac.textDocument

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.DocumentSymbolOptions
import com.pajato.isaac.api.DocumentSymbolOptionsSerializer
import com.pajato.isaac.api.SerializerTest
import kotlin.test.Test
import kotlin.test.assertEquals

class DocumentSymbolOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default document symbol options object, verify a correct result`() {
        val data = DocumentSymbolOptions()
        val json = """{}"""

        testEncode(json, DocumentSymbolOptionsSerializer, data)
        testDecode(json, DocumentSymbolOptionsSerializer) { actualObject ->
            assertEquals(null, actualObject.workDoneProgress)
        }
    }

    @Test fun `When serializing a non-default document symbol options object, verify a correct result`() {
        val data = DocumentSymbolOptions(true)
        val json = """{"workDoneProgress":true}"""

        testEncode(json, DocumentSymbolOptionsSerializer, data)
        testDecode(json, DocumentSymbolOptionsSerializer) { actualObject ->
            assertEquals(true, actualObject.workDoneProgress)
        }
    }
}
