package com.pajato.isaac.textDocument

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.IsaacTextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TextDocumentClientCapabilitiesUnitTest : ApiTestProfiler() {
    private val capabilitiesJson = """{}"""

    @Test
    fun `When encoding a null text document client capabilities object, verify the result()`() {
        val capabilities: TextDocumentClientCapabilities = IsaacTextDocumentClientCapabilities(synchronization = null)
        val textDocumentClientCapabilitiesAsJson =
            Json.encodeToString(TextDocumentClientCapabilitiesSerializer, capabilities)

        assertEquals(capabilitiesJson, textDocumentClientCapabilitiesAsJson)
    }

    @Test
    fun `When decoding a null text document client capabilities object, verify the result()`() {
        val actualObject = Json.decodeFromString(TextDocumentClientCapabilitiesSerializer, capabilitiesJson)

        assertTrue(actualObject is IsaacTextDocumentClientCapabilities)
        assertEquals(null, actualObject.synchronization)
    }
}
