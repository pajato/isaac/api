package com.pajato.isaac.textDocument

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanSave
import com.pajato.isaac.api.OptionsSave
import com.pajato.isaac.api.SaveOptions
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentSyncKind.Incremental
import com.pajato.isaac.api.TextDocumentSyncOptions
import com.pajato.isaac.api.TextDocumentSyncOptionsSerializer
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

class TextDocumentSyncOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertOptions(expected: TextDocumentSyncOptions, actual: TextDocumentSyncOptions) {
        fun assertSave() {
            fun assertBooleanSave() {
                val expectedSave = expected.save as BooleanSave
                val actualSave = actual.save as BooleanSave

                assertEquals(expectedSave.value, actualSave.value, getDiagnostic("boolean save"))
            }

            fun assertOptionsSave() {
                val expectedSave = (expected.save as OptionsSave).value
                val actualSave = (actual.save as OptionsSave).value

                assertEquals(expectedSave.includeText, actualSave.includeText, getDiagnostic("options save"))
            }

            when {
                expected.save == null -> assertTrue(actual.save == null, getDiagnostic("null save"))
                expected.save is BooleanSave && actual.save is BooleanSave -> assertBooleanSave()
                expected.save is OptionsSave && actual.save is OptionsSave -> assertOptionsSave()
                else -> fail("Case not covered!")
            }
        }

        assertEquals(expected.openClose, actual.openClose, getDiagnostic("open/close"))
        assertEquals(expected.change, actual.change, getDiagnostic("change"))
        assertEquals(expected.willSave, actual.willSave, getDiagnostic("will save"))
        assertEquals(expected.willSaveWaitUntil, actual.willSaveWaitUntil, getDiagnostic("will save wait until"))
        assertSave()
    }

    @Test fun `When serializing a text document sync options default object, verify result`() {
        val data = TextDocumentSyncOptions()
        val json = """{}"""

        testEncode(json, TextDocumentSyncOptionsSerializer, data)
        testDecode(json, TextDocumentSyncOptionsSerializer) { actual -> assertOptions(data, actual) }
    }

    @Test fun `When serializing a text document sync options object with a boolean save property, verify result`() {
        val data = TextDocumentSyncOptions(
            openClose = true, change = Incremental, willSave = false,
            willSaveWaitUntil = false, save = BooleanSave(true)
        )
        val json = """{"openClose":true,"change":$Incremental,"willSave":false,"willSaveWaitUntil":false,""" +
            """"save":true}"""

        testEncode(json, TextDocumentSyncOptionsSerializer, data)
        testDecode(json, TextDocumentSyncOptionsSerializer) { actual -> assertOptions(data, actual) }
    }

    @Test fun `When serializing a text document sync options object with an options save property, verify result`() {
        val data = TextDocumentSyncOptions(
            openClose = true, change = Incremental, willSave = false,
            willSaveWaitUntil = false, save = OptionsSave(value = SaveOptions(includeText = true))
        )
        val json = """{"openClose":true,"change":$Incremental,"willSave":false,"willSaveWaitUntil":false,""" +
            """"save":{"includeText":true}}"""

        testEncode(json, TextDocumentSyncOptionsSerializer, data)
        testDecode(json, TextDocumentSyncOptionsSerializer) { actual -> assertOptions(data, actual) }
    }
}
