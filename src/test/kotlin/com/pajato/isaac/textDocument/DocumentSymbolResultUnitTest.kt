package com.pajato.isaac.textDocument

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.DocumentSymbol
import com.pajato.isaac.api.DocumentSymbolResult
import com.pajato.isaac.api.DocumentSymbolResultSerializer
import com.pajato.isaac.api.IsaacDocumentSymbolResult
import com.pajato.isaac.api.Position
import com.pajato.isaac.api.Range
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.SymbolKind.Constructor
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DocumentSymbolResultUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a non-default document symbol result verify correctness`() {
        fun assertResult(expected: DocumentSymbol, actual: DocumentSymbolResult) {
            fun assertSymbol(expected: DocumentSymbol, actual: DocumentSymbol) {
                fun assertRange(expected: Range, actual: Range) {
                    val startLine = expected.start.line
                    val startChar = expected.start.character
                    val endLine = expected.end.line
                    val endChar = expected.end.character

                    assertEquals(startLine, actual.start.line, getDiagnostic("range start line"))
                    assertEquals(startChar, actual.start.character, getDiagnostic("range start character"))
                    assertEquals(endLine, actual.end.line, getDiagnostic("range end line"))
                    assertEquals(endChar, actual.end.character, getDiagnostic("range end character"))
                }

                assertEquals(expected.name, actual.name, getDiagnostic("name"))
                assertEquals(Constructor.ordinal, actual.kind, getDiagnostic("kind"))
                assertRange(expected.range, actual.range)
            }

            assertTrue(actual is IsaacDocumentSymbolResult, getDiagnostic("type"))
            assertEquals(1, actual.list.size, getDiagnostic("list size"))
            assertSymbol(expected, actual.list[0])
        }

        val name = "DocumentSymbolResultTest"
        val start = Position(13, 3)
        val end = Position(49, 1)
        val range = Range(start, end)
        val kind = Constructor
        val symbol = DocumentSymbol(name = name, kind = kind, range = range, selectionRange = range)
        val data = DocumentSymbolResult(list = listOf(symbol))
        val json = """{"list":[{"name":"DocumentSymbolResultTest","kind":8,"range":{"start":{"line":13,"character":3},""" +
            """"end":{"line":49,"character":1}},"selectionRange":{"start":{"line":13,"character":3},""" +
            """"end":{"line":49,"character":1}}}]}"""

        testEncode(json, DocumentSymbolResultSerializer, data)
        testDecode(json, DocumentSymbolResultSerializer) { actual -> assertResult(symbol, actual) }
    }

    /* @Ignore
    @Test fun `When serializing a default document symbol result, verify a correct result`() {
        val data = DocumentSymbolResult()
        val json = """{[]]"""

        testEncode(json, DocumentSymbolResultSerializer, data)
        testDecode(json, DocumentSymbolResultSerializer) { actualObject ->
            assertTrue(actualObject is IsaacWorkspaceSymbolResult, getDiagnostic("result type"))
            assertEquals(0, actualObject.list.size, getDiagnostic("result list size"))

        }
        //val result = Json.decodeFromString(ResultTransformingSerializer, json)
    } */
}
