package com.pajato.isaac.textDocument

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.DocumentSymbol
import com.pajato.isaac.api.DocumentSymbolSerializer
import com.pajato.isaac.api.IsaacDocumentSymbol
import com.pajato.isaac.api.IsaacPosition
import com.pajato.isaac.api.IsaacRange
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.SymbolKind.Constructor
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DocumentSymbolUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a document symbol result, verify correctness`() {
        fun assertSymbol(expected: DocumentSymbol, actual: DocumentSymbol) {
            val actualRange = actual.range

            assertTrue(actual is IsaacDocumentSymbol, getDiagnostic("type"))
            assertEquals(expected.name, actual.name, getDiagnostic("name"))
            assertEquals(Constructor.ordinal, actual.kind, getDiagnostic("kind"))
            assertEquals(expected.range.start.line, actualRange.start.line, getDiagnostic("range start line"))
            assertEquals(expected.range.start.character, actualRange.start.character, getDiagnostic("range start char"))
            assertEquals(expected.range.end.line, actualRange.end.line, getDiagnostic("range end line"))
            assertEquals(expected.range.end.character, actualRange.end.character, getDiagnostic("range end char"))
        }

        val name = "DocumentSymbolUnitTest"
        val start = IsaacPosition(13, 3)
        val end = IsaacPosition(49, 1)
        val range = IsaacRange(start, end)
        val kind = Constructor.ordinal
        val data = IsaacDocumentSymbol(name = name, kind = kind, range = range, selectionRange = range)
        val json = """{"name":"$name","kind":8,"range":{"start":{"line":13,"character":3},"end":{"line":49,""" +
            """"character":1}},"selectionRange":{"start":{"line":13,"character":3},"end":{"line":49,""" +
            """"character":1}}}"""

        testEncode(json, DocumentSymbolSerializer, data)
        testDecode(json, DocumentSymbolSerializer) { actual -> assertSymbol(data, actual) }
    }
}
