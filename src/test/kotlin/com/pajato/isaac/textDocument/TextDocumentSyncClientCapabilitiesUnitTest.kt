package com.pajato.isaac.textDocument

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentSyncClientCapabilities
import com.pajato.isaac.api.TextDocumentSyncClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals

class TextDocumentSyncClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default text document sync client capabilities object, verify a correct result`() {
        val data = TextDocumentSyncClientCapabilities()
        val json = """{}"""

        testEncode(json, TextDocumentSyncClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentSyncClientCapabilitiesSerializer) { actualObject ->
            assertEquals(null, actualObject.dynamicRegistration)
            assertEquals(null, actualObject.willSave)
            assertEquals(null, actualObject.willSaveWaitUntil)
            assertEquals(null, actualObject.didSave)
        }
    }

    @Test fun `When serializing a non-default text document sync client capabilities object, verify correct result`() {
        val data = TextDocumentSyncClientCapabilities(
            dynamicRegistration = true, willSave = false,
            willSaveWaitUntil = true, didSave = false
        )
        val json = """{"dynamicRegistration":true,"willSave":false,"willSaveWaitUntil":true,"didSave":false}"""

        testEncode(json, TextDocumentSyncClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentSyncClientCapabilitiesSerializer) { actualObject ->
            assertEquals(true, actualObject.dynamicRegistration)
            assertEquals(false, actualObject.willSave)
            assertEquals(true, actualObject.willSaveWaitUntil)
            assertEquals(false, actualObject.didSave)
        }
    }
}
