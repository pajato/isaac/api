package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanDefinitionProvider
import com.pajato.isaac.api.DefinitionOptions
import com.pajato.isaac.api.IsaacDefinitionOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.OptionsDefinitionProvider
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DefinitionProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with a boolean definition provider property), verify result`() {
        val data: ServerCapabilities = IsaacServerCapabilities(definitionProvider = BooleanDefinitionProvider(true))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"definitionProvider":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a boolean definition provider property), verify result`() {
        val incomingJson = """{"definitionProvider":true}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualDefinitionProvider = actualObject.definitionProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualDefinitionProvider is BooleanDefinitionProvider)
        assertEquals(true, actualDefinitionProvider.state)
    }

    @Test
    fun `When encoding a server capabilities object (with an options definitions provider property), verify result`() {
        val options: DefinitionOptions = IsaacDefinitionOptions(true)
        val data: ServerCapabilities = IsaacServerCapabilities(definitionProvider = OptionsDefinitionProvider(options))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"definitionProvider":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with an options definition provider property), verify result`() {
        val incomingJson = """{"definitionProvider":{"workDoneProgress":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualDefinitionProvider = actualObject.definitionProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualDefinitionProvider is OptionsDefinitionProvider)
        assertTrue(actualDefinitionProvider.options is IsaacDefinitionOptions)
        assertEquals(true, actualDefinitionProvider.options.workDoneProgress)
    }
}
