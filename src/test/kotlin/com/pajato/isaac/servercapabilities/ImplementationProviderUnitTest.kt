package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanImplementationProvider
import com.pajato.isaac.api.ImplementationOptions
import com.pajato.isaac.api.ImplementationRegistrationOptions
import com.pajato.isaac.api.IsaacDocumentFilter
import com.pajato.isaac.api.IsaacImplementationOptions
import com.pajato.isaac.api.IsaacImplementationRegistrationOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.OptionsImplementationProvider
import com.pajato.isaac.api.RegistrationOptionsImplementationProvider
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ImplementationProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with a boolean implementation provider property), verify`() {
        val provider = BooleanImplementationProvider(true)
        val data: ServerCapabilities = IsaacServerCapabilities(implementationProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"implementationProvider":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a boolean implementation provider property), verify`() {
        val incomingJson = """{"implementationProvider":true}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualImplementationProvider = actualObject.implementationProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualImplementationProvider is BooleanImplementationProvider)
        assertEquals(true, actualImplementationProvider.state)
    }

    @Test
    fun `When encoding a server capabilities object (with an options implementation provider property), verify`() {
        val options: ImplementationOptions = IsaacImplementationOptions(true)
        val provider = OptionsImplementationProvider(options)
        val data: ServerCapabilities = IsaacServerCapabilities(implementationProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"implementationProvider":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with an options implementation provider property), verify`() {
        val incomingJson = """{"implementationProvider":{"workDoneProgress":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualImplementationProvider = actualObject.implementationProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualImplementationProvider is OptionsImplementationProvider)
        assertTrue(actualImplementationProvider.options is IsaacImplementationOptions)
        assertEquals(true, actualImplementationProvider.options.workDoneProgress)
    }

    @Test
    fun `When encoding a server capabilities object (with a implementation registration options property), verify`() {
        val options = ImplementationRegistrationOptions(
            documentSelector = listOf(IsaacDocumentFilter("Kotlin", "httpsScheme", "*.{ts,kt}")),
            workDoneProgress = true, id = "anId"
        )
        val registrationOptions: ImplementationRegistrationOptions = options
        val provider = RegistrationOptionsImplementationProvider(registrationOptions)
        val data: ServerCapabilities = IsaacServerCapabilities(implementationProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"implementationProvider":{"documentSelector":[""" +
            """{"language":"Kotlin","scheme":"httpsScheme","pattern":"*.{ts,kt}"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a implementation registration options property), verify`() {
        val incomingJson = """{"implementationProvider":{"documentSelector":[""" +
            """{"language":"Kotlin","scheme":"httpsScheme","pattern":"*.{ts,kt}"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualImplementationProvider = actualObject.implementationProvider

        assertTrue(actualImplementationProvider is RegistrationOptionsImplementationProvider)
        assertTrue(actualImplementationProvider.registrationOptions is IsaacImplementationRegistrationOptions)
        assertEquals(1, actualImplementationProvider.registrationOptions.documentSelector!!.size)
        assertEquals("Kotlin", actualImplementationProvider.registrationOptions.documentSelector!![0].language)
        assertEquals("httpsScheme", actualImplementationProvider.registrationOptions.documentSelector!![0].scheme)
        assertEquals("*.{ts,kt}", actualImplementationProvider.registrationOptions.documentSelector!![0].pattern)
        assertEquals(true, actualImplementationProvider.registrationOptions.workDoneProgress)
        assertEquals("anId", actualImplementationProvider.registrationOptions.id)
    }
}
