package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanDocumentHighlightProvider
import com.pajato.isaac.api.DocumentHighlightOptions
import com.pajato.isaac.api.IsaacDocumentHighlightOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.OptionsDocumentHighlightProvider
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DocumentHighlightProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with a boolean document highlight provider property), verify`() {
        val data: ServerCapabilities =
            IsaacServerCapabilities(documentHighlightProvider = BooleanDocumentHighlightProvider(true))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"documentHighlightProvider":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a boolean document highlight provider property), verify`() {
        val incomingJson = """{"documentHighlightProvider":true}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualDocumentHighlightProvider = actualObject.documentHighlightProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualDocumentHighlightProvider is BooleanDocumentHighlightProvider)
        assertEquals(true, actualDocumentHighlightProvider.state)
    }

    @Test
    fun `When encoding a server capabilities object (with an options document highlight provider property), verify`() {
        val options: DocumentHighlightOptions = IsaacDocumentHighlightOptions(true)
        val data: ServerCapabilities =
            IsaacServerCapabilities(documentHighlightProvider = OptionsDocumentHighlightProvider(options))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"documentHighlightProvider":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with an options document highlight provider property), verify `() {
        val incomingJson = """{"documentHighlightProvider":{"workDoneProgress":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualDocumentHighlightProvider = actualObject.documentHighlightProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualDocumentHighlightProvider is OptionsDocumentHighlightProvider)
        assertTrue(actualDocumentHighlightProvider.options is IsaacDocumentHighlightOptions)
        assertEquals(true, actualDocumentHighlightProvider.options.workDoneProgress)
    }
}
