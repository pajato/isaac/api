package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanHoverProvider
import com.pajato.isaac.api.HoverOptions
import com.pajato.isaac.api.IsaacHoverOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.OptionsHoverProvider
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class HoverProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with a boolean hover provider property), verify result`() {
        val data: ServerCapabilities = IsaacServerCapabilities(hoverProvider = BooleanHoverProvider(true))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"hoverProvider":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a boolean hover provider property), verify result`() {
        val incomingJson = """{"hoverProvider":true}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualHoverProvider = actualObject.hoverProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualHoverProvider is BooleanHoverProvider)
        assertEquals(true, actualHoverProvider.data)
    }

    @Test
    fun `When encoding a server capabilities object (with an options hover provider property), verify result`() {
        val options: HoverOptions = IsaacHoverOptions(true)
        val data: ServerCapabilities = IsaacServerCapabilities(hoverProvider = OptionsHoverProvider(options))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"hoverProvider":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with an options hover provider property), verify result`() {
        val incomingJson = """{"hoverProvider":{"workDoneProgress":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualHoverProvider = actualObject.hoverProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualHoverProvider is OptionsHoverProvider)
        assertTrue(actualHoverProvider.data is IsaacHoverOptions)
        assertEquals(true, actualHoverProvider.data.workDoneProgress)
    }
}
