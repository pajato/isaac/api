package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanWorkspaceSymbolProvider
import com.pajato.isaac.api.OptionsWorkspaceSymbolProvider
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import com.pajato.isaac.api.WorkspaceSymbolOptions
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class WorkspaceSymbolProviderUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing with a boolean workspace symbol provider property, verify a correct result`() {
        val data = ServerCapabilities(workspaceSymbolProvider = BooleanWorkspaceSymbolProvider(true))
        val json = """{"workspaceSymbolProvider":true}"""

        testEncode(json, ServerCapabilitiesSerializer, data)
        testDecode(json, ServerCapabilitiesSerializer) { actualObject ->
            val actualWorkspaceSymbolProvider = actualObject.workspaceSymbolProvider

            assertTrue(actualWorkspaceSymbolProvider is BooleanWorkspaceSymbolProvider)
            assertEquals(true, actualWorkspaceSymbolProvider.state)
        }
    }

    @Test fun `When serializing with an options workspace symbol provider property, verify a correct result`() {
        val options = WorkspaceSymbolOptions(true)
        val data = ServerCapabilities(workspaceSymbolProvider = OptionsWorkspaceSymbolProvider(options))
        val json = """{"workspaceSymbolProvider":{"workDoneProgress":true}}"""

        testEncode(json, ServerCapabilitiesSerializer, data)
        testDecode(json, ServerCapabilitiesSerializer) { actualObject ->
            val actualWorkspaceSymbolProvider = actualObject.workspaceSymbolProvider

            assertTrue(actualWorkspaceSymbolProvider is OptionsWorkspaceSymbolProvider)
            assertEquals(true, actualWorkspaceSymbolProvider.options.workDoneProgress)
        }
    }
}
