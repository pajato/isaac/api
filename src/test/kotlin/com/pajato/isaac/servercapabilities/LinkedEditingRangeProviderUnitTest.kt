package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanLinkedEditingRangeProvider
import com.pajato.isaac.api.IsaacDocumentFilter
import com.pajato.isaac.api.IsaacLinkedEditingRangeOptions
import com.pajato.isaac.api.IsaacLinkedEditingRangeRegistrationOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.LinkedEditingRangeOptions
import com.pajato.isaac.api.LinkedEditingRangeProvider
import com.pajato.isaac.api.LinkedEditingRangeRegistrationOptions
import com.pajato.isaac.api.OptionsLinkedEditingRangeProvider
import com.pajato.isaac.api.RegistrationOptionsLinkedEditingRangeProvider
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class LinkedEditingRangeProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with a boolean linked editing range provider), verify`() {
        val provider = BooleanLinkedEditingRangeProvider(true)
        val data: ServerCapabilities = IsaacServerCapabilities(linkedEditingRangeProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"linkedEditingRangeProvider":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a boolean linked editing range provider), verify`() {
        val incomingJson = """{"linkedEditingRangeProvider":true}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualLinkedEditingRangeProvider = actualObject.linkedEditingRangeProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualLinkedEditingRangeProvider is BooleanLinkedEditingRangeProvider)
        assertEquals(true, actualLinkedEditingRangeProvider.state)
    }

    @Test
    fun `When encoding a server capabilities object (with an options linked editing range provider), verify`() {
        val options: LinkedEditingRangeOptions = IsaacLinkedEditingRangeOptions(true)
        val provider: LinkedEditingRangeProvider = OptionsLinkedEditingRangeProvider(options)
        val data: ServerCapabilities = IsaacServerCapabilities(linkedEditingRangeProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"linkedEditingRangeProvider":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with an options linked editing range provider), verify`() {
        val incomingJson = """{"linkedEditingRangeProvider":{"workDoneProgress":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualLinkedEditingRangeProvider = actualObject.linkedEditingRangeProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualLinkedEditingRangeProvider is OptionsLinkedEditingRangeProvider)
        assertTrue(actualLinkedEditingRangeProvider.options is IsaacLinkedEditingRangeOptions)
        assertEquals(true, actualLinkedEditingRangeProvider.options.workDoneProgress)
    }

    @Test
    fun `When encoding a server capabilities object (using a linked editing range registration options), verify`() {
        val options: LinkedEditingRangeRegistrationOptions = IsaacLinkedEditingRangeRegistrationOptions(
            documentSelector = listOf(IsaacDocumentFilter("Kotlin", "httpsScheme", "*.{ts,kt}")),
            workDoneProgress = true, id = "anId"
        )
        val provider: LinkedEditingRangeProvider = RegistrationOptionsLinkedEditingRangeProvider(options)
        val data: ServerCapabilities = IsaacServerCapabilities(linkedEditingRangeProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"linkedEditingRangeProvider":{"documentSelector":[""" +
            """{"language":"Kotlin","scheme":"httpsScheme","pattern":"*.{ts,kt}"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (using a linked editing range registration options), verify`() {
        val incomingJson = """{"linkedEditingRangeProvider":{"documentSelector":[""" +
            """{"language":"Kotlin","scheme":"httpsScheme","pattern":"*.{ts,kt}"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualLinkedEditingRangeProvider = actualObject.linkedEditingRangeProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualLinkedEditingRangeProvider is RegistrationOptionsLinkedEditingRangeProvider)
        assertTrue(actualLinkedEditingRangeProvider.registrationOptions is IsaacLinkedEditingRangeRegistrationOptions)
        assertEquals(1, actualLinkedEditingRangeProvider.registrationOptions.documentSelector!!.size)
        assertEquals("Kotlin", actualLinkedEditingRangeProvider.registrationOptions.documentSelector!![0].language)
        assertEquals("httpsScheme", actualLinkedEditingRangeProvider.registrationOptions.documentSelector!![0].scheme)
        assertEquals("*.{ts,kt}", actualLinkedEditingRangeProvider.registrationOptions.documentSelector!![0].pattern)
        assertEquals(true, actualLinkedEditingRangeProvider.registrationOptions.workDoneProgress)
        assertEquals("anId", actualLinkedEditingRangeProvider.registrationOptions.id)
    }
}
