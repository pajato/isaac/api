package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.IsaacDocumentOnTypeFormattingOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DocumentOnTypeFormattingOptionsUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a document on type formatting options property, verify the result`() {
        val options = IsaacDocumentOnTypeFormattingOptions("]", arrayOf("}", ")"))
        val data: ServerCapabilities = IsaacServerCapabilities(documentOnTypeFormattingProvider = options)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"documentOnTypeFormattingProvider":{"firstTriggerCharacter":"]",""" +
            """"moreTriggerCharacter":["}",")"]}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a document on type formatting options property, verify the result`() {
        val incomingJson = """{"documentOnTypeFormattingProvider":{"firstTriggerCharacter":"]",""" +
            """"moreTriggerCharacter":["}",")"]}}"""
        val actualObject = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)

        assertTrue(actualObject is IsaacServerCapabilities)
        assertEquals("]", actualObject.documentOnTypeFormattingProvider!!.firstTriggerCharacter)
        assertEquals(2, actualObject.documentOnTypeFormattingProvider!!.moreTriggerCharacter!!.size)
        assertEquals("}", actualObject.documentOnTypeFormattingProvider!!.moreTriggerCharacter!![0])
        assertEquals(")", actualObject.documentOnTypeFormattingProvider!!.moreTriggerCharacter!![1])
    }
}
