package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanDocumentFormattingProvider
import com.pajato.isaac.api.DocumentFormattingOptions
import com.pajato.isaac.api.IsaacDocumentFormattingOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.OptionsDocumentFormattingProvider
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DocumentFormattingProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with a boolean document formatting provider property), verify`() {
        val data: ServerCapabilities =
            IsaacServerCapabilities(documentFormattingProvider = BooleanDocumentFormattingProvider(true))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"documentFormattingProvider":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a boolean document formatting provider property), verify`() {
        val incomingJson = """{"documentFormattingProvider":true}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualDocumentFormattingProvider = actualObject.documentFormattingProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualDocumentFormattingProvider is BooleanDocumentFormattingProvider)
        assertEquals(true, actualDocumentFormattingProvider.state)
    }

    @Test
    fun `When encoding a server capabilities object (with an options document formatting provider property), verify`() {
        val options: DocumentFormattingOptions = IsaacDocumentFormattingOptions(true)
        val data: ServerCapabilities =
            IsaacServerCapabilities(documentFormattingProvider = OptionsDocumentFormattingProvider(options))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"documentFormattingProvider":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with an options document formatting provider property), verify`() {
        val incomingJson = """{"documentFormattingProvider":{"workDoneProgress":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualDocumentFormattingProvider = actualObject.documentFormattingProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualDocumentFormattingProvider is OptionsDocumentFormattingProvider)
        assertTrue(actualDocumentFormattingProvider.options is IsaacDocumentFormattingOptions)
        assertEquals(true, actualDocumentFormattingProvider.options.workDoneProgress)
    }
}
