package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanDeclarationProvider
import com.pajato.isaac.api.DeclarationOptions
import com.pajato.isaac.api.DeclarationRegistrationOptions
import com.pajato.isaac.api.IsaacDeclarationOptions
import com.pajato.isaac.api.IsaacDeclarationRegistrationOptions
import com.pajato.isaac.api.IsaacDocumentFilter
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.OptionsDeclarationProvider
import com.pajato.isaac.api.RegistrationOptionsDeclarationProvider
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DeclarationProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with a boolean declaration provider property), verify result`() {
        val data: ServerCapabilities = IsaacServerCapabilities(declarationProvider = BooleanDeclarationProvider(true))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"declarationProvider":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a boolean declaration provider property), verify result`() {
        val incomingJson = """{"declarationProvider":true}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualDeclarationProvider = actualObject.declarationProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualDeclarationProvider is BooleanDeclarationProvider)
        assertEquals(true, actualDeclarationProvider.state)
    }

    @Test
    fun `When encoding a server capabilities object (with an options declaration provider property), verify result`() {
        val options: DeclarationOptions = IsaacDeclarationOptions(true)
        val data: ServerCapabilities = IsaacServerCapabilities(declarationProvider = OptionsDeclarationProvider(options))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"declarationProvider":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with an options declaration provider property), verify result`() {
        val incomingJson = """{"declarationProvider":{"workDoneProgress":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualDeclarationProvider = actualObject.declarationProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualDeclarationProvider is OptionsDeclarationProvider)
        assertTrue(actualDeclarationProvider.options is IsaacDeclarationOptions)
        assertEquals(true, actualDeclarationProvider.options.workDoneProgress)
    }

    @Test
    fun `When encoding a server capabilities object (with a declaration registration options property), verify`() {
        val options: DeclarationRegistrationOptions = IsaacDeclarationRegistrationOptions(
            documentSelector = listOf(IsaacDocumentFilter("Kotlin", "httpsScheme", "*.{ts,kt}")),
            workDoneProgress = true, id = "anId"
        )
        val registrationOptions: DeclarationRegistrationOptions = options
        val provider = RegistrationOptionsDeclarationProvider(registrationOptions)
        val data: ServerCapabilities = IsaacServerCapabilities(declarationProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"declarationProvider":{"documentSelector":[""" +
            """{"language":"Kotlin","scheme":"httpsScheme","pattern":"*.{ts,kt}"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a declaration registration options property), verify`() {
        val incomingJson = """{"declarationProvider":{"documentSelector":[""" +
            """{"language":"Kotlin","scheme":"httpsScheme","pattern":"*.{ts,kt}"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualDeclarationProvider = actualObject.declarationProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualDeclarationProvider is RegistrationOptionsDeclarationProvider)
        assertTrue(actualDeclarationProvider.registrationOptions is IsaacDeclarationRegistrationOptions)
        assertEquals(1, actualDeclarationProvider.registrationOptions.documentSelector!!.size)
        assertEquals("Kotlin", actualDeclarationProvider.registrationOptions.documentSelector!![0].language)
        assertEquals("httpsScheme", actualDeclarationProvider.registrationOptions.documentSelector!![0].scheme)
        assertEquals("*.{ts,kt}", actualDeclarationProvider.registrationOptions.documentSelector!![0].pattern)
        assertEquals(true, actualDeclarationProvider.registrationOptions.workDoneProgress)
        assertEquals("anId", actualDeclarationProvider.registrationOptions.id)
    }
}
