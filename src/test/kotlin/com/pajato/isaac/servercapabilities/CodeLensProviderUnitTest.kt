package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.IsaacCodeLensOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class CodeLensProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with a code lens provider property), verify result`() {
        val options = IsaacCodeLensOptions(workDoneProgress = true, resolveProvider = true)
        val data: ServerCapabilities = IsaacServerCapabilities(codeLensProvider = options)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"codeLensProvider":{"workDoneProgress":true,"resolveProvider":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a code lens provider property), verify result`() {
        val incomingJson = """{"codeLensProvider":{"workDoneProgress":true,"resolveProvider":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualCodeLensProvider = actualObject.codeLensProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualCodeLensProvider is IsaacCodeLensOptions)
        assertEquals(true, actualCodeLensProvider.workDoneProgress)
        assertEquals(true, actualCodeLensProvider.resolveProvider)
    }
}
