package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanCallHierarchyProvider
import com.pajato.isaac.api.CallHierarchyOptions
import com.pajato.isaac.api.CallHierarchyProvider
import com.pajato.isaac.api.CallHierarchyRegistrationOptions
import com.pajato.isaac.api.IsaacCallHierarchyRegistrationOptions
import com.pajato.isaac.api.IsaacDocumentFilter
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.OptionsCallHierarchyProvider
import com.pajato.isaac.api.RegistrationOptionsCallHierarchyProvider
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class CallHierarchyProviderUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a server capabilities object (with a default call hierarchy provider), verify`() {
        val data = ServerCapabilities()
        val json = """{}"""

        testEncode(json, ServerCapabilitiesSerializer, data)
        testDecode(json, ServerCapabilitiesSerializer) { actualObject ->
            assertEquals(null, actualObject.callHierarchyProvider)
        }
    }

    @Test fun `When serializing a server capabilities object (with a boolean call hierarchy provider), verify`() {
        val provider = BooleanCallHierarchyProvider(true)
        val data = ServerCapabilities(callHierarchyProvider = provider)
        val json = """{"callHierarchyProvider":true}"""

        testEncode(json, ServerCapabilitiesSerializer, data)
        testDecode(json, ServerCapabilitiesSerializer) { actualObject ->
            val actualCallHierarchyProvider = actualObject.callHierarchyProvider
            assertTrue(actualCallHierarchyProvider is BooleanCallHierarchyProvider)
            assertEquals(true, actualCallHierarchyProvider.state)
        }
    }

    @Test
    fun `When encoding a server capabilities object (with an options call hierarchy provider), verify`() {
        val options = CallHierarchyOptions(true)
        val provider: CallHierarchyProvider = OptionsCallHierarchyProvider(options)
        val data: ServerCapabilities = IsaacServerCapabilities(callHierarchyProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"callHierarchyProvider":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with an options call hierarchy provider), verify`() {
        val incomingJson = """{"callHierarchyProvider":{"workDoneProgress":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualCallHierarchyProvider = actualObject.callHierarchyProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualCallHierarchyProvider is OptionsCallHierarchyProvider)
        assertEquals(true, actualCallHierarchyProvider.options.workDoneProgress)
    }

    @Test
    fun `When encoding a server capabilities object (using a call hierarchy registration options), verify`() {
        val options: CallHierarchyRegistrationOptions = IsaacCallHierarchyRegistrationOptions(
            documentSelector = listOf(IsaacDocumentFilter("Kotlin", "httpsScheme", "*.{ts,kt}")),
            workDoneProgress = true, id = "anId"
        )
        val provider: CallHierarchyProvider = RegistrationOptionsCallHierarchyProvider(options)
        val data: ServerCapabilities = IsaacServerCapabilities(callHierarchyProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"callHierarchyProvider":{"documentSelector":[""" +
            """{"language":"Kotlin","scheme":"httpsScheme","pattern":"*.{ts,kt}"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (using a call hierarchy registration options), verify`() {
        val incomingJson = """{"callHierarchyProvider":{"documentSelector":[""" +
            """{"language":"Kotlin","scheme":"httpsScheme","pattern":"*.{ts,kt}"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualCallHierarchyProvider = actualObject.callHierarchyProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualCallHierarchyProvider is RegistrationOptionsCallHierarchyProvider)
        assertTrue(actualCallHierarchyProvider.registrationOptions is IsaacCallHierarchyRegistrationOptions)
        assertEquals(1, actualCallHierarchyProvider.registrationOptions.documentSelector!!.size)
        assertEquals("Kotlin", actualCallHierarchyProvider.registrationOptions.documentSelector!![0].language)
        assertEquals("httpsScheme", actualCallHierarchyProvider.registrationOptions.documentSelector!![0].scheme)
        assertEquals("*.{ts,kt}", actualCallHierarchyProvider.registrationOptions.documentSelector!![0].pattern)
        assertEquals(true, actualCallHierarchyProvider.registrationOptions.workDoneProgress)
        assertEquals("anId", actualCallHierarchyProvider.registrationOptions.id)
    }
}
