package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanSelectionRangeProvider
import com.pajato.isaac.api.IsaacDocumentFilter
import com.pajato.isaac.api.IsaacSelectionRangeOptions
import com.pajato.isaac.api.IsaacSelectionRangeRegistrationOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.OptionsSelectionRangeProvider
import com.pajato.isaac.api.RegistrationOptionsSelectionRangeProvider
import com.pajato.isaac.api.SelectionRangeOptions
import com.pajato.isaac.api.SelectionRangeProvider
import com.pajato.isaac.api.SelectionRangeRegistrationOptions
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class SelectionRangeProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with a boolean selection range provider property), verify`() {
        val provider: SelectionRangeProvider = BooleanSelectionRangeProvider(true)
        val data: ServerCapabilities = IsaacServerCapabilities(selectionRangeProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"selectionRangeProvider":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a boolean selection range provider property), verify`() {
        val incomingJson = """{"selectionRangeProvider":true}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualSelectionRangeProvider = actualObject.selectionRangeProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualSelectionRangeProvider is BooleanSelectionRangeProvider)
        assertEquals(true, actualSelectionRangeProvider.state)
    }

    @Test
    fun `When encoding a server capabilities object (with an options selection range provider property), verify`() {
        val options: SelectionRangeOptions = IsaacSelectionRangeOptions(true)
        val provider: SelectionRangeProvider = OptionsSelectionRangeProvider(options)
        val data: ServerCapabilities = IsaacServerCapabilities(selectionRangeProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"selectionRangeProvider":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with an options selection range provider property), verify`() {
        val incomingJson = """{"selectionRangeProvider":{"workDoneProgress":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualSelectionRangeProvider = actualObject.selectionRangeProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualSelectionRangeProvider is OptionsSelectionRangeProvider)
        assertTrue(actualSelectionRangeProvider.options is IsaacSelectionRangeOptions)
        assertEquals(true, actualSelectionRangeProvider.options.workDoneProgress)
    }

    @Test
    fun `When encoding a server capabilities object (with a selection range registration options property), verify`() {
        val options: SelectionRangeRegistrationOptions = IsaacSelectionRangeRegistrationOptions(
            documentSelector = listOf(IsaacDocumentFilter("Kotlin", "httpsScheme", "*.{ts,kt}")),
            workDoneProgress = true, id = "anId"
        )
        val registrationOptions: SelectionRangeRegistrationOptions = options
        val provider = RegistrationOptionsSelectionRangeProvider(registrationOptions)
        val data: ServerCapabilities = IsaacServerCapabilities(selectionRangeProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"selectionRangeProvider":{"documentSelector":[""" +
            """{"language":"Kotlin","scheme":"httpsScheme","pattern":"*.{ts,kt}"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a selectionRange registration options property), verify`() {
        val incomingJson = """{"selectionRangeProvider":{"documentSelector":[""" +
            """{"language":"Kotlin","scheme":"httpsScheme","pattern":"*.{ts,kt}"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualSelectionRangeProvider = actualObject.selectionRangeProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualSelectionRangeProvider is RegistrationOptionsSelectionRangeProvider)
        assertTrue(actualSelectionRangeProvider.registrationOptions is IsaacSelectionRangeRegistrationOptions)
        assertEquals(1, actualSelectionRangeProvider.registrationOptions.documentSelector!!.size)
        assertEquals("Kotlin", actualSelectionRangeProvider.registrationOptions.documentSelector!![0].language)
        assertEquals("httpsScheme", actualSelectionRangeProvider.registrationOptions.documentSelector!![0].scheme)
        assertEquals("*.{ts,kt}", actualSelectionRangeProvider.registrationOptions.documentSelector!![0].pattern)
        assertEquals(true, actualSelectionRangeProvider.registrationOptions.workDoneProgress)
        assertEquals("anId", actualSelectionRangeProvider.registrationOptions.id)
    }
}
