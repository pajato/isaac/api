package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanFullOptions
import com.pajato.isaac.api.DeltaFullOptions
import com.pajato.isaac.api.Full
import com.pajato.isaac.api.IsaacDocumentFilter
import com.pajato.isaac.api.IsaacSemanticTokensLegend
import com.pajato.isaac.api.IsaacSemanticTokensOptions
import com.pajato.isaac.api.IsaacSemanticTokensRegistrationOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.OptionsSemanticTokensProvider
import com.pajato.isaac.api.RegistrationOptionsSemanticTokensProvider
import com.pajato.isaac.api.SemanticTokensOptions
import com.pajato.isaac.api.SemanticTokensProvider
import com.pajato.isaac.api.SemanticTokensRegistrationOptions
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import com.pajato.isaac.assertLegend
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class SemanticTokensProviderUnitTest : ApiTestProfiler() {

    private fun getInvalidOptionsClassNameMessage(provider: OptionsSemanticTokensProvider) =
        "Invalid options type: ${provider.options::class.java.name}"

    @Test
    fun `When encoding a server capabilities object (with a basic options semantic tokens provider), verify`() {
        val options: SemanticTokensOptions = IsaacSemanticTokensOptions(
            workDoneProgress = true,
            legend = IsaacSemanticTokensLegend(arrayOf("type1", "type2"), arrayOf("modifier1", "modifier2"))
        )
        val provider = OptionsSemanticTokensProvider(options)
        val data: ServerCapabilities = IsaacServerCapabilities(semanticTokensProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"semanticTokensProvider":{"workDoneProgress":true,"legend":""" +
            """{"tokenTypes":["type1","type2"],"tokenModifiers":["modifier1","modifier2"]}}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a basic options semantic tokens provider), verify`() {
        fun assertOptions(provider: SemanticTokensProvider?) {
            assertTrue(provider is OptionsSemanticTokensProvider)
            assertEquals(true, provider.options.workDoneProgress)
            assertLegend(provider.options)
        }

        val incomingJson = """{"semanticTokensProvider":{"workDoneProgress":true,"legend":""" +
            """{"tokenTypes":["type1","type2"],"tokenModifiers":["modifier1","modifier2"]}}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualObject.semanticTokensProvider != null, "No provider was found!")
        assertTrue(actualObject.semanticTokensProvider is OptionsSemanticTokensProvider)
        assertOptions(actualObject.semanticTokensProvider)
    }

    @Test
    fun `When encoding a server capabilities object (with a range options semantic tokens provider), verify`() {
        val options: SemanticTokensOptions = IsaacSemanticTokensOptions(
            workDoneProgress = true,
            legend = IsaacSemanticTokensLegend(arrayOf("type1", "type2"), arrayOf("modifier1", "modifier2")),
            range = true,
        )
        val provider = OptionsSemanticTokensProvider(options)
        val data: ServerCapabilities = IsaacServerCapabilities(semanticTokensProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"semanticTokensProvider":{"workDoneProgress":true,"legend":""" +
            """{"tokenTypes":["type1","type2"],"tokenModifiers":["modifier1","modifier2"]},"range":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a range options semantic tokens provider), verify`() {
        fun assertOptions(provider: OptionsSemanticTokensProvider) {
            fun assertRange(options: SemanticTokensOptions) {
                assertTrue(options.full == null, """Expected the "full" property to be missing!""")
                assertEquals(true, options.range)
            }

            assertTrue(provider.options is IsaacSemanticTokensOptions, getInvalidOptionsClassNameMessage(provider))
            assertEquals(true, provider.options.workDoneProgress)
            assertLegend(provider.options)
            assertRange(provider.options)
        }

        val incomingJson = """{"semanticTokensProvider":{"workDoneProgress":true,"legend":""" +
            """{"tokenTypes":["type1","type2"],"tokenModifiers":["modifier1","modifier2"]},"range":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualObject.semanticTokensProvider != null, "No provider was found!")
        assertTrue(actualObject.semanticTokensProvider is OptionsSemanticTokensProvider)
        assertOptions(actualObject.semanticTokensProvider as OptionsSemanticTokensProvider)
    }

    @Test
    fun `When encoding a server capabilities object (with a boolean full options semantic tokens provider), verify`() {
        val options: SemanticTokensOptions = IsaacSemanticTokensOptions(
            workDoneProgress = true,
            legend = IsaacSemanticTokensLegend(arrayOf("type1", "type2"), arrayOf("modifier1", "modifier2")),
            full = BooleanFullOptions(true)
        )
        val provider = OptionsSemanticTokensProvider(options)
        val data: ServerCapabilities = IsaacServerCapabilities(semanticTokensProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"semanticTokensProvider":{"workDoneProgress":true,"legend":""" +
            """{"tokenTypes":["type1","type2"],"tokenModifiers":["modifier1","modifier2"]},"full":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a full options semantic tokens provider), verify`() {
        fun assertOptions(provider: OptionsSemanticTokensProvider) {
            fun assertFull(options: SemanticTokensOptions) {
                fun assertFullValue() {
                    val full: Full? = options.full
                    assertTrue(full is BooleanFullOptions, "Expected a boolean range options type!")
                    assertEquals(true, full.state)
                }

                assertTrue(options.range == null, """Expected the "range" property to be missing!""")
                assertFullValue()
            }

            assertTrue(provider.options is IsaacSemanticTokensOptions, getInvalidOptionsClassNameMessage(provider))
            assertEquals(true, provider.options.workDoneProgress)
            assertLegend(provider.options)
            assertFull(provider.options)
        }

        val incomingJson = """{"semanticTokensProvider":{"workDoneProgress":true,"legend":""" +
            """{"tokenTypes":["type1","type2"],"tokenModifiers":["modifier1","modifier2"]},"full":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualObject.semanticTokensProvider != null, "No provider was found!")
        assertTrue(actualObject.semanticTokensProvider is OptionsSemanticTokensProvider)
        assertOptions(actualObject.semanticTokensProvider as OptionsSemanticTokensProvider)
    }

    @Test
    fun `When encoding a server capabilities object (with a delta full options semantic tokens provider), verify`() {
        val options: SemanticTokensOptions = IsaacSemanticTokensOptions(
            workDoneProgress = true,
            legend = IsaacSemanticTokensLegend(arrayOf("type1", "type2"), arrayOf("modifier1", "modifier2")),
            full = DeltaFullOptions(true)
        )
        val provider = OptionsSemanticTokensProvider(options)
        val data: ServerCapabilities = IsaacServerCapabilities(semanticTokensProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"semanticTokensProvider":{"workDoneProgress":true,"legend":""" +
            """{"tokenTypes":["type1","type2"],"tokenModifiers":["modifier1","modifier2"]},"full":""" +
            """{"delta":true}}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a delta full options semantic tokens provider), verify`() {
        fun assertOptions(provider: OptionsSemanticTokensProvider) {
            fun assertFull(options: SemanticTokensOptions) {
                fun assertFullValue() {
                    val full: Full? = options.full
                    assertTrue(full is DeltaFullOptions, "Expected a delta full type!")
                    assertEquals(true, full.delta)
                }

                assertTrue(options.range == null, """Expected the "range" property to be missing!""")
                assertFullValue()
            }

            assertTrue(provider.options is IsaacSemanticTokensOptions, getInvalidOptionsClassNameMessage(provider))
            assertEquals(true, provider.options.workDoneProgress)
            assertLegend(provider.options)
            assertFull(provider.options)
        }

        val incomingJson = """{"semanticTokensProvider":{"workDoneProgress":true,"legend":""" +
            """{"tokenTypes":["type1","type2"],"tokenModifiers":["modifier1","modifier2"]},"full":""" +
            """{"delta":true}}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualObject.semanticTokensProvider != null, "No provider was found!")
        assertTrue(actualObject.semanticTokensProvider is OptionsSemanticTokensProvider)
        assertOptions(actualObject.semanticTokensProvider as OptionsSemanticTokensProvider)
    }

    @Test
    fun `When encoding a server capabilities object (with a semantic tokens registration options property), verify`() {
        val options: SemanticTokensRegistrationOptions = IsaacSemanticTokensRegistrationOptions(
            documentSelector = listOf(IsaacDocumentFilter("Kotlin", "httpsScheme", "*.{ts,kt}")), id = "anId",
            legend = IsaacSemanticTokensLegend(arrayOf("type1", "type2"), arrayOf("modifier1", "modifier2")),
        )
        val registrationOptions: SemanticTokensRegistrationOptions = options
        val provider = RegistrationOptionsSemanticTokensProvider(registrationOptions)
        val data: ServerCapabilities = IsaacServerCapabilities(semanticTokensProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"semanticTokensProvider":{"documentSelector":[""" +
            """{"language":"Kotlin","scheme":"httpsScheme","pattern":"*.{ts,kt}"}],"legend":""" +
            """{"tokenTypes":["type1","type2"],"tokenModifiers":["modifier1","modifier2"]},"id":"anId"}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a semantic tokens registration options property), verify`() {
        fun assertOptions(options: SemanticTokensRegistrationOptions) {
            fun assertDocumentSelector() {
                assertEquals(1, options.documentSelector!!.size)
                assertEquals("Kotlin", options.documentSelector!![0].language)
                assertEquals("httpsScheme", options.documentSelector!![0].scheme)
                assertEquals("*.{ts,kt}", options.documentSelector!![0].pattern)
            }

            assertTrue(options.documentSelector != null)
            assertDocumentSelector()
            assertEquals("anId", options.id)
        }

        val incomingJson = """{"semanticTokensProvider":{"documentSelector":[""" +
            """{"language":"Kotlin","scheme":"httpsScheme","pattern":"*.{ts,kt}"}],"legend":""" +
            """{"tokenTypes":["type1","type2"],"tokenModifiers":["modifier1","modifier2"]},"id":"anId"}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualSemanticTokensProvider = actualObject.semanticTokensProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualSemanticTokensProvider is RegistrationOptionsSemanticTokensProvider)
        assertTrue(actualSemanticTokensProvider.registrationOptions is IsaacSemanticTokensRegistrationOptions)
        assertOptions(actualSemanticTokensProvider.registrationOptions)
    }
}
