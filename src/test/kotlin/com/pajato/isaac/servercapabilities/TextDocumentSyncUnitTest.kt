package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.IsaacTextDocumentSyncOptions
import com.pajato.isaac.api.KindTextDocumentSync
import com.pajato.isaac.api.OptionsTextDocumentSync
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import com.pajato.isaac.api.TextDocumentSyncKind.Full
import com.pajato.isaac.api.TextDocumentSyncKind.None
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TextDocumentSyncUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with text document sync kind property), verify result`() {
        val data: ServerCapabilities = IsaacServerCapabilities(textDocumentSync = KindTextDocumentSync(None))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"textDocumentSync":0}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with text document sync kind property), verify result`() {
        val incomingJson = """{"textDocumentSync":0}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualTextDocumentSync = actualObject.textDocumentSync

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualTextDocumentSync is KindTextDocumentSync)
        assertEquals(None, actualTextDocumentSync.kind)
    }

    @Test
    fun `When encoding a server capabilities object (with text document sync options property), verify result`() {
        val data: ServerCapabilities = IsaacServerCapabilities(
            textDocumentSync = OptionsTextDocumentSync(
                IsaacTextDocumentSyncOptions(
                    openClose = false,
                    change = Full,
                    willSave = true,
                    willSaveWaitUntil = false,
                    save = null
                )
            ),
        )
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = "{" +
            """"textDocumentSync":{"openClose":false,"change":1,"willSave":true,"willSaveWaitUntil":false}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with text document sync options), verify result`() {
        val incomingJson = "{" +
            """"textDocumentSync":{"openClose":false,"change":1,"willSave":true,"willSaveWaitUntil":false}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualTextDocumentSync = actualObject.textDocumentSync

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualTextDocumentSync is OptionsTextDocumentSync)
        assertEquals(false, actualTextDocumentSync.options.openClose)
        assertEquals(Full, actualTextDocumentSync.options.change)
        assertEquals(true, actualTextDocumentSync.options.willSave)
        assertEquals(false, actualTextDocumentSync.options.willSaveWaitUntil)
        assertEquals(null, actualTextDocumentSync.options.save)
    }
}
