package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.ExecuteCommandOptions
import com.pajato.isaac.api.IsaacExecuteCommandOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ExecuteCommandsProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with an execute commands provider property), verify`() {
        val options: ExecuteCommandOptions = IsaacExecuteCommandOptions(true, listOf("command1", "command2"))
        val data: ServerCapabilities = IsaacServerCapabilities(executeCommandProvider = options)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"executeCommandProvider":{"workDoneProgress":true,"commands":["command1","command2"]}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with an execute commands provider property), verify`() {
        val incomingJson = """{"executeCommandProvider":{"workDoneProgress":true,"commands":["command1","command2"]}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualExecuteCommandProvider = actualObject.executeCommandProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualExecuteCommandProvider is IsaacExecuteCommandOptions)
        assertEquals(true, actualExecuteCommandProvider.workDoneProgress)
        assertEquals(2, actualExecuteCommandProvider.commands.size)
        assertEquals("command1", actualExecuteCommandProvider.commands[0])
        assertEquals("command2", actualExecuteCommandProvider.commands[1])
    }
}
