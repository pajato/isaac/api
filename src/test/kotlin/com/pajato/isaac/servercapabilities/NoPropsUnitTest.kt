package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class NoPropsUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with no specific properties), verify result`() {
        assertEquals("{}", Json.encodeToString(ServerCapabilitiesSerializer, IsaacServerCapabilities()))
    }

    @Test
    fun `When decoding a server capabilities object (with no specific properties), verify result`() {
        val uut = Json.decodeFromString(ServerCapabilitiesSerializer, "{}")

        assertTrue(uut is IsaacServerCapabilities)
        assertEquals(null, uut.textDocumentSync)
        assertEquals(null, uut.completionProvider)
        assertEquals(null, uut.hoverProvider)
        assertEquals(null, uut.workspace)
    }
}
