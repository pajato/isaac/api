package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.IsaacDocumentLinkOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DocumentLinkProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with a document link provider property), verify result`() {
        val options = IsaacDocumentLinkOptions(workDoneProgress = true, resolveProvider = true)
        val data: ServerCapabilities = IsaacServerCapabilities(documentLinkProvider = options)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"documentLinkProvider":{"workDoneProgress":true,"resolveProvider":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a document link provider property), verify result`() {
        val incomingJson = """{"documentLinkProvider":{"workDoneProgress":true,"resolveProvider":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualDocumentLinkProvider = actualObject.documentLinkProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualDocumentLinkProvider is IsaacDocumentLinkOptions)
        assertEquals(true, actualDocumentLinkProvider.workDoneProgress)
        assertEquals(true, actualDocumentLinkProvider.resolveProvider)
    }
}
