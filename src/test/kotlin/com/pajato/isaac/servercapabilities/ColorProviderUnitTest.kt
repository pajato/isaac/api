package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanColorProvider
import com.pajato.isaac.api.ColorOptions
import com.pajato.isaac.api.ColorRegistrationOptions
import com.pajato.isaac.api.IsaacColorOptions
import com.pajato.isaac.api.IsaacColorRegistrationOptions
import com.pajato.isaac.api.IsaacDocumentFilter
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.OptionsColorProvider
import com.pajato.isaac.api.RegistrationOptionsColorProvider
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ColorProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with a boolean color provider property), verify result`() {
        val data: ServerCapabilities = IsaacServerCapabilities(colorProvider = BooleanColorProvider(true))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"colorProvider":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a boolean color provider property), verify result`() {
        val incomingJson = """{"colorProvider":true}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualColorProvider = actualObject.colorProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualColorProvider is BooleanColorProvider)
        assertEquals(true, actualColorProvider.state)
    }

    @Test
    fun `When encoding a server capabilities object (with an options color provider property), verify result`() {
        val options: ColorOptions = IsaacColorOptions(true)
        val data: ServerCapabilities = IsaacServerCapabilities(colorProvider = OptionsColorProvider(options))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"colorProvider":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with an options color provider property), verify result`() {
        val incomingJson = """{"colorProvider":{"workDoneProgress":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualColorProvider = actualObject.colorProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualColorProvider is OptionsColorProvider)
        assertTrue(actualColorProvider.options is IsaacColorOptions)
        assertEquals(true, actualColorProvider.options.workDoneProgress)
    }

    @Test
    fun `When encoding a server capabilities object (with a color registration options property), verify`() {
        val options: ColorRegistrationOptions = IsaacColorRegistrationOptions(
            documentSelector = listOf(IsaacDocumentFilter("Kotlin", "httpsScheme", "*.{ts,kt}")),
            workDoneProgress = true, id = "anId"
        )
        val registrationOptions: ColorRegistrationOptions = options
        val provider = RegistrationOptionsColorProvider(registrationOptions)
        val data: ServerCapabilities = IsaacServerCapabilities(colorProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"colorProvider":{"documentSelector":[""" +
            """{"language":"Kotlin","scheme":"httpsScheme","pattern":"*.{ts,kt}"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a color registration options property), verify`() {
        val incomingJson = """{"colorProvider":{"documentSelector":[""" +
            """{"language":"Kotlin","scheme":"httpsScheme","pattern":"*.{ts,kt}"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualColorProvider = actualObject.colorProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualColorProvider is RegistrationOptionsColorProvider)
        assertTrue(actualColorProvider.registrationOptions is IsaacColorRegistrationOptions)
        assertEquals(1, actualColorProvider.registrationOptions.documentSelector!!.size)
        assertEquals("Kotlin", actualColorProvider.registrationOptions.documentSelector!![0].language)
        assertEquals("httpsScheme", actualColorProvider.registrationOptions.documentSelector!![0].scheme)
        assertEquals("*.{ts,kt}", actualColorProvider.registrationOptions.documentSelector!![0].pattern)
        assertEquals(true, actualColorProvider.registrationOptions.workDoneProgress)
        assertEquals("anId", actualColorProvider.registrationOptions.id)
    }
}
