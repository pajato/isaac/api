package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanReferencesProvider
import com.pajato.isaac.api.IsaacReferencesOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.OptionsReferencesProvider
import com.pajato.isaac.api.ReferencesOptions
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ReferencesProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with a boolean references provider property), verify result`() {
        val data: ServerCapabilities = IsaacServerCapabilities(referencesProvider = BooleanReferencesProvider(true))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"referencesProvider":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a boolean references provider property), verify result`() {
        val incomingJson = """{"referencesProvider":true}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualReferencesProvider = actualObject.referencesProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualReferencesProvider is BooleanReferencesProvider)
        assertEquals(true, actualReferencesProvider.state)
    }

    @Test
    fun `When encoding a server capabilities object (with an options references provider property), verify result`() {
        val options: ReferencesOptions = IsaacReferencesOptions(true)
        val data: ServerCapabilities = IsaacServerCapabilities(referencesProvider = OptionsReferencesProvider(options))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"referencesProvider":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with an options references provider property), verify result`() {
        val incomingJson = """{"referencesProvider":{"workDoneProgress":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualReferencesProvider = actualObject.referencesProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualReferencesProvider is OptionsReferencesProvider)
        assertTrue(actualReferencesProvider.options is IsaacReferencesOptions)
        assertEquals(true, actualReferencesProvider.options.workDoneProgress)
    }
}
