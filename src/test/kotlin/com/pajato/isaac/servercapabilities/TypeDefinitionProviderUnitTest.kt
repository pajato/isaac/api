package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanTypeDefinitionProvider
import com.pajato.isaac.api.IsaacDocumentFilter
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.IsaacTypeDefinitionOptions
import com.pajato.isaac.api.IsaacTypeDefinitionRegistrationOptions
import com.pajato.isaac.api.OptionsTypeDefinitionProvider
import com.pajato.isaac.api.RegistrationOptionsTypeDefinitionProvider
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import com.pajato.isaac.api.TypeDefinitionOptions
import com.pajato.isaac.api.TypeDefinitionRegistrationOptions
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TypeDefinitionProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with a boolean type definition provider property), verify`() {
        val provider = BooleanTypeDefinitionProvider(true)
        val data: ServerCapabilities = IsaacServerCapabilities(typeDefinitionProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"typeDefinitionProvider":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a boolean type definition provider property), verify`() {
        val incomingJson = """{"typeDefinitionProvider":true}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualTypeDefinitionProvider = actualObject.typeDefinitionProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualTypeDefinitionProvider is BooleanTypeDefinitionProvider)
        assertEquals(true, actualTypeDefinitionProvider.state)
    }

    @Test
    fun `When encoding a server capabilities object (with an options type definition provider property), verify`() {
        val options: TypeDefinitionOptions = IsaacTypeDefinitionOptions(true)
        val provider = OptionsTypeDefinitionProvider(options)
        val data: ServerCapabilities = IsaacServerCapabilities(typeDefinitionProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"typeDefinitionProvider":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with an options type definition provider property), verify`() {
        val incomingJson = """{"typeDefinitionProvider":{"workDoneProgress":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualTypeDefinitionProvider = actualObject.typeDefinitionProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualTypeDefinitionProvider is OptionsTypeDefinitionProvider)
        assertTrue(actualTypeDefinitionProvider.options is IsaacTypeDefinitionOptions)
        assertEquals(true, actualTypeDefinitionProvider.options.workDoneProgress)
    }

    @Test
    fun `When encoding a server capabilities object (with a type definition registration options property), verify`() {
        val options: TypeDefinitionRegistrationOptions = IsaacTypeDefinitionRegistrationOptions(
            documentSelector = listOf(IsaacDocumentFilter("Kotlin", "httpsScheme", "*.{ts,kt}")),
            workDoneProgress = true, id = "anId"
        )
        val registrationOptions: TypeDefinitionRegistrationOptions = options
        val provider = RegistrationOptionsTypeDefinitionProvider(registrationOptions)
        val data: ServerCapabilities = IsaacServerCapabilities(typeDefinitionProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"typeDefinitionProvider":{"documentSelector":[""" +
            """{"language":"Kotlin","scheme":"httpsScheme","pattern":"*.{ts,kt}"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a type definition registration options property), verify`() {
        val incomingJson = """{"typeDefinitionProvider":{"documentSelector":[""" +
            """{"language":"Kotlin","scheme":"httpsScheme","pattern":"*.{ts,kt}"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualTypeDefinitionProvider = actualObject.typeDefinitionProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualTypeDefinitionProvider is RegistrationOptionsTypeDefinitionProvider)
        assertTrue(actualTypeDefinitionProvider.registrationOptions is IsaacTypeDefinitionRegistrationOptions)
        assertEquals(1, actualTypeDefinitionProvider.registrationOptions.documentSelector!!.size)
        assertEquals("Kotlin", actualTypeDefinitionProvider.registrationOptions.documentSelector!![0].language)
        assertEquals("httpsScheme", actualTypeDefinitionProvider.registrationOptions.documentSelector!![0].scheme)
        assertEquals("*.{ts,kt}", actualTypeDefinitionProvider.registrationOptions.documentSelector!![0].pattern)
        assertEquals(true, actualTypeDefinitionProvider.registrationOptions.workDoneProgress)
        assertEquals("anId", actualTypeDefinitionProvider.registrationOptions.id)
    }
}
