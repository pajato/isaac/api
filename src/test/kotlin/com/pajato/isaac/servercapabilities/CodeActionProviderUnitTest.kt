package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanCodeActionProvider
import com.pajato.isaac.api.CodeActionOptions
import com.pajato.isaac.api.IsaacCodeActionOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.OptionsCodeActionProvider
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class CodeActionProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with a boolean code action provider property), verify`() {
        val data: ServerCapabilities =
            IsaacServerCapabilities(codeActionProvider = BooleanCodeActionProvider(true))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"codeActionProvider":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a boolean code action provider property), verify`() {
        val incomingJson = """{"codeActionProvider":true}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualCodeActionProvider = actualObject.codeActionProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualCodeActionProvider is BooleanCodeActionProvider)
        assertEquals(true, actualCodeActionProvider.state)
    }

    @Test
    fun `When encoding a server capabilities object (with an options code action provider property), verify`() {
        val options: CodeActionOptions = IsaacCodeActionOptions(true)
        val data: ServerCapabilities =
            IsaacServerCapabilities(codeActionProvider = OptionsCodeActionProvider(options))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"codeActionProvider":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with an options code action provider property), verify `() {
        val incomingJson = """{"codeActionProvider":{"workDoneProgress":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualCodeActionProvider = actualObject.codeActionProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualCodeActionProvider is OptionsCodeActionProvider)
        assertTrue(actualCodeActionProvider.options is IsaacCodeActionOptions)
        assertEquals(true, actualCodeActionProvider.options.workDoneProgress)
    }
}
