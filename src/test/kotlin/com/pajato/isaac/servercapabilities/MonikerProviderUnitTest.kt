package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanMonikerProvider
import com.pajato.isaac.api.IsaacDocumentFilter
import com.pajato.isaac.api.IsaacMonikerOptions
import com.pajato.isaac.api.IsaacMonikerRegistrationOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.MonikerOptions
import com.pajato.isaac.api.MonikerRegistrationOptions
import com.pajato.isaac.api.OptionsMonikerProvider
import com.pajato.isaac.api.RegistrationOptionsMonikerProvider
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class MonikerProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with a boolean moniker provider property), verify result`() {
        val data: ServerCapabilities = IsaacServerCapabilities(monikerProvider = BooleanMonikerProvider(true))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"monikerProvider":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a boolean moniker provider property), verify result`() {
        val incomingJson = """{"monikerProvider":true}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualMonikerProvider = actualObject.monikerProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualMonikerProvider is BooleanMonikerProvider)
        assertEquals(true, actualMonikerProvider.state)
    }

    @Test
    fun `When encoding a server capabilities object (with an options moniker provider property), verify result`() {
        val options: MonikerOptions = IsaacMonikerOptions(true)
        val data: ServerCapabilities = IsaacServerCapabilities(monikerProvider = OptionsMonikerProvider(options))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"monikerProvider":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with an options moniker provider property), verify result`() {
        val incomingJson = """{"monikerProvider":{"workDoneProgress":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualMonikerProvider = actualObject.monikerProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualMonikerProvider is OptionsMonikerProvider)
        assertTrue(actualMonikerProvider.options is IsaacMonikerOptions)
        assertEquals(true, actualMonikerProvider.options.workDoneProgress)
    }

    @Test
    fun `When encoding a server capabilities object (with a moniker registration options property), verify`() {
        val options: MonikerRegistrationOptions = IsaacMonikerRegistrationOptions(
            documentSelector = listOf(IsaacDocumentFilter("Kotlin", "httpsScheme", "*.{ts,kt}")),
            workDoneProgress = true, id = "anId"
        )
        val registrationOptions: MonikerRegistrationOptions = options
        val provider = RegistrationOptionsMonikerProvider(registrationOptions)
        val data: ServerCapabilities = IsaacServerCapabilities(monikerProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"monikerProvider":{"documentSelector":[""" +
            """{"language":"Kotlin","scheme":"httpsScheme","pattern":"*.{ts,kt}"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a moniker registration options property), verify`() {
        val incomingJson = """{"monikerProvider":{"documentSelector":[""" +
            """{"language":"Kotlin","scheme":"httpsScheme","pattern":"*.{ts,kt}"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualMonikerProvider = actualObject.monikerProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualMonikerProvider is RegistrationOptionsMonikerProvider)
        assertTrue(actualMonikerProvider.registrationOptions is IsaacMonikerRegistrationOptions)
        assertEquals(1, actualMonikerProvider.registrationOptions.documentSelector!!.size)
        assertEquals("Kotlin", actualMonikerProvider.registrationOptions.documentSelector!![0].language)
        assertEquals("httpsScheme", actualMonikerProvider.registrationOptions.documentSelector!![0].scheme)
        assertEquals("*.{ts,kt}", actualMonikerProvider.registrationOptions.documentSelector!![0].pattern)
        assertEquals(true, actualMonikerProvider.registrationOptions.workDoneProgress)
        assertEquals("anId", actualMonikerProvider.registrationOptions.id)
    }
}
