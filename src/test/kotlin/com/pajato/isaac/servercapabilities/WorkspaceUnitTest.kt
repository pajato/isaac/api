package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanChangeNotifications
import com.pajato.isaac.api.ChangeNotifications
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.IsaacWorkspace
import com.pajato.isaac.api.IsaacWorkspaceFoldersServerCapabilities
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import com.pajato.isaac.api.Workspace
import com.pajato.isaac.api.WorkspaceFoldersServerCapabilities
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class WorkspaceUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object(with workspace property), verify result`() {
        fun getWorkspace(): Workspace {
            val changeNotifications: ChangeNotifications = BooleanChangeNotifications(true)
            val workspaceFolders: WorkspaceFoldersServerCapabilities =
                IsaacWorkspaceFoldersServerCapabilities(true, changeNotifications)

            return IsaacWorkspace(workspaceFolders)
        }

        val data: ServerCapabilities = IsaacServerCapabilities(workspace = getWorkspace())
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = "{" +
            """"workspace":{"workspaceFolders":{"supported":true,"changeNotifications":true}}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with workspace property), verify result`() {
        val incomingJson = "{" +
            """"workspace":{"workspaceFolders":{"supported":true,"changeNotifications":true}}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualChangeNotificationsValue = actualObject.workspace!!.workspaceFolders!!.changeNotifications

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualChangeNotificationsValue is BooleanChangeNotifications)
        assertEquals(true, actualObject.workspace!!.workspaceFolders!!.supported)
        assertEquals(true, actualChangeNotificationsValue.flag)
    }
}
