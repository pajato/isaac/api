package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanDocumentSymbolProvider
import com.pajato.isaac.api.DocumentSymbolOptions
import com.pajato.isaac.api.IsaacDocumentSymbolOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.OptionsDocumentSymbolProvider
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DocumentSymbolProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with a boolean document symbol provider property), verify`() {
        val data: ServerCapabilities =
            IsaacServerCapabilities(documentSymbolProvider = BooleanDocumentSymbolProvider(true))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"documentSymbolProvider":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a boolean document symbol provider property), verify`() {
        val incomingJson = """{"documentSymbolProvider":true}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualDocumentSymbolProvider = actualObject.documentSymbolProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualDocumentSymbolProvider is BooleanDocumentSymbolProvider)
        assertEquals(true, actualDocumentSymbolProvider.state)
    }

    @Test
    fun `When encoding a server capabilities object (with an options document symbol provider property), verify`() {
        val options: DocumentSymbolOptions = IsaacDocumentSymbolOptions(true)
        val data: ServerCapabilities =
            IsaacServerCapabilities(documentSymbolProvider = OptionsDocumentSymbolProvider(options))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"documentSymbolProvider":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with an options document symbol provider property), verify `() {
        val incomingJson = """{"documentSymbolProvider":{"workDoneProgress":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualDocumentSymbolProvider = actualObject.documentSymbolProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualDocumentSymbolProvider is OptionsDocumentSymbolProvider)
        assertTrue(actualDocumentSymbolProvider.options is IsaacDocumentSymbolOptions)
        assertEquals(true, actualDocumentSymbolProvider.options.workDoneProgress)
    }
}
