package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanFoldingRangeProvider
import com.pajato.isaac.api.FoldingRangeOptions
import com.pajato.isaac.api.FoldingRangeRegistrationOptions
import com.pajato.isaac.api.IsaacDocumentFilter
import com.pajato.isaac.api.IsaacFoldingRangeOptions
import com.pajato.isaac.api.IsaacFoldingRangeRegistrationOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.OptionsFoldingRangeProvider
import com.pajato.isaac.api.RegistrationOptionsFoldingRangeProvider
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class FoldingRangeProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with a boolean folding range provider property), verify result`() {
        val data: ServerCapabilities = IsaacServerCapabilities(foldingRangeProvider = BooleanFoldingRangeProvider(true))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"foldingRangeProvider":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a boolean folding range provider property), verify result`() {
        val incomingJson = """{"foldingRangeProvider":true}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualFoldingRangeProvider = actualObject.foldingRangeProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualFoldingRangeProvider is BooleanFoldingRangeProvider)
        assertEquals(true, actualFoldingRangeProvider.state)
    }

    @Test
    fun `When encoding a server capabilities object (with an options folding range provider property), verify result`() {
        val options: FoldingRangeOptions = IsaacFoldingRangeOptions(true)
        val data: ServerCapabilities = IsaacServerCapabilities(foldingRangeProvider = OptionsFoldingRangeProvider(options))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"foldingRangeProvider":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with an options folding range provider property), verify result`() {
        val incomingJson = """{"foldingRangeProvider":{"workDoneProgress":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualFoldingRangeProvider = actualObject.foldingRangeProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualFoldingRangeProvider is OptionsFoldingRangeProvider)
        assertTrue(actualFoldingRangeProvider.options is IsaacFoldingRangeOptions)
        assertEquals(true, actualFoldingRangeProvider.options.workDoneProgress)
    }

    @Test
    fun `When encoding a server capabilities object (with a foldingRange registration options property), verify`() {
        val options: FoldingRangeRegistrationOptions = IsaacFoldingRangeRegistrationOptions(
            documentSelector = listOf(IsaacDocumentFilter("Kotlin", "httpsScheme", "*.{ts,kt}")),
            workDoneProgress = true, id = "anId"
        )
        val registrationOptions: FoldingRangeRegistrationOptions = options
        val provider = RegistrationOptionsFoldingRangeProvider(registrationOptions)
        val data: ServerCapabilities = IsaacServerCapabilities(foldingRangeProvider = provider)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"foldingRangeProvider":{"documentSelector":[""" +
            """{"language":"Kotlin","scheme":"httpsScheme","pattern":"*.{ts,kt}"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a foldingRange registration options property), verify`() {
        val incomingJson = """{"foldingRangeProvider":{"documentSelector":[""" +
            """{"language":"Kotlin","scheme":"httpsScheme","pattern":"*.{ts,kt}"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualFoldingRangeProvider = actualObject.foldingRangeProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualFoldingRangeProvider is RegistrationOptionsFoldingRangeProvider)
        assertTrue(actualFoldingRangeProvider.registrationOptions is IsaacFoldingRangeRegistrationOptions)
        assertEquals(1, actualFoldingRangeProvider.registrationOptions.documentSelector!!.size)
        assertEquals("Kotlin", actualFoldingRangeProvider.registrationOptions.documentSelector!![0].language)
        assertEquals("httpsScheme", actualFoldingRangeProvider.registrationOptions.documentSelector!![0].scheme)
        assertEquals("*.{ts,kt}", actualFoldingRangeProvider.registrationOptions.documentSelector!![0].pattern)
        assertEquals(true, actualFoldingRangeProvider.registrationOptions.workDoneProgress)
        assertEquals("anId", actualFoldingRangeProvider.registrationOptions.id)
    }
}
