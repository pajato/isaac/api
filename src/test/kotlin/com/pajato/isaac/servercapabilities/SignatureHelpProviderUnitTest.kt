package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.IsaacSignatureHelpOptions
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import com.pajato.isaac.api.SignatureHelpOptions
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class SignatureHelpProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with a signature help provider property), verify result`() {
        val options: SignatureHelpOptions = IsaacSignatureHelpOptions(true, arrayOf("(", ","), arrayOf("-"))
        val data: ServerCapabilities = IsaacServerCapabilities(signatureHelpProvider = options)
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"signatureHelpProvider":""" +
            """{"workDoneProgress":true,"triggerCharacters":["(",","],"retriggerCharacters":["-"]}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a signature help provider property), verify result`() {
        val incomingJson = """{"signatureHelpProvider":""" +
            """{"workDoneProgress":true,"triggerCharacters":["(",","],"retriggerCharacters":["-"]}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualSignatureHelpProvider = actualObject.signatureHelpProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualSignatureHelpProvider is IsaacSignatureHelpOptions)
        assertEquals(true, actualSignatureHelpProvider.workDoneProgress)
        assertEquals(2, actualSignatureHelpProvider.triggerCharacters!!.size)
        assertEquals("(", actualSignatureHelpProvider.triggerCharacters!![0])
        assertEquals(",", actualSignatureHelpProvider.triggerCharacters!![1])
        assertEquals(1, actualSignatureHelpProvider.retriggerCharacters!!.size)
        assertEquals("-", actualSignatureHelpProvider.retriggerCharacters!![0])
    }
}
