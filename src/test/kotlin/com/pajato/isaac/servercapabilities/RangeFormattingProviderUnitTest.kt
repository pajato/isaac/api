package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanDocumentRangeFormattingProvider
import com.pajato.isaac.api.DocumentRangeFormattingOptions
import com.pajato.isaac.api.IsaacDocumentRangeFormattingOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.OptionsDocumentRangeFormattingProvider
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class RangeFormattingProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object - boolean document range formatting provider property, verify`() {
        val data: ServerCapabilities =
            IsaacServerCapabilities(documentRangeFormattingProvider = BooleanDocumentRangeFormattingProvider(true))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"documentRangeFormattingProvider":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object - boolean document range formatting provider property, verify`() {
        val incomingJson = """{"documentRangeFormattingProvider":true}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualDocumentRangeFormattingProvider = actualObject.documentRangeFormattingProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualDocumentRangeFormattingProvider is BooleanDocumentRangeFormattingProvider)
        assertEquals(true, actualDocumentRangeFormattingProvider.state)
    }

    @Test
    fun `When encoding a server capabilities object - options document range formatting provider property, verify`() {
        val options: DocumentRangeFormattingOptions = IsaacDocumentRangeFormattingOptions(true)
        val data: ServerCapabilities =
            IsaacServerCapabilities(documentRangeFormattingProvider = OptionsDocumentRangeFormattingProvider(options))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"documentRangeFormattingProvider":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object - options document range formatting provider property, verify`() {
        val incomingJson = """{"documentRangeFormattingProvider":{"workDoneProgress":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualDocumentRangeFormattingProvider = actualObject.documentRangeFormattingProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualDocumentRangeFormattingProvider is OptionsDocumentRangeFormattingProvider)
        assertTrue(actualDocumentRangeFormattingProvider.options is IsaacDocumentRangeFormattingOptions)
        assertEquals(true, actualDocumentRangeFormattingProvider.options.workDoneProgress)
    }
}
