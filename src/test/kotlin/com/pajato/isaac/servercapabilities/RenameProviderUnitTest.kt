package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanRenameProvider
import com.pajato.isaac.api.IsaacRenameOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.OptionsRenameProvider
import com.pajato.isaac.api.RenameOptions
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class RenameProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object (with a boolean rename provider property), verify result`() {
        val data: ServerCapabilities = IsaacServerCapabilities(renameProvider = BooleanRenameProvider(true))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"renameProvider":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with a boolean rename provider property), verify result`() {
        val incomingJson = """{"renameProvider":true}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualRenameProvider = actualObject.renameProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualRenameProvider is BooleanRenameProvider)
        assertEquals(true, actualRenameProvider.state)
    }

    @Test
    fun `When encoding a server capabilities object (with an options rename provider property), verify result`() {
        val options: RenameOptions = IsaacRenameOptions(true)
        val data: ServerCapabilities = IsaacServerCapabilities(renameProvider = OptionsRenameProvider(options))
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = """{"renameProvider":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with an options rename provider property), verify result`() {
        val incomingJson = """{"renameProvider":{"workDoneProgress":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualRenameProvider = actualObject.renameProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualRenameProvider is OptionsRenameProvider)
        assertTrue(actualRenameProvider.options is IsaacRenameOptions)
        assertEquals(true, actualRenameProvider.options.workDoneProgress)
    }
}
