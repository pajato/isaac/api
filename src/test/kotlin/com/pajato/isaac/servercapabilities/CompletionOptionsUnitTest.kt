package com.pajato.isaac.servercapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.CompletionOptions
import com.pajato.isaac.api.IsaacCompletionOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerCapabilitiesSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class CompletionOptionsUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a server capabilities object(with completion options property), verify result`() {
        fun getCompletionProvider(): CompletionOptions = IsaacCompletionOptions(null, arrayOf("."), null, true)

        val data: ServerCapabilities = IsaacServerCapabilities(completionProvider = getCompletionProvider())
        val actualJson = Json.encodeToString(ServerCapabilitiesSerializer, data)
        val expectedJson = "{" +
            """"completionProvider":{"triggerCharacters":["."],"resolveProvider":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a server capabilities object (with completions options property), verify result`() {
        val incomingJson = "{" +
            """"completionProvider":{"triggerCharacters":["."],"resolveProvider":true}}"""
        val actualObject: ServerCapabilities = Json.decodeFromString(ServerCapabilitiesSerializer, incomingJson)
        val actualCompletionProvider = actualObject.completionProvider

        assertTrue(actualObject is IsaacServerCapabilities)
        assertTrue(actualCompletionProvider is IsaacCompletionOptions)
        assertEquals(null, actualCompletionProvider.workDoneProgress)
        assertEquals(1, actualCompletionProvider.triggerCharacters!!.size)
        assertEquals(".", actualCompletionProvider.triggerCharacters!![0])
        assertEquals(null, actualCompletionProvider.allCommitCharacters)
        assertEquals(true, actualCompletionProvider.resolveProvider)
    }
}
