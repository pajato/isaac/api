package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class DocumentOnTypeFormattingOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default document on type formatting options object, verify a correct result`() {
        val trigger = "}"
        val data = DocumentOnTypeFormattingOptions(trigger)
        val json = """{"firstTriggerCharacter":"$trigger"}"""

        testEncode(json, DocumentOnTypeFormattingOptionsSerializer, data)
        testDecode(json, DocumentOnTypeFormattingOptionsSerializer) { actualObject ->
            assertEquals(trigger, actualObject.firstTriggerCharacter)
            assertEquals(null, actualObject.moreTriggerCharacter)
        }
    }

    @Test fun `When serializing a document on type formatting options object, verify the result`() {
        fun assertOptions(expected: DocumentOnTypeFormattingOptions, actual: DocumentOnTypeFormattingOptions) {
            assertEquals(
                expected.firstTriggerCharacter, actual.firstTriggerCharacter,
                getDiagnostic("first trigger character")
            )
            assertEquals(
                expected.moreTriggerCharacter!!.size, actual.moreTriggerCharacter!!.size,
                getDiagnostic("more trigger character size")
            )
            assertEquals(
                expected.moreTriggerCharacter!![0], actual.moreTriggerCharacter!![0],
                getDiagnostic("first more trigger character")
            )
            assertEquals(
                expected.moreTriggerCharacter!![1], actual.moreTriggerCharacter!![1],
                getDiagnostic("second more trigger character")
            )
        }

        val data = DocumentOnTypeFormattingOptions("}", arrayOf(")", "]"))
        val json = """{"firstTriggerCharacter":"}","moreTriggerCharacter":[")","]"]}"""

        testEncode(json, DocumentOnTypeFormattingOptionsSerializer, data)
        testDecode(json, DocumentOnTypeFormattingOptionsSerializer) { actual -> assertOptions(data, actual) }
    }
}
