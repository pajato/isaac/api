package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import java.lang.IllegalStateException
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class SelectionRangeProviderUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a boolean selection range provider object, verify a correct result`() {
        val data = BooleanSelectionRangeProvider(true)
        val json = """{"state":true}"""

        testEncode(json, SelectionRangeProviderSerializer, data)
        testDecode(json, SelectionRangeProviderSerializer) { actualObject ->
            assertTrue(actualObject is BooleanSelectionRangeProvider)
            assertEquals(true, actualObject.state)
        }
    }

    @Test fun `When serializing an options selection range provider object, verify a correct result`() {
        val data: SelectionRangeProvider = OptionsSelectionRangeProvider(options = SelectionRangeOptions(true))
        val json = """{"options":{"workDoneProgress":true}}"""

        testEncode(json, SelectionRangeProviderSerializer, data)
        testDecode(json, SelectionRangeProviderSerializer) { actualObject ->
            assertTrue(actualObject is OptionsSelectionRangeProvider)
            assertEquals(true, actualObject.options.workDoneProgress)
        }
    }

    @Test fun `When serializing a registration options selection range provider object, verify a correct result`() {
        val registrationOptions = SelectionRangeRegistrationOptions(
            documentSelector = listOf(IsaacDocumentFilter("Kotlin", "scheme", "pattern")), workDoneProgress = true,
            id = "anId"
        )
        val data: SelectionRangeProvider = RegistrationOptionsSelectionRangeProvider(registrationOptions)
        val json = """{"registrationOptions":{"documentSelector":""" +
            """[{"language":"Kotlin","scheme":"scheme","pattern":"pattern"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""

        testEncode(json, SelectionRangeProviderSerializer, data)
        testDecode(json, SelectionRangeProviderSerializer) { actualObject ->
            assertTrue(actualObject is RegistrationOptionsSelectionRangeProvider)
            assertTrue(actualObject.registrationOptions is IsaacSelectionRangeRegistrationOptions)
            assertEquals(1, actualObject.registrationOptions.documentSelector!!.size)
            assertEquals("Kotlin", actualObject.registrationOptions.documentSelector!![0].language)
            assertEquals("scheme", actualObject.registrationOptions.documentSelector!![0].scheme)
            assertEquals("pattern", actualObject.registrationOptions.documentSelector!![0].pattern)
            assertEquals(true, actualObject.registrationOptions.workDoneProgress)
        }
    }

    @Test fun `When invalid JSON is used, verify an illegal state exception is thrown`() {
        val incomingJson = """{"xyzzy":true}"""
        assertFailsWith<IllegalStateException> { Json.decodeFromString(SelectionRangeProviderSerializer, incomingJson) }
    }
}
