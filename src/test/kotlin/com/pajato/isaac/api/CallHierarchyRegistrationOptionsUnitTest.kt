package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.assertOptions
import kotlin.test.Test

class CallHierarchyRegistrationOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertAll(expected: CallHierarchyRegistrationOptions, actual: CallHierarchyRegistrationOptions) {
        assertOptions(expected, actual)
    }

    @Test fun `When serializing a default call hierarchy registration options object, verify result`() {
        val data = CallHierarchyRegistrationOptions()
        val json = """{}"""

        testEncode(json, CallHierarchyRegistrationOptionsSerializer, data)
        testDecode(json, CallHierarchyRegistrationOptionsSerializer) { actual -> assertAll(data, actual) }
    }

    @Test fun `When decoding a call hierarchy range registration options object, verify a correct result`() {
        val filter = DocumentFilter("Kotlin", "scheme", "pattern")
        val data = CallHierarchyRegistrationOptions(
            documentSelector = listOf(filter), workDoneProgress = true, id = "registrationId"
        )
        val json = """{"documentSelector":[{"language":"Kotlin","scheme":"scheme","pattern":"pattern"}],""" +
            """"workDoneProgress":true,"id":"registrationId"}"""

        testEncode(json, CallHierarchyRegistrationOptionsSerializer, data)
        testDecode(json, CallHierarchyRegistrationOptionsSerializer) { actual -> assertAll(data, actual) }
    }
}
