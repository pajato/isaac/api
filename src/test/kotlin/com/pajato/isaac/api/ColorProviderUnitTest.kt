package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class ColorProviderUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a boolean color provider object, verify a correct result`() {
        val data = BooleanColorProvider(true)
        val json = """{"state":true}"""

        testEncode(json, ColorProviderSerializer, data)
        testDecode(json, ColorProviderSerializer) { actualObject ->
            assertTrue(actualObject is BooleanColorProvider)
            assertEquals(true, actualObject.state)
        }
    }

    @Test fun `When serializing an options color provider object, verify a correct result`() {
        val data = OptionsColorProvider(options = ColorOptions(true))
        val json = """{"options":{"workDoneProgress":true}}"""

        testEncode(json, ColorProviderSerializer, data)
        testDecode(json, ColorProviderSerializer) { actualObject ->
            assertTrue(actualObject is OptionsColorProvider)
            assertEquals(true, actualObject.options.workDoneProgress)
        }
    }

    @Test fun `When serializing a registration options color provider object, verify a correct result`() {
        val filter = DocumentFilter("Kotlin", "scheme", "pattern")
        val options = ColorRegistrationOptions(documentSelector = listOf(filter), workDoneProgress = true, id = "anId")
        val data = RegistrationOptionsColorProvider(registrationOptions = options)
        val json = """{"registrationOptions":{"documentSelector":""" +
            """[{"language":"Kotlin","scheme":"scheme","pattern":"pattern"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""

        testEncode(json, ColorProviderSerializer, data)
        testDecode(json, ColorProviderSerializer) { actualObject ->
            assertTrue(actualObject is RegistrationOptionsColorProvider)
            assertEquals(1, actualObject.registrationOptions.documentSelector!!.size)
            assertEquals("Kotlin", actualObject.registrationOptions.documentSelector!![0].language)
            assertEquals("scheme", actualObject.registrationOptions.documentSelector!![0].scheme)
            assertEquals("pattern", actualObject.registrationOptions.documentSelector!![0].pattern)
            assertEquals(true, actualObject.registrationOptions.workDoneProgress)
        }
    }

    @Test fun `When invalid JSON is used, verify an illegal state exception is thrown`() {
        val incomingJson = """{"xyzzy":true}"""

        assertFailsWith<IllegalStateException> { Json.decodeFromString(ColorProviderSerializer, incomingJson) }
    }
}
