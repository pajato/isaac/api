package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class ChangeAnnotationSupportUnitTest : SerializerTest, ApiTestProfiler() {
    @Test fun `When serializing a default change annotation support object, verify a correct result`() {
        val data = ChangeAnnotationSupport()
        val json = """{}"""

        testEncode(json, ChangeAnnotationSupportSerializer, data)
        testDecode(json, ChangeAnnotationSupportSerializer) { actualObject ->
            assertEquals(null, actualObject.groupsOnLabel)
        }
    }

    @Test fun `When serializing a non-default change annotation support object, verify a correct result`() {
        val data = ChangeAnnotationSupport(groupsOnLabel = true)
        val json = """{"groupsOnLabel":true}"""

        testEncode(json, ChangeAnnotationSupportSerializer, data)
        testDecode(json, ChangeAnnotationSupportSerializer) { actualObject ->
            assertEquals(true, actualObject.groupsOnLabel)
        }
    }
}
