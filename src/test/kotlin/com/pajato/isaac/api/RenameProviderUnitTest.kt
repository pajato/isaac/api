package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import java.lang.IllegalStateException
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class RenameProviderUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a boolean rename provider object, verify a correct result`() {
        val data: RenameProvider = BooleanRenameProvider(true)
        val json = """{"state":true}"""

        testEncode(json, RenameProviderSerializer, data)
        testDecode(json, RenameProviderSerializer) { actualObject ->
            assertTrue(actualObject is BooleanRenameProvider)
            assertEquals(true, actualObject.state)
        }
    }

    @Test fun `When serializing an options rename provider object, verify a correct result`() {
        val data: RenameProvider = OptionsRenameProvider(options = RenameOptions(true))
        val json = """{"options":{"workDoneProgress":true}}"""

        testEncode(json, RenameProviderSerializer, data)
        testDecode(json, RenameProviderSerializer) { actualObject ->
            assertTrue(actualObject is OptionsRenameProvider)
            assertEquals(true, actualObject.options.workDoneProgress)
        }
    }

    @Test fun `When invalid JSON is used, verify an illegal state exception is thrown`() {
        val incomingJson = """{"xyzzy":true}"""

        assertFailsWith<IllegalStateException> { Json.decodeFromString(RenameProviderSerializer, incomingJson) }
    }
}
