package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.assertOptions
import kotlin.test.Test

class MonikerRegistrationOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertAll(expected: MonikerRegistrationOptions, actual: MonikerRegistrationOptions) {
        assertOptions(expected, actual)
    }

    @Test fun `When serializing a moniker registration options default object, verify result`() {
        val data = MonikerRegistrationOptions()
        val json = """{}"""

        testEncode(json, MonikerRegistrationOptionsSerializer, data)
        testDecode(json, MonikerRegistrationOptionsSerializer) { actual -> assertAll(data, actual) }
    }

    @Test fun `When serializing a moniker registration options non-default object, verify result`() {
        val filter = DocumentFilter("Kotlin", "scheme", "pattern")
        val data = MonikerRegistrationOptions(
            documentSelector = listOf(filter), workDoneProgress = true, id = "registrationId"
        )
        val json = """{"documentSelector":[{"language":"Kotlin","scheme":"scheme","pattern":"pattern"}],""" +
            """"workDoneProgress":true,"id":"registrationId"}"""

        testEncode(json, MonikerRegistrationOptionsSerializer, data)
        testDecode(json, MonikerRegistrationOptionsSerializer) { actual -> assertAll(data, actual) }
    }
}
