package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class TypeDefinitionClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default type definition client capabilities object, verify a correct result`() {
        val data = TypeDefinitionClientCapabilities()
        val json = """{}"""

        testEncode(json, TypeDefinitionClientCapabilitiesSerializer, data)
        testDecode(json, TypeDefinitionClientCapabilitiesSerializer) { actualObject ->
            assertEquals(null, actualObject.dynamicRegistration)
            assertEquals(null, actualObject.linkSupport)
        }
    }

    @Test fun `When serializing a non-default type definition client capabilities object, verify a correct result`() {
        val data = TypeDefinitionClientCapabilities(dynamicRegistration = true, linkSupport = false)
        val json = """{"dynamicRegistration":true,"linkSupport":false}"""

        testEncode(json, TypeDefinitionClientCapabilitiesSerializer, data)
        testDecode(json, TypeDefinitionClientCapabilitiesSerializer) { actualObject ->
            assertEquals(true, actualObject.dynamicRegistration)
            assertEquals(false, actualObject.linkSupport)
        }
    }
}
