package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class WindowClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default window client capabilities object, verify a correct result`() {
        val data = WindowClientCapabilities()
        val json = """{}"""

        testEncode(json, WindowClientCapabilitiesSerializer, data)
        testDecode(json, WindowClientCapabilitiesSerializer) { actualObject ->
            assertEquals(null, actualObject.workDoneProgress)
            assertEquals(null, actualObject.showMessage)
            assertEquals(null, actualObject.showDocument)
        }
    }
}
