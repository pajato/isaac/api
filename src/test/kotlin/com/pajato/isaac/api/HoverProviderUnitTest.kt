package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class HoverProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a boolean hover provider object, verify the result`() {
        val actualJson = Json.encodeToString(TransformingHoverProviderSerializer, BooleanHoverProvider(true))
        val expectedJson = """true"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a boolean hover provider object, verify the result`() {
        val incomingJson = """true"""
        val actualObject = Json.decodeFromString(TransformingHoverProviderSerializer, incomingJson)

        assertTrue(actualObject is BooleanHoverProvider)
        assertEquals(true, actualObject.data)
    }

    @Test
    fun `When encoding an options hover provider object, verify the result`() {
        val provider = OptionsHoverProvider(IsaacHoverOptions(true))
        val actualJson = Json.encodeToString(TransformingHoverProviderSerializer, provider)
        val expectedJson = """{"workDoneProgress":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding an options hover provider object, verify the result`() {
        val incomingJson = """{"workDoneProgress":true}"""
        val actualObject = Json.decodeFromString(TransformingHoverProviderSerializer, incomingJson)

        assertTrue(actualObject is OptionsHoverProvider)
        assertEquals(true, (actualObject.data as IsaacHoverOptions).workDoneProgress)
    }
}
