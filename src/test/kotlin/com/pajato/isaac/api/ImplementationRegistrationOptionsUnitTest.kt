package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.assertOptions
import kotlin.test.Test

class ImplementationRegistrationOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertAll(expected: ImplementationRegistrationOptions, actual: ImplementationRegistrationOptions) {
        assertOptions(expected, actual)
    }

    @Test fun `When serializing a implementation registration options default object, verify result`() {
        val data = ImplementationRegistrationOptions()
        val json = """{}"""

        testEncode(json, ImplementationRegistrationOptionsSerializer, data)
        testDecode(json, ImplementationRegistrationOptionsSerializer) { actual -> assertAll(data, actual) }
    }

    @Test fun `When decoding a implementation registration options object, verify a correct result`() {
        val filter = DocumentFilter("Kotlin", "scheme", "pattern")
        val data = ImplementationRegistrationOptions(
            documentSelector = listOf(filter), workDoneProgress = true, id = "registrationId"
        )
        val json = """{"documentSelector":[{"language":"Kotlin","scheme":"scheme","pattern":"pattern"}],""" +
            """"workDoneProgress":true,"id":"registrationId"}"""

        testEncode(json, ImplementationRegistrationOptionsSerializer, data)
        testDecode(json, ImplementationRegistrationOptionsSerializer) { actual -> assertAll(data, actual) }
    }
}
