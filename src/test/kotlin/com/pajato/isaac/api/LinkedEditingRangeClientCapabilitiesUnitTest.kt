package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class LinkedEditingRangeClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertCapabilities(expected: Boolean?, actual: LinkedEditingRangeClientCapabilities) {
        assertEquals(expected, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
    }

    @Test fun `When serializing a linked editing range options default object, verify result`() {
        val data = LinkedEditingRangeClientCapabilities()
        val json = """{}"""

        testEncode(json, LinkedEditingRangeClientCapabilitiesSerializer, data)
        testDecode(json, LinkedEditingRangeClientCapabilitiesSerializer) { actual -> assertCapabilities(null, actual) }
    }

    @Test fun `When serializing a linked editing range options non-default object, verify result`() {
        val data = LinkedEditingRangeClientCapabilities(true)
        val json = """{"dynamicRegistration":true}"""

        testEncode(json, LinkedEditingRangeClientCapabilitiesSerializer, data)
        testDecode(json, LinkedEditingRangeClientCapabilitiesSerializer) { actual -> assertCapabilities(true, actual) }
    }
}
