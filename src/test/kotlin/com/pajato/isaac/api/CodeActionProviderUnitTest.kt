package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class CodeActionProviderUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a boolean code action provider object, verify a correct result`() {
        val data = BooleanCodeActionProvider(true)
        val json = """{"state":true}"""

        testEncode(json, CodeActionProviderSerializer, data)
        testDecode(json, CodeActionProviderSerializer) { actualObject ->
            assertTrue(actualObject is BooleanCodeActionProvider)
            assertEquals(true, actualObject.state)
        }
    }

    @Test fun `When serializing an options code action provider object, verify a correct result`() {
        val data = OptionsCodeActionProvider(options = CodeActionOptions(true))
        val json = """{"options":{"workDoneProgress":true}}"""

        testEncode(json, CodeActionProviderSerializer, data)
        testDecode(json, CodeActionProviderSerializer) { actualObject ->
            assertTrue(actualObject is OptionsCodeActionProvider)
            assertEquals(true, actualObject.options.workDoneProgress)
        }
    }

    @Test
    fun `When invalid JSON is used, verify an illegal state exception is thrown`() {
        val incomingJson = """{"xyzzy":true}"""

        assertFailsWith<IllegalStateException> { Json.decodeFromString(CodeActionProviderSerializer, incomingJson) }
    }
}
