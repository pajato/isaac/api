package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class CodeLensWorkspaceClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertCapabilities(
        expected: CodeLensWorkspaceClientCapabilities,
        actual: CodeLensWorkspaceClientCapabilities
    ) = assertEquals(expected.refreshSupport, actual.refreshSupport, getDiagnostic("refresh support"))

    @Test fun `When serializing a code lens workspace client capabilities options default object, verify result`() {
        val data = CodeLensWorkspaceClientCapabilities()
        val json = """{}"""

        testEncode(json, CodeLensWorkspaceClientCapabilitiesSerializer, data)
        testDecode(json, CodeLensWorkspaceClientCapabilitiesSerializer) { actual -> assertCapabilities(data, actual) }
    }

    @Test fun `When serializing a code lens workspace client capabilities options non-default object, verify result`() {
        val data = CodeLensWorkspaceClientCapabilities(true)
        val json = """{"refreshSupport":true}"""

        testEncode(json, CodeLensWorkspaceClientCapabilitiesSerializer, data)
        testDecode(json, CodeLensWorkspaceClientCapabilitiesSerializer) { actual -> assertCapabilities(data, actual) }
    }
}
