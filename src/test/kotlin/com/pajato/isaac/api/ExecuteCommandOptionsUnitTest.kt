package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals

class ExecuteCommandOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertOptions(expected: ExecuteCommandOptions, actual: ExecuteCommandOptions) {
        assertEquals(expected.workDoneProgress, actual.workDoneProgress, getDiagnostic("work done progress"))
        assertEquals(expected.commands.size, actual.commands.size, getDiagnostic("commands array size"))
        assertEquals(expected.commands[0], actual.commands[0], getDiagnostic("commands first element"))
        assertEquals(expected.commands[1], actual.commands[1], getDiagnostic("commands second element"))
    }

    @Test fun `When serializing an execute command options default object, verify result`() {

        val data = ExecuteCommandOptions(commands = listOf("s1", "s2"))
        val json = Json.encodeToString(ExecuteCommandOptionsSerializer, data)

        testEncode(json, ExecuteCommandOptionsSerializer, data)
        testDecode(json, ExecuteCommandOptionsSerializer) { actual -> assertOptions(data, actual) }
    }

    @Test fun `When serializing an execute command options non-default object, verify result`() {
        val data = ExecuteCommandOptions(true, listOf("s1", "s2"))
        val json = """{"workDoneProgress":true,"commands":["s1","s2"]}"""

        testEncode(json, ExecuteCommandOptionsSerializer, data)
        testDecode(json, ExecuteCommandOptionsSerializer) { actual -> assertOptions(data, actual) }
    }
}
