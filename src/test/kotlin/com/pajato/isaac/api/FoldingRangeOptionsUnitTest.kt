package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class FoldingRangeOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default folding range options object, verify a correct result`() {
        val data = FoldingRangeOptions()
        val json = """{}"""

        testEncode(json, FoldingRangeOptionsSerializer, data)
        testDecode(json, FoldingRangeOptionsSerializer) { actualObject ->
            assertEquals(null, actualObject.workDoneProgress)
        }
    }

    @Test fun `When serializing a non-default folding range options object, verify a correct result`() {
        val data = FoldingRangeOptions(true)
        val json = """{"workDoneProgress":true}"""

        testEncode(json, FoldingRangeOptionsSerializer, data)
        testDecode(json, FoldingRangeOptionsSerializer) { actualObject ->
            assertTrue(actualObject is IsaacFoldingRangeOptions)
            assertEquals(true, actualObject.workDoneProgress)
        }
    }
}
