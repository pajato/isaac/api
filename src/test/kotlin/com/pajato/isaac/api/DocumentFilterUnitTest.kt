package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class DocumentFilterUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a document filter default object, verify result`() {
        val data = DocumentFilter()
        val json = """{}"""

        testEncode(json, DocumentFilterSerializer, data)
        testDecode(json, DocumentFilterSerializer) { actual -> assertFilter(null, null, null, actual) }
    }

    @Test fun `When serializing a document filter non-default object, verify result`() {
        val data = DocumentFilter(language = "Kotlin", scheme = "xyzzy", pattern = "*.{ts,js}")
        val json = """{"language":"Kotlin","scheme":"xyzzy","pattern":"*.{ts,js}"}"""

        testEncode(json, DocumentFilterSerializer, data)
        testDecode(json, DocumentFilterSerializer) { actual -> assertFilter("Kotlin", "xyzzy", "*.{ts,js}", actual) }
    }

    private fun assertFilter(expLanguage: String?, expScheme: String?, expPattern: String?, actual: DocumentFilter) {
        assertEquals(expLanguage, actual.language, getDiagnostic("language"))
        assertEquals(expScheme, actual.scheme, getDiagnostic("scheme"))
        assertEquals(expPattern, actual.pattern, getDiagnostic("pattern"))
    }
}
