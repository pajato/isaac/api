package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.appName
import com.pajato.isaac.appVersion
import com.pajato.isaac.getInitializeRequestMessage
import com.pajato.isaac.getProcessIdFromFactory
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class InitializeRequestUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default initialize request message object, verify the correct result()`() {
        fun getParams() = InitializeParams(capabilities = ClientCapabilities())

        val initializeRequestId = 1
        val initializeRequestMethodName = "initialize"
        val data: RequestMessage = getInitializeRequestMessage(getParams())
        val json = """{"jsonrpc":"2.0","id":1,"method":"initialize","params":{"processId":0,"capabilities":{}}}"""

        testEncode(json, RequestMessageSerializer, data)
        testDecode(json, RequestMessageSerializer) { actualObject ->
            fun assertRequestWithoutParams(message: RequestMessage) {
                val numberId = message.id

                assertEquals(jsonrpcVersion, message.jsonrpc, "Wrong JSON RPC version!")
                assertTrue(numberId is Id.NumberId, "Wrong id type! Should be Id.NumberId")
                assertEquals(initializeRequestId, numberId.id, "Wrong initialize request id value!")
                assertEquals(initializeRequestMethodName, message.method, "Wrong initialize request method name!")
            }

            fun assertParams(params: InitializeParams) {
                assertEquals(0, params.processId, "Process id has wrong value!")
                assertEquals(null, params.clientInfo?.name, "Client name is wrong!")
                assertEquals(null, params.trace, "Trace value is wrong!")
            }

            assertRequestWithoutParams(actualObject)
            assertParams(actualObject.params as InitializeParams)
        }
    }

    @Test fun `When serializing the initialize request message object, verify the correct result()`() {
        fun getParams(processId: Int) = InitializeParams(
            workDoneToken = IntegerProgressToken(0), processId = processId,
            clientInfo = ClientInfo(appName, appVersion), capabilities = ClientCapabilities(), trace = Trace.off
        )

        val initializeRequestId = 1
        val initializeRequestMethodName = "initialize"
        val processId = getProcessIdFromFactory()
        val clientName = "Test App"
        val clientVersion = "1.0-SNAPSHOT"
        val data: RequestMessage = getInitializeRequestMessage(getParams(processId))
        val json = """{"jsonrpc":"2.0","id":$initializeRequestId,"method":"$initializeRequestMethodName",""" +
            """"params":{"workDoneToken":0,"processId":$processId,""" +
            """"clientInfo":{"name":"$clientName","version":"$clientVersion"},""" +
            """"capabilities":{},"trace":"off"}}"""

        testEncode(json, RequestMessageSerializer, data)
        testDecode(json, RequestMessageSerializer) { actualObject ->
            fun assertRequestWithoutParams(message: RequestMessage) {
                val numberId = message.id

                assertEquals(jsonrpcVersion, message.jsonrpc, "Wrong JSON RPC version!")
                assertTrue(numberId is Id.NumberId, "Wrong id type! Should be Id.NumberId")
                assertEquals(initializeRequestId, numberId.id, "Wrong initialize request id value!")
                assertEquals(initializeRequestMethodName, message.method, "Wrong initialize request method name!")
            }

            fun assertParams(params: InitializeParams) {
                val token = params.workDoneToken

                assertEquals(processId, params.processId, "Process id has wrong value!")
                assertEquals(clientName, params.clientInfo!!.name, "Client name is wrong!")
                assertEquals(Trace.off, params.trace, "Trace value is wrong!")
                assertTrue(token is IntegerProgressToken, "Token should be integer type!")
                assertEquals(0, token.intToken, "Token value is wrong!")
            }

            assertRequestWithoutParams(actualObject)
            assertParams(actualObject.params as InitializeParams)
        }
    }
}
