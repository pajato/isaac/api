package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class CompletionClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertCapabilities(expected: CompletionClientCapabilities, actual: CompletionClientCapabilities) {
        assertEquals(expected.dynamicRegistration, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
    }

    @Test fun `When serializing a completion client capabilities options default object, verify result`() {
        val data = CompletionClientCapabilities()
        val json = """{}"""

        testEncode(json, CompletionClientCapabilitiesSerializer, data)
        testDecode(json, CompletionClientCapabilitiesSerializer) { actual -> assertCapabilities(data, actual) }
    }

    @Test fun `When serializing a completion client capabilities options non-default object, verify result`() {
        val data = CompletionClientCapabilities(true)
        val json = """{"dynamicRegistration":true}"""

        testEncode(json, CompletionClientCapabilitiesSerializer, data)
        testDecode(json, CompletionClientCapabilitiesSerializer) { actual -> assertCapabilities(data, actual) }
    }
}
