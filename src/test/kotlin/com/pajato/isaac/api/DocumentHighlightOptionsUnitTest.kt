package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class DocumentHighlightOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertOptions(expected: Boolean?, actual: DocumentHighlightOptions) {
        assertEquals(expected, actual.workDoneProgress, getDiagnostic("document highlight options"))
    }

    @Test fun `When serializing a default document options object, verify result`() {
        val data = DocumentHighlightOptions()
        val json = """{}"""

        testEncode(json, DocumentHighlightOptionsSerializer, data)
        testDecode(json, DocumentHighlightOptionsSerializer) { actual -> assertOptions(null, actual) }
    }

    @Test fun `When serializing a non-default document options object, verify a correct result`() {
        val data = DocumentHighlightOptions(true)
        val json = """{"workDoneProgress":true}"""

        testEncode(json, DocumentHighlightOptionsSerializer, data)
        testDecode(json, DocumentHighlightOptionsSerializer) { actual -> assertOptions(true, actual) }
    }
}
