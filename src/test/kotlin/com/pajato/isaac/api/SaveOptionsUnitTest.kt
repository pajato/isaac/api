package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class SaveOptionsUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a save options object, verify correct results!`() {
        val actualJson = Json.encodeToString(SaveOptionsTransformingSerializer, IsaacSaveOptions(true))
        val expectedJson = """true"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a save options object, verify correct results!`() {
        val incomingJson = """true"""
        val actualObject = Json.decodeFromString(SaveOptionsTransformingSerializer, incomingJson)

        assertTrue(actualObject.includeText == true)
    }
}
