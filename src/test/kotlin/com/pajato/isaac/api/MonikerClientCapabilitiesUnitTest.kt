package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class MonikerClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertClientCapabilities(expected: Boolean?, actual: MonikerClientCapabilities) {
        assertEquals(expected, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
    }

    @Test
    fun `When serializing a default moniker client capabilities object, verify result`() {
        val data = MonikerClientCapabilities()
        val json = """{}"""

        testEncode(json, MonikerClientCapabilitiesSerializer, data)
        testDecode(json, MonikerClientCapabilitiesSerializer) { actual -> assertClientCapabilities(null, actual) }
    }

    @Test
    fun `When decoding a non-default moniker client capabilities object, verify a correct result`() {
        val data = MonikerClientCapabilities(true)
        val json = """{"dynamicRegistration":true}"""

        testEncode(json, MonikerClientCapabilitiesSerializer, data)
        testDecode(json, MonikerClientCapabilitiesSerializer) { actual -> assertClientCapabilities(true, actual) }
    }
}
