package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class SemanticTokensWorkspaceClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertCapabilities(expected: Boolean?, actual: SemanticTokensWorkspaceCC) {
        assertEquals(expected, actual.refreshSupport, getDiagnostic("refresh support"))
    }

    @Test fun `When serializing a semantic tokens workspace client capabilities default object, verify result`() {
        val data = SemanticTokensWorkspaceClientCapabilities()
        val json = """{}"""

        testEncode(json, SemanticTokensWorkspaceCCSerializer, data)
        testDecode(json, SemanticTokensWorkspaceCCSerializer) { actual -> assertCapabilities(null, actual) }
    }

    @Test fun `When decoding a semantic tokens workspace client capabilities non-default object, verify result`() {
        val data = SemanticTokensWorkspaceClientCapabilities(true)
        val json = """{"refreshSupport":true}"""

        testEncode(json, SemanticTokensWorkspaceCCSerializer, data)
        testDecode(json, SemanticTokensWorkspaceCCSerializer) { actual -> assertCapabilities(true, actual) }
    }
}
