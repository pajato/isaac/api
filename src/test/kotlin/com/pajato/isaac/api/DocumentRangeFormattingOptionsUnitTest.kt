package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DocumentRangeFormattingOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default document range formatting options object, verify the result`() {
        val data = DocumentRangeFormattingOptions()
        val json = """{}"""

        testEncode(json, DocumentRangeFormattingOptionsSerializer, data)
        testDecode(json, DocumentRangeFormattingOptionsSerializer) { actual ->
            assertEquals(null, actual.workDoneProgress)
        }
    }

    @Test fun `When encoding a document range formatting options object, verify a correct result`() {
        val data = DocumentRangeFormattingOptions(true)
        val actualJson = Json.encodeToString(DocumentRangeFormattingOptionsSerializer, data)
        val expectedJson = """{"workDoneProgress":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test fun `When decoding a document range formatting options object, verify a correct result`() {
        val incomingJson = """{"workDoneProgress":true}"""
        val actualObject = Json.decodeFromString(DocumentRangeFormattingOptionsSerializer, incomingJson)

        assertTrue(actualObject is IsaacDocumentRangeFormattingOptions)
        assertEquals(true, actualObject.workDoneProgress)
    }
}
