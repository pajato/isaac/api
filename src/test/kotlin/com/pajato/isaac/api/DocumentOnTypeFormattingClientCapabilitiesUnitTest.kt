package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class DocumentOnTypeFormattingClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {
    private fun assertCapabilities(expected: Boolean?, actual: DocumentOnTypeFormattingClientCapabilities) {
        assertEquals(expected, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
    }

    @Test fun `When serializing a default document on type formatting client capabilities object, verify result`() {
        val data = DocumentOnTypeFormattingClientCapabilities()
        val json = """{}"""

        testEncode(json, DocumentOnTypeFormattingCCSerializer, data)
        testDecode(json, DocumentOnTypeFormattingCCSerializer) { actual -> assertCapabilities(null, actual) }
    }

    @Test fun `When serializing a non-default document on type formatting client capabilities object, verify`() {
        val data = DocumentOnTypeFormattingClientCapabilities(true)
        val json = """{"dynamicRegistration":true}"""

        testEncode(json, DocumentOnTypeFormattingCCSerializer, data)
        testDecode(json, DocumentOnTypeFormattingCCSerializer) { actual -> assertCapabilities(true, actual) }
    }
}
