package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import java.lang.IllegalStateException
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class TypeDefinitionProviderUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a boolean type definition provider object, verify a correct result`() {
        val data = BooleanTypeDefinitionProvider(true)
        val json = """{"state":true}"""

        testEncode(json, TypeDefinitionProviderSerializer, data)
        testDecode(json, TypeDefinitionProviderSerializer) { actualObject ->
            assertTrue(actualObject is BooleanTypeDefinitionProvider)
            assertEquals(true, actualObject.state)
        }
    }

    @Test fun `When serializing an options type definition provider object, verify a correct result`() {
        val data = OptionsTypeDefinitionProvider(options = TypeDefinitionOptions(true))
        val json = """{"options":{"workDoneProgress":true}}"""

        testEncode(json, TypeDefinitionProviderSerializer, data)
        testDecode(json, TypeDefinitionProviderSerializer) { actualObject ->
            assertTrue(actualObject is OptionsTypeDefinitionProvider)
            assertEquals(true, actualObject.options.workDoneProgress)
        }
    }

    @Test
    fun `When serializing a registration options type definition provider object, verify a correct result`() {
        val filter = DocumentFilter("Kotlin", "scheme", "pattern")
        val options = TypeDefinitionRegistrationOptions(
            documentSelector = listOf(filter), workDoneProgress = true,
            id = "anId"
        )
        val data = RegistrationOptionsTypeDefinitionProvider(options)
        val json = """{"registrationOptions":{"documentSelector":""" +
            """[{"language":"Kotlin","scheme":"scheme","pattern":"pattern"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""

        testEncode(json, TypeDefinitionProviderSerializer, data)
        testDecode(json, TypeDefinitionProviderSerializer) { actualObject ->
            assertTrue(actualObject is RegistrationOptionsTypeDefinitionProvider)
            assertEquals(1, actualObject.registrationOptions.documentSelector!!.size)
            assertEquals("Kotlin", actualObject.registrationOptions.documentSelector!![0].language)
            assertEquals("scheme", actualObject.registrationOptions.documentSelector!![0].scheme)
            assertEquals("pattern", actualObject.registrationOptions.documentSelector!![0].pattern)
            assertEquals(true, actualObject.registrationOptions.workDoneProgress)
        }
    }

    @Test fun `When invalid JSON is used, verify an illegal state exception is thrown`() {
        val incomingJson = """{"xyzzy":true}"""

        assertFailsWith<IllegalStateException> { Json.decodeFromString(TypeDefinitionProviderSerializer, incomingJson) }
    }
}
