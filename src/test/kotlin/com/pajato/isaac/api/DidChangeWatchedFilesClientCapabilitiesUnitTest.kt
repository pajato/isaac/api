package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class DidChangeWatchedFilesClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a did change watchedFiles client capabilities default object, verify result`() {
        fun assertCapabilities(actual: DidChangeWatchedFilesClientCapabilities) {
            assertEquals(null, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
        }

        val data = DidChangeWatchedFilesClientCapabilities()
        val json = """{}"""

        testEncode(json, DidChangeWatchedFilesSerializer, data)
        testDecode(json, DidChangeWatchedFilesSerializer) { actual -> assertCapabilities(actual) }
    }

    @Test fun `When serializing a did change watchedFiles client capabilities non-default object, verify result`() {
        fun assertCapabilities(actual: DidChangeWatchedFilesClientCapabilities) {
            assertEquals(true, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
        }

        val data = DidChangeWatchedFilesClientCapabilities(
            dynamicRegistration = true
        )
        val json = """{"dynamicRegistration":true}"""

        testEncode(json, DidChangeWatchedFilesSerializer, data)
        testDecode(json, DidChangeWatchedFilesSerializer) { actual -> assertCapabilities(actual) }
    }
}
