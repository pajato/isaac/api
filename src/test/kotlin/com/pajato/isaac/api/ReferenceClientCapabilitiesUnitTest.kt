package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class ReferenceClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertClientCapabilities(expected: Boolean?, actual: ReferenceClientCapabilities) {
        assertEquals(expected, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
    }

    @Test fun `When serializing a references client capabilities default object, verify result`() {
        val data = ReferenceClientCapabilities()
        val json = """{}"""

        testEncode(json, ReferenceClientCapabilitiesSerializer, data)
        testDecode(json, ReferenceClientCapabilitiesSerializer) { actual -> assertClientCapabilities(null, actual) }
    }

    @Test fun `When decoding a references client capabilities non-default object, verify a correct result`() {
        val data = ReferenceClientCapabilities(true)
        val json = """{"dynamicRegistration":true}"""

        testEncode(json, ReferenceClientCapabilitiesSerializer, data)
        testDecode(json, ReferenceClientCapabilitiesSerializer) { actual -> assertClientCapabilities(true, actual) }
    }
}
