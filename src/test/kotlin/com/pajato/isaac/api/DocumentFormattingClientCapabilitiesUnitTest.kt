package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class DocumentFormattingClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a document filter default object, verify result`() {
        fun assertFormattingClientCapabilities(actual: DocumentFormattingClientCapabilities) {
            assertEquals(null, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
        }

        val json = """{}"""
        val data = DocumentFormattingClientCapabilities()

        testEncode(json, DocumentFormattingSerializer, data)
        testDecode(json, DocumentFormattingSerializer) { actual -> assertFormattingClientCapabilities(actual) }
    }

    @Test fun `When serializing a document filter non-default object, verify result`() {
        fun assertFormattingClientCapabilities(actual: DocumentFormattingClientCapabilities) {
            assertEquals(true, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
        }

        val data = DocumentFormattingClientCapabilities(dynamicRegistration = true)
        val json = """{"dynamicRegistration":true}"""

        testEncode(json, DocumentFormattingSerializer, data)
        testDecode(json, DocumentFormattingSerializer) { actual -> assertFormattingClientCapabilities(actual) }
    }
}
