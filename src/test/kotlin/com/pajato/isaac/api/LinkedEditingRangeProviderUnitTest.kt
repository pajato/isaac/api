package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import java.lang.IllegalStateException
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class LinkedEditingRangeProviderUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a boolean linked editing range provider object, verify a correct result`() {
        val data = BooleanLinkedEditingRangeProvider(true)
        val json = """{"state":true}"""

        testEncode(json, LinkedEditingRangeProviderSerializer, data)
        testDecode(json, LinkedEditingRangeProviderSerializer) { actualObject ->
            assertTrue(actualObject is BooleanLinkedEditingRangeProvider)
            assertEquals(true, actualObject.state)
        }
    }

    @Test fun `When serializing an options linked editing range provider object, verify a correct result`() {
        val options = LinkedEditingRangeOptions(true)
        val data = OptionsLinkedEditingRangeProvider(options = options)
        val json = """{"options":{"workDoneProgress":true}}"""

        testEncode(json, LinkedEditingRangeProviderSerializer, data)
        testDecode(json, LinkedEditingRangeProviderSerializer) { actualObject ->
            assertTrue(actualObject is OptionsLinkedEditingRangeProvider)
            assertEquals(true, actualObject.options.workDoneProgress)
        }
    }

    @Test fun `When serializing a registration options linked editing range provider object, verify result`() {
        val filter = DocumentFilter("Kotlin", "scheme", "pattern")
        val registrationOptions = LinkedEditingRangeRegistrationOptions(
            documentSelector = listOf(filter), workDoneProgress = true, id = "anId"
        )
        val data = RegistrationOptionsLinkedEditingRangeProvider(registrationOptions)
        val json = """{"registrationOptions":{"documentSelector":""" +
            """[{"language":"Kotlin","scheme":"scheme","pattern":"pattern"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""

        testEncode(json, LinkedEditingRangeProviderSerializer, data)
        testDecode(json, LinkedEditingRangeProviderSerializer) { actualObject ->
            assertTrue(actualObject is RegistrationOptionsLinkedEditingRangeProvider)
            assertEquals(1, actualObject.registrationOptions.documentSelector!!.size)
            assertEquals("Kotlin", actualObject.registrationOptions.documentSelector!![0].language)
            assertEquals("scheme", actualObject.registrationOptions.documentSelector!![0].scheme)
            assertEquals("pattern", actualObject.registrationOptions.documentSelector!![0].pattern)
            assertEquals(true, actualObject.registrationOptions.workDoneProgress)
        }
    }

    @Test
    fun `When invalid JSON is used, verify an illegal state exception is thrown`() {
        val json = """{"xyzzy":true}"""

        assertFailsWith<IllegalStateException> { Json.decodeFromString(LinkedEditingRangeProviderSerializer, json) }
    }
}
