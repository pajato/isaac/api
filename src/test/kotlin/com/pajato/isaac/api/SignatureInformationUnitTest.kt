package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class SignatureInformationUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default signature information object, verify a correct result`() {
        val data = SignatureInformation()
        val json = """{}"""

        testEncode(json, SignatureInformationSerializer, data)
        testDecode(json, SignatureInformationSerializer) { actualObject ->
            assertEquals(null, actualObject.documentationFormat, getDiagnostic("documentation format"))
            assertEquals(null, actualObject.parameterInformation, getDiagnostic("parameter information"))
            assertEquals(null, actualObject.activeParameterSupport, getDiagnostic("active parameter support"))
        }
    }
}
