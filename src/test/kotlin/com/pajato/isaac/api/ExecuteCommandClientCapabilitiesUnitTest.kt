package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class ExecuteCommandClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertCapabilities(expected: Boolean?, actual: ExecuteCommandClientCapabilities) {
        assertEquals(expected, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
    }

    @Test fun `When serializing an execute command options default object, verify result`() {
        val data = ExecuteCommandClientCapabilities()
        val json = """{}"""

        testEncode(json, ExecuteCommandClientCapabilitiesSerializer, data)
        testDecode(json, ExecuteCommandClientCapabilitiesSerializer) { actual -> assertCapabilities(null, actual) }
    }

    @Test fun `When serializing an execute command options non-default object, verify result`() {
        val data = ExecuteCommandClientCapabilities(true)
        val json = """{"dynamicRegistration":true}"""

        testEncode(json, ExecuteCommandClientCapabilitiesSerializer, data)
        testDecode(json, ExecuteCommandClientCapabilitiesSerializer) { actual -> assertCapabilities(true, actual) }
    }
}
