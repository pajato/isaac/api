package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class SelectionRangeClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertCapabilities(expected: Boolean?, actual: SelectionRangeClientCapabilities) {
        assertEquals(expected, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
    }

    @Test fun `When serializing a selection range client capabilities default object, verify result`() {
        val data = SelectionRangeClientCapabilities()
        val json = """{}"""

        testEncode(json, SelectionRangeClientCapabilitiesSerializer, data)
        testDecode(json, SelectionRangeClientCapabilitiesSerializer) { actual -> assertCapabilities(null, actual) }
    }

    @Test fun `When serializing a selection range client capabilities non-default object, verify result`() {
        val data = SelectionRangeClientCapabilities(true)
        val json = """{"dynamicRegistration":true}"""

        testEncode(json, SelectionRangeClientCapabilitiesSerializer, data)
        testDecode(json, SelectionRangeClientCapabilitiesSerializer) { actual -> assertCapabilities(true, actual) }
    }
}
