package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getShutdownRequestMessage
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ShutdownRequestUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a shutdown request message, verify the correct result()`() {
        val requestId = 2
        val requestMethodName = "shutdown"
        val data: RequestMessage = getShutdownRequestMessage(requestId)
        val json = """{"jsonrpc":"$jsonrpcVersion","id":$requestId,"method":"$requestMethodName","params":{}}"""

        testEncode(json, RequestMessageSerializer, data)
        testDecode(json, RequestMessageSerializer) { actualObject ->
            fun assertRequestWithoutParams(message: RequestMessage) {
                val numberId = message.id

                assertTrue(message is IsaacRequestMessage)
                assertEquals(jsonrpcVersion, message.jsonrpc, "Wrong JSON RPC version!")
                assertTrue(numberId is Id.NumberId, "Wrong id type! Should be Id.NumberId")
                assertEquals(requestId, numberId.id, "Wrong request id value!")
                assertEquals(requestMethodName, message.method, "Wrong request method name!")
            }

            assertRequestWithoutParams(actualObject)
        }
    }
}
