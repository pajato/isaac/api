package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class HoverOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default hover options object, verify a correct result`() {
        val data = HoverOptions()
        val json = """{}"""

        testEncode(json, HoverOptionsSerializer, data)
        testDecode(json, HoverOptionsSerializer) { actualObject ->
            assertEquals(null, actualObject.workDoneProgress)
        }
    }

    @Test fun `When serializing a non-default hover options object, verify the result`() {
        val data = HoverOptions(true)
        val json = """{"workDoneProgress":true}"""

        testEncode(json, HoverOptionsSerializer, data)
        testDecode(json, HoverOptionsSerializer) { actualObject -> assertEquals(true, actualObject.workDoneProgress) }
    }
}
