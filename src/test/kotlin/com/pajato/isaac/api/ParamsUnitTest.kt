package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ParamsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a show message params object, verify the result`() {
        val type = 3
        val message = "Some helpful message..."
        val data = ShowMessageParams(type, message)
        val json = """{"type":3,"message":"Some helpful message..."}"""

        testEncode(json, ParamsSerializer, data)
        testDecode(json, ParamsSerializer) { actualObject ->
            assertTrue(actualObject is ShowMessageParams, "The data type is wrong!")
            assertEquals(type, actualObject.type, "The type value is wrong!")
            assertEquals(message, actualObject.message, "The message value is wrong!")
        }
    }

    @Test fun `When serializing a document symbol params object, verify the result`() {
        val uri: DocumentUri = "xyzzy"
        val textDocumentId = TextDocumentIdentifier(uri)
        val data = DocumentSymbolParams(textDocument = textDocumentId)
        val json = """{"textDocument":{"uri":"xyzzy"}}"""

        testEncode(json, ParamsSerializer, data)
        testDecode(json, ParamsSerializer) { actualObject ->
            assertTrue(actualObject is DocumentSymbolParams, getDiagnostic("data type"))
            assertEquals(textDocumentId.uri, actualObject.textDocument.uri, getDiagnostic("text document id value"))
        }
    }

    @Test fun `When serializing default initialize params, verify correct behavior`() {
        val capabilities = ClientCapabilities()
        val data: Params = InitializeParams(processId = 0, capabilities = capabilities)
        val json = """{"processId":0,"capabilities":{}}"""

        testEncode(json, ParamsSerializer, data)
        testDecode(json, ParamsSerializer) { actualObject ->
            assertTrue(actualObject is InitializeParams, "The data type is wrong!")
        }
    }

    @Test fun `When serializing initialize params with an empty list of workspace folders, verify correct behavior`() {
        val capabilities = ClientCapabilities()
        val data: Params = InitializeParams(processId = 0, capabilities = capabilities, workspaceFolders = listOf())
        val json = """{"processId":0,"capabilities":{},"workspaceFolders":[]}"""

        testEncode(json, ParamsSerializer, data)
        testDecode(json, ParamsSerializer) { actualObject ->
            assertTrue(actualObject is InitializeParams, "The data type is wrong!")
        }
    }
}
