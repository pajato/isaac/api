package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class WorkDoneProgressOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default work done progress options object, verify a correct result`() {
        val data = WorkDoneProgressOptions()
        val json = """{}"""

        testEncode(json, WorkDoneProgressOptionsSerializer, data)
        testDecode(json, WorkDoneProgressOptionsSerializer) { actualObject ->
            assertEquals(null, actualObject.workDoneProgress)
        }
    }

    @Test fun `When serializing a non-default work done progress options object, verify a correct result`() {
        val data = WorkDoneProgressOptions(true)
        val json = """{"workDoneProgress":true}"""

        testEncode(json, WorkDoneProgressOptionsSerializer, data)
        testDecode(json, WorkDoneProgressOptionsSerializer) { actualObject ->
            assertEquals(true, actualObject.workDoneProgress)
        }
    }
}
