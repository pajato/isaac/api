package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class CodeLensClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertCapabilities(expected: CodeLensClientCapabilities, actual: CodeLensClientCapabilities) {
        assertEquals(expected.dynamicRegistration, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
    }

    @Test fun `When serializing a default code lens client capabilities, verify result`() {
        val data = CodeLensClientCapabilities()
        val json = """{}"""

        testEncode(json, CodeLensClientCapabilitiesSerializer, data)
        testDecode(json, CodeLensClientCapabilitiesSerializer) { actual -> assertCapabilities(data, actual) }
    }

    @Test fun `When serializing a non-default code lens client capabilities, verify result`() {
        val data = CodeLensClientCapabilities(true)
        val json = """{"dynamicRegistration":true}"""

        testEncode(json, CodeLensClientCapabilitiesSerializer, data)
        testDecode(json, CodeLensClientCapabilitiesSerializer) { actual -> assertCapabilities(data, actual) }
    }
}
