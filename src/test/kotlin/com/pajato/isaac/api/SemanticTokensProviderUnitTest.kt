package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.assertLegend
import com.pajato.isaac.assertSelectorIsEmpty
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class SemanticTokensProviderUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing an options semantic tokens provider object, verify result`() {
        val options = SemanticTokensOptions(
            workDoneProgress = true,
            legend = SemanticTokensLegend(arrayOf("type1", "type2"), arrayOf("modifier1", "modifier2"))
        )
        val data: SemanticTokensProvider = OptionsSemanticTokensProvider(options)
        val json = """{"options":{"workDoneProgress":true,"legend":{"tokenTypes":["type1","type2"],""" +
            """"tokenModifiers":["modifier1","modifier2"]}}}"""

        testEncode(json, SemanticTokensProviderSerializer, data)
        testDecode(json, SemanticTokensProviderSerializer) { actualObject ->
            assertTrue(actualObject is OptionsSemanticTokensProvider)
            assertEquals(true, actualObject.options.workDoneProgress)
            assertLegend(actualObject.options)
        }
    }

    @Test fun `When serializing a registration options semantic tokens provider object, verify result`() {

        val options = SemanticTokensRegistrationOptions(
            workDoneProgress = true,
            legend = SemanticTokensLegend(arrayOf("type1", "type2"), arrayOf("modifier1", "modifier2")),
            documentSelector = listOf(), id = "anId"
        )
        val id = "anId"
        val data = RegistrationOptionsSemanticTokensProvider(options)
        val json = """{"registrationOptions":{"documentSelector":[],"workDoneProgress":true,"legend":""" +
            """{"tokenTypes":["type1","type2"],"tokenModifiers":["modifier1","modifier2"]},"id":"$id"}}"""

        testEncode(json, SemanticTokensProviderSerializer, data)
        testDecode(json, SemanticTokensProviderSerializer) { actualObject ->
            assertTrue(actualObject is RegistrationOptionsSemanticTokensProvider)
            assertSelectorIsEmpty(actualObject.registrationOptions.documentSelector)
            assertEquals(true, actualObject.registrationOptions.workDoneProgress)
            assertLegend(actualObject.registrationOptions)
            assertEquals(id, actualObject.registrationOptions.id)
        }
    }

    @Test fun `When decoding an invalid semantic tokens provider object, verify an illegal state exception`() {
        val incomingJson = """{"invalid":true}"""
        assertFailsWith<IllegalStateException> {
            Json.decodeFromString(SemanticTokensProviderSerializer, incomingJson)
        }
    }
}
