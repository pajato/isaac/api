package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import java.lang.IllegalStateException
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class MonikerProviderUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a boolean moniker provider, verify a correct result`() {
        fun assert(actualObject: MonikerProvider, expected: Boolean) {
            assertTrue(actualObject is BooleanMonikerProvider)
            assertEquals(expected, actualObject.state)
        }

        val data: MonikerProvider = BooleanMonikerProvider(true)
        val json = """{"state":true}"""

        testEncode(json, MonikerProviderSerializer, data)
        testDecode(json, MonikerProviderSerializer) { actualObject -> assert(actualObject, true) }
    }

    @Test fun `When serializing an options moniker provider, verify a correct result`() {
        fun assert(actualObject: MonikerProvider, expected: Boolean) {
            assertTrue(actualObject is OptionsMonikerProvider)
            assertEquals(expected, actualObject.options.workDoneProgress)
        }

        val data: MonikerProvider = OptionsMonikerProvider(options = MonikerOptions(true))
        val json = """{"options":{"workDoneProgress":true}}"""

        testEncode(json, MonikerProviderSerializer, data)
        testDecode(json, MonikerProviderSerializer) { actualObject -> assert(actualObject, true) }
    }

    @Test fun `When serializing a registration options moniker provider, verify a correct result`() {
        fun assert(actualObject: MonikerProvider, expected: MonikerRegistrationOptions) {
            assertTrue(actualObject is RegistrationOptionsMonikerProvider)
            assertEquals(expected.documentSelector!!.size, actualObject.registrationOptions.documentSelector!!.size)
            assertEquals(expected.workDoneProgress, actualObject.registrationOptions.workDoneProgress)
            assertEquals(expected.id, actualObject.registrationOptions.id)
        }

        val options = MonikerRegistrationOptions(
            documentSelector = listOf(DocumentFilter("Kotlin", "scheme", "pattern")),
            workDoneProgress = true, id = "anId"
        )
        val data: MonikerProvider = RegistrationOptionsMonikerProvider(options)
        val json = """{"registrationOptions":{"documentSelector":""" +
            """[{"language":"Kotlin","scheme":"scheme","pattern":"pattern"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""

        testEncode(json, MonikerProviderSerializer, data)
        testDecode(json, MonikerProviderSerializer) { actualObject -> assert(actualObject, options) }
    }

    @Test
    fun `When invalid JSON is used, verify an illegal state exception is thrown`() {
        val incomingJson = """{"xyzzy":true}"""

        assertFailsWith<IllegalStateException> {
            Json.decodeFromString(MonikerProviderSerializer, incomingJson)
        }
    }
}
