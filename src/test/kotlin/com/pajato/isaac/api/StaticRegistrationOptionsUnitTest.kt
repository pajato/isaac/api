package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class StaticRegistrationOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default static registration object, verify a correct result`() {
        val data = StaticRegistrationOptions()
        val json = """{}"""

        testEncode(json, StaticRegistrationOptionsSerializer, data)
        testDecode(json, StaticRegistrationOptionsSerializer) { actualObject ->
            assertEquals(null, actualObject.id)
        }
    }

    @Test fun `When serializing a non-default static registration object, verify a correct result`() {
        val data = StaticRegistrationOptions("registrationId")
        val json = """{"id":"registrationId"}"""

        testEncode(json, StaticRegistrationOptionsSerializer, data)
        testDecode(json, StaticRegistrationOptionsSerializer) { actualObject ->
            assertEquals("registrationId", actualObject.id)
        }
    }
}
