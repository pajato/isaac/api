package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class DocumentRangeFormattingClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a document filter default object, verify result`() {
        val json = """{}"""
        val data = DocumentRangeFormattingClientCapabilities()

        testEncode(json, DocumentRangeFormattingClientCapabilitiesSerializer, data)
        testDecode(json, DocumentRangeFormattingClientCapabilitiesSerializer) { actual ->
            assertEquals(null, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
        }
    }

    @Test fun `When serializing a document filter non-default object, verify result`() {
        val json = """{"dynamicRegistration":true}"""
        val data = DocumentRangeFormattingClientCapabilities(
            dynamicRegistration = true,
        )

        testEncode(json, DocumentRangeFormattingClientCapabilitiesSerializer, data)
        testDecode(json, DocumentRangeFormattingClientCapabilitiesSerializer) { actual ->
            assertEquals(true, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
        }
    }
}
