package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class SymbolInformationUnitTest : SerializerTest, ApiTestProfiler() {
    private val name = "WorkspaceClientCapabilitiesTest"
    private val root = "file:///Users/pmr/IdeaProjects/edgar/features/workspace-folders/src/test/kotlin"
    private val uri: DocumentUri = "$root/com/pajato/lsp4k/WorkspaceClientCapabilitiesTest.kt"
    private val startJson = """{"line":13,"character":3}"""
    private val endJson = """{"line":49,"character":1}"""
    private val rangeJson = """{"start":$startJson,"end":$endJson}"""
    private val locationJson = """{"uri":"$uri","range":$rangeJson}"""

    private val start = Position(13, 3)
    private val end = Position(49, 1)
    private val range = Range(start, end)
    private val location = Location(uri, range)

    @Test fun `When serializing a default symbol information object, verify a correct result`() {
        val data = SymbolInformation(name, SymbolKind.Constructor, location = location)
        val json = """{"name":"$name","kind":8,"location":$locationJson}"""

        testEncode(json, SymbolInformationSerializer, data)
        testDecode(json, SymbolInformationSerializer) { actualObject ->
            val actualLocation = actualObject.location
            val actualRange = actualLocation.range

            assertEquals(name, actualObject.name, getDiagnostic("name"))
            assertEquals(SymbolKind.Constructor.ordinal, actualObject.kind, getDiagnostic("kind"))
            assertEquals(location.uri, actualLocation.uri, getDiagnostic("location uri"))
            assertEquals(range.start.line, actualRange.start.line, getDiagnostic("range start line"))
            assertEquals(range.start.character, actualRange.start.character, getDiagnostic("range start character"))
            assertEquals(range.end.line, actualRange.end.line, getDiagnostic("range end line"))
            assertEquals(range.end.character, actualRange.end.character, getDiagnostic("range end character"))
            assertEquals(null, actualObject.containerName, getDiagnostic("container name"))
        }
    }
}
