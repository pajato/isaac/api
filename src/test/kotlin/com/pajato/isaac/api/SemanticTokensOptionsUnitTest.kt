package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.assertLegend
import kotlin.test.Test
import kotlin.test.assertEquals

class SemanticTokensOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default semantic tokens object, verify the result`() {
        val legend = SemanticTokensLegend(arrayOf("type1", "type2"), arrayOf("modifier1", "modifier2"))
        val data = SemanticTokensOptions(legend = legend)
        val json = """{"legend":{"tokenTypes":["type1","type2"],"tokenModifiers":["modifier1","modifier2"]}}"""

        testEncode(json, SemanticTokensOptionsSerializer, data)
        testDecode(json, SemanticTokensOptionsSerializer) { actualObject ->
            assertEquals(null, actualObject.workDoneProgress)
            assertLegend(actualObject)
        }
    }

    @Test fun `When serializing a semantic tokens object with a progress token, verify the result`() {
        val legend = SemanticTokensLegend(arrayOf("type1", "type2"), arrayOf("modifier1", "modifier2"))
        val data = SemanticTokensOptions(workDoneProgress = true, legend = legend)
        val json = """{"workDoneProgress":true,"legend":{"tokenTypes":["type1","type2"],""" +
            """"tokenModifiers":["modifier1","modifier2"]}}"""

        testEncode(json, SemanticTokensOptionsSerializer, data)
        testDecode(json, SemanticTokensOptionsSerializer) { actualObject ->
            assertEquals(true, actualObject.workDoneProgress)
            assertLegend(actualObject)
        }
    }
}
