package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DidChangeConfigurationClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a did change configuration client capabilities default object, verify result`() {
        fun assertCapabilities(actual: DidChangeConfigurationClientCapabilities) {
            assertTrue(actual is IsaacDidChangeConfigurationClientCapabilities)
            assertEquals(null, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
        }

        val data = DidChangeConfigurationClientCapabilities()
        val json = """{}"""

        testEncode(json, DidChangeConfigurationSerializer, data)
        testDecode(json, DidChangeConfigurationSerializer) { actual -> assertCapabilities(actual) }
    }

    @Test fun `When serializing a did change configuration client capabilities non-default object, verify result`() {
        fun assertCapabilities(actual: DidChangeConfigurationClientCapabilities) {
            assertTrue(actual is IsaacDidChangeConfigurationClientCapabilities)
            assertEquals(true, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
        }

        val data = DidChangeConfigurationClientCapabilities(dynamicRegistration = true)
        val json = """{"dynamicRegistration":true}"""

        testEncode(json, DidChangeConfigurationSerializer, data)
        testDecode(json, DidChangeConfigurationSerializer) { actual -> assertCapabilities(actual) }
    }
}
