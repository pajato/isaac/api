package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class DocumentRangeFormattingProviderUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a boolean document range formatting provider object, verify a correct result`() {
        val data = BooleanDocumentRangeFormattingProvider(true)
        val json = """{"state":true}"""

        testEncode(json, DocumentRangeFormattingProviderSerializer, data)
        testDecode(json, DocumentRangeFormattingProviderSerializer) { actualObject ->
            assertTrue(actualObject is BooleanDocumentRangeFormattingProvider)
            assertEquals(true, actualObject.state)
        }
    }

    @Test fun `When serializing an options document range formatting provider object, verify a correct result`() {
        val data = OptionsDocumentRangeFormattingProvider(options = DocumentRangeFormattingOptions(true))
        val json = """{"options":{"workDoneProgress":true}}"""

        testEncode(json, DocumentRangeFormattingProviderSerializer, data)
        testDecode(json, DocumentRangeFormattingProviderSerializer) { actualObject ->
            assertTrue(actualObject is OptionsDocumentRangeFormattingProvider)
            assertEquals(true, actualObject.options.workDoneProgress)
        }
    }

    @Test fun `When invalid JSON is used, verify an illegal state exception is thrown`() {
        val json = """{"xyzzy":true}"""

        assertFailsWith<IllegalStateException> {
            Json.decodeFromString(DocumentRangeFormattingProviderSerializer, json)
        }
    }
}
