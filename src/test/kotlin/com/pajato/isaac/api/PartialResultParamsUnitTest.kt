package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class PartialResultParamsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default partial result params object, verify a correct result`() {
        val data = PartialResultParams()
        val json = """{}"""

        testEncode(json, PartialResultParamsSerializer, data)
        testDecode(json, PartialResultParamsSerializer) { actualObject ->
            assertEquals(null, actualObject.partialResultToken)
        }
    }

    @Test fun `When serializing a non-default partial result params object, verify a correct result`() {
        val token = "token"
        val data = PartialResultParams(partialResultToken = StringProgressToken(token))
        val json = """{"partialResultToken":{"stringToken":"token"}}"""

        testEncode(json, PartialResultParamsSerializer, data)
        testDecode(json, PartialResultParamsSerializer) { actualObject ->
            val partialToken = actualObject.partialResultToken

            assertTrue(partialToken is StringProgressToken)
            assertEquals(token, partialToken.stringToken)
        }
    }
}
