package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class SealedNumberUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a byte number, verify the correct result`() {
        val number: SealedNumber = ByteNumber(23.toByte())
        val actualJson = Json.encodeToString(NumberSerializer, number)
        val expectedJson = """${23.toByte()}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When serializing an byte number, verify the correct result`() {
        val byteValue = 23.toByte()
        val incomingJson = "$byteValue"
        val actualObject = Json.decodeFromString(NumberSerializer, incomingJson)
        val number: SealedNumber = ByteNumber(byteValue)

        assertEquals(number.number, actualObject.number)
        assertTrue(actualObject.isByte())
        assertTrue(actualObject.isDouble())
        assertTrue(actualObject.isFloat())
        assertTrue(actualObject.isInt())
        assertTrue(actualObject.isLong())
        assertTrue(actualObject.isShort())
    }

    @Test
    fun `When serializing a float, verify the correct result`() {
        val floatValue = 23.3f
        val number: SealedNumber = FloatNumber(floatValue)
        val expectedJson = "$floatValue"
        val actualJson = Json.encodeToString(NumberSerializer, number)
        val actualObject = Json.decodeFromString(NumberSerializer, expectedJson)

        assertEquals(expectedJson, actualJson)
        assertEquals(number.number, actualObject.number)
        assertFalse(actualObject.isByte())
        assertTrue(actualObject.isDouble())
        assertTrue(actualObject.isFloat())
        assertFalse(actualObject.isInt())
        assertFalse(actualObject.isLong())
        assertFalse(actualObject.isShort())
    }

    @Test
    fun `When serializing a double, verify the correct result`() {
        val doubleValue = 1.7976931348623158E307
        val number: SealedNumber = DoubleNumber(doubleValue)
        val expectedJson = "$doubleValue"
        val actualJson = Json.encodeToString(NumberSerializer, number)
        val actualObject = Json.decodeFromString(NumberSerializer, expectedJson)

        assertEquals(expectedJson, actualJson)
        assertEquals(number.number, actualObject.number)
        assertFalse(actualObject.isByte())
        assertTrue(actualObject.isDouble())
        assertTrue(actualObject.isFloat())
        assertFalse(actualObject.isInt())
        assertFalse(actualObject.isLong())
        assertFalse(actualObject.isShort())
    }

    @Test
    fun `When serializing an integer number, verify the correct result`() {
        val intValue = 46723
        val number: SealedNumber = IntNumber(intValue)
        val expectedJson = "$intValue"
        val actualJson = Json.encodeToString(NumberSerializer, number)
        val actualObject = Json.decodeFromString(NumberSerializer, expectedJson)

        assertEquals(expectedJson, actualJson)
        assertEquals(number.number, actualObject.number)
        assertFalse(actualObject.isByte())
        assertTrue(actualObject.isDouble())
        assertTrue(actualObject.isFloat())
        assertTrue(actualObject.isInt())
        assertTrue(actualObject.isLong())
        assertFalse(actualObject.isShort())
    }

    @Test
    fun `When serializing a long, verify the correct result`() {
        val longValue = 4677489876
        val number: SealedNumber = LongNumber(longValue)
        val expectedJson = "$longValue"
        val actualJson = Json.encodeToString(NumberSerializer, number)
        val actualObject = Json.decodeFromString(NumberSerializer, expectedJson)

        assertEquals(expectedJson, actualJson)
        assertEquals(number.number, actualObject.number)
        assertFalse(actualObject.isByte())
        assertTrue(actualObject.isDouble())
        assertTrue(actualObject.isFloat())
        assertFalse(actualObject.isInt())
        assertTrue(actualObject.isLong())
        assertFalse(actualObject.isShort())
    }

    @Test
    fun `When serializing a short, verify the correct result`() {
        val shortValue = 467.toShort()
        val number: SealedNumber = ShortNumber(shortValue)
        val expectedJson = "$shortValue"
        val actualJson = Json.encodeToString(NumberSerializer, number)
        val actualObject = Json.decodeFromString(NumberSerializer, expectedJson)

        assertEquals(expectedJson, actualJson)
        assertEquals(number.number, actualObject.number)
        assertFalse(actualObject.isByte())
        assertTrue(actualObject.isDouble())
        assertTrue(actualObject.isFloat())
        assertTrue(actualObject.isInt())
        assertTrue(actualObject.isLong())
        assertTrue(actualObject.isShort())
    }

    @Test
    fun `When the sealed number subclass properties are accesses, verify they are correct`() {
        val byteNumber = ByteNumber(58)
        assertEquals(58, byteNumber.byteValue)
        val doubleNumber = DoubleNumber(58.0)
        assertEquals(58.0, doubleNumber.doubleValue)
        val floatNumber = FloatNumber(58.0f)
        assertEquals(58.0f, floatNumber.floatValue)
        val intNumber = IntNumber(58)
        assertEquals(58, intNumber.intValue)
        val longNumber = LongNumber(58L)
        assertEquals(58, longNumber.longValue)
        val shortNumber = ShortNumber(58)
        assertEquals(58, shortNumber.shortValue)
    }
}
