package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class ShutdownParamsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test
    fun `When serializing a default shutdown request message params object, verify a correct result`() {
        val data = ShutdownParams
        val json = """{}"""

        testEncode(json, ParamsSerializer, data)
        testDecode(json, ParamsSerializer) { actualObject ->
            assertTrue(actualObject is ShutdownParams)
        }
    }

    @Test
    fun `When deserializing some invalid json, verify an illegal state exception is thrown`() {
        assertFailsWith<IllegalStateException> { Json.decodeFromString(ParamsSerializer, """{"foo":0}""") }
    }
}
