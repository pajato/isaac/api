package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class CompletionsOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default completions options object, verify a correct result`() {
        val data = CompletionOptions()
        val json = """{}"""

        testEncode(json, CompletionOptionsSerializer, data)
        testDecode(json, CompletionOptionsSerializer) { actualObject ->
            assertEquals(data.workDoneProgress, actualObject.workDoneProgress)
            assertEquals(data.triggerCharacters, actualObject.triggerCharacters)
            assertEquals(data.allCommitCharacters, actualObject.allCommitCharacters)
            assertEquals(data.resolveProvider, actualObject.resolveProvider)
        }
    }

    @Test fun `When serializing a completions options with a progress token, verify a correct result`() {
        val data = CompletionOptions(workDoneProgress = true)
        val json = """{"workDoneProgress":true}"""

        testEncode(json, CompletionOptionsSerializer, data)
        testDecode(json, CompletionOptionsSerializer) { actualObject ->
            assertEquals(data.workDoneProgress, actualObject.workDoneProgress)
            assertEquals(data.triggerCharacters, actualObject.triggerCharacters)
            assertEquals(data.allCommitCharacters, actualObject.allCommitCharacters)
            assertEquals(data.resolveProvider, actualObject.resolveProvider)
        }
    }

    @Test fun `When serializing a completions options with a trigger character and a resolve provider, verify`() {
        val data = CompletionOptions(triggerCharacters = arrayOf("."), resolveProvider = true)
        val json = """{"triggerCharacters":["."],"resolveProvider":true}"""

        testEncode(json, CompletionOptionsSerializer, data)
        testDecode(json, CompletionOptionsSerializer) { actualObject ->
            assertEquals(data.workDoneProgress, actualObject.workDoneProgress)
            assertEquals(data.triggerCharacters!!.size, actualObject.triggerCharacters!!.size)
            assertEquals(data.triggerCharacters!![0], actualObject.triggerCharacters!![0])
            assertEquals(data.allCommitCharacters, actualObject.allCommitCharacters)
            assertEquals(data.resolveProvider, actualObject.resolveProvider)
        }
    }

    @Test
    fun `When completion options are out of order, verify the correct decoding result`() {
        val outOfOrderJson = """{"resolveProvider":true,"triggerCharacters":["."]}"""
        val data = CompletionOptions(null, arrayOf("."), null, true)

        testDecode(outOfOrderJson, CompletionOptionsSerializer) { actualObject ->
            assertEquals(data.workDoneProgress, actualObject.workDoneProgress)
            assertEquals(data.triggerCharacters!!.size, actualObject.triggerCharacters!!.size)
            assertEquals(data.triggerCharacters!![0], actualObject.triggerCharacters!![0])
            assertEquals(data.allCommitCharacters, actualObject.allCommitCharacters)
            assertEquals(data.resolveProvider, actualObject.resolveProvider)
        }
    }
}
