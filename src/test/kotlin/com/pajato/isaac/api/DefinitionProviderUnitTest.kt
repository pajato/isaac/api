package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import java.lang.IllegalStateException
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class DefinitionProviderUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a boolean definition provider object, verify a correct result`() {
        val data = BooleanDefinitionProvider(true)
        val json = """{"state":true}"""

        testEncode(json, DefinitionProviderSerializer, data)
        testDecode(json, DefinitionProviderSerializer) { actualObject ->
            assertTrue(actualObject is BooleanDefinitionProvider)
            assertEquals(true, actualObject.state)
        }
    }

    @Test fun `When serializing an options definition provider object, verify a correct result`() {
        val data = OptionsDefinitionProvider(options = DefinitionOptions(true))
        val json = """{"options":{"workDoneProgress":true}}"""

        testEncode(json, DefinitionProviderSerializer, data)
        testDecode(json, DefinitionProviderSerializer) { actualObject ->
            assertTrue(actualObject is OptionsDefinitionProvider)
            assertEquals(true, actualObject.options.workDoneProgress)
        }
    }

    @Test
    fun `When invalid JSON is used, verify an illegal state exception is thrown`() {
        val json = """{"xyzzy":true}"""

        assertFailsWith<IllegalStateException> { Json.decodeFromString(DefinitionProviderSerializer, json) }
    }
}
