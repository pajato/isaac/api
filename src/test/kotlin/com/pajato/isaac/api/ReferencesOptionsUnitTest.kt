package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class ReferencesOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertOptions(expected: Boolean?, actual: ReferencesOptions) {
        assertEquals(expected, actual.workDoneProgress, getDiagnostic("work done progress"))
    }

    @Test fun `When serializing a references options default object, verify result`() {
        val data = ReferencesOptions()
        val json = """{}"""

        testEncode(json, ReferencesOptionsSerializer, data)
        testDecode(json, ReferencesOptionsSerializer) { actual -> assertOptions(null, actual) }
    }

    @Test fun `When decoding a references options non-default object, verify a correct result`() {
        val data = ReferencesOptions(true)
        val json = """{"workDoneProgress":true}"""

        testEncode(json, ReferencesOptionsSerializer, data)
        testDecode(json, ReferencesOptionsSerializer) { actual -> assertOptions(true, actual) }
    }
}
