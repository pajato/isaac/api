package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ShowMessageRequestClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default show message request client capabilities object, verify result`() {
        val data = ShowMessageRequestClientCapabilities()
        val json = """{}"""

        testEncode(json, ShowMessageRequestClientCapabilitiesSerializer, data)
        testDecode(json, ShowMessageRequestClientCapabilitiesSerializer) { actualObject ->
            assertTrue(actualObject.messageActionItem == null)
        }
    }

    @Test fun `When decoding a non-default show message request client capabilities object, verify result`() {
        val action = MessageActionItemCapability(additionalPropertiesSupport = true)
        val data = ShowMessageRequestClientCapabilities(messageActionItem = action)
        val json = """{"messageActionItem":{"additionalPropertiesSupport":true}}"""

        testEncode(json, ShowMessageRequestClientCapabilitiesSerializer, data)
        testDecode(json, ShowMessageRequestClientCapabilitiesSerializer) { actualObject ->
            val messageActionItem = actualObject.messageActionItem
            assertTrue(messageActionItem != null)
            assertEquals(true, messageActionItem.additionalPropertiesSupport, getDiagnostic("message action item"))
        }
    }
}
