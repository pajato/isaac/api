package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.assertOptions
import kotlin.test.Test

class ColorRegistrationOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertAll(expected: ColorRegistrationOptions, actual: ColorRegistrationOptions) {
        assertOptions(expected, actual)
    }

    @Test fun `When serializing a non-default color registration options object, verify correct result`() {
        val filter = DocumentFilter("Kotlin", "scheme", "pattern")
        val data = ColorRegistrationOptions(
            documentSelector = listOf(filter),
            workDoneProgress = true,
            id = "registrationId"
        )
        val json = """{"documentSelector":[{"language":"Kotlin","scheme":"scheme","pattern":"pattern"}],""" +
            """"workDoneProgress":true,"id":"registrationId"}"""

        testEncode(json, ColorRegistrationOptionsSerializer, data)
        testDecode(json, ColorRegistrationOptionsSerializer) { actual -> assertAll(data, actual) }
    }

    @Test fun `When serializing a default color registration options object, verify correct result`() {
        val data = ColorRegistrationOptions()
        val json = """{}"""

        testEncode(json, ColorRegistrationOptionsSerializer, data)
        testDecode(json, ColorRegistrationOptionsSerializer) { actual -> assertAll(data, actual) }
    }
}
