package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class ServerInfoUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a server info object, verify correct result`() {
        val data = ServerInfo("Some Name", "1.2.3")
        val json = """{"name":"Some Name","version":"1.2.3"}"""

        testEncode(json, ServerInfoSerializer, data)
        testDecode(json, ServerInfoSerializer) { actualObject ->
            assertEquals(data.name, actualObject.name)
            assertEquals(data.version, actualObject.version)
        }
    }
}
