package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class RenameOptionsUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a rename options object, verify a correct result`() {
        val actualJson = Json.encodeToString(RenameOptionsSerializer, IsaacRenameOptions(true))
        val expectedJson = """{"workDoneProgress":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a rename options object, verify a correct result`() {
        val incomingJson = """{"workDoneProgress":true}"""
        val actualObject = Json.decodeFromString(RenameOptionsSerializer, incomingJson)

        assertTrue(actualObject is IsaacRenameOptions)
        assertEquals(true, actualObject.workDoneProgress)
    }
}
