package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class SelectionRangeOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertOptions(expected: Boolean?, actual: SelectionRangeOptions) {
        assertEquals(expected, actual.workDoneProgress, getDiagnostic("work done progress"))
    }

    @Test fun `When serializing a selection range options default object, verify result`() {
        val data = SelectionRangeOptions()
        val json = """{}"""

        testEncode(json, SelectionRangeOptionsSerializer, data)
        testDecode(json, SelectionRangeOptionsSerializer) { actual -> assertOptions(null, actual) }
    }

    @Test fun `When serializing a selection range options non-default object, verify result`() {
        val data = SelectionRangeOptions(true)
        val json = """{"workDoneProgress":true}"""

        testEncode(json, SelectionRangeOptionsSerializer, data)
        testDecode(json, SelectionRangeOptionsSerializer) { actual -> assertOptions(true, actual) }
    }
}
