package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals

@Suppress("EXPERIMENTAL_IS_NOT_ENABLED")
@OptIn(kotlinx.serialization.ExperimentalSerializationApi::class)
class ErrorDataUnitTest : ApiTestProfiler() {

    private fun getErrorDataAsJson(errorData: ErrorData) = when (errorData) {
        is StringErrorData -> Json.encodeToString(errorData.data)
        // is ListErrorData -> Json.encodeToString(errorData.list)
        is NumberErrorData -> Json.encodeToString(errorData.data)
        is BooleanErrorData -> Json.encodeToString(errorData.data)
        // is ObjectErrorData -> Json.encodeToString(errorData.objectErrorData)
    }

    @Test
    fun `When encoding error data subclasses, verify a correct result`() {
        assertEquals(""""testData"""", getErrorDataAsJson(StringErrorData("testData")))
        // assertEquals("""["testData"]""", getErrorDataAsJson(ListErrorData(listOf("testData"))))
        assertEquals("""23""", getErrorDataAsJson(NumberErrorData(23)))
        assertEquals("""true""", getErrorDataAsJson(BooleanErrorData(true)))
        // assertEquals(""""testData"""", getErrorDataAsJson(ObjectErrorData("testData")))
    }

    @Test
    fun `When decoding error data subclasses, verify a correct result`() {
        assertEquals("testData", Json.decodeFromString<StringErrorData>("""{"data":"testData"}""").data)
        // assertEquals("""["testData"]""", getErrorDataAsJson(ListErrorData(listOf("testData"))))
        assertEquals(23, Json.decodeFromString<NumberErrorData>("""{"data":23}""").data)
        assertEquals(true, Json.decodeFromString<BooleanErrorData>("""{"data":true}""").data)
        // assertEquals(""""testData"""", getErrorDataAsJson(ObjectErrorData("testData")))
    }
}
