package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.assertOptions
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertTrue

class DeclarationRegistrationOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertAll(expected: DeclarationRegistrationOptions, actual: DeclarationRegistrationOptions) {
        assertTrue(actual is IsaacDeclarationRegistrationOptions, getDiagnostic("type"))
        assertOptions(expected, actual)
    }

    @Test fun `When serializing a non-default declaration registration options object, verify a correct result`() {
        val filter = DocumentFilter("Kotlin", "scheme", "pattern")
        val data = DeclarationRegistrationOptions(
            documentSelector = listOf(filter), workDoneProgress = true, id = "registrationId"
        )
        val json = """{"documentSelector":[{"language":"Kotlin","scheme":"scheme","pattern":"pattern"}],""" +
            """"workDoneProgress":true,"id":"registrationId"}"""

        testEncode(json, DeclarationRegistrationOptionsSerializer, data)
        testDecode(json, DeclarationRegistrationOptionsSerializer) { actual -> assertAll(data, actual) }
    }

    @Test fun `When serializing a default declaration registration options object, verify a correct result`() {
        val data = DeclarationRegistrationOptions()
        val json = """{}"""

        testEncode(json, DeclarationRegistrationOptionsSerializer, data)
        testDecode(json, DeclarationRegistrationOptionsSerializer) { actual -> assertAll(data, actual) }
    }
}
