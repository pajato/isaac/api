package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DefinitionOptionsUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a definition options object, verify a correct result`() {
        val actualJson = Json.encodeToString(ContentDefinitionOptionsSerializer, IsaacDefinitionOptions(true))
        val expectedJson = """{"workDoneProgress":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a definition options object, verify a correct result`() {
        val incomingJson = """{"workDoneProgress":true}"""
        val actualObject = Json.decodeFromString(ContentDefinitionOptionsSerializer, incomingJson)

        assertTrue(actualObject is IsaacDefinitionOptions)
        assertEquals(true, actualObject.workDoneProgress)
    }
}
