package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.clientErrorTag
import com.pajato.isaac.clientInfoTag
import com.pajato.isaac.clientNotificationTag
import com.pajato.isaac.clientRequestTag
import com.pajato.isaac.clientResponseTag
import com.pajato.isaac.serverErrorTag
import com.pajato.isaac.serverInfoTag
import com.pajato.isaac.serverNotificationTag
import com.pajato.isaac.serverResponseTag
import kotlin.test.Test
import kotlin.test.assertTrue

class UtilsUnitTest : ApiTestProfiler() {

    @Test fun `Ensure that all Log category types are referenced`() {
        assertTrue(clientErrorTag.isNotEmpty())
        assertTrue(clientInfoTag.isNotEmpty())
        assertTrue(clientNotificationTag.isNotEmpty())
        assertTrue(clientRequestTag.isNotEmpty())
        assertTrue(clientResponseTag.isNotEmpty())
        assertTrue(serverResponseTag.isNotEmpty())
        assertTrue(serverErrorTag.isNotEmpty())
        assertTrue(serverInfoTag.isNotEmpty())
        assertTrue(serverNotificationTag.isNotEmpty())
    }
}
