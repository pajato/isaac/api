package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class DocumentHighlightClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default document client capabilities, verify result`() {
        val json = """{}"""
        val data = DocumentHighlightClientCapabilities()

        testEncode(json, DocumentHighlightSerializer, data)
        testDecode(json, DocumentHighlightSerializer) { actual ->
            assertEquals(null, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
        }
    }

    @Test fun `When serializing a document client capabilities with a dynamic registration object, verify result`() {
        val json = """{"dynamicRegistration":true}"""
        val data = DocumentHighlightClientCapabilities(dynamicRegistration = true)

        testEncode(json, DocumentHighlightSerializer, data)
        testDecode(json, DocumentHighlightSerializer) { actual ->
            assertEquals(true, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
        }
    }
}
