package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class DocumentLinkOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertOptions(expected: DocumentLinkOptions, actual: DocumentLinkOptions) {
        assertEquals(expected.workDoneProgress, actual.workDoneProgress, getDiagnostic("work done progress"))
        assertEquals(expected.resolveProvider, actual.resolveProvider, getDiagnostic("resolve provider"))
    }

    @Test fun `When serializing a document link options default object, verify result`() {
        val data = DocumentLinkOptions()
        val json = """{}"""

        testEncode(json, DocumentLinkOptionsSerializer, data)
        testDecode(json, DocumentLinkOptionsSerializer) { actual -> assertOptions(data, actual) }
    }

    @Test
    fun `When decoding a document link options object, verify a correct result`() {
        val data = DocumentLinkOptions(workDoneProgress = true, resolveProvider = true)
        val json = """{"workDoneProgress":true,"resolveProvider":true}"""

        testEncode(json, DocumentLinkOptionsSerializer, data)
        testDecode(json, DocumentLinkOptionsSerializer) { actual -> assertOptions(data, actual) }
    }
}
