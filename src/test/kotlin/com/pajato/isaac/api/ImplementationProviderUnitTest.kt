package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import java.lang.IllegalStateException
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class ImplementationProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a boolean implementation provider object, verify a correct result`() {
        val provider: ImplementationProvider = BooleanImplementationProvider(true)
        val actualJson = Json.encodeToString(ImplementationProviderSerializer, provider)
        val expectedJson = """{"state":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a boolean implementation provider object, verify a correct result`() {
        val incomingJson = """{"state":true}"""
        val actualObject = Json.decodeFromString(ImplementationProviderSerializer, incomingJson)

        assertTrue(actualObject is BooleanImplementationProvider)
        assertEquals(true, actualObject.state)
    }

    @Test
    fun `When encoding an options implementation provider object, verify a correct result`() {
        val provider: ImplementationProvider = OptionsImplementationProvider(options = ImplementationOptions(true))
        val actualJson = Json.encodeToString(ImplementationProviderSerializer, provider)
        val expectedJson = """{"options":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding an options implementation provider object, verify a correct result`() {
        val incomingJson = """{"options":{"workDoneProgress":true}}"""
        val actualObject = Json.decodeFromString(ImplementationProviderSerializer, incomingJson)

        assertTrue(actualObject is OptionsImplementationProvider)
        assertEquals(true, actualObject.options.workDoneProgress)
    }

    @Test
    fun `When encoding a registration options implementation provider object, verify a correct result`() {
        val registrationOptions = ImplementationRegistrationOptions(
            documentSelector = listOf(IsaacDocumentFilter("Kotlin", "scheme", "pattern")), workDoneProgress = true,
            id = "anId",
        )
        val provider: ImplementationProvider = RegistrationOptionsImplementationProvider(registrationOptions)
        val actualJson = Json.encodeToString(ImplementationProviderSerializer, provider)
        val expectedJson = """{"registrationOptions":{"documentSelector":""" +
            """[{"language":"Kotlin","scheme":"scheme","pattern":"pattern"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a registration options implementation provider object, verify a correct result`() {
        val incomingJson = """{"registrationOptions":{"documentSelector":""" +
            """[{"language":"Kotlin","scheme":"scheme","pattern":"pattern"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""
        val actualObject = Json.decodeFromString(ImplementationProviderSerializer, incomingJson)

        assertTrue(actualObject is RegistrationOptionsImplementationProvider)
        assertTrue(actualObject.registrationOptions is IsaacImplementationRegistrationOptions)
        assertEquals(1, actualObject.registrationOptions.documentSelector!!.size)
        assertEquals("Kotlin", actualObject.registrationOptions.documentSelector!![0].language)
        assertEquals("scheme", actualObject.registrationOptions.documentSelector!![0].scheme)
        assertEquals("pattern", actualObject.registrationOptions.documentSelector!![0].pattern)
        assertEquals(true, actualObject.registrationOptions.workDoneProgress)
    }

    @Test
    fun `When invalid JSON is used, verify an illegal state exception is thrown`() {
        val incomingJson = """{"xyzzy":true}"""

        assertFailsWith<IllegalStateException> {
            Json.decodeFromString(ImplementationProviderSerializer, incomingJson)
        }
    }
}
