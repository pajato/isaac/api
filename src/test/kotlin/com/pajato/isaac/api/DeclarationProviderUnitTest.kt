package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import java.lang.IllegalStateException
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class DeclarationProviderUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a boolean declaration provider object, verify a correct result`() {
        val data = BooleanDeclarationProvider(true)
        val json = """{"state":true}"""

        testEncode(json, DeclarationProviderSerializer, data)
        testDecode(json, DeclarationProviderSerializer) { actualObject ->
            assertTrue(actualObject is BooleanDeclarationProvider)
            assertEquals(true, actualObject.state)
        }
    }

    @Test fun `When serializing an options declaration provider object, verify a correct result`() {
        val data = OptionsDeclarationProvider(options = DeclarationOptions(true))
        val json = """{"options":{"workDoneProgress":true}}"""

        testEncode(json, DeclarationProviderSerializer, data)
        testDecode(json, DeclarationProviderSerializer) { actualObject ->
            assertTrue(actualObject is OptionsDeclarationProvider)
            assertEquals(true, actualObject.options.workDoneProgress)
        }
    }

    @Test fun `When serializing a registration options declaration provider object, verify a correct result`() {
        val filter = DocumentFilter("Kotlin", "scheme", "pattern")
        val options = DeclarationRegistrationOptions(
            documentSelector = listOf(filter), workDoneProgress = true, id = "anId"
        )
        val data = RegistrationOptionsDeclarationProvider(registrationOptions = options)
        val json = """{"registrationOptions":{"documentSelector":""" +
            """[{"language":"Kotlin","scheme":"scheme","pattern":"pattern"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""

        testEncode(json, DeclarationProviderSerializer, data)
        testDecode(json, DeclarationProviderSerializer) { actualObject ->
            assertTrue(actualObject is RegistrationOptionsDeclarationProvider)
            assertEquals(1, actualObject.registrationOptions.documentSelector!!.size)
            assertEquals("Kotlin", actualObject.registrationOptions.documentSelector!![0].language)
            assertEquals("scheme", actualObject.registrationOptions.documentSelector!![0].scheme)
            assertEquals("pattern", actualObject.registrationOptions.documentSelector!![0].pattern)
            assertEquals(true, actualObject.registrationOptions.workDoneProgress)
        }
    }

    @Test fun `When invalid JSON is used, verify an illegal state exception is thrown`() {
        val incomingJson = """{"xyzzy":true}"""

        assertFailsWith<IllegalStateException> { Json.decodeFromString(DeclarationProviderSerializer, incomingJson) }
    }
}
