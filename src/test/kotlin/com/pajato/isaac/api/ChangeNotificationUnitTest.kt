package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals

class ChangeNotificationUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a boolean change notification, verify correct result`() {
        val data: ChangeNotifications = BooleanChangeNotifications(true)
        val actualJson = Json.encodeToString(ChangeNotificationsTransformingSerializer, data)
        val expectedJson = """true"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a boolean change notification, verify correct result`() {
        val incomingJson = """true"""
        val actualObject = Json.decodeFromString(ChangeNotificationsTransformingSerializer, incomingJson)

        assertEquals(true, (actualObject as BooleanChangeNotifications).flag)
    }

    @Test
    fun `When encoding a string change notification, verify correct result`() {
        val id = "37AB9"
        val actualJson = Json.encodeToString(ChangeNotificationsTransformingSerializer, StringChangeNotifications(id))
        val expectedJson = """"$id""""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When  decoding a string change notification, verify correct result`() {
        val id = "37AB9"
        val incomingJson = """"$id""""
        val decodedObject = Json.decodeFromString(ChangeNotificationsTransformingSerializer, incomingJson)

        assertEquals(id, (decodedObject as StringChangeNotifications).id)
    }
}
