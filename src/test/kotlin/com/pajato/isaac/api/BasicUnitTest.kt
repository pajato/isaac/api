package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class BasicUnitTest : ApiTestProfiler() {

    @Test
    fun `Exercise the Trace values`() {
        val off = Trace.off
        val messages = Trace.messages
        val verbose = Trace.verbose
        assertEquals(off, Trace.valueOf("off"))
        assertEquals(messages, Trace.valueOf("messages"))
        assertEquals(verbose, Trace.valueOf("verbose"))
    }

    @Test
    fun `Exercise the SymbolKind values`() {
        var expected = 0
        assertEquals(expected++, SymbolKind.File.ordinal)
        assertEquals(expected++, SymbolKind.Module.ordinal)
        assertEquals(expected++, SymbolKind.Namespace.ordinal)
        assertEquals(expected++, SymbolKind.Package.ordinal)
        assertEquals(expected++, SymbolKind.Class.ordinal)
        assertEquals(expected++, SymbolKind.Method.ordinal)
        assertEquals(expected++, SymbolKind.Property.ordinal)
        assertEquals(expected++, SymbolKind.Field.ordinal)
        assertEquals(expected++, SymbolKind.Constructor.ordinal)
        assertEquals(expected++, SymbolKind.Enum1.ordinal)
        assertEquals(expected++, SymbolKind.Interface.ordinal)
        assertEquals(expected++, SymbolKind.Function.ordinal)
        assertEquals(expected++, SymbolKind.Variable.ordinal)
        assertEquals(expected++, SymbolKind.Constant.ordinal)
        assertEquals(expected++, SymbolKind.String.ordinal)
        assertEquals(expected++, SymbolKind.Number.ordinal)
        assertEquals(expected++, SymbolKind.Boolean.ordinal)
        assertEquals(expected++, SymbolKind.Array.ordinal)
        assertEquals(expected++, SymbolKind.Object.ordinal)
        assertEquals(expected++, SymbolKind.Key.ordinal)
        assertEquals(expected++, SymbolKind.Null.ordinal)
        assertEquals(expected++, SymbolKind.EnumMember.ordinal)
        assertEquals(expected++, SymbolKind.Struct.ordinal)
        assertEquals(expected++, SymbolKind.Event.ordinal)
        assertEquals(expected++, SymbolKind.Operator.ordinal)
        assertEquals(expected, SymbolKind.TypeParameter.ordinal)
    }
}
