package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class FileOperationsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default file operations object, verify the result`() {
        val data = FileOperations()
        val json = """{}"""

        testEncode(json, FileOperationsSerializer, data)
        testDecode(json, FileOperationsSerializer) { actualObject ->
            assertTrue(actualObject is IsaacFileOperations)
            assertEquals(null, actualObject.didCreate)
            assertEquals(null, actualObject.didDelete)
            assertEquals(null, actualObject.didRename)
            assertEquals(null, actualObject.dynamicRegistration)
            assertEquals(null, actualObject.willCreate)
            assertEquals(null, actualObject.willDelete)
            assertEquals(null, actualObject.willRename)
        }
    }

    @Test
    fun `When serializing a file operations object with all properties, verify the result`() {
        val data = FileOperations(
            dynamicRegistration = true, didCreate = true, willCreate = true, didRename = true, willRename = true,
            didDelete = true, willDelete = true
        )
        val json = """{"dynamicRegistration":true,"didCreate":true,"willCreate":true,"didRename":true,""" +
            """"willRename":true,"didDelete":true,"willDelete":true}"""

        testEncode(json, FileOperationsSerializer, data)
        testDecode(json, FileOperationsSerializer) {
            assertEquals(true, it.didCreate)
            assertEquals(true, it.didDelete)
            assertEquals(true, it.didRename)
            assertEquals(true, it.dynamicRegistration)
            assertEquals(true, it.willCreate)
            assertEquals(true, it.willDelete)
            assertEquals(true, it.willRename)
        }
    }
}
