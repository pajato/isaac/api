package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ColorOptionsUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a color options object, verify a correct result`() {
        val actualJson = Json.encodeToString(ColorOptionsSerializer, IsaacColorOptions(true))
        val expectedJson = """{"workDoneProgress":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a color options object, verify a correct result`() {
        val incomingJson = """{"workDoneProgress":true}"""
        val actualObject = Json.decodeFromString(ColorOptionsSerializer, incomingJson)

        assertTrue(actualObject is IsaacColorOptions)
        assertEquals(true, actualObject.workDoneProgress)
    }
}
