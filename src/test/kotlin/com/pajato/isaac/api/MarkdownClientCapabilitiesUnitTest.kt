package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class MarkdownClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {
    private fun assertCapabilities(expected: MarkdownClientCapabilities, actual: MarkdownClientCapabilities) {
        assertEquals(expected.parser, actual.parser, getDiagnostic("parser"))
        assertEquals(expected.version, actual.version, getDiagnostic("version"))
    }

    @Test
    fun `When serializing a document on type formatting client capabilities default object, verify the result`() {
        val data = MarkdownClientCapabilities("aParser")
        val json = """{"parser":"aParser"}"""

        testEncode(json, MarkdownClientCapabilitiesSerializer, data)
        testDecode(json, MarkdownClientCapabilitiesSerializer) { actual -> assertCapabilities(data, actual) }
    }

    @Test
    fun `When serializing a document on type formatting client capabilities non-default object, verify the result`() {
        val data = MarkdownClientCapabilities("aParser", "1.0.0")
        val json = """{"parser":"aParser","version":"1.0.0"}"""

        testEncode(json, MarkdownClientCapabilitiesSerializer, data)
        testDecode(json, MarkdownClientCapabilitiesSerializer) { actual -> assertCapabilities(data, actual) }
    }
}
