package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.assertTokenModifiers
import com.pajato.isaac.assertTokenTypes
import kotlin.test.Test

class SemanticTokensLegendUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a semantic tokens legend object, verify a correct result`() {
        val types = arrayOf("type1", "type2")
        val modifiers = arrayOf("modifier1", "modifier2")
        val data = SemanticTokensLegend(tokenModifiers = modifiers, tokenTypes = types)
        val json = """{"tokenTypes":["type1","type2"],"tokenModifiers":["modifier1","modifier2"]}"""

        testEncode(json, SemanticTokensLegendSerializer, data)
        testDecode(json, SemanticTokensLegendSerializer) { actualObject ->
            assertTokenTypes(actualObject.tokenTypes)
            assertTokenModifiers(actualObject.tokenModifiers)
        }
    }
}
