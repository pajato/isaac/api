package com.pajato.isaac.api

import kotlin.test.Test
import kotlin.test.assertEquals

class LocationUnitTest {

    @Test fun `When a default location is created, verify a correct result`() {
        val actual = Location("", Range())
        val expectedUri = ""
        val expected = 0

        assertEquals(expectedUri, actual.uri)
        assertEquals(expected, actual.range.start.line)
        assertEquals(expected, actual.range.start.character)
        assertEquals(expected, actual.range.end.line)
        assertEquals(expected, actual.range.end.character)
    }
}
