package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class CallHierarchyClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {
    private fun assertCapabilities(expected: Boolean?, actual: CallHierarchyClientCapabilities) {
        assertTrue(actual is IsaacCallHierarchyClientCapabilities, getDiagnostic("type"))
        assertEquals(expected, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
    }

    @Test fun `When serializing a call hierarchy client capabilities default object, verify result`() {
        val data = CallHierarchyClientCapabilities()
        val json = """{}"""

        testEncode(json, CallHierarchyClientCapabilitiesSerializer, data)
        testDecode(json, CallHierarchyClientCapabilitiesSerializer) { actual -> assertCapabilities(null, actual) }
    }

    @Test fun `When serializing a call hierarchy configuration client capabilities non-default object, verify`() {
        val data = CallHierarchyClientCapabilities(dynamicRegistration = true)
        val json = """{"dynamicRegistration":true}"""

        testEncode(json, CallHierarchyClientCapabilitiesSerializer, data)
        testDecode(json, CallHierarchyClientCapabilitiesSerializer) { actual -> assertCapabilities(true, actual) }
    }
}
