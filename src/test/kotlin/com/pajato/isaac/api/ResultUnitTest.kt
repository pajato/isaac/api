package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.appName
import com.pajato.isaac.appVersion
import com.pajato.isaac.getNormalCapabilities
import com.pajato.isaac.getServerInfo
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class ResultUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When an invalid result is serialized, verify an illegal state exception is thrown`() {
        assertFailsWith<IllegalStateException> { Json.decodeFromString(ResultTransformingSerializer, "xyzzy") }
    }

    @Test fun `When serializing an initialize object, verify`() {
        val data: Result = InitializeResult(getNormalCapabilities(), getServerInfo()) as Result
        val json = """{"capabilities":{""" +
            """"textDocumentSync":1,""" +
            """"completionProvider":{"triggerCharacters":["."],"resolveProvider":false},""" +
            """"hoverProvider":true,""" +
            """"signatureHelpProvider":{"workDoneProgress":true,"triggerCharacters":["(",","],""" +
            """"retriggerCharacters":["-"]},""" +
            """"definitionProvider":true,""" +
            """"referencesProvider":true,""" +
            """"documentSymbolProvider":true,""" +
            """"codeActionProvider":true,""" +
            """"documentFormattingProvider":true,""" +
            """"documentRangeFormattingProvider":true,""" +
            """"executeCommandProvider":{"commands":["convertJavaToKotlin"]},""" +
            """"workspaceSymbolProvider":true,""" +
            """"workspace":{"workspaceFolders":{"supported":true,"changeNotifications":true}}},""" +
            """"serverInfo":{"name":"$appName","version":"$appVersion"}}"""

        testEncode(json, ResultTransformingSerializer, data)
        testDecode(json, ResultTransformingSerializer) { actualObject ->
            assertTrue(actualObject is InitializeResult)
            assertEquals(appName, actualObject.serverInfo!!.name)
        }
    }
}
