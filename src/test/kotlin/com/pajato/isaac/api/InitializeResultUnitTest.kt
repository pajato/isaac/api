package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.TextDocumentSyncKind.Full
import com.pajato.isaac.appName
import com.pajato.isaac.appVersion
import com.pajato.isaac.getNormalCapabilities
import com.pajato.isaac.getServerInfo
import kotlinx.serialization.SerializationException
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

@Suppress("EXPERIMENTAL_IS_NOT_ENABLED")
@OptIn(kotlinx.serialization.ExperimentalSerializationApi::class)
class InitializeResultUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When decoding an empty initialize result, verify a serialization exception is thrown`() {
        val jsonResult = ""

        assertFailsWith<SerializationException> { Json.decodeFromString(jsonResult) }
    }

    @Test fun `When decoding junk text, verify a serialization exception is thrown`() {
        val jsonResult = "some xyz type junk!"

        assertFailsWith<SerializationException> { Json.decodeFromString(jsonResult) }
    }

    @Test fun `When serializing a default initialize result object, verify a correct result`() {
        val data = InitializeResult(ServerCapabilities())
        val json = """{"capabilities":{}}"""

        testEncode(json, InitializeResultSerializer, data)
        testDecode(json, InitializeResultSerializer) { actualObject ->
            assertEquals(null, actualObject.capabilities.textDocumentSync)
            assertEquals(null, actualObject.capabilities.completionProvider)
            assertEquals(null, actualObject.capabilities.hoverProvider)
            assertEquals(null, actualObject.capabilities.signatureHelpProvider)
            assertEquals(null, actualObject.capabilities.definitionProvider)
            assertEquals(null, actualObject.capabilities.referencesProvider)
            assertEquals(null, actualObject.capabilities.documentSymbolProvider)
            assertEquals(null, actualObject.capabilities.codeActionProvider)
            assertEquals(null, actualObject.capabilities.documentFormattingProvider)
            assertEquals(null, actualObject.capabilities.documentRangeFormattingProvider)
            assertEquals(null, actualObject.capabilities.executeCommandProvider)
            assertEquals(null, actualObject.capabilities.workspaceSymbolProvider)
            assertEquals(null, actualObject.capabilities.workspace)
            assertEquals(null, actualObject.serverInfo)
        }
    }

    @Test fun `When serializing a non-default initialize result, verify a correct result`() {
        fun assertTextDocumentSync(capabilities: ServerCapabilities) {
            val actualTextDocumentSync = capabilities.textDocumentSync

            assertTrue(actualTextDocumentSync is KindTextDocumentSync)
            assertEquals(Full, actualTextDocumentSync.kind)
        }

        fun assertCompletionProvider(capabilities: ServerCapabilities) {
            val actualCompletionProvider = capabilities.completionProvider

            assertTrue(actualCompletionProvider is IsaacCompletionOptions)
            assertEquals(false, actualCompletionProvider.resolveProvider)
            assertEquals(1, actualCompletionProvider.triggerCharacters!!.size)
            assertEquals(".", actualCompletionProvider.triggerCharacters!![0])
        }

        fun assertHoverProvider(capabilities: ServerCapabilities) {
            val actualHoverProvider = capabilities.hoverProvider

            assertTrue(actualHoverProvider is BooleanHoverProvider)
            assertEquals(true, actualHoverProvider.data)
        }

        fun assertSignatureHelpProvider(capabilities: ServerCapabilities) {
            val actualSignatureHelpProvider = capabilities.signatureHelpProvider

            assertTrue(actualSignatureHelpProvider is IsaacSignatureHelpOptions)
            assertEquals(true, actualSignatureHelpProvider.workDoneProgress)
            assertEquals(2, actualSignatureHelpProvider.triggerCharacters!!.size)
            assertEquals("(", actualSignatureHelpProvider.triggerCharacters!![0])
            assertEquals(",", actualSignatureHelpProvider.triggerCharacters!![1])
            assertEquals(1, actualSignatureHelpProvider.retriggerCharacters!!.size)
            assertEquals("-", actualSignatureHelpProvider.retriggerCharacters!![0])
        }

        fun assertDefinitionProvider(capabilities: ServerCapabilities) {
            val actualDefinitionProvider = capabilities.definitionProvider

            assertTrue(actualDefinitionProvider is BooleanDefinitionProvider)
            assertEquals(true, actualDefinitionProvider.state)
        }

        fun assertReferencesProvider(capabilities: ServerCapabilities) {
            val actualReferencesProvider = capabilities.referencesProvider

            assertTrue(actualReferencesProvider is BooleanReferencesProvider)
            assertEquals(true, actualReferencesProvider.state)
        }

        fun assertDocumentSymbolProvider(capabilities: ServerCapabilities) {
            val actualDocumentSymbolProvider = capabilities.documentSymbolProvider

            assertTrue(actualDocumentSymbolProvider is BooleanDocumentSymbolProvider)
            assertEquals(true, actualDocumentSymbolProvider.state)
        }

        fun assertCodeActionProvider(capabilities: ServerCapabilities) {
            val actualCodeActionProvider = capabilities.codeActionProvider

            assertTrue(actualCodeActionProvider is BooleanCodeActionProvider)
            assertEquals(true, actualCodeActionProvider.state)
        }

        fun assertDocumentFormattingProvider(capabilities: ServerCapabilities) {
            val actualDocumentFormattingProvider = capabilities.documentFormattingProvider

            assertTrue(actualDocumentFormattingProvider is BooleanDocumentFormattingProvider)
            assertEquals(true, actualDocumentFormattingProvider.state)
        }

        fun assertDocumentRangeFormattingProvider(capabilities: ServerCapabilities) {
            val actualDocumentRangeFormattingProvider = capabilities.documentRangeFormattingProvider

            assertTrue(actualDocumentRangeFormattingProvider is BooleanDocumentRangeFormattingProvider)
            assertEquals(true, actualDocumentRangeFormattingProvider.state)
        }

        fun assertExecuteCommandProvider(capabilities: ServerCapabilities) {
            val actualExecuteCommandProvider = capabilities.executeCommandProvider

            assertTrue(actualExecuteCommandProvider is IsaacExecuteCommandOptions)
            assertEquals(1, actualExecuteCommandProvider.commands.size)
            assertEquals("convertJavaToKotlin", actualExecuteCommandProvider.commands[0])
        }

        fun assertWorkspaceSymbolProvider(capabilities: ServerCapabilities) {
            val actualWorkspaceSymbolProvider = capabilities.workspaceSymbolProvider

            assertTrue(actualWorkspaceSymbolProvider is BooleanWorkspaceSymbolProvider)
            assertEquals(true, actualWorkspaceSymbolProvider.state)
        }

        fun assertWorkspace(capabilities: ServerCapabilities) {
            val actualWorkspace = capabilities.workspace

            assertTrue(actualWorkspace is Workspace)
            assertEquals(true, actualWorkspace.workspaceFolders!!.supported)
        }

        val data = InitializeResult(getNormalCapabilities(), getServerInfo())
        val json = """{"capabilities":{""" +
            """"textDocumentSync":1,""" +
            """"completionProvider":{"triggerCharacters":["."],"resolveProvider":false},""" +
            """"hoverProvider":true,""" +
            """"signatureHelpProvider":{"workDoneProgress":true,"triggerCharacters":["(",","],""" +
            """"retriggerCharacters":["-"]},""" +
            """"definitionProvider":true,""" +
            """"referencesProvider":true,""" +
            """"documentSymbolProvider":true,""" +
            """"codeActionProvider":true,""" +
            """"documentFormattingProvider":true,""" +
            """"documentRangeFormattingProvider":true,""" +
            """"executeCommandProvider":{"commands":["convertJavaToKotlin"]},""" +
            """"workspaceSymbolProvider":true,""" +
            """"workspace":{"workspaceFolders":{"supported":true,"changeNotifications":true}}},""" +
            """"serverInfo":{"name":"$appName","version":"$appVersion"}}"""

        testEncode(json, InitializeResultSerializer, data)
        testDecode(json, InitializeResultSerializer) { actualObject ->
            assertTextDocumentSync(actualObject.capabilities)
            assertCompletionProvider(actualObject.capabilities)
            assertHoverProvider(actualObject.capabilities)
            assertSignatureHelpProvider(actualObject.capabilities)
            assertDefinitionProvider(actualObject.capabilities)
            assertReferencesProvider(actualObject.capabilities)
            assertDocumentSymbolProvider(actualObject.capabilities)
            assertCodeActionProvider(actualObject.capabilities)
            assertDocumentFormattingProvider(actualObject.capabilities)
            assertDocumentRangeFormattingProvider(actualObject.capabilities)
            assertExecuteCommandProvider(actualObject.capabilities)
            assertWorkspaceSymbolProvider(actualObject.capabilities)
            assertWorkspace(actualObject.capabilities)

            assertEquals(appName, actualObject.serverInfo!!.name)
            assertEquals(appVersion, actualObject.serverInfo!!.version)
        }
    }
}
