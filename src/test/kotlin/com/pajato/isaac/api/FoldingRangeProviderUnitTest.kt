package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import java.lang.IllegalStateException
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class FoldingRangeProviderUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a boolean folding range provider object, verify a correct result`() {
        val data = BooleanFoldingRangeProvider(true)
        val json = """{"state":true}"""

        testEncode(json, FoldingRangeProviderSerializer, data)
        testDecode(json, FoldingRangeProviderSerializer) { actualObject ->
            assertTrue(actualObject is BooleanFoldingRangeProvider)
            assertEquals(true, actualObject.state)
        }
    }

    @Test fun `When serializing an options folding range provider object, verify a correct result`() {
        val data = OptionsFoldingRangeProvider(options = FoldingRangeOptions(true))
        val json = """{"options":{"workDoneProgress":true}}"""

        testEncode(json, FoldingRangeProviderSerializer, data)
        testDecode(json, FoldingRangeProviderSerializer) { actualObject ->
            assertTrue(actualObject is OptionsFoldingRangeProvider)
            assertEquals(true, actualObject.options.workDoneProgress)
        }
    }

    @Test fun `When serializing a registration options folding range provider object, verify a correct result`() {
        val filter = DocumentFilter("Kotlin", "scheme", "pattern")
        val registrationOptions = FoldingRangeRegistrationOptions(
            documentSelector = listOf(filter), workDoneProgress = true, id = "anId"
        )
        val data = RegistrationOptionsFoldingRangeProvider(registrationOptions)
        val json = """{"registrationOptions":{"documentSelector":""" +
            """[{"language":"Kotlin","scheme":"scheme","pattern":"pattern"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""

        testEncode(json, FoldingRangeProviderSerializer, data)
        testDecode(json, FoldingRangeProviderSerializer) { actualObject ->
            assertTrue(actualObject is RegistrationOptionsFoldingRangeProvider)
            assertTrue(actualObject.registrationOptions is IsaacFoldingRangeRegistrationOptions)
            assertEquals(1, actualObject.registrationOptions.documentSelector!!.size)
            assertEquals("Kotlin", actualObject.registrationOptions.documentSelector!![0].language)
            assertEquals("scheme", actualObject.registrationOptions.documentSelector!![0].scheme)
            assertEquals("pattern", actualObject.registrationOptions.documentSelector!![0].pattern)
            assertEquals(true, actualObject.registrationOptions.workDoneProgress)
        }
    }

    @Test fun `When invalid JSON is used, verify an illegal state exception is thrown`() {
        val incomingJson = """{"xyzzy":true}"""
        assertFailsWith<IllegalStateException> { Json.decodeFromString(FoldingRangeProviderSerializer, incomingJson) }
    }
}
