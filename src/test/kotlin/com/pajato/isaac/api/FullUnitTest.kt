package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import java.lang.IllegalStateException
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class FullUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a boolean full object, verify the result`() {
        val options: Full = BooleanFullOptions(true)
        val actualJson = Json.encodeToString(TransformingFullSerializer, options)
        val expectedJson = "true"

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a boolean full semantic tokens options object, verify the result`() {
        val incomingJson = "true"
        val actualObject = Json.decodeFromString(TransformingFullSerializer, incomingJson)

        assertTrue(actualObject is BooleanFullOptions)
        assertEquals(true, actualObject.state)
    }

    @Test
    fun `When encoding a delta full semantic tokens options object, verify the result`() {
        val options: Full = DeltaFullOptions(true)
        val actualJson = Json.encodeToString(ContentFullSerializer, options)
        val expectedJson = """{"delta":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a delta full semantic tokens options object, verify the result`() {
        val incomingJson = """{"delta":true}"""
        val actualObject = Json.decodeFromString(ContentFullSerializer, incomingJson)

        assertTrue(actualObject is DeltaFullOptions)
        assertEquals(true, actualObject.delta)
    }

    @Test
    fun `When decoding an invalid full object using the content serializer, verify an illegal state exception`() {
        val incomingJson = """{"invalid":true}"""
        assertFailsWith<IllegalStateException> {
            Json.decodeFromString(ContentFullSerializer, incomingJson)
        }
    }

    @Test
    fun `When decoding an invalid full object using the transforming serializer, verify an illegal state exception`() {
        val incomingJson = """{"invalid":true}"""
        assertFailsWith<IllegalStateException> {
            Json.decodeFromString(TransformingFullSerializer, incomingJson)
        }
    }
}
