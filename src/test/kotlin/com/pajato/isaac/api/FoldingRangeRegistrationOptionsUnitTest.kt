package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.assertOptions
import kotlin.test.Test

class FoldingRangeRegistrationOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertAll(expected: FoldingRangeRegistrationOptions, actual: FoldingRangeRegistrationOptions) {
        assertOptions(expected, actual)
    }

    @Test fun `When encoding a folding range registration options object, verify a correct result`() {
        val data = FoldingRangeRegistrationOptions()
        val json = """{}"""

        testEncode(json, FoldingRangeRegistrationOptionsSerializer, data)
        testDecode(json, FoldingRangeRegistrationOptionsSerializer) { actual -> assertAll(data, actual) }
    }

    @Test fun `When decoding a folding range registration options object, verify a correct result`() {
        val filter = DocumentFilter("Kotlin", "scheme", "pattern")
        val data = FoldingRangeRegistrationOptions(
            workDoneProgress = true,
            documentSelector = listOf(filter),
            id = "registrationId"
        )
        val json = """{"documentSelector":[{"language":"Kotlin","scheme":"scheme","pattern":"pattern"}],""" +
            """"workDoneProgress":true,"id":"registrationId"}"""

        testEncode(json, FoldingRangeRegistrationOptionsSerializer, data)
        testDecode(json, FoldingRangeRegistrationOptionsSerializer) { actual -> assertAll(data, actual) }
    }
}
