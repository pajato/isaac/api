package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.assertOptions
import kotlin.test.Test

class TypeDefinitionRegistrationOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a type definition registration options default object, verify result`() {
        val data = TypeDefinitionRegistrationOptions()
        val json = """{}"""

        testEncode(json, TypeDefinitionRegistrationOptionsSerializer, data)
        testDecode(json, TypeDefinitionRegistrationOptionsSerializer) { actual ->
            assertOptions(data, actual)
        }
    }

    @Test fun `When serializing a type definition registration options non-default object, verify result`() {
        val filter = DocumentFilter("Kotlin", "scheme", "pattern")
        val data = TypeDefinitionRegistrationOptions(
            documentSelector = listOf(filter),
            workDoneProgress = true,
            id = "registrationId"
        )
        val json = """{"documentSelector":[{"language":"Kotlin","scheme":"scheme","pattern":"pattern"}],""" +
            """"workDoneProgress":true,"id":"registrationId"}"""

        testEncode(json, TypeDefinitionRegistrationOptionsSerializer, data)
        testDecode(json, TypeDefinitionRegistrationOptionsSerializer) { actual ->
            assertOptions(data, actual)
        }
    }
}
