package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class CodeLensOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertOptions(expected: CodeLensOptions, actual: CodeLensOptions) {
        assertEquals(expected.workDoneProgress, actual.workDoneProgress, getDiagnostic("work done progress"))
        assertEquals(expected.resolveProvider, actual.resolveProvider, getDiagnostic("resolve provider"))
    }

    @Test fun `When serializing a code lens options default object, verify result`() {
        val data = CodeLensOptions()
        val json = """{}"""

        testEncode(json, CodeLensOptionsSerializer, data)
        testDecode(json, CodeLensOptionsSerializer) { actual -> assertOptions(data, actual) }
    }

    @Test fun `When serializing a code lens options non-default object, verify result`() {
        val data = CodeLensOptions(workDoneProgress = true, resolveProvider = true)
        val json = """{"workDoneProgress":true,"resolveProvider":true}"""

        testEncode(json, CodeLensOptionsSerializer, data)
        testDecode(json, CodeLensOptionsSerializer) { actual -> assertOptions(data, actual) }
    }
}
