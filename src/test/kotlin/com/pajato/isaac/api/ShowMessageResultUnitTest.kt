package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ShowMessageResultUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default show message result object, verify`() {
        val data = ShowMessageResult()
        val json = """{}"""

        testEncode(json, ShowMessageResultSerializer, data)
        testDecode(json, ShowMessageResultSerializer) { actualObject ->
            assertEquals(null, actualObject.action)
        }
    }

    @Test fun `When serializing a non-default show message result, verify`() {
        val data = ShowMessageResult(action = MessageActionItem(title = "Retry"))
        val json = """{"action":{"title":"Retry"}}"""

        testEncode(json, ShowMessageResultSerializer, data)
        testDecode(json, ShowMessageResultSerializer) { actualObject ->
            val action = actualObject.action
            assertTrue(action != null)
            assertEquals("Retry", action.title)
        }
    }
}
