package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class IdUnitTest : SerializerTest, ApiTestProfiler() {

    @Test
    fun `When serializing a number id, verify a correct result`() {
        val value = 54
        val data = Id.NumberId(value)
        val json = """{"id":$value}"""

        testEncode(json, IdSerializer, data)
        testDecode(json, IdSerializer) { actualObject ->
            val typeErrorMessage = "The expected type is wrong!"
            val valueErrorMessage = "The id value is wrong!"

            assertTrue(actualObject is Id.NumberId, typeErrorMessage)
            assertEquals(value, actualObject.id, valueErrorMessage)
            assertEquals(value, actualObject.toInt(), valueErrorMessage)
        }
    }

    @Test
    fun `When serializing a string id, verify a correct result`() {
        val value = "27"
        val data = Id.StringId(value)
        val json = """{"id":"$value"}"""

        testEncode(json, IdSerializer, data)
        testDecode(json, IdSerializer) { actualObject ->
            val typeErrorMessage = "The expected type is wrong!"
            val valueErrorMessage = "The id value is wrong!"

            assertTrue(actualObject is Id.StringId, typeErrorMessage)
            assertEquals(value, actualObject.id, valueErrorMessage)
            assertEquals(value.toInt(), actualObject.toInt())
        }
    }

    @Test
    fun `When deserializing a compacted string id, verify a correct transformation`() {
        val actualObject = Json.decodeFromString(IdTransformingSerializer, """"22"""")

        assertTrue(actualObject is Id.StringId)
        assertEquals("22", actualObject.id)
    }

    @Test
    fun `When deserializing some invalid JSON, verify an illegal state exception is thrown`() {
        assertFailsWith<IllegalStateException> { Json.decodeFromString(IdTransformingSerializer, """{"invalid":2}""") }
    }
}
