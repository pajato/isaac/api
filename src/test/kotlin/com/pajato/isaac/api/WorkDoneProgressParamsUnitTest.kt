package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class WorkDoneProgressParamsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default work done progress params object, verify a correct result`() {
        val data = WorkDoneProgressParams()
        val json = """{}"""

        testEncode(json, WorkDoneProgressParamsSerializer, data)
        testDecode(json, WorkDoneProgressParamsSerializer) { actualObject ->
            assertEquals(null, actualObject.workDoneToken)
        }
    }

    @Test fun `When serializing a non-default work done progress params object, verify a correct result`() {
        val data = WorkDoneProgressParams(workDoneProgress = StringProgressToken("token"))
        val json = """{"workDoneToken":{"stringToken":"token"}}"""

        testEncode(json, WorkDoneProgressParamsSerializer, data)
        testDecode(json, WorkDoneProgressParamsSerializer) { actualObject ->
            val token = actualObject.workDoneToken
            assertTrue(token is StringProgressToken)
            assertEquals("token", token.stringToken)
        }
    }
}
