package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

class SemanticTokensClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertCapabilities(expected: SemanticTokensCC, actual: SemanticTokensCC) {
        assertEquals(expected.dynamicRegistration, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
        assertContentEquals(expected.formats, actual.formats, getDiagnostic("formats"))
        assertContentEquals(expected.tokenModifiers, actual.tokenModifiers, getDiagnostic("token modifiers"))
        assertContentEquals(expected.tokenTypes, actual.tokenTypes, getDiagnostic("token types"))
    }

    @Test fun `When serializing a default semantic tokens client capabilities, verify result`() {
        val json = """{"tokenTypes":[],"tokenModifiers":[],"formats":[]}"""
        val data = SemanticTokensClientCapabilities(
            formats = arrayOf(),
            tokenModifiers = arrayOf(),
            tokenTypes = arrayOf(),
        )

        testEncode(json, SemanticTokensClientCapabilitiesSerializer, data)
        testDecode(json, SemanticTokensClientCapabilitiesSerializer) { actual -> assertCapabilities(data, actual) }
    }

    @Test fun `When decoding a semantic tokens client capabilities non-default object, verify a correct result`() {
        val data = SemanticTokensClientCapabilities(
            dynamicRegistration = true,
            formats = arrayOf(TokenFormat.Relative),
            tokenModifiers = arrayOf("aModifier"),
            tokenTypes = arrayOf("aType"),
        )
        val json = """{"dynamicRegistration":true,"tokenTypes":["aType"],"tokenModifiers":["aModifier"],""" +
            """"formats":["Relative"]}"""

        testEncode(json, SemanticTokensClientCapabilitiesSerializer, data)
        testDecode(json, SemanticTokensClientCapabilitiesSerializer) { actual -> assertCapabilities(data, actual) }
    }
}
