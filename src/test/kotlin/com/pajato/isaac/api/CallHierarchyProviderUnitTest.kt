package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class CallHierarchyProviderUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a boolean call hierarchy object, verify a correct result`() {
        val data: CallHierarchyProvider = BooleanCallHierarchyProvider(true)
        val json = """{"state":true}"""

        testEncode(json, CallHierarchyProviderSerializer, data)
        testDecode(json, CallHierarchyProviderSerializer) { actualObject ->
            assertTrue(actualObject is BooleanCallHierarchyProvider)
            assertEquals(true, actualObject.state)
        }
    }

    @Test fun `When serializing an options call hierarchy provider object, verify a correct result`() {
        val options = CallHierarchyOptions(true)
        val data: CallHierarchyProvider = OptionsCallHierarchyProvider(options)
        val json = """{"options":{"workDoneProgress":true}}"""

        testEncode(json, CallHierarchyProviderSerializer, data)
        testDecode(json, CallHierarchyProviderSerializer) { actualObject ->
            assertTrue(actualObject is OptionsCallHierarchyProvider)
            assertEquals(true, actualObject.options.workDoneProgress)
        }
    }

    @Test fun `When serializing a registration options call hierarchy provider object, verify a correct result`() {
        val options = CallHierarchyRegistrationOptions(
            documentSelector = listOf(DocumentFilter("Kotlin", "scheme", "pattern")), workDoneProgress = true,
            id = "anId"
        )
        val data: CallHierarchyProvider = RegistrationOptionsCallHierarchyProvider(options)
        val json = """{"registrationOptions":{"documentSelector":""" +
            """[{"language":"Kotlin","scheme":"scheme","pattern":"pattern"}],""" +
            """"workDoneProgress":true,"id":"anId"}}"""

        testEncode(json, CallHierarchyProviderSerializer, data)
        testDecode(json, CallHierarchyProviderSerializer) { actualObject ->
            assertTrue(actualObject is RegistrationOptionsCallHierarchyProvider)
            assertEquals(1, actualObject.registrationOptions.documentSelector!!.size)
            assertEquals("Kotlin", actualObject.registrationOptions.documentSelector!![0].language)
            assertEquals("scheme", actualObject.registrationOptions.documentSelector!![0].scheme)
            assertEquals("pattern", actualObject.registrationOptions.documentSelector!![0].pattern)
            assertEquals(true, actualObject.registrationOptions.workDoneProgress)
        }
    }

    @Test fun `When invalid JSON is used, verify an illegal state exception is thrown`() {
        val json = """{"xyzzy":true}"""

        assertFailsWith<IllegalStateException> { Json.decodeFromString(CallHierarchyProviderSerializer, json) }
    }
}
