package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class DocumentFormattingOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When decoding a default document formatting options object, verify a correct result`() {
        val data = DocumentFormattingOptions()
        val json = """{}"""

        testEncode(json, DocumentFormattingOptionsSerializer, data)
        testDecode(json, DocumentFormattingOptionsSerializer) { actualObject ->
            assertEquals(null, actualObject.workDoneProgress)
        }
    }

    @Test fun `When serializing a document formatting options object, verify a correct result`() {
        val data = DocumentFormattingOptions(true)
        val json = """{"workDoneProgress":true}"""

        testEncode(json, DocumentFormattingOptionsSerializer, data)
        testDecode(json, DocumentFormattingOptionsSerializer) { actualObject ->
            assertEquals(true, actualObject.workDoneProgress)
        }
    }
}
