package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class SaveUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a boolean save object, verify correct result`() {
        val expectedJson = """true"""
        val actualJson = Json.encodeToString(SaveTransformingSerializer, BooleanSave(true))

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a boolean save object, verify correct result`() {
        val incomingJson = """true"""
        val actualObject = Json.decodeFromString(SaveTransformingSerializer, incomingJson)

        assertTrue(actualObject is BooleanSave && actualObject.value)
    }

    @Test
    fun `When encoding an options save object, verify correct result`() {
        val actualJson = Json.encodeToString(SaveTransformingSerializer, OptionsSave(IsaacSaveOptions(true)))
        val expectedJson = """{"includeText":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding an options save object, verify correct result`() {
        val incomingJson = """{"includeText":true}"""
        val actualObject = Json.decodeFromString(SaveTransformingSerializer, incomingJson)

        assertTrue(actualObject is OptionsSave && actualObject.value.includeText == true)
    }

    @Test
    fun `When decoding an invalid options save object, verify correct result`() {
        val incomingJson = """["xyzzy"]"""

        assertFailsWith<IllegalStateException> { Json.decodeFromString(SaveTransformingSerializer, incomingJson) }
    }
}
