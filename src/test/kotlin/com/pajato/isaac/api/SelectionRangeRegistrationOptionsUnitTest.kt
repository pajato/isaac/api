package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.assertOptions
import kotlin.test.Test

class SelectionRangeRegistrationOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertAll(expected: SelectionRangeRegistrationOptions, actual: SelectionRangeRegistrationOptions) {
        assertOptions(expected, actual)
    }

    @Test fun `When serializing a selection range registration options default object, verify result`() {
        val data = SelectionRangeRegistrationOptions()
        val json = """{}"""

        testEncode(json, SelectionRangeRegistrationOptionsSerializer, data)
        testDecode(json, SelectionRangeRegistrationOptionsSerializer) { actual -> assertAll(data, actual) }
    }

    @Test fun `When serializing a selection range registration options non-default object, verify result`() {
        val filter = DocumentFilter("Kotlin", "scheme", "pattern")
        val data = SelectionRangeRegistrationOptions(
            documentSelector = listOf(filter),
            workDoneProgress = true,
            id = "registrationId"
        )
        val json = """{"documentSelector":[{"language":"Kotlin","scheme":"scheme","pattern":"pattern"}],""" +
            """"workDoneProgress":true,"id":"registrationId"}"""

        testEncode(json, SelectionRangeRegistrationOptionsSerializer, data)
        testDecode(json, SelectionRangeRegistrationOptionsSerializer) { actual -> assertAll(data, actual) }
    }
}
