package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class WorkDoneProgressUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default work done progress object, verify a correct result`() {
        val data = WorkDoneProgress()
        val json = """{}"""

        testEncode(json, WorkDoneProgressSerializer, data)
        testDecode(json, WorkDoneProgressSerializer) { actualObject ->
            assertEquals(null, actualObject.workDoneProgress)
        }
    }

    @Test fun `When serializing a non-default work done progress object, verify a correct result`() {
        val data = WorkDoneProgress(true)
        val json = """{"workDoneProgress":true}"""

        testEncode(json, WorkDoneProgressSerializer, data)
        testDecode(json, WorkDoneProgressSerializer) { actualObject ->
            assertEquals(true, actualObject.workDoneProgress)
        }
    }
}
