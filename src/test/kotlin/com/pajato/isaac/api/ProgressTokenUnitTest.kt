package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class ProgressTokenUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a string progress token object, verify the result`() {
        val token = "1d546990-40a3-4b77-b134-46622995f6ae"
        val data = StringProgressToken(token)
        val json = """{"stringToken":"$token"}"""

        testEncode(json, ProgressTokenSerializer, data)
        testDecode(json, ProgressTokenSerializer) { actualObject ->
            val typeErrorMessage = "The expected type is wrong!"
            val valueErrorMessage = "The work done token value is wrong!"

            assertTrue(actualObject is StringProgressToken, typeErrorMessage)
            assertEquals(token, actualObject.stringToken, valueErrorMessage)
        }
    }

    @Test fun `When serializing an integer progress token object, verify the result`() {
        val token = 437198205
        val data = IntegerProgressToken(token)
        val json = """{"intToken":$token}"""

        testEncode(json, ProgressTokenSerializer, data)
        testDecode(json, ProgressTokenSerializer) { actualObject ->
            val typeErrorMessage = "The expected type is wrong!"
            val valueErrorMessage = "The work done token value is wrong!"

            assertTrue(actualObject is IntegerProgressToken, typeErrorMessage)
            assertEquals(token, actualObject.intToken, valueErrorMessage)
        }
    }

    @Test fun `When deserializing a compacted string progress token, verify a correct transformation`() {
        val actualObject = Json.decodeFromString(ProgressTokenTransformingSerializer, """"22"""")

        assertTrue(actualObject is StringProgressToken)
        assertEquals("22", actualObject.stringToken)
    }

    @Test fun `When deserializing some invalid JSON, verify an illegal state exception is thrown`() {
        assertFailsWith<IllegalStateException> {
            Json.decodeFromString(ProgressTokenTransformingSerializer, """{"invalid":2}""")
        }
    }
}
