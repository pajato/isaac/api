package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class MonikerOptionsUnitTest : SerializerTest, ApiTestProfiler() {
    private fun assertOptions(expected: Boolean?, actual: MonikerOptions) {
        assertEquals(expected, actual.workDoneProgress)
    }

    @Test fun `When serializing a moniker options default object, verify a correct result`() {
        val data = MonikerOptions()
        val json = """{}"""

        testEncode(json, MonikerOptionsSerializer, data)
        testDecode(json, MonikerOptionsSerializer) { actual -> assertOptions(null, actual) }
    }

    @Test fun `When decoding a moniker options object, verify a correct result`() {
        val data = MonikerOptions(true)
        val json = """{"workDoneProgress":true}"""

        testEncode(json, MonikerOptionsSerializer, data)
        testDecode(json, MonikerOptionsSerializer) { actual -> assertOptions(true, actual) }
    }
}
