package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class CallHierarchyOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertOptions(expected: Boolean?, actual: CallHierarchyOptions) {
        assertEquals(expected, actual.workDoneProgress, getDiagnostic("work done progress"))
    }

    @Test fun `When serializing a call hierarchy range options default object, verify result`() {
        val data = CallHierarchyOptions()
        val json = """{}"""

        testEncode(json, CallHierarchyOptionsSerializer, data)
        testDecode(json, CallHierarchyOptionsSerializer) { actual -> assertOptions(null, actual) }
    }

    @Test
    fun `When serializing a call hierarchy range options non-default object, verify result`() {
        val data = CallHierarchyOptions(true)
        val json = """{"workDoneProgress":true}"""

        testEncode(json, CallHierarchyOptionsSerializer, data)
        testDecode(json, CallHierarchyOptionsSerializer) { actual -> assertOptions(true, actual) }
    }
}
