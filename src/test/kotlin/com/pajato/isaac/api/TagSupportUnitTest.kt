package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TagSupportUnitTest : SerializerTest, ApiTestProfiler() {
    private val prop: Array<SymbolTag> = arrayOf(SymbolTag.Deprecated)
    private val data = SymbolTagSupport(valueSet = prop)
    private val json = """{"valueSet":["Deprecated"]}"""

    @Test fun `When serializing a tag support object, verify the result()`() {
        testEncode(json, SymbolTagSupportSerializer, data)
        testDecode(json, SymbolTagSupportSerializer) {
            assertTrue(it is IsaacSymbolTagSupport)
            assertEquals(1, it.valueSet.size)
            assertEquals(SymbolTag.Deprecated, it.valueSet[0])
        }
    }
}
