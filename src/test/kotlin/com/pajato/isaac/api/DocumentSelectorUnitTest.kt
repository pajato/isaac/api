package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.builtins.ListSerializer
import kotlin.test.Test
import kotlin.test.assertEquals

class DocumentSelectorUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a document selector, verify a correct result`() {
        val filter1 = DocumentFilter("Kotlin", "xyzzyScheme", "*.{ts,kt}")
        val filter2 = DocumentFilter("Rust", "xyzzyScheme", "*.{js,rs}")
        val data: DocumentSelector = listOf(filter1, filter2)
        val json = """[{"language":"Kotlin","scheme":"xyzzyScheme","pattern":"*.{ts,kt}"},""" +
            """{"language":"Rust","scheme":"xyzzyScheme","pattern":"*.{js,rs}"}]"""

        testEncode(json, ListSerializer(DocumentFilterSerializer), data)
        testDecode(json, ListSerializer(DocumentFilterSerializer)) { actualObject ->
            assertEquals(2, actualObject.size)
            assertEquals("Kotlin", actualObject[0].language)
            assertEquals("xyzzyScheme", actualObject[0].scheme)
            assertEquals("*.{ts,kt}", actualObject[0].pattern)
            assertEquals("Rust", actualObject[1].language)
            assertEquals("xyzzyScheme", actualObject[1].scheme)
            assertEquals("*.{js,rs}", actualObject[1].pattern)
        }
    }
}
