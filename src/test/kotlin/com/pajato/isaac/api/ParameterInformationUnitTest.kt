package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class ParameterInformationUnitTest : SerializerTest, ApiTestProfiler() {
    private fun assertOptions(expected: Boolean?, actual: ParameterInformation) {
        assertEquals(expected, actual.labelOffsetSupport)
    }

    @Test fun `When serializing a parameter information default object, verify result`() {
        val data = ParameterInformation()
        val json = """{}"""

        testEncode(json, ParameterInformationSerializer, data)
        testDecode(json, ParameterInformationSerializer) { actual -> assertOptions(null, actual) }
    }

    @Test fun `When decoding a parameter information object, verify result`() {
        val data = ParameterInformation(true)
        val json = """{"labelOffsetSupport":true}"""

        testEncode(json, ParameterInformationSerializer, data)
        testDecode(json, ParameterInformationSerializer) { actual -> assertOptions(true, actual) }
    }
}
