package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class DocumentColorClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {
    private fun assertCapabilities(expected: Boolean?, actual: DocumentColorClientCapabilities) {
        assertEquals(expected, actual.dynamicRegistration, getDiagnostic("dynamic registration"))
    }

    @Test fun `When serializing a document color client capabilities default object, verify result`() {
        val data = DocumentColorClientCapabilities()
        val json = """{}"""

        testEncode(json, DocumentColorSerializer, data)
        testDecode(json, DocumentColorSerializer) { actual -> assertCapabilities(null, actual) }
    }

    @Test fun `When serializing a document color configuration client capabilities non-default object, verify`() {
        val data = DocumentColorClientCapabilities(dynamicRegistration = true)
        val json = """{"dynamicRegistration":true}"""

        testEncode(json, DocumentColorSerializer, data)
        testDecode(json, DocumentColorSerializer) { actual -> assertCapabilities(true, actual) }
    }
}
