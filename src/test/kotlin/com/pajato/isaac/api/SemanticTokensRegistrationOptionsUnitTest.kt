package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.assertLegend
import com.pajato.isaac.assertSelectorIsEmpty
import com.pajato.isaac.getTestLegend
import kotlin.test.Test
import kotlin.test.assertEquals

class SemanticTokensRegistrationOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default semantic tokens registration options object, verify a correct result`() {
        val types = arrayOf("type1", "type2")
        val modifiers = arrayOf("modifier1", "modifier2")
        val data = SemanticTokensRegistrationOptions(legend = SemanticTokensLegend(types, modifiers))
        val json = """{"legend":{"tokenTypes":["type1","type2"],"tokenModifiers":["modifier1","modifier2"]}}"""

        testEncode(json, SemanticTokensRegistrationOptionsSerializer, data)
        testDecode(json, SemanticTokensRegistrationOptionsSerializer) { actualObject ->
            assertEquals(null, actualObject.documentSelector)
            assertEquals(null, actualObject.workDoneProgress)
            assertLegend(actualObject)
            assertEquals(null, actualObject.range)
            assertEquals(null, actualObject.full)
            assertEquals(null, actualObject.id)
        }
    }

    @Test fun `When serializing a non-default semantic tokens registration options object, verify a correct result`() {
        val id = "anId"
        val data = SemanticTokensRegistrationOptions(
            workDoneProgress = true, legend = getTestLegend(), documentSelector = listOf(), id = id
        )
        val json = """{"documentSelector":[],"workDoneProgress":true,"legend":""" +
            """{"tokenTypes":["type1","type2"],"tokenModifiers":["modifier1","modifier2"]},"id":"anId"}"""

        testEncode(json, SemanticTokensRegistrationOptionsSerializer, data)
        testDecode(json, SemanticTokensRegistrationOptionsSerializer) { actualObject ->
            assertSelectorIsEmpty(actualObject.documentSelector)
            assertEquals(true, actualObject.workDoneProgress)
            assertLegend(actualObject)
            assertEquals(null, actualObject.range)
            assertEquals(null, actualObject.full)
            assertEquals("anId", actualObject.id)
        }
    }
}
