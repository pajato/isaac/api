package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ResponseErrorUnitTest : SerializerTest, ApiTestProfiler() {
    private val code = -32807
    private val message = "Something went wrong!"

    @Test fun `When serializing a response error object with no error data, verify a correct result`() {
        val data = ResponseError(code, message)
        val json = """{"code":$code,"message":"$message"}"""

        testEncode(json, ResponseErrorSerializer, data)
        testDecode(json, ResponseErrorSerializer) { actualObject ->
            assertEquals(code, actualObject.code)
            assertEquals(message, actualObject.message)
            assertEquals(null, actualObject.data)
        }
    }

    @Test fun `When serializing a response error object with numeric error data, verify a correct result`() {
        val data = ResponseError(code, message, NumberErrorData(23))
        val json = """{"code":$code,"message":"$message","data":23}"""

        testEncode(json, ResponseErrorSerializer, data)
        testDecode(json, ResponseErrorSerializer) { actualObject ->
            val errorData = actualObject.data

            assertEquals(code, actualObject.code)
            assertEquals(message, actualObject.message)
            assertTrue(errorData is NumberErrorData)
            assertEquals(23, errorData.data)
        }
    }

    @Test fun `When serializing a response error object with string error data, verify a correct result`() {
        val data = ResponseError(code, message, StringErrorData("string message"))
        val json = """{"code":$code,"message":"$message","data":"string message"}"""

        testEncode(json, ResponseErrorSerializer, data)
        testDecode(json, ResponseErrorSerializer) { actualObject ->
            val errorData = actualObject.data

            assertEquals(code, actualObject.code)
            assertEquals(message, actualObject.message)
            assertTrue(errorData is StringErrorData)
            assertEquals("string message", errorData.data)
        }
    }

    @Test fun `When serializing a response error object with boolean error data, verify a correct result`() {
        val data = ResponseError(code, message, BooleanErrorData(true))
        val json = """{"code":$code,"message":"$message","data":true}"""

        testEncode(json, ResponseErrorSerializer, data)
        testDecode(json, ResponseErrorSerializer) { actualObject ->
            val errorData = actualObject.data

            assertEquals(code, actualObject.code)
            assertEquals(message, actualObject.message)
            assertTrue(errorData is BooleanErrorData)
            assertEquals(true, errorData.data)
        }
    }
}
