package com.pajato.isaac.api

import kotlin.test.Test
import kotlin.test.assertEquals

class PositionUnitTest {

    @Test fun `When a default position is created, verify a correct result`() {
        val actual = Position()
        val expected = 0

        assertEquals(expected, actual.line)
        assertEquals(expected, actual.character)
    }
}
