package com.pajato.isaac.api

import kotlin.test.Test
import kotlin.test.assertEquals

class RangeUnitTest {

    @Test fun `When a default range is created, verify a correct result`() {
        val actual = Range(Position(), Position())
        val expected = 0

        assertEquals(expected, actual.start.line)
        assertEquals(expected, actual.start.character)
        assertEquals(expected, actual.end.line)
        assertEquals(expected, actual.end.character)
    }
}
