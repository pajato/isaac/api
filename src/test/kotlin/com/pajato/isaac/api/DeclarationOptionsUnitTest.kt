package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class DeclarationOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default declaration options unit test`() {
        val data = DeclarationOptions()
        val json = """{}"""

        testEncode(json, DeclarationOptionsSerializer, data)
        testDecode(json, DeclarationOptionsSerializer) { actualObject ->
            assertEquals(null, actualObject.workDoneProgress)
        }
    }

    @Test fun `When serializing a non-default declaration options object, verify a correct result`() {
        val data = DeclarationOptions(true)
        val json = """{"workDoneProgress":true}"""

        testEncode(json, DeclarationOptionsSerializer, data)
        testDecode(json, DeclarationOptionsSerializer) { actualObject ->
            assertEquals(true, actualObject.workDoneProgress)
        }
    }
}
