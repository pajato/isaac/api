package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class MessageActionItemCapabilityUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default message action item capability object, verify the result`() {
        val data = MessageActionItemCapability()
        val json = """{}"""

        testEncode(json, MessageActionItemCapabilitySerializer, data)
        testDecode(json, MessageActionItemCapabilitySerializer) { actual ->
            assertEquals(null, actual.additionalPropertiesSupport, getDiagnostic("additional properties support"))
        }
    }
}
