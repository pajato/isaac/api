package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class DocumentHighlightProviderUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a boolean document highlight provider object, verify a correct result`() {
        val data = BooleanDocumentHighlightProvider(true)
        val json = """{"state":true}"""

        testEncode(json, DocumentHighlightProviderSerializer, data)
        testDecode(json, DocumentHighlightProviderSerializer) { actualObject ->
            assertTrue(actualObject is BooleanDocumentHighlightProvider)
            assertEquals(true, actualObject.state)
        }
    }

    @Test fun `When serializing an options document highlight provider object, verify a correct result`() {
        val data = OptionsDocumentHighlightProvider(options = DocumentHighlightOptions(true))
        val json = """{"options":{"workDoneProgress":true}}"""

        testEncode(json, DocumentHighlightProviderSerializer, data)
        testDecode(json, DocumentHighlightProviderSerializer) { actualObject ->
            assertTrue(actualObject is OptionsDocumentHighlightProvider)
            assertEquals(true, actualObject.options.workDoneProgress)
        }
    }

    @Test fun `When invalid JSON is used, verify an illegal state exception is thrown`() {
        val json = """{"xyzzy":true}"""

        assertFailsWith<IllegalStateException> { Json.decodeFromString(DocumentHighlightProviderSerializer, json) }
    }
}
