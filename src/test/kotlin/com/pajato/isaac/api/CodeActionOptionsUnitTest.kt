package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class CodeActionOptionsUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a definition options object, verify a correct result`() {
        val actualJson = Json.encodeToString(CodeActionOptionsSerializer, IsaacCodeActionOptions(true))
        val expectedJson = """{"workDoneProgress":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a definition options object, verify a correct result`() {
        val incomingJson = """{"workDoneProgress":true}"""
        val actualObject = Json.decodeFromString(CodeActionOptionsSerializer, incomingJson)

        assertTrue(actualObject is IsaacCodeActionOptions)
        assertEquals(true, actualObject.workDoneProgress)
    }
}
