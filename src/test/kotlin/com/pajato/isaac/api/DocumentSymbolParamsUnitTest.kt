package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class DocumentSymbolParamsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default document symbol params object, verify the result`() {
        val uri: DocumentUri = "xyzzy"
        val textDocumentId = TextDocumentIdentifier(uri)
        val data = DocumentSymbolParams(textDocument = textDocumentId)
        val json = """{"textDocument":{"uri":"xyzzy"}}"""

        testEncode(json, DocumentSymbolParamsSerializer, data)
        testDecode(json, DocumentSymbolParamsSerializer) { actualObject ->
            assertEquals(textDocumentId.uri, actualObject.textDocument.uri, getDiagnostic("text document id value"))
        }
    }

    @Test fun `When serializing a non-default document symbol params object, verify the result`() {
        val token = StringProgressToken("80")
        val uri: DocumentUri = "xyzzy"
        val textDocumentId = TextDocumentIdentifier(uri)
        val data = DocumentSymbolParams(
            workDoneToken = token, partialResultToken = token, textDocument = textDocumentId
        )
        val json = """{"workDoneToken":"80","partialResultToken":"80","textDocument":{"uri":"xyzzy"}}"""

        testEncode(json, DocumentSymbolParamsSerializer, data)
        testDecode(json, DocumentSymbolParamsSerializer) { actualObject ->
            assertEquals(textDocumentId.uri, actualObject.textDocument.uri, getDiagnostic("text document id value"))
        }
    }
}
