package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ResponseMessageUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing an empty response message, verify the correct behavior`() {
        val json = """{"jsonrpc":"2.0"}"""
        val data = ResponseMessage()

        testEncode(json, ResponseMessageSerializer, data)
        testDecode(json, ResponseMessageSerializer) {
            assertEquals(data.jsonrpc, it.jsonrpc, "The JSON RPC version is wrong!")
            assertEquals(null, it.id, "Expected a null message id!")
            assertEquals(null, it.result, "Expected a null response message result!")
            assertEquals(null, it.error, "Expected a null response message error!")
        }
    }

    @Test fun `When serializing a response message with no result and an error, verify the correct behavior`() {
        val message = "Something bad happened!"
        val json = """{"jsonrpc":"2.0","error":{"code":0,"message":"$message"}}"""
        val data = ResponseMessage(error = ResponseError(0, message))

        testEncode(json, ResponseMessageSerializer, data)
        testDecode(json, ResponseMessageSerializer) {
            assertEquals(data.jsonrpc, it.jsonrpc, "The JSON RPC version is wrong!")
            assertEquals(null, it.id, "Expected a null message id!")
            assertEquals(null, it.result, "Expected a null response message result!")
            assertEquals(0, it.error!!.code, "Got the wrong response message error code!")
            assertEquals(message, it.error!!.message, "Got the wrong response message error message!")
        }
    }

    @Test fun `When serializing a response message with a workspace symbol result, verify the correct behavior`() {
        val json = """{"jsonrpc":"2.0","result":[]}"""
        val result = WorkspaceSymbolResult(listOf()) as Result // The "as" clause looks like a bug!?!
        val data = ResponseMessage(result = result)

        testEncode(json, ResponseMessageSerializer, data)
        testDecode(json, ResponseMessageSerializer) {
            assertEquals(data.jsonrpc, it.jsonrpc, "The JSON RPC version is wrong!")
            assertEquals(null, it.id, "Expected a null message id!")
            assertEquals(null, it.error, "Expected a null response message error!")
            assertTrue(it.result is WorkspaceSymbolResult)
        }
    }

    @Test fun `When serializing a response message with a document symbol result, verify the correct behavior`() {
        fun getSymbol(name: String, kind: SymbolKind, range: Range): DocumentSymbol =
            DocumentSymbol(name, kind, range = range)

        val json = """{"jsonrpc":"2.0","result":[{"name":"aName","kind":17,"range":{"start":{"line":0,"character":0},"end":{"line":0,"character":0}},"selectionRange":{"start":{"line":0,"character":0},"end":{"line":0,"character":0}}}]}"""
        val name = "aName"
        val kind = SymbolKind.Array
        val range = Range(Position(), Position())
        val result = DocumentSymbolResult(listOf(getSymbol(name, kind, range))) as Result // The "as" clause looks like a bug!?!
        val data = ResponseMessage(result = result)

        testEncode(json, ResponseMessageSerializer, data)
        testDecode(json, ResponseMessageSerializer) {
            assertEquals(data.jsonrpc, it.jsonrpc, "The JSON RPC version is wrong!")
            assertEquals(null, it.id, "Expected a null message id!")
            assertEquals(null, it.error, "Expected a null response message error!")
            assertTrue(it.result is DocumentSymbolResult)
        }
    }

    @Test fun `When serializing a shutdown response message with no error, verify the correct behavior`() {
        val expectedJson = """{"jsonrpc":"2.0"}"""
        val json = """{"jsonrpc":"2.0","result":null}"""
        val data = ResponseMessage(result = null)

        testEncode(expectedJson, ResponseMessageSerializer, data)
        testDecode(json, ResponseMessageSerializer) {
            assertEquals(data.jsonrpc, it.jsonrpc, "The JSON RPC version is wrong!")
            assertEquals(null, it.id, "Expected a null message id!")
            assertEquals(null, it.error, "Expected a null response message error!")
            assertEquals(null, it.result)
        }
    }

    @Test fun `When serializing a shutdown response message with an error, verify the correct behavior`() {
        val errorCode = -1
        val errorMessage = "No message of any substance..."
        val json = """{"jsonrpc":"2.0","error":{"code":$errorCode,"message":"$errorMessage"}}"""
        val data = ResponseMessage(result = null, error = ResponseError(errorCode, errorMessage))

        testEncode(json, ResponseMessageSerializer, data)
        testDecode(json, ResponseMessageSerializer) {
            assertEquals(data.jsonrpc, it.jsonrpc, "The JSON RPC version is wrong!")
            assertEquals(null, it.id, "Expected a null message id!")
            assertEquals(errorCode, it.error!!.code, "Wrong error code!")
            assertEquals(errorMessage, it.error!!.message, "Wrong error message!")
            assertEquals(null, it.result)
        }
    }
}
