package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class DocumentFormattingProviderUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a boolean document formatting provider object, verify a correct result`() {
        val data = BooleanDocumentFormattingProvider(true)
        val json = """{"state":true}"""

        testEncode(json, DocumentFormattingProviderSerializer, data)
        testDecode(json, DocumentFormattingProviderSerializer) { actualObject ->
            assertTrue(actualObject is BooleanDocumentFormattingProvider)
            assertEquals(true, actualObject.state)
        }
    }

    @Test fun `When serializing an options document formatting provider object, verify a correct result`() {
        val data = OptionsDocumentFormattingProvider(options = DocumentFormattingOptions(true))
        val json = """{"options":{"workDoneProgress":true}}"""

        testEncode(json, DocumentFormattingProviderSerializer, data)
        testDecode(json, DocumentFormattingProviderSerializer) { actualObject ->
            assertTrue(actualObject is OptionsDocumentFormattingProvider)
            assertEquals(true, actualObject.options.workDoneProgress)
        }
    }

    @Test fun `When invalid JSON is used, verify an illegal state exception is thrown`() {
        val json = """{"xyzzy":true}"""

        assertFailsWith<IllegalStateException> {
            Json.decodeFromString(DocumentFormattingProviderSerializer, json)
        }
    }
}
