package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

class SignatureHelpOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertOptions(expected: SignatureHelpOptions, actual: SignatureHelpOptions) {
        assertEquals(expected.workDoneProgress, actual.workDoneProgress, getDiagnostic("works done progress"))
        assertContentEquals(expected.triggerCharacters, actual.triggerCharacters, getDiagnostic("trigger chars"))
        assertContentEquals(expected.retriggerCharacters, actual.retriggerCharacters, getDiagnostic("retrigger chars"))
    }

    @Test fun `When serializing a default signature help options object, verify result`() {
        val data = SignatureHelpOptions()
        val json = """{}"""

        testEncode(json, SignatureHelpOptionsSerializer, data)
        testDecode(json, SignatureHelpOptionsSerializer) { actual -> assertOptions(data, actual) }
    }

    @Test fun `When serializing a non-default signature help options object, verify result`() {
        val data = SignatureHelpOptions(true, arrayOf("(", ","), arrayOf("-"))
        val json = """{"workDoneProgress":true,"triggerCharacters":["(",","],"retriggerCharacters":["-"]}"""

        testEncode(json, SignatureHelpOptionsSerializer, data)
        testDecode(json, SignatureHelpOptionsSerializer) { actual -> assertOptions(data, actual) }
    }
}
