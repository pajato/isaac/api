package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class MessageUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When passing invalid Json to the message serializer, verify an illegal state exception is thrown!`() {
        assertFailsWith<IllegalStateException> { Json.decodeFromString(MessageSerializer, "invalid json") }
    }

    @Test fun `When serializing a cancel notification message with a numeric parameter, verify the result`() {
        val method = "$/cancelRequest"
        val json = """{"jsonrpc":"2.0","method":"$method","params":{"value":0}}"""
        val data = NotificationMessage(method, NumberParams(0), jsonrpcVersion)

        testEncode(json, NotificationMessageSerializer, data)
        testDecode(json, NotificationMessageSerializer) {
            val params = it.params

            assertEquals(data.jsonrpc, it.jsonrpc, "The JSON RPC version is wrong!")
            assertEquals(method, it.method, "The notification method name is wrong!")
            assertTrue(params is NumberParams, "The parameter type is wrong!")
            assertEquals(0, params.value, "The parameter value is wrong!")
        }
    }

    @Test fun `When serializing a cancel notification message with a string parameter, verify the result`() {
        val value = "abort"
        val method = "$/cancelRequest"
        val json = """{"jsonrpc":"2.0","method":"$method","params":{"value":"$value"}}"""
        val data = NotificationMessage(method, StringParams(value))

        testEncode(json, NotificationMessageSerializer, data)
        testDecode(json, NotificationMessageSerializer) {
            val params = it.params

            assertEquals(data.jsonrpc, it.jsonrpc, "The JSON RPC version is wrong!")
            assertEquals(method, it.method, "The notification method name is wrong!")
            assertTrue(params is StringParams, "The parameter type is wrong!")
            assertEquals(value, params.value, "The parameter value is wrong!")
        }
    }

    @Test fun `When serializing an exit notification message with a default parameter, verify the result`() {
        val method = "exit"
        val json = """{"jsonrpc":"2.0","method":"exit"}"""
        val data = NotificationMessage(method)

        testEncode(json, NotificationMessageSerializer, data)
        testDecode(json, NotificationMessageSerializer) {
            assertEquals(data.jsonrpc, it.jsonrpc, "The JSON RPC version is wrong!")
            assertEquals(method, it.method, "The notification method name is wrong!")
            assertEquals(null, it.params, "The parameter value is wrong!")
        }
    }

    @Test fun `When serializing an exit notification message with an array parameter, verify the result`() {
        val method = "exit"
        val params: Params = ListParams(listOf("a", "b", "c", "d"))
        val json = """{"jsonrpc":"2.0","method":"$method","params":{"value":["a","b","c","d"]}}"""
        val data = NotificationMessage(method, params)

        testEncode(json, NotificationMessageSerializer, data)
        testDecode(json, NotificationMessageSerializer) {
            val actualParams = it.params

            assertEquals(data.jsonrpc, it.jsonrpc, "The JSON RPC version is wrong!")
            assertEquals(method, it.method, "The notification method name is wrong!")
            assertTrue(actualParams is ListParams, "The notification params type is wrong!")
            assertEquals(4, actualParams.value.size, "The parameter list size is wrong!")
            assertEquals("a", actualParams.value[0], "The first parameter value is wrong!")
            assertEquals("b", actualParams.value[1], "The second parameter value is wrong!")
            assertEquals("c", actualParams.value[2], "The third parameter value is wrong!")
            assertEquals("d", actualParams.value[3], "The fourth parameter value is wrong!")
        }
    }

    @Test fun `When serializing an exit notification message with a numeric parameter, verify the result`() {
        val value = 23
        val method = "exit"
        val json = """{"jsonrpc":"2.0","method":"$method","params":{"value":$value}}"""
        val data = NotificationMessage(method, NumberParams(value))

        testEncode(json, NotificationMessageSerializer, data)
        testDecode(json, NotificationMessageSerializer) {
            val params = it.params

            assertEquals(data.jsonrpc, it.jsonrpc, "The JSON RPC version is wrong!")
            assertEquals(method, it.method, "The notification method name is wrong!")
            assertTrue(params is NumberParams, "The parameter type is wrong!")
            assertEquals(value, params.value, "The parameter value is wrong!")
        }
    }

    @Test fun `When serializing a log message, verify the result`() {
        val text = "Initialize response handler: Initialized!"
        val json = """{"jsonrpc":"2.0","text":"$text"}"""
        val data = LogMessage(text)

        testEncode(json, LogMessageSerializer, data)
        testDecode(json, LogMessageSerializer) {
            val actualText = it.text
            assertEquals(text, actualText)
        }
    }
}
