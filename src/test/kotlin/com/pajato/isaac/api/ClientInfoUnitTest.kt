package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.appName
import com.pajato.isaac.appVersion
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ClientInfoUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertInfo(expected: ClientInfo, actual: ClientInfo) {
        assertTrue(actual is IsaacClientInfo, getDiagnostic("type"))
        assertEquals(expected.name, actual.name, getDiagnostic("name"))
        assertEquals(expected.version, actual.version, getDiagnostic("version"))
    }

    @Test fun `When serializing a client info default object, verify the result`() {
        val data = ClientInfo(appName)
        val json = """{"name":"Test App"}"""

        testEncode(json, ClientInfoSerializer, data)
        testDecode(json, ClientInfoSerializer) { actual -> assertInfo(data, actual) }
    }

    @Test fun `When serializing a client info non-default object, verify the result`() {
        val data = ClientInfo(appName, appVersion)
        val json = """{"name":"Test App","version":"1.0-SNAPSHOT"}"""

        testEncode(json, ClientInfoSerializer, data)
        testDecode(json, ClientInfoSerializer) { actual -> assertInfo(data, actual) }
    }
}
