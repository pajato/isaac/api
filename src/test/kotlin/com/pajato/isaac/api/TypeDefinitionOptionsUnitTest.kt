package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class TypeDefinitionOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default type definition options object, verify a correct result`() {
        val data = TypeDefinitionOptions()
        val json = """{}"""

        testEncode(json, TypeDefinitionOptionsSerializer, data)
        testDecode(json, TypeDefinitionOptionsSerializer) { actualObject ->
            assertEquals(null, actualObject.workDoneProgress)
        }
    }

    @Test fun `When serializing a non-default type definition options object, verify a correct result`() {
        val data = TypeDefinitionOptions(true)
        val json = """{"workDoneProgress":true}"""

        testEncode(json, TypeDefinitionOptionsSerializer, data)
        testDecode(json, TypeDefinitionOptionsSerializer) { actualObject ->
            assertEquals(true, actualObject.workDoneProgress)
        }
    }
}
