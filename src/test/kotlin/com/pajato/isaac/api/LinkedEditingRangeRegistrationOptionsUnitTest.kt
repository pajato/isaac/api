package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.assertOptions
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertTrue

private typealias Options = LinkedEditingRangeRegistrationOptions

class LinkedEditingRangeRegistrationOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertAll(expected: Options, actual: Options) {
        assertTrue(actual is IsaacLinkedEditingRangeRegistrationOptions, getDiagnostic("type"))
        assertOptions(expected, actual)
    }

    @Test fun `When serializing a default linked editing range registration options object, verify result`() {
        val data = LinkedEditingRangeRegistrationOptions()
        val json = """{}"""

        testEncode(json, LinkedEditingRangeRegistrationOptionsSerializer, data)
        testDecode(json, LinkedEditingRangeRegistrationOptionsSerializer) { actual -> assertAll(data, actual) }
    }

    @Test fun `When decoding a linked editing range registration options object, verify a correct result`() {
        val filter = DocumentFilter("Kotlin", "scheme", "pattern")
        val data = LinkedEditingRangeRegistrationOptions(
            documentSelector = listOf(filter), workDoneProgress = true, id = "registrationId"
        )
        val json = """{"documentSelector":[{"language":"Kotlin","scheme":"scheme","pattern":"pattern"}],""" +
            """"workDoneProgress":true,"id":"registrationId"}"""

        testEncode(json, LinkedEditingRangeRegistrationOptionsSerializer, data)
        testDecode(json, LinkedEditingRangeRegistrationOptionsSerializer) { actual -> assertAll(data, actual) }
    }
}
