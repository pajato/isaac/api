package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlinx.serialization.json.Json
import java.lang.IllegalStateException
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class ReferencesProviderUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a boolean references provider object, verify a correct result()`() {
        fun assert(actualObject: ReferencesProvider, expected: Boolean) {
            assertTrue(actualObject is BooleanReferencesProvider)
            assertEquals(expected, actualObject.state)
        }

        val data: ReferencesProvider = BooleanReferencesProvider(true)
        val json = """{"state":true}"""

        testEncode(json, ReferencesProviderSerializer, data)
        testDecode(json, ReferencesProviderSerializer) { actualObject -> assert(actualObject, true) }
    }

    @Test fun `When serializing an options reference provider object, verify a correct result`() {
        fun assert(actualObject: ReferencesProvider, expected: Boolean) {
            assertTrue(actualObject is OptionsReferencesProvider)
            assertEquals(expected, actualObject.options.workDoneProgress)
        }

        val data: ReferencesProvider = OptionsReferencesProvider(ReferencesOptions(true))
        val json = """{"options":{"workDoneProgress":true}}"""

        testEncode(json, ReferencesProviderSerializer, data)
        testDecode(json, ReferencesProviderSerializer) { actualObject -> assert(actualObject, true) }
    }

    @Test
    fun `When invalid JSON is used, verify an illegal state exception is thrown`() {
        val incomingJson = """{"xyzzy":true}"""

        assertFailsWith<IllegalStateException> {
            Json.decodeFromString(ReferencesProviderSerializer, incomingJson)
        }
    }
}
