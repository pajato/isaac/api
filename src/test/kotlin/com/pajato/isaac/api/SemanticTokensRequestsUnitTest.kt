package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class SemanticTokensRequestsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default semantic tokens requests object, verify a correct result`() {
        val data = SemanticTokensRequests()
        val json = """{}"""

        testEncode(json, SemanticTokensRequestsSerializer, data)
        testDecode(json, SemanticTokensRequestsSerializer) { actualObject ->
            assertEquals(null, actualObject.range)
            assertEquals(null, actualObject.full)
        }
    }
}
