package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class LinkedEditingRangeOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertOptions(expected: Boolean?, actual: LinkedEditingRangeOptions) {
        assertEquals(expected, actual.workDoneProgress, getDiagnostic("work done progress"))
    }

    @Test fun `When serializing a linked editing range options default object, verify result`() {
        val data = LinkedEditingRangeOptions()
        val json = """{}"""

        testEncode(json, LinkedEditingRangeOptionsSerializer, data)
        testDecode(json, LinkedEditingRangeOptionsSerializer) { actual -> assertOptions(null, actual) }
    }

    @Test fun `When serializing a linked editing range options non-default object, verify result`() {
        val data = LinkedEditingRangeOptions(true)
        val json = """{"workDoneProgress":true}"""

        testEncode(json, LinkedEditingRangeOptionsSerializer, data)
        testDecode(json, LinkedEditingRangeOptionsSerializer) { actual -> assertOptions(true, actual) }
    }
}
