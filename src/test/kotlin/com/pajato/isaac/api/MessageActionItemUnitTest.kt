package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class MessageActionItemUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a message action item object, verify the result`() {
        val data = MessageActionItem(title = "Retry")
        val json = """{"title":"Retry"}"""

        testEncode(json, MessageActionItemSerializer, data)
        testDecode(json, MessageActionItemSerializer) { actual ->
            assertEquals("Retry", actual.title, getDiagnostic("title"))
        }
    }
}
