package com.pajato.isaac.api

import com.pajato.isaac.ApiTestProfiler
import kotlin.test.Test
import kotlin.test.assertEquals

class ImplementationOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing default implementation options, verify a correct result`() {
        val data = ImplementationOptions()
        val json = """{}"""

        testEncode(json, ImplementationOptionsSerializer, data)
        testDecode(json, ImplementationOptionsSerializer) { actualObject ->
            assertEquals(null, actualObject.workDoneProgress)
        }
    }

    @Test fun `When serializing implementation options with a boolean progress token, verify a correct result`() {
        val data = ImplementationOptions(true)
        val json = """{"workDoneProgress":true}"""

        testEncode(json, ImplementationOptionsSerializer, data)
        testDecode(json, ImplementationOptionsSerializer) { actualObject ->
            assertEquals(true, actualObject.workDoneProgress)
        }
    }
}
