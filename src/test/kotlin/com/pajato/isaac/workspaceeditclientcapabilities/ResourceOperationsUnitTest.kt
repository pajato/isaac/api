package com.pajato.isaac.workspaceeditclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.IsaacWorkspaceEditClientCapabilities
import com.pajato.isaac.api.ResourceOperationKind
import com.pajato.isaac.api.ResourceOperationKind.Create
import com.pajato.isaac.api.ResourceOperationKind.Delete
import com.pajato.isaac.api.ResourceOperationKind.Rename
import com.pajato.isaac.api.WorkspaceEditClientCapabilities
import com.pajato.isaac.api.WorkspaceEditClientSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ResourceOperationsUnitTest : ApiTestProfiler() {
    private val uut: WorkspaceEditClientCapabilities = IsaacWorkspaceEditClientCapabilities(
        resourceOperations = arrayOf(Create)
    )
    private val json = """{"resourceOperations":["Create"]}"""

    @Test
    fun `When encoding a workspace edit client capabilities object with a resource operations property, verify()`() {
        val actualJson = Json.encodeToString(WorkspaceEditClientSerializer, uut)

        assertEquals(json, actualJson)
    }

    @Test
    fun `When decoding a workspace edit client capabilities object with a resource operations property, verify()`() {
        val actualObject = Json.decodeFromString(WorkspaceEditClientSerializer, json)
        val resourceOperations = actualObject.resourceOperations

        assertTrue(actualObject is IsaacWorkspaceEditClientCapabilities)
        assertTrue(resourceOperations is Array<ResourceOperationKind>)
        assertEquals(1, resourceOperations.size)
        assertEquals(Create, resourceOperations[0])
    }

    @Test
    fun `Ensure that all resource operation kind values are accessed so no warnings are generated`() {
        assertTrue(ResourceOperationKind.values().contains(Create))
        assertTrue(ResourceOperationKind.values().contains(Delete))
        assertTrue(ResourceOperationKind.values().contains(Rename))
    }
}
