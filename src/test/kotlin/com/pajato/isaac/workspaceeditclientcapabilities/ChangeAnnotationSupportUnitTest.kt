package com.pajato.isaac.workspaceeditclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.ChangeAnnotationSupport
import com.pajato.isaac.api.IsaacChangeAnnotationSupport
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.WorkspaceEditClientCapabilities
import com.pajato.isaac.api.WorkspaceEditClientSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ChangeAnnotationSupportUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing with a document changes property, verify a correct result`() {
        val support = ChangeAnnotationSupport(groupsOnLabel = true)
        val data = WorkspaceEditClientCapabilities(changeAnnotationSupport = support)
        val json = """{"changeAnnotationSupport":{"groupsOnLabel":true}}"""

        testEncode(json, WorkspaceEditClientSerializer, data)
        testDecode(json, WorkspaceEditClientSerializer) { actualObject ->
            val changeAnnotationSupport = actualObject.changeAnnotationSupport

            assertTrue(changeAnnotationSupport is IsaacChangeAnnotationSupport)
            assertEquals(true, changeAnnotationSupport.groupsOnLabel)
        }
    }
}
