package com.pajato.isaac.workspaceeditclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.IsaacWorkspaceEditClientCapabilities
import com.pajato.isaac.api.WorkspaceEditClientCapabilities
import com.pajato.isaac.api.WorkspaceEditClientSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class NoPropertiesUnitTest : ApiTestProfiler() {
    private val uut: WorkspaceEditClientCapabilities = IsaacWorkspaceEditClientCapabilities()
    private val json = "{}"

    @Test
    fun `When encoding a fully empty workspace client capabilities object, verify the result()`() {
        val actualJson = Json.encodeToString(WorkspaceEditClientSerializer, uut)

        assertEquals(json, actualJson)
    }

    @Test
    fun `When decoding a fully empty workspace client capabilities object, verify the result()`() {
        val actualObject = Json.decodeFromString(WorkspaceEditClientSerializer, json)

        assertTrue(actualObject is IsaacWorkspaceEditClientCapabilities)
        assertEquals(null, actualObject.documentChanges)
        assertEquals(null, actualObject.resourceOperations)
        assertEquals(null, actualObject.failureHandling)
        assertEquals(null, actualObject.normalizesLineEndings)
        assertEquals(null, actualObject.changeAnnotationSupport)
    }
}
