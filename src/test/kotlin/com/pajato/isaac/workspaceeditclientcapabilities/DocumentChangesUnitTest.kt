package com.pajato.isaac.workspaceeditclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.IsaacWorkspaceEditClientCapabilities
import com.pajato.isaac.api.WorkspaceEditClientCapabilities
import com.pajato.isaac.api.WorkspaceEditClientSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DocumentChangesUnitTest : ApiTestProfiler() {
    private val uut: WorkspaceEditClientCapabilities = IsaacWorkspaceEditClientCapabilities(
        documentChanges = true
    )
    private val json = """{"documentChanges":true}"""

    @Test
    fun `When encoding a workspace edit client capabilities object with a document changes property, verify()`() {
        val actualJson = Json.encodeToString(WorkspaceEditClientSerializer, uut)

        assertEquals(json, actualJson)
    }

    @Test
    fun `When decoding a workspace edit client capabilities object with a document changes property, verify()`() {
        val actualObject = Json.decodeFromString(WorkspaceEditClientSerializer, json)

        assertTrue(actualObject is IsaacWorkspaceEditClientCapabilities)
        assertEquals(true, actualObject.documentChanges)
    }
}
