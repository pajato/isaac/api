package com.pajato.isaac.workspaceeditclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.FailureHandlingKind
import com.pajato.isaac.api.FailureHandlingKind.Abort
import com.pajato.isaac.api.FailureHandlingKind.TextOnlyTransactional
import com.pajato.isaac.api.FailureHandlingKind.Transactional
import com.pajato.isaac.api.FailureHandlingKind.Undo
import com.pajato.isaac.api.IsaacWorkspaceEditClientCapabilities
import com.pajato.isaac.api.WorkspaceEditClientCapabilities
import com.pajato.isaac.api.WorkspaceEditClientSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class FailureHandlingUnitTest : ApiTestProfiler() {
    private val uut: WorkspaceEditClientCapabilities = IsaacWorkspaceEditClientCapabilities(
        failureHandling = Abort
    )
    private val json = """{"failureHandling":"Abort"}"""

    @Test
    fun `When encoding a workspace edit client capabilities object with a failure handling property, verify()`() {
        val actualJson = Json.encodeToString(WorkspaceEditClientSerializer, uut)

        assertEquals(json, actualJson)
    }

    @Test
    fun `When decoding a workspace edit client capabilities object with a failure handling property, verify()`() {
        val actualObject = Json.decodeFromString(WorkspaceEditClientSerializer, json)
        val failureHandling = actualObject.failureHandling

        assertTrue(actualObject is IsaacWorkspaceEditClientCapabilities)
        assertTrue(failureHandling is FailureHandlingKind)
        assertEquals(Abort, failureHandling)
    }

    @Test
    fun `Ensure that all failure handling kind values are accessed so no warnings are generated`() {
        assertTrue(FailureHandlingKind.values().contains(Abort))
        assertTrue(FailureHandlingKind.values().contains(Transactional))
        assertTrue(FailureHandlingKind.values().contains(Undo))
        assertTrue(FailureHandlingKind.values().contains(TextOnlyTransactional))
    }
}
