package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.DocumentFormattingClientCapabilities
import com.pajato.isaac.api.IsaacDocumentFormattingClientCapabilities
import com.pajato.isaac.api.IsaacTextDocumentClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class FormattingUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a dynamic registration formatting property, verify()`() {
        val prop = IsaacDocumentFormattingClientCapabilities(dynamicRegistration = true)
        val data: TextDocumentClientCapabilities = IsaacTextDocumentClientCapabilities(formatting = prop)
        val json = """{"formatting":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val formatting: DocumentFormattingClientCapabilities = actualObject.formatting!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject is IsaacTextDocumentClientCapabilities)
            assertTrue(actualObject.formatting is IsaacDocumentFormattingClientCapabilities)
            assertEquals(true, formatting.dynamicRegistration, errorMessage)
        }
    }
}
