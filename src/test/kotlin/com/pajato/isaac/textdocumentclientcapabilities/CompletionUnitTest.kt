package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.CompletionClientCapabilities
import com.pajato.isaac.api.IsaacCompletionClientCapabilities
import com.pajato.isaac.api.IsaacTextDocumentClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class CompletionUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a dynamic registration completion property, verify()`() {
        val prop = IsaacCompletionClientCapabilities(dynamicRegistration = true)
        val data: TextDocumentClientCapabilities = IsaacTextDocumentClientCapabilities(completion = prop)
        val json = """{"completion":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val completion: CompletionClientCapabilities = actualObject.completion!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject is IsaacTextDocumentClientCapabilities)
            assertTrue(actualObject.completion is IsaacCompletionClientCapabilities)
            assertEquals(true, completion.dynamicRegistration, errorMessage)
        }
    }
}
