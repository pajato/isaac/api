package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.IsaacTextDocumentClientCapabilities
import com.pajato.isaac.api.IsaacTypeDefinitionClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import com.pajato.isaac.api.TypeDefinitionClientCapabilities
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TypeDefinitionUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a dynamic registration type definition property, verify()`() {
        val prop = IsaacTypeDefinitionClientCapabilities(dynamicRegistration = true)
        val data: TextDocumentClientCapabilities = IsaacTextDocumentClientCapabilities(typeDefinition = prop)
        val json = """{"typeDefinition":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val typeDefinition: TypeDefinitionClientCapabilities = actualObject.typeDefinition!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject is IsaacTextDocumentClientCapabilities)
            assertTrue(actualObject.typeDefinition is IsaacTypeDefinitionClientCapabilities)
            assertEquals(true, typeDefinition.dynamicRegistration, errorMessage)
        }
    }

    @Test fun `When serializing using a link support type definition property, verify()`() {
        val prop = IsaacTypeDefinitionClientCapabilities(linkSupport = true)
        val data: TextDocumentClientCapabilities = IsaacTextDocumentClientCapabilities(typeDefinition = prop)
        val json = """{"typeDefinition":{"linkSupport":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val typeDefinition: TypeDefinitionClientCapabilities = actualObject.typeDefinition!!
            val errorMessage = "The link support flag is wrong!"

            assertTrue(actualObject is IsaacTextDocumentClientCapabilities)
            assertTrue(actualObject.typeDefinition is IsaacTypeDefinitionClientCapabilities)
            assertEquals(true, typeDefinition.linkSupport, errorMessage)
        }
    }
}
