package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.FoldingRangeClientCapabilities
import com.pajato.isaac.api.IsaacFoldingRangeClientCapabilities
import com.pajato.isaac.api.IsaacTextDocumentClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class FoldingRangeClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a dynamic registration folding range property, verify()`() {
        val range = FoldingRangeClientCapabilities(dynamicRegistration = true)
        val data = TextDocumentClientCapabilities(foldingRange = range)
        val json = """{"foldingRange":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val foldingRange: FoldingRangeClientCapabilities = actualObject.foldingRange!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject is IsaacTextDocumentClientCapabilities)
            assertTrue(actualObject.foldingRange is FoldingRangeClientCapabilities)
            assertEquals(true, foldingRange.dynamicRegistration, errorMessage)
        }
    }

    @Test fun `When serializing using a range limit folding range property, verify()`() {
        val rangeLimit = 12
        val range = FoldingRangeClientCapabilities(rangeLimit = rangeLimit)
        val data = TextDocumentClientCapabilities(foldingRange = range)
        val json = """{"foldingRange":{"rangeLimit":12}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val foldingRange: FoldingRangeClientCapabilities = actualObject.foldingRange!!
            val errorMessage = "The range limit value is wrong!"

            assertTrue(actualObject.foldingRange is IsaacFoldingRangeClientCapabilities)
            assertEquals(rangeLimit, foldingRange.rangeLimit, errorMessage)
        }
    }

    @Test fun `When serializing using a line folding only property, verify()`() {
        val range = FoldingRangeClientCapabilities(lineFoldingOnly = true)
        val data = TextDocumentClientCapabilities(foldingRange = range)
        val json = """{"foldingRange":{"lineFoldingOnly":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val foldingRange: FoldingRangeClientCapabilities = actualObject.foldingRange!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject.foldingRange is IsaacFoldingRangeClientCapabilities)
            assertEquals(true, foldingRange.lineFoldingOnly, errorMessage)
        }
    }
}
