package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.DiagnosticTag
import com.pajato.isaac.api.DiagnosticTagSupport
import com.pajato.isaac.api.PublishDiagnosticsClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class PublishDiagnosticsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a related information publish diagnostics property, verify()`() {
        val prop = PublishDiagnosticsClientCapabilities(relatedInformation = true)
        val data = TextDocumentClientCapabilities(publishDiagnostics = prop)
        val json = """{"publishDiagnostics":{"relatedInformation":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val publishDiagnostics = actualObject.publishDiagnostics
            val errorMessage = "The related information flag is wrong!"

            assertTrue(publishDiagnostics != null)
            assertEquals(true, publishDiagnostics.relatedInformation, errorMessage)
        }
    }

    @Test fun `When serializing using a tag support publish diagnostics property, verify()`() {
        val support = DiagnosticTagSupport(arrayOf(DiagnosticTag.Deprecated))
        val prop = PublishDiagnosticsClientCapabilities(tagSupport = support)
        val data = TextDocumentClientCapabilities(publishDiagnostics = prop)
        val json = """{"publishDiagnostics":{"tagSupport":{"valueSet":["Deprecated"]}}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val publishDiagnostics = actualObject.publishDiagnostics
            val sizeErrorMessage = "The value set array size is wrong!"
            val errorMessage = "The tag support flag is wrong!"

            assertTrue(publishDiagnostics != null)
            assertTrue(publishDiagnostics.tagSupport is DiagnosticTagSupport)
            assertTrue(publishDiagnostics.tagSupport != null)
            assertEquals(1, publishDiagnostics.tagSupport!!.valueSet.size, sizeErrorMessage)
            assertEquals(DiagnosticTag.Deprecated, publishDiagnostics.tagSupport!!.valueSet[0], errorMessage)
        }
    }

    @Test fun `When serializing using a version support publish diagnostics property, verify()`() {
        val prop = PublishDiagnosticsClientCapabilities(versionSupport = true)
        val data = TextDocumentClientCapabilities(publishDiagnostics = prop)
        val json = """{"publishDiagnostics":{"versionSupport":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val publishDiagnostics = actualObject.publishDiagnostics
            val errorMessage = "The version support flag is wrong!"

            assertTrue(publishDiagnostics != null)
            assertEquals(true, publishDiagnostics.versionSupport, errorMessage)
        }
    }

    @Test fun `When serializing using a code description support publish diagnostics property, verify()`() {
        val prop = PublishDiagnosticsClientCapabilities(codeDescriptionSupport = true)
        val data = TextDocumentClientCapabilities(publishDiagnostics = prop)
        val json = """{"publishDiagnostics":{"codeDescriptionSupport":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val publishDiagnostics = actualObject.publishDiagnostics
            val errorMessage = "The code description support flag is wrong!"

            assertTrue(publishDiagnostics != null)
            assertEquals(true, publishDiagnostics.codeDescriptionSupport, errorMessage)
        }
    }

    @Test fun `When serializing using a data support publish diagnostics property, verify a correct result`() {
        val prop = PublishDiagnosticsClientCapabilities(dataSupport = true)
        val data = TextDocumentClientCapabilities(publishDiagnostics = prop)
        val json = """{"publishDiagnostics":{"dataSupport":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val publishDiagnostics = actualObject.publishDiagnostics
            val errorMessage = "The data support flag is wrong!"

            assertTrue(publishDiagnostics != null)
            assertEquals(true, publishDiagnostics.dataSupport, errorMessage)
        }
    }
}
