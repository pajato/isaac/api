package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.IsaacTextDocumentClientCapabilities
import com.pajato.isaac.api.IsaacTextDocumentSyncClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import com.pajato.isaac.api.TextDocumentSyncClientCapabilities
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class SynchronizationUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a dynamic registration synchronization property, verify()`() {
        val prop = IsaacTextDocumentSyncClientCapabilities(dynamicRegistration = true)
        val data: TextDocumentClientCapabilities = IsaacTextDocumentClientCapabilities(synchronization = prop)
        val json = """{"synchronization":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val synchronization: TextDocumentSyncClientCapabilities = actualObject.synchronization!!
            val errorMessage = "The dynamic registration flag value is wrong!"

            assertTrue(actualObject is IsaacTextDocumentClientCapabilities)
            assertTrue(actualObject.synchronization is IsaacTextDocumentSyncClientCapabilities)
            assertEquals(true, synchronization.dynamicRegistration, errorMessage)
        }
    }

    @Test fun `When serializing using a will save synchronization property, verify()`() {
        val prop = IsaacTextDocumentSyncClientCapabilities(willSave = true)
        val data: TextDocumentClientCapabilities = IsaacTextDocumentClientCapabilities(synchronization = prop)
        val json = """{"synchronization":{"willSave":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val synchronization: TextDocumentSyncClientCapabilities = actualObject.synchronization!!
            val errorMessage = "The will save flag value is wrong!"

            assertTrue(actualObject is IsaacTextDocumentClientCapabilities)
            assertTrue(actualObject.synchronization is IsaacTextDocumentSyncClientCapabilities)
            assertEquals(true, synchronization.willSave, errorMessage)
        }
    }

    @Test fun `When serializing using a will save wait until synchronization property, verify()`() {
        val prop = IsaacTextDocumentSyncClientCapabilities(willSaveWaitUntil = true)
        val data: TextDocumentClientCapabilities = IsaacTextDocumentClientCapabilities(synchronization = prop)
        val json = """{"synchronization":{"willSaveWaitUntil":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val synchronization: TextDocumentSyncClientCapabilities = actualObject.synchronization!!
            val errorMessage = "The will save wait until flag value is wrong!"

            assertTrue(actualObject is IsaacTextDocumentClientCapabilities)
            assertTrue(actualObject.synchronization is IsaacTextDocumentSyncClientCapabilities)
            assertEquals(true, synchronization.willSaveWaitUntil, errorMessage)
        }
    }

    @Test fun `When serializing using a did save synchronization property, verify()`() {
        val prop = IsaacTextDocumentSyncClientCapabilities(didSave = true)
        val data: TextDocumentClientCapabilities = IsaacTextDocumentClientCapabilities(synchronization = prop)
        val json = """{"synchronization":{"didSave":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val synchronization: TextDocumentSyncClientCapabilities = actualObject.synchronization!!
            val errorMessage = "The did save flag value is wrong!"

            assertTrue(actualObject is IsaacTextDocumentClientCapabilities)
            assertTrue(actualObject.synchronization is IsaacTextDocumentSyncClientCapabilities)
            assertEquals(true, synchronization.didSave, errorMessage)
        }
    }
}
