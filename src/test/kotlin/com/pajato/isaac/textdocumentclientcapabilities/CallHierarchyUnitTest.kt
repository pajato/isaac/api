package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.CallHierarchyClientCapabilities
import com.pajato.isaac.api.IsaacCallHierarchyClientCapabilities
import com.pajato.isaac.api.IsaacTextDocumentClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class CallHierarchyUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a dynamic registration linked editing property, verify()`() {
        val prop = CallHierarchyClientCapabilities(dynamicRegistration = true)
        val data: TextDocumentClientCapabilities = IsaacTextDocumentClientCapabilities(callHierarchy = prop)
        val json = """{"callHierarchy":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val callHierarchy: CallHierarchyClientCapabilities = actualObject.callHierarchy!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject is IsaacTextDocumentClientCapabilities)
            assertTrue(actualObject.callHierarchy is IsaacCallHierarchyClientCapabilities)
            assertEquals(true, callHierarchy.dynamicRegistration, errorMessage)
        }
    }
}
