package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.IsaacMonikerClientCapabilities
import com.pajato.isaac.api.IsaacTextDocumentClientCapabilities
import com.pajato.isaac.api.MonikerClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class MonikerUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a dynamic registration moniker property, verify()`() {
        val prop = IsaacMonikerClientCapabilities(dynamicRegistration = true)
        val data: TextDocumentClientCapabilities = IsaacTextDocumentClientCapabilities(moniker = prop)
        val json = """{"moniker":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val moniker: MonikerClientCapabilities = actualObject.moniker!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject is IsaacTextDocumentClientCapabilities)
            assertTrue(actualObject.moniker is IsaacMonikerClientCapabilities)
            assertEquals(true, moniker.dynamicRegistration, errorMessage)
        }
    }
}
