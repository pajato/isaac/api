package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.DocumentRangeFormattingClientCapabilities
import com.pajato.isaac.api.IsaacDocumentRangeFormattingClientCapabilities
import com.pajato.isaac.api.IsaacTextDocumentClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class RangeFormattingUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a dynamic registration formatting property, verify()`() {
        val prop = IsaacDocumentRangeFormattingClientCapabilities(dynamicRegistration = true)
        val data: TextDocumentClientCapabilities = IsaacTextDocumentClientCapabilities(rangeFormatting = prop)
        val json = """{"rangeFormatting":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val rangeFormatting: DocumentRangeFormattingClientCapabilities = actualObject.rangeFormatting!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject is IsaacTextDocumentClientCapabilities)
            assertTrue(actualObject.rangeFormatting is IsaacDocumentRangeFormattingClientCapabilities)
            assertEquals(true, rangeFormatting.dynamicRegistration, errorMessage)
        }
    }
}
