package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.DefinitionClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DefinitionUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a dynamic registration definition property, verify()`() {
        val prop = DefinitionClientCapabilities(dynamicRegistration = true)
        val data = TextDocumentClientCapabilities(definition = prop)
        val json = """{"definition":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val definition: DefinitionClientCapabilities = actualObject.definition!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject.definition is DefinitionClientCapabilities)
            assertEquals(true, definition.dynamicRegistration, errorMessage)
        }
    }

    @Test fun `When serializing using a link support definition property, verify()`() {
        val prop = DefinitionClientCapabilities(linkSupport = true)
        val data = TextDocumentClientCapabilities(definition = prop)
        val json = """{"definition":{"linkSupport":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val definition: DefinitionClientCapabilities = actualObject.definition!!
            val errorMessage = "The link support flag is wrong!"

            assertTrue(actualObject.definition is DefinitionClientCapabilities)
            assertEquals(true, definition.linkSupport, errorMessage)
        }
    }
}
