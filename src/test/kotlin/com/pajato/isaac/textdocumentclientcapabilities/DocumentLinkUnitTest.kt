package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.DocumentLinkClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DocumentLinkUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a dynamic registration document link property, verify()`() {
        val prop = DocumentLinkClientCapabilities(dynamicRegistration = true)
        val data = TextDocumentClientCapabilities(documentLink = prop)
        val json = """{"documentLink":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val documentLink: DocumentLinkClientCapabilities = actualObject.documentLink!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject.documentLink is DocumentLinkClientCapabilities)
            assertEquals(true, documentLink.dynamicRegistration, errorMessage)
        }
    }

    @Test fun `When serializing using a tooltip support document link property, verify()`() {
        val prop = DocumentLinkClientCapabilities(tooltipSupport = true)
        val data = TextDocumentClientCapabilities(documentLink = prop)
        val json = """{"documentLink":{"tooltipSupport":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val documentLink: DocumentLinkClientCapabilities = actualObject.documentLink!!
            val errorMessage = "The tooltip support flag is wrong!"

            assertTrue(actualObject.documentLink is DocumentLinkClientCapabilities)
            assertEquals(true, documentLink.tooltipSupport, errorMessage)
        }
    }
}
