package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.IsaacSelectionRangeClientCapabilities
import com.pajato.isaac.api.IsaacTextDocumentClientCapabilities
import com.pajato.isaac.api.SelectionRangeClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class SelectionRangeUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a dynamic registration selection range property, verify()`() {
        val prop = IsaacSelectionRangeClientCapabilities(dynamicRegistration = true)
        val data: TextDocumentClientCapabilities = IsaacTextDocumentClientCapabilities(selectionRange = prop)
        val json = """{"selectionRange":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val selectionRange: SelectionRangeClientCapabilities = actualObject.selectionRange!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject is IsaacTextDocumentClientCapabilities)
            assertTrue(actualObject.selectionRange is IsaacSelectionRangeClientCapabilities)
            assertEquals(true, selectionRange.dynamicRegistration, errorMessage)
        }
    }
}
