package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.IsaacLinkedEditingRangeClientCapabilities
import com.pajato.isaac.api.IsaacTextDocumentClientCapabilities
import com.pajato.isaac.api.LinkedEditingRangeClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class LinkedEditingRangeUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a dynamic registration linked editing range property, verify()`() {
        val prop = IsaacLinkedEditingRangeClientCapabilities(dynamicRegistration = true)
        val data: TextDocumentClientCapabilities = IsaacTextDocumentClientCapabilities(linkedEditingRange = prop)
        val json = """{"linkedEditingRange":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val linkedEditingRange: LinkedEditingRangeClientCapabilities = actualObject.linkedEditingRange!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject is IsaacTextDocumentClientCapabilities)
            assertTrue(actualObject.linkedEditingRange is IsaacLinkedEditingRangeClientCapabilities)
            assertEquals(true, linkedEditingRange.dynamicRegistration, errorMessage)
        }
    }
}
