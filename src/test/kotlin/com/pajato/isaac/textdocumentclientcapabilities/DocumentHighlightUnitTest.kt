package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.DocumentHighlightClientCapabilities
import com.pajato.isaac.api.IsaacDocumentHighlightClientCapabilities
import com.pajato.isaac.api.IsaacTextDocumentClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DocumentHighlightUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a dynamic registration document highlight property, verify()`() {
        val prop = IsaacDocumentHighlightClientCapabilities(dynamicRegistration = true)
        val data: TextDocumentClientCapabilities = IsaacTextDocumentClientCapabilities(documentHighlight = prop)
        val json = """{"documentHighlight":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val documentHighlight: DocumentHighlightClientCapabilities = actualObject.documentHighlight!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject is IsaacTextDocumentClientCapabilities)
            assertTrue(actualObject.documentHighlight is IsaacDocumentHighlightClientCapabilities)
            assertEquals(true, documentHighlight.dynamicRegistration, errorMessage)
        }
    }
}
