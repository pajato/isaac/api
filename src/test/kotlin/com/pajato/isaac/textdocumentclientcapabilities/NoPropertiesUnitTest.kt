package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals

class NoPropertiesUnitTest : SerializerTest, ApiTestProfiler() {
    private val data = TextDocumentClientCapabilities()
    private val json = """{}"""

    @Test fun `When serializing a fully empty text document client capabilities object, verify the result()`() {
        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { assertDecodedObject(it) }
    }

    private fun assertDecodedObject(actualObject: TextDocumentClientCapabilities) {
        assertEquals(null, actualObject.synchronization)
        assertEquals(null, actualObject.completion)
        assertEquals(null, actualObject.hover)
        assertEquals(null, actualObject.signatureHelp)
        assertEquals(null, actualObject.declaration)
        assertEquals(null, actualObject.definition)
        assertEquals(null, actualObject.typeDefinition)
        assertEquals(null, actualObject.implementation)
        assertEquals(null, actualObject.references)
        assertEquals(null, actualObject.documentHighlight)
        assertEquals(null, actualObject.documentSymbol)
        assertEquals(null, actualObject.codeAction)
        assertEquals(null, actualObject.codeLens)
        assertEquals(null, actualObject.documentLink)
        assertEquals(null, actualObject.colorProvider)
        assertEquals(null, actualObject.formatting)
        assertEquals(null, actualObject.rangeFormatting)
        assertEquals(null, actualObject.onTypeFormatting)
        assertEquals(null, actualObject.rename)
        assertEquals(null, actualObject.publishDiagnostics)
        assertEquals(null, actualObject.foldingRange)
        assertEquals(null, actualObject.selectionRange)
        assertEquals(null, actualObject.linkedEditingRange)
        assertEquals(null, actualObject.callHierarchy)
        assertEquals(null, actualObject.semanticTokens)
        assertEquals(null, actualObject.moniker)
    }
}
