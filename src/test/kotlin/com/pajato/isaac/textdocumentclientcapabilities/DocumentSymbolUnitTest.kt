package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.DocumentSymbolClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.SymbolKind
import com.pajato.isaac.api.SymbolTag.Deprecated
import com.pajato.isaac.api.SymbolTagSupport
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DocumentSymbolUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a dynamic registration document symbol property, verify()`() {
        val prop = DocumentSymbolClientCapabilities(dynamicRegistration = true)
        val data = TextDocumentClientCapabilities(documentSymbol = prop)
        val json = """{"documentSymbol":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val documentSymbol: DocumentSymbolClientCapabilities = actualObject.documentSymbol!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject.documentSymbol is DocumentSymbolClientCapabilities)
            assertEquals(true, documentSymbol.dynamicRegistration, errorMessage)
        }
    }

    @Test fun `When serializing using a symbol kind document symbol property, verify()`() {
        val prop = DocumentSymbolClientCapabilities(symbolKind = SymbolKind.Array)
        val data = TextDocumentClientCapabilities(documentSymbol = prop)
        val json = """{"documentSymbol":{"symbolKind":"Array"}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val documentSymbol: DocumentSymbolClientCapabilities = actualObject.documentSymbol!!
            val errorMessage = "The symbol kind value is wrong!"

            assertTrue(actualObject.documentSymbol is DocumentSymbolClientCapabilities)
            assertEquals(SymbolKind.Array, documentSymbol.symbolKind, errorMessage)
        }
    }

    @Test fun `When serializing using a hierarchical document symbol property, verify()`() {
        val prop = DocumentSymbolClientCapabilities(hierarchicalDocumentSymbolSupport = true)
        val data = TextDocumentClientCapabilities(documentSymbol = prop)
        val json = """{"documentSymbol":{"hierarchicalDocumentSymbolSupport":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val documentSymbol: DocumentSymbolClientCapabilities = actualObject.documentSymbol!!
            val errorMessage = "The hierarchical document symbol support flag is wrong!"

            assertTrue(actualObject.documentSymbol is DocumentSymbolClientCapabilities)
            assertEquals(true, documentSymbol.hierarchicalDocumentSymbolSupport, errorMessage)
        }
    }

    @Test fun `When serializing using a tag support document symbol property, verify()`() {
        val prop = DocumentSymbolClientCapabilities(tagSupport = SymbolTagSupport(arrayOf(Deprecated)))
        val data = TextDocumentClientCapabilities(documentSymbol = prop)
        val json = """{"documentSymbol":{"tagSupport":{"valueSet":["Deprecated"]}}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val documentSymbol: DocumentSymbolClientCapabilities = actualObject.documentSymbol!!
            val tagSupport = documentSymbol.tagSupport
            val errorMessage = "The tag support value set element is wrong!"

            assertTrue(actualObject.documentSymbol is DocumentSymbolClientCapabilities)
            assertTrue(tagSupport is SymbolTagSupport)
            assertEquals(1, tagSupport.valueSet.size, "The tag support value set size is wrong!")
            assertEquals(Deprecated, tagSupport.valueSet[0], errorMessage)
        }
    }

    @Test fun `When serializing using a label support document symbol property, verify()`() {
        val prop = DocumentSymbolClientCapabilities(labelSupport = true)
        val data = TextDocumentClientCapabilities(documentSymbol = prop)
        val json = """{"documentSymbol":{"labelSupport":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val documentSymbol: DocumentSymbolClientCapabilities = actualObject.documentSymbol!!
            val errorMessage = "The label support flag is wrong!"

            assertTrue(actualObject.documentSymbol is DocumentSymbolClientCapabilities)
            assertEquals(true, documentSymbol.labelSupport, errorMessage)
        }
    }
}
