package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.SemanticTokensClientCapabilities
import com.pajato.isaac.api.SemanticTokensRequests
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import com.pajato.isaac.api.TokenFormat
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class SemanticTokensUnitTest : SerializerTest, ApiTestProfiler() {
    private val tokenModifier = "token modifier"
    private val tokenType = "token type"

    @Test fun `When serializing using a default semantic tokens property, verify()`() {
        val prop = SemanticTokensClientCapabilities(
            formats = arrayOf(TokenFormat.Relative),
            tokenModifiers = arrayOf(tokenModifier), tokenTypes = arrayOf(tokenType)
        )
        val data = TextDocumentClientCapabilities(semanticTokens = prop)
        val json = """{"semanticTokens":{""" +
            """"tokenTypes":["token type"],"tokenModifiers":["token modifier"],"formats":["Relative"]}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val semanticTokens: SemanticTokensClientCapabilities = actualObject.semanticTokens!!
            fun getSizeErrorMessage(name: String) = "The $name array size is wrong!"
            fun getValueErrorMessage(name: String) = "The $name array value is wrong!"

            assertTrue(actualObject.semanticTokens is SemanticTokensClientCapabilities)
            assertEquals(1, semanticTokens.formats.size, getSizeErrorMessage("formats"))
            assertEquals(TokenFormat.Relative, semanticTokens.formats[0], getValueErrorMessage("formats"))
            assertEquals(1, semanticTokens.tokenModifiers.size, getSizeErrorMessage("tokenModifiers"))
            assertEquals(tokenModifier, semanticTokens.tokenModifiers[0], getValueErrorMessage("tokenModifiers"))
            assertEquals(1, semanticTokens.tokenTypes.size, getSizeErrorMessage("tokenTypes"))
            assertEquals(tokenType, semanticTokens.tokenTypes[0], getValueErrorMessage("tokenTypes"))
        }
    }

    @Test fun `When serializing using a dynamic registration semantic tokens property, verify()`() {
        val prop = SemanticTokensClientCapabilities(
            dynamicRegistration = true, formats = arrayOf(TokenFormat.Relative),
            tokenModifiers = arrayOf(tokenModifier), tokenTypes = arrayOf(tokenType)
        )
        val data = TextDocumentClientCapabilities(semanticTokens = prop)
        val json = """{"semanticTokens":{"dynamicRegistration":true,""" +
            """"tokenTypes":["token type"],"tokenModifiers":["token modifier"],"formats":["Relative"]}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val semanticTokens: SemanticTokensClientCapabilities = actualObject.semanticTokens!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject.semanticTokens is SemanticTokensClientCapabilities)
            assertEquals(true, semanticTokens.dynamicRegistration, errorMessage)
        }
    }

    @Test fun `When serializing using a requests semantic tokens property, verify()`() {
        val prop = SemanticTokensClientCapabilities(
            requests = SemanticTokensRequests(range = true),
            formats = arrayOf(TokenFormat.Relative), tokenModifiers = arrayOf(tokenModifier),
            tokenTypes = arrayOf(tokenType)
        )
        val data = TextDocumentClientCapabilities(semanticTokens = prop)
        val json = """{"semanticTokens":{"requests":{"range":true},""" +
            """"tokenTypes":["token type"],"tokenModifiers":["token modifier"],"formats":["Relative"]}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val semanticTokens = actualObject.semanticTokens
            val errorMessage = "The requests range flag is wrong!"

            assertTrue(semanticTokens != null)
            assertTrue(semanticTokens.requests != null)
            assertEquals(true, semanticTokens.requests!!.range, errorMessage)
        }
    }

    @Test fun `When serializing using a overlapping token support semantic tokens property, verify()`() {
        val prop = SemanticTokensClientCapabilities(
            overlappingTokenSupport = true,
            formats = arrayOf(TokenFormat.Relative), tokenModifiers = arrayOf(tokenModifier),
            tokenTypes = arrayOf(tokenType)
        )
        val data = TextDocumentClientCapabilities(semanticTokens = prop)
        val json = """{"semanticTokens":{""" +
            """"tokenTypes":["token type"],"tokenModifiers":["token modifier"],"formats":["Relative"],""" +
            """"overlappingTokenSupport":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val semanticTokens: SemanticTokensClientCapabilities = actualObject.semanticTokens!!
            val errorMessage = "The overlapping token support flag is wrong!"

            assertTrue(actualObject.semanticTokens is SemanticTokensClientCapabilities)
            assertEquals(true, semanticTokens.overlappingTokenSupport, errorMessage)
        }
    }

    @Test fun `When serializing using a multiline token support semantic tokens property, verify()`() {
        val prop = SemanticTokensClientCapabilities(
            multilineTokenSupport = true,
            formats = arrayOf(TokenFormat.Relative), tokenModifiers = arrayOf(tokenModifier),
            tokenTypes = arrayOf(tokenType)
        )
        val data = TextDocumentClientCapabilities(semanticTokens = prop)
        val json = """{"semanticTokens":{""" +
            """"tokenTypes":["token type"],"tokenModifiers":["token modifier"],"formats":["Relative"],""" +
            """"multilineTokenSupport":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val semanticTokens: SemanticTokensClientCapabilities = actualObject.semanticTokens!!
            val errorMessage = "The overlapping token support flag is wrong!"

            assertTrue(actualObject.semanticTokens is SemanticTokensClientCapabilities)
            assertEquals(true, semanticTokens.multilineTokenSupport, errorMessage)
        }
    }
}
