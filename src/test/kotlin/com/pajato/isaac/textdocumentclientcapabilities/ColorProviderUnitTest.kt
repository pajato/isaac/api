package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.DocumentColorClientCapabilities
import com.pajato.isaac.api.IsaacDocumentColorClientCapabilities
import com.pajato.isaac.api.IsaacTextDocumentClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ColorProviderUnitTest : SerializerTest, ApiTestProfiler() {

    @Test
    fun `When serializing using a dynamic registration color provider property, verify()`() {
        val prop = IsaacDocumentColorClientCapabilities(dynamicRegistration = true)
        val data: TextDocumentClientCapabilities = IsaacTextDocumentClientCapabilities(colorProvider = prop)
        val json = """{"colorProvider":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val colorProvider: DocumentColorClientCapabilities = actualObject.colorProvider!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject is IsaacTextDocumentClientCapabilities)
            assertTrue(actualObject.colorProvider is IsaacDocumentColorClientCapabilities)
            assertEquals(true, colorProvider.dynamicRegistration, errorMessage)
        }
    }
}
