package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.HoverClientCapabilities
import com.pajato.isaac.api.MarkupKind.Markdown
import com.pajato.isaac.api.MarkupKind.PlainText
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class HoverUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a dynamic registration hover property, verify()`() {
        val prop = HoverClientCapabilities(dynamicRegistration = true)
        val data = TextDocumentClientCapabilities(hover = prop)
        val json = """{"hover":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val hover: HoverClientCapabilities = actualObject.hover!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject.hover is HoverClientCapabilities)
            assertEquals(true, hover.dynamicRegistration, errorMessage)
        }
    }

    @Test fun `When serializing using a markdown content format hover property, verify()`() {
        val prop = HoverClientCapabilities(contentFormat = arrayOf(Markdown, PlainText))
        val data = TextDocumentClientCapabilities(hover = prop)
        val json = """{"hover":{"contentFormat":["Markdown","PlainText"]}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val hover: HoverClientCapabilities = actualObject.hover!!

            assertTrue(actualObject.hover is HoverClientCapabilities)
            assertEquals(2, hover.contentFormat!!.size, "The content format array size is wrong!")
            assertEquals(Markdown, hover.contentFormat!![0])
            assertEquals(PlainText, hover.contentFormat!![1])
        }
    }
}
