package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.PrepareSupportDefaultBehavior
import com.pajato.isaac.api.RenameClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class RenameUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a dynamic registration rename property, verify()`() {
        val prop = RenameClientCapabilities(dynamicRegistration = true)
        val data = TextDocumentClientCapabilities(rename = prop)
        val json = """{"rename":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val rename: RenameClientCapabilities = actualObject.rename!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject.rename is RenameClientCapabilities)
            assertEquals(true, rename.dynamicRegistration, errorMessage)
        }
    }

    @Test fun `When serializing using a prepare support rename property, verify()`() {
        val prop = RenameClientCapabilities(prepareSupport = true)
        val data = TextDocumentClientCapabilities(rename = prop)
        val json = """{"rename":{"prepareSupport":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val rename: RenameClientCapabilities = actualObject.rename!!
            val errorMessage = "The prepare support flag is wrong!"

            assertTrue(actualObject.rename is RenameClientCapabilities)
            assertEquals(true, rename.prepareSupport, errorMessage)
        }
    }

    @Test fun `When serializing using a prepare support default behavior rename property, verify()`() {
        val behavior: PrepareSupportDefaultBehavior = PrepareSupportDefaultBehavior.Identifier
        val prop = RenameClientCapabilities(prepareSupportDefaultBehavior = behavior)
        val data = TextDocumentClientCapabilities(rename = prop)
        val json = """{"rename":{"prepareSupportDefaultBehavior":"Identifier"}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val rename: RenameClientCapabilities = actualObject.rename!!
            val errorMessage = "The prepare support default behavior value is wrong!"

            assertTrue(actualObject.rename is RenameClientCapabilities)
            assertEquals(PrepareSupportDefaultBehavior.Identifier, rename.prepareSupportDefaultBehavior, errorMessage)
        }
    }

    @Test fun `When serializing using a honors change annotations rename property, verify()`() {
        val prop = RenameClientCapabilities(honorsChangeAnnotations = true)
        val data = TextDocumentClientCapabilities(rename = prop)
        val json = """{"rename":{"honorsChangeAnnotations":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val rename: RenameClientCapabilities = actualObject.rename!!
            val errorMessage = "The honors change annotations flag is wrong!"

            assertTrue(actualObject.rename is RenameClientCapabilities)
            assertEquals(true, rename.honorsChangeAnnotations, errorMessage)
        }
    }
}
