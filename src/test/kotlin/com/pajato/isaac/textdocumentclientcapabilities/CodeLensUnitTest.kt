package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.CodeLensClientCapabilities
import com.pajato.isaac.api.IsaacCodeLensClientCapabilities
import com.pajato.isaac.api.IsaacTextDocumentClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class CodeLensUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a dynamic registration code lens property, verify()`() {
        val prop = IsaacCodeLensClientCapabilities(dynamicRegistration = true)
        val data: TextDocumentClientCapabilities = IsaacTextDocumentClientCapabilities(codeLens = prop)
        val json = """{"codeLens":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val codeLens: CodeLensClientCapabilities = actualObject.codeLens!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject is IsaacTextDocumentClientCapabilities)
            assertTrue(actualObject.codeLens is IsaacCodeLensClientCapabilities)
            assertEquals(true, codeLens.dynamicRegistration, errorMessage)
        }
    }
}
