package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.MarkupKind.Markdown
import com.pajato.isaac.api.MarkupKind.PlainText
import com.pajato.isaac.api.ParameterInformation
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.SignatureHelpClientCapabilities
import com.pajato.isaac.api.SignatureInformation
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class SignatureHelpUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a default signature help client capabilities object, verify a correct result`() {
        val prop = SignatureHelpClientCapabilities()
        val data = TextDocumentClientCapabilities(signatureHelp = prop)
        val json = """{"signatureHelp":{}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val signatureHelp = actualObject.signatureHelp

            assertTrue(signatureHelp != null)
            assertEquals(null, signatureHelp.dynamicRegistration)
            assertEquals(null, signatureHelp.signatureInformation)
            assertEquals(null, signatureHelp.contextSupport)
        }
    }
    @Test fun `When serializing using a dynamic registration property, verify()`() {
        val prop = SignatureHelpClientCapabilities(dynamicRegistration = true)
        val data = TextDocumentClientCapabilities(signatureHelp = prop)
        val json = """{"signatureHelp":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val signatureHelp: SignatureHelpClientCapabilities = actualObject.signatureHelp!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject.signatureHelp is SignatureHelpClientCapabilities)
            assertEquals(true, signatureHelp.dynamicRegistration, errorMessage)
        }
    }

    @Test fun `When serializing using a signature information property, verify()`() {
        val information = SignatureInformation(
            documentationFormat = arrayOf(PlainText, Markdown),
            parameterInformation = ParameterInformation(labelOffsetSupport = true),
            activeParameterSupport = true
        )
        val prop = SignatureHelpClientCapabilities(signatureInformation = information)
        val data = TextDocumentClientCapabilities(signatureHelp = prop)
        val json = """{"signatureHelp":{"signatureInformation":{""" +
            """"documentationFormat":["PlainText","Markdown"],""" +
            """"parameterInformation":{"labelOffsetSupport":true},""" +
            """"activeParameterSupport":true}}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val signatureHelp: SignatureHelpClientCapabilities = actualObject.signatureHelp!!
            val signatureInformation: SignatureInformation = signatureHelp.signatureInformation!!

            fun assertSignatureInformation() {

                fun assertDocumentationFormat() {
                    val formatErrorMessage = "The documentation format size is wrong!"

                    assertEquals(2, signatureInformation.documentationFormat!!.size, formatErrorMessage)
                    assertEquals(PlainText, signatureInformation.documentationFormat!![0])
                    assertEquals(Markdown, signatureInformation.documentationFormat!![1])
                }

                fun assertParameterInformation() {
                    val parameter = signatureInformation.parameterInformation
                    val errorMessage = "The parameter information is wrong!"

                    assertTrue(parameter is ParameterInformation)
                    assertEquals(true, signatureInformation.parameterInformation!!.labelOffsetSupport, errorMessage)
                }

                fun assertActiveParameterSupport() {
                    val support = signatureInformation.activeParameterSupport
                    val errorMessage = "The support flag is wrong!"

                    assertTrue(support is Boolean)
                    assertEquals(true, support, errorMessage)
                }

                assertTrue(signatureHelp.signatureInformation is SignatureInformation)
                assertDocumentationFormat()
                assertParameterInformation()
                assertActiveParameterSupport()
            }

            assertTrue(actualObject.signatureHelp is SignatureHelpClientCapabilities)
            assertSignatureInformation()
        }
    }

    @Test fun `When serializing using a context support property, verify()`() {
        val prop = SignatureHelpClientCapabilities(contextSupport = true)
        val data = TextDocumentClientCapabilities(signatureHelp = prop)
        val json = """{"signatureHelp":{"contextSupport":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val signatureHelp: SignatureHelpClientCapabilities = actualObject.signatureHelp!!
            val errorMessage = "The context support flag is wrong!"

            assertTrue(actualObject.signatureHelp is SignatureHelpClientCapabilities)
            assertEquals(true, signatureHelp.contextSupport, errorMessage)
        }
    }
}
