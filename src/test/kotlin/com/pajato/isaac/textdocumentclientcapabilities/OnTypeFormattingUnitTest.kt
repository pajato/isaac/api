package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.DocumentOnTypeFormattingClientCapabilities
import com.pajato.isaac.api.IsaacDocumentOnTypeFormattingClientCapabilities
import com.pajato.isaac.api.IsaacTextDocumentClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class OnTypeFormattingUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a dynamic registration on type formatting property, verify()`() {
        val prop = IsaacDocumentOnTypeFormattingClientCapabilities(dynamicRegistration = true)
        val data: TextDocumentClientCapabilities = IsaacTextDocumentClientCapabilities(onTypeFormatting = prop)
        val json = """{"onTypeFormatting":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val onTypeFormatting: DocumentOnTypeFormattingClientCapabilities = actualObject.onTypeFormatting!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject is IsaacTextDocumentClientCapabilities)
            assertTrue(actualObject.onTypeFormatting is IsaacDocumentOnTypeFormattingClientCapabilities)
            assertEquals(true, onTypeFormatting.dynamicRegistration, errorMessage)
        }
    }
}
