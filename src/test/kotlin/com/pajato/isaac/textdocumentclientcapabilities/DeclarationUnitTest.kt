package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.DeclarationClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class DeclarationUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a dynamic registration declaration property, verify()`() {
        val prop = DeclarationClientCapabilities(dynamicRegistration = true)
        val data = TextDocumentClientCapabilities(declaration = prop)
        val json = """{"declaration":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val declaration: DeclarationClientCapabilities = actualObject.declaration!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject.declaration is DeclarationClientCapabilities)
            assertEquals(true, declaration.dynamicRegistration, errorMessage)
        }
    }

    @Test fun `When serializing using a link support declaration property, verify()`() {
        val prop = DeclarationClientCapabilities(linkSupport = true)
        val data = TextDocumentClientCapabilities(declaration = prop)
        val json = """{"declaration":{"linkSupport":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val declaration: DeclarationClientCapabilities = actualObject.declaration!!
            val errorMessage = "The link support flag is wrong!"

            assertTrue(actualObject.declaration is DeclarationClientCapabilities)
            assertEquals(true, declaration.linkSupport, errorMessage)
        }
    }
}
