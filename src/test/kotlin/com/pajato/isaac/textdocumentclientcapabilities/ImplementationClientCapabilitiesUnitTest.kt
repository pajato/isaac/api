package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.ImplementationClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ImplementationClientCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing using a dynamic registration implementation property, verify()`() {
        val prop = ImplementationClientCapabilities(dynamicRegistration = true)
        val data = TextDocumentClientCapabilities(implementation = prop)
        val json = """{"implementation":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val implementation: ImplementationClientCapabilities = actualObject.implementation!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject.implementation is ImplementationClientCapabilities)
            assertEquals(true, implementation.dynamicRegistration, errorMessage)
        }
    }

    @Test fun `When serializing using a link support implementation property, verify()`() {
        val prop = ImplementationClientCapabilities(linkSupport = true)
        val data = TextDocumentClientCapabilities(implementation = prop)
        val json = """{"implementation":{"linkSupport":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val implementation: ImplementationClientCapabilities = actualObject.implementation!!
            val errorMessage = "The link support flag is wrong!"

            assertTrue(actualObject.implementation is ImplementationClientCapabilities)
            assertEquals(true, implementation.linkSupport, errorMessage)
        }
    }
}
