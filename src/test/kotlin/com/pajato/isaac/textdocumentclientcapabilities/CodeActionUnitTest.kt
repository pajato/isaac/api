package com.pajato.isaac.textdocumentclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.CodeAction
import com.pajato.isaac.api.CodeActionClientCapabilities
import com.pajato.isaac.api.CodeActionKind
import com.pajato.isaac.api.CodeActionKind.Empty
import com.pajato.isaac.api.CodeActionKind.QuickFix
import com.pajato.isaac.api.CodeActionKind.Refactor
import com.pajato.isaac.api.CodeActionKind.RefactorExtract
import com.pajato.isaac.api.CodeActionKind.RefactorInline
import com.pajato.isaac.api.CodeActionKind.RefactorRewrite
import com.pajato.isaac.api.CodeActionKind.Source
import com.pajato.isaac.api.CodeActionKind.SourceOrganizeImports
import com.pajato.isaac.api.CodeActionLiteralSupport
import com.pajato.isaac.api.ResolveSupport
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.TextDocumentClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class CodeActionUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `Test that all code action kind values are valid`() {
        val sizeErrorMessage = "There are too few or too many code action kind values!"
        val checkArray = listOf(
            Empty.name, QuickFix.name, Refactor.name, RefactorExtract.name, RefactorInline.name,
            RefactorRewrite.name, Source.name, SourceOrganizeImports.name
        )

        assertEquals(checkArray.size, CodeActionKind.values().size, sizeErrorMessage)
    }

    @Test fun `When serializing using a dynamic registration code action property, verify()`() {
        val prop = CodeActionClientCapabilities(dynamicRegistration = true)
        val data = TextDocumentClientCapabilities(codeAction = prop)
        val json = """{"codeAction":{"dynamicRegistration":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val codeAction: CodeActionClientCapabilities = actualObject.codeAction!!
            val errorMessage = "The dynamic registration flag is wrong!"

            assertTrue(actualObject.codeAction is CodeActionClientCapabilities)
            assertEquals(true, codeAction.dynamicRegistration, errorMessage)
        }
    }

    @Test fun `When serializing using a code action literal support property, verify()`() {
        val kind = CodeAction(arrayOf(QuickFix))
        val prop = CodeActionClientCapabilities(codeActionLiteralSupport = CodeActionLiteralSupport(kind))
        val data = TextDocumentClientCapabilities(codeAction = prop)
        val json = """{"codeAction":{"codeActionLiteralSupport":{"codeActionKind":{"valueSet":["QuickFix"]}}}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val codeAction: CodeActionClientCapabilities = actualObject.codeAction!!
            val literalSupport: CodeActionLiteralSupport = codeAction.codeActionLiteralSupport!!
            val sizeErrorMessage = "The value set size is wrong!"
            val errorMessage = "The value set item is wrong!"

            assertEquals(1, codeAction.codeActionLiteralSupport!!.codeActionKind.valueSet.size, sizeErrorMessage)
            assertEquals(QuickFix, literalSupport.codeActionKind.valueSet[0], errorMessage)
        }
    }

    @Test fun `When serializing using the is preferred support code action property, verify()`() {
        val prop = CodeActionClientCapabilities(isPreferredSupport = true)
        val data = TextDocumentClientCapabilities(codeAction = prop)
        val json = """{"codeAction":{"isPreferredSupport":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val codeAction: CodeActionClientCapabilities = actualObject.codeAction!!
            val errorMessage = "The is preferred support flag is wrong!"

            assertTrue(actualObject.codeAction is CodeActionClientCapabilities)
            assertEquals(true, codeAction.isPreferredSupport, errorMessage)
        }
    }

    @Test fun `When serializing using the disabled support code action property, verify()`() {
        val prop = CodeActionClientCapabilities(disabledSupport = true)
        val data = TextDocumentClientCapabilities(codeAction = prop)
        val json = """{"codeAction":{"disabledSupport":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val codeAction: CodeActionClientCapabilities = actualObject.codeAction!!
            val errorMessage = "The disabled support flag is wrong!"

            assertTrue(actualObject.codeAction is CodeActionClientCapabilities)
            assertEquals(true, codeAction.disabledSupport, errorMessage)
        }
    }

    @Test fun `When serializing using the data support code action property, verify()`() {
        val prop = CodeActionClientCapabilities(dataSupport = true)
        val data = TextDocumentClientCapabilities(codeAction = prop)
        val json = """{"codeAction":{"dataSupport":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val codeAction: CodeActionClientCapabilities = actualObject.codeAction!!
            val errorMessage = "The data support flag is wrong!"

            assertTrue(actualObject.codeAction is CodeActionClientCapabilities)
            assertEquals(true, codeAction.dataSupport, errorMessage)
        }
    }

    @Test fun `When serializing using the resolve support code action property, verify()`() {
        val item = "some item"
        val support = ResolveSupport(arrayOf(item))
        val prop = CodeActionClientCapabilities(resolveSupport = support)
        val data = TextDocumentClientCapabilities(codeAction = prop)
        val json = """{"codeAction":{"resolveSupport":{"properties":["some item"]}}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val codeAction: CodeActionClientCapabilities = actualObject.codeAction!!
            val resolveSupport = codeAction.resolveSupport
            val sizeErrorMessage = "The resolve support properties array size is wrong!"
            val errorMessage = "The resolve support item is wrong!"

            assertTrue(actualObject.codeAction is CodeActionClientCapabilities)
            assertTrue(resolveSupport is ResolveSupport)
            assertEquals(1, resolveSupport.properties.size, sizeErrorMessage)
            assertEquals(item, resolveSupport.properties[0], errorMessage)
        }
    }

    @Test fun `When serializing using the honors change annotations code action property, verify()`() {
        val prop = CodeActionClientCapabilities(honorsChangeAnnotations = true)
        val data = TextDocumentClientCapabilities(codeAction = prop)
        val json = """{"codeAction":{"honorsChangeAnnotations":true}}"""

        testEncode(json, TextDocumentClientCapabilitiesSerializer, data)
        testDecode(json, TextDocumentClientCapabilitiesSerializer) { actualObject ->
            val codeAction: CodeActionClientCapabilities = actualObject.codeAction!!
            val errorMessage = "The honors change annotations flag is wrong!"

            assertTrue(actualObject.codeAction is CodeActionClientCapabilities)
            assertEquals(true, codeAction.honorsChangeAnnotations, errorMessage)
        }
    }
}
