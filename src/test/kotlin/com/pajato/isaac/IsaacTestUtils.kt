package com.pajato.isaac

import com.pajato.isaac.api.BooleanChangeNotifications
import com.pajato.isaac.api.BooleanCodeActionProvider
import com.pajato.isaac.api.BooleanDefinitionProvider
import com.pajato.isaac.api.BooleanDocumentFormattingProvider
import com.pajato.isaac.api.BooleanDocumentRangeFormattingProvider
import com.pajato.isaac.api.BooleanDocumentSymbolProvider
import com.pajato.isaac.api.BooleanHoverProvider
import com.pajato.isaac.api.BooleanReferencesProvider
import com.pajato.isaac.api.BooleanWorkspaceSymbolProvider
import com.pajato.isaac.api.ChangeNotifications
import com.pajato.isaac.api.DocumentSelector
import com.pajato.isaac.api.DocumentUri
import com.pajato.isaac.api.IsaacCompletionOptions
import com.pajato.isaac.api.IsaacExecuteCommandOptions
import com.pajato.isaac.api.IsaacServerCapabilities
import com.pajato.isaac.api.IsaacServerInfo
import com.pajato.isaac.api.IsaacSignatureHelpOptions
import com.pajato.isaac.api.IsaacWorkspace
import com.pajato.isaac.api.IsaacWorkspaceFoldersServerCapabilities
import com.pajato.isaac.api.KindTextDocumentSync
import com.pajato.isaac.api.SemanticTokensLegend
import com.pajato.isaac.api.SemanticTokensOptions
import com.pajato.isaac.api.ServerCapabilities
import com.pajato.isaac.api.ServerInfo
import com.pajato.isaac.api.StaticRegistrationOptions
import com.pajato.isaac.api.StringChangeNotifications
import com.pajato.isaac.api.TextDocumentRegistrationOptions
import com.pajato.isaac.api.TextDocumentSync
import com.pajato.isaac.api.TextDocumentSyncKind
import com.pajato.isaac.api.WorkDoneProgressOptions
import com.pajato.isaac.api.Workspace
import java.io.File
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

const val appName = "Test App"
const val appVersion = "1.0-SNAPSHOT"

fun getServerInfo(): ServerInfo = IsaacServerInfo(appName, appVersion)

fun getDiagnostic(item: String) = "The actual $item is wrong!"

fun ClassLoader.getTestProject(projectDir: String, projectName: String): DocumentUri {
    val path = "sample_projects/$projectDir/$projectName"
    val resource = this.getResource(path)
    val resourceJar = this.getResource("$path.jar")
    return when {
        resource != null -> resource.toURI().toString()
        resourceJar != null -> System.getProperty("user.dir").unpackJarFile(projectDir, projectName)
        else -> """Error: The project with path "$path" is not supported (cannot be found)!"""
    }
}

fun String.unpackJarFile(projectDir: String, projectName: String): DocumentUri {
    fun String.runCommand(workingDir: File) =
        ProcessBuilder(*split(" ").toTypedArray())
            .directory(workingDir)
            .redirectOutput(ProcessBuilder.Redirect.INHERIT)
            .redirectError(ProcessBuilder.Redirect.INHERIT)
            .start()
            .waitFor()

    val resourceDir = "$this/build/resources/test/sample_projects/$projectDir"
    val command = "jar xf $projectName.jar $projectName"
    val exitValue = command.runCommand(File(resourceDir))
    return if (exitValue == 0) "file:$resourceDir/$projectName" else "error: Could not unpack the jar file!"
}

internal fun assertChangeNotifications(expected: ChangeNotifications?, actual: ChangeNotifications?) {
    fun assertBooleanChangeNotifications(flag: Boolean) {
        assertTrue(actual is BooleanChangeNotifications, getDiagnostic("type"))
        assertEquals(flag, actual.flag, getDiagnostic("flag"))
    }

    fun assertStringChangeNotifications(id: String) {
        assertTrue(actual is StringChangeNotifications, getDiagnostic("type"))
        assertEquals(id, actual.id, getDiagnostic("id"))
    }

    when (expected) {
        null -> assertTrue(actual == null, getDiagnostic("type (should be null)"))
        is BooleanChangeNotifications -> assertBooleanChangeNotifications(expected.flag)
        is StringChangeNotifications -> assertStringChangeNotifications(expected.id)
    }
}

internal fun <T> assertOptions(
    expected: T,
    actual: T,
    assertWorkDoneProgress: Boolean = true,
    assertId: Boolean = true
) {
    fun assertSelector() {
        fun assertSelector(expected: DocumentSelector?, actual: DocumentSelector?) {
            fun isInvalidSize() = expected != null && actual != null && expected.size != actual.size
            fun isInvalidLanguage() = expected != null && actual != null && expected[0].language != actual[0].language
            fun isInvalidScheme() = expected != null && actual != null && expected[0].scheme != actual[0].scheme
            fun isInvalidPattern() = expected != null && actual != null && expected[0].pattern != actual[0].pattern

            when {
                expected == null && actual != null -> fail("The actual document selector is not null!")
                expected != null && actual == null -> fail("The actual document selector is null!")
                isInvalidSize() -> fail("The actual selector list size is wrong!")
                isInvalidLanguage() -> fail("The actual selector language field is wrong!")
                isInvalidScheme() -> fail("The actual selector scheme field is wrong!")
                isInvalidPattern() -> fail("The actual selector pattern field is wrong!")
            }
        }

        assertTrue(expected is TextDocumentRegistrationOptions && actual is TextDocumentRegistrationOptions)
        assertSelector(expected.documentSelector, actual.documentSelector)
    }

    fun assertWorkDoneProgress() {
        assertTrue(expected is WorkDoneProgressOptions && actual is WorkDoneProgressOptions)
        assertEquals(expected.workDoneProgress, actual.workDoneProgress, getDiagnostic("work done progress"))
    }

    fun assertId() {
        assertTrue(expected is StaticRegistrationOptions && actual is StaticRegistrationOptions)
        assertEquals(expected.id, actual.id, getDiagnostic("id"))
    }

    if (assertWorkDoneProgress) assertWorkDoneProgress()
    assertSelector()
    if (assertId) assertId()
}

internal fun getNormalCapabilities(): ServerCapabilities {

    fun getTextDocumentSync(): TextDocumentSync {
        val kind = TextDocumentSyncKind.Full
        return KindTextDocumentSync(kind)
    }

    fun getCompletionProvider() = IsaacCompletionOptions(resolveProvider = false, triggerCharacters = arrayOf("."))

    fun getWorkspace(): Workspace {
        val changeNotifications: ChangeNotifications = BooleanChangeNotifications(true)
        return IsaacWorkspace(IsaacWorkspaceFoldersServerCapabilities(true, changeNotifications))
    }

    return IsaacServerCapabilities(
        textDocumentSync = getTextDocumentSync(),
        completionProvider = getCompletionProvider(),
        hoverProvider = BooleanHoverProvider(true),
        signatureHelpProvider = IsaacSignatureHelpOptions(true, arrayOf("(", ","), arrayOf("-")),
        definitionProvider = BooleanDefinitionProvider(true),
        referencesProvider = BooleanReferencesProvider(true),
        documentSymbolProvider = BooleanDocumentSymbolProvider(true),
        codeActionProvider = BooleanCodeActionProvider(true),
        documentFormattingProvider = BooleanDocumentFormattingProvider(true),
        documentRangeFormattingProvider = BooleanDocumentRangeFormattingProvider(true),
        executeCommandProvider = IsaacExecuteCommandOptions(commands = listOf("convertJavaToKotlin")),
        workspaceSymbolProvider = BooleanWorkspaceSymbolProvider(true),
        workspace = getWorkspace()
    )
}

internal fun assertSelectorIsEmpty(selector: DocumentSelector?) {
    assertTrue(selector != null)
    assertTrue(selector.isEmpty())
}

internal fun getTestLegend() = SemanticTokensLegend(arrayOf("type1", "type2"), arrayOf("modifier1", "modifier2"))

internal fun assertLegend(options: SemanticTokensOptions) {
    fun assertTokenTypes(types: Array<String>) {
        assertEquals(2, types.size, "The token types array is the wrong size!")
        assertEquals("type1", types[0], "The first token type has the wrong value!")
        assertEquals("type2", types[1], "The second token type has the wrong value!")
    }

    fun assertTokenModifiers(modifiers: Array<String>) {
        assertEquals(2, modifiers.size, "The token types array is the wrong size!")
        assertEquals("modifier1", modifiers[0], "The first token modifier has the wrong value!")
        assertEquals("modifier2", modifiers[1], "The second token modifier has the wrong value!")
    }

    assertTokenTypes(options.legend.tokenTypes)
    assertTokenModifiers(options.legend.tokenModifiers)
}

internal fun assertTokenTypes(types: Array<String>) {
    assertEquals(2, types.size, "The token types array is the wrong size!")
    assertEquals("type1", types[0], "The first token type has the wrong value!")
    assertEquals("type2", types[1], "The second token type has the wrong value!")
}

internal fun assertTokenModifiers(modifiers: Array<String>) {
    assertEquals(2, modifiers.size, "The token types array is the wrong size!")
    assertEquals("modifier1", modifiers[0], "The first token modifier has the wrong value!")
    assertEquals("modifier2", modifiers[1], "The second token modifier has the wrong value!")
}
