package com.pajato.isaac.windowclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.IsaacShowDocumentClientCapabilities
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.ShowDocumentClientCapabilities
import com.pajato.isaac.api.WindowClientCapabilities
import com.pajato.isaac.api.WindowClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ShowDocumentUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing with a show document client capabilities object, verify a correct result`() {
        val data = WindowClientCapabilities(showDocument = ShowDocumentClientCapabilities(true))
        val json = """{"showDocument":{"support":true}}"""

        testEncode(json, WindowClientCapabilitiesSerializer, data)
        testDecode(json, WindowClientCapabilitiesSerializer) { actualObject ->
            val showDocument = actualObject.showDocument
            val support = showDocument!!.support

            assertTrue(showDocument is IsaacShowDocumentClientCapabilities)
            assertEquals(null, actualObject.workDoneProgress)
            assertEquals(null, actualObject.showMessage)
            assertEquals(true, support)
        }
    }
}
