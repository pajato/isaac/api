package com.pajato.isaac.windowclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.MessageActionItemCapability
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.ShowMessageRequestClientCapabilities
import com.pajato.isaac.api.WindowClientCapabilities
import com.pajato.isaac.api.WindowClientCapabilitiesSerializer
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ShowMessageUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a window client capabilities show message object, verify the result`() {
        val action = MessageActionItemCapability(additionalPropertiesSupport = true)
        val data = WindowClientCapabilities(showMessage = ShowMessageRequestClientCapabilities(action))
        val json = """{"showMessage":{"messageActionItem":{"additionalPropertiesSupport":true}}}"""

        testEncode(json, WindowClientCapabilitiesSerializer, data)
        testDecode(json, WindowClientCapabilitiesSerializer) { actualObject ->
            val showMessage = actualObject.showMessage
            assertEquals(null, actualObject.workDoneProgress)
            assertTrue(showMessage != null, getDiagnostic("show message"))
            assertEquals(true, showMessage.messageActionItem!!.additionalPropertiesSupport)
            assertEquals(null, actualObject.showDocument)
        }
    }
}
