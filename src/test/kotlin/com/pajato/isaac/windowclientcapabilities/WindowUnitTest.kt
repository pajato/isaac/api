package com.pajato.isaac.windowclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.WindowClientCapabilities
import com.pajato.isaac.api.WindowClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals

class WindowUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing with a work done progress property, verify a correct result`() {
        val data = WindowClientCapabilities(workDoneProgress = true)
        val json = """{"workDoneProgress":true}"""

        testEncode(json, WindowClientCapabilitiesSerializer, data)
        testDecode(json, WindowClientCapabilitiesSerializer) { actualObject ->
            assertEquals(true, actualObject.workDoneProgress)
            assertEquals(null, actualObject.showMessage)
            assertEquals(null, actualObject.showDocument)
        }
    }
}
