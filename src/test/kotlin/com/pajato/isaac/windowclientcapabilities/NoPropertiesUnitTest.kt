package com.pajato.isaac.windowclientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.WindowClientCapabilities
import com.pajato.isaac.api.WindowClientCapabilitiesSerializer
import kotlin.test.Test
import kotlin.test.assertEquals

class NoPropertiesUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing with a default window client capabilities object, verify a correct result`() {
        val data = WindowClientCapabilities()
        val json = """{}"""

        testEncode(json, WindowClientCapabilitiesSerializer, data)
        testDecode(json, WindowClientCapabilitiesSerializer) { actualObject ->
            assertEquals(null, actualObject.workDoneProgress)
            assertEquals(null, actualObject.showMessage)
            assertEquals(null, actualObject.showDocument)
        }
    }
}
