package com.pajato.isaac.clientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.ClientCapabilities
import com.pajato.isaac.api.ClientCapabilitiesSerializer
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.WorkspaceClientCapabilities
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class WorkspacePropertyUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing with a workspace property, verify a correct result`() {
        fun assertProperty(expected: ClientCapabilities, actual: ClientCapabilities) {
            val actualWorkspace = actual.workspace

            assertTrue(actualWorkspace is WorkspaceClientCapabilities, getDiagnostic("workspace"))
            assertEquals(expected.workspace!!.applyEdit, actualWorkspace.applyEdit, getDiagnostic("apply edit"))
        }

        val data = ClientCapabilities(workspace = WorkspaceClientCapabilities(true))
        val json = """{"workspace":{"applyEdit":true}}"""

        testEncode(json, ClientCapabilitiesSerializer, data)
        testDecode(json, ClientCapabilitiesSerializer) { actual -> assertProperty(data, actual) }
    }
}
