package com.pajato.isaac.clientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.ClientCapabilities
import com.pajato.isaac.api.ClientCapabilitiesSerializer
import com.pajato.isaac.api.IsaacClientCapabilities
import com.pajato.isaac.api.IsaacWindowClientCapabilities
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class WindowUnitTest : ApiTestProfiler() {
    private val capabilities: ClientCapabilities = IsaacClientCapabilities(
        window = IsaacWindowClientCapabilities(true)
    )
    private val json = """{"window":{"workDoneProgress":true}}"""

    @Test
    fun `When encoding a client capabilities object with a window property, verify the result()`() {
        val actualJson = Json.encodeToString(ClientCapabilitiesSerializer, capabilities)

        assertEquals(json, actualJson)
    }

    @Test
    fun `When decoding a client capabilities object with a window property, verify the result()`() {
        val actualObject = Json.decodeFromString(ClientCapabilitiesSerializer, json)
        val window = actualObject.window

        assertTrue(actualObject is IsaacClientCapabilities)
        assertTrue(window is IsaacWindowClientCapabilities)
        assertEquals(true, window.workDoneProgress)
    }
}
