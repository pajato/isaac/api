package com.pajato.isaac.clientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.ClientCapabilities
import com.pajato.isaac.api.ClientCapabilitiesSerializer
import com.pajato.isaac.api.IsaacClientCapabilities
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class NoPropertiesUnitTest : ApiTestProfiler() {
    private val capabilities: ClientCapabilities = IsaacClientCapabilities()
    private val json = "{}"

    @Test
    fun `When encoding a fully empty client capabilities object, verify the result()`() {
        val actualJson = Json.encodeToString(ClientCapabilitiesSerializer, capabilities)

        assertEquals(json, actualJson)
    }

    @Test
    fun `When decoding a fully empty client capabilities object, verify the result()`() {
        val actualObject = Json.decodeFromString(ClientCapabilitiesSerializer, json)

        assertTrue(actualObject is IsaacClientCapabilities)
        assertEquals(null, actualObject.workspace)
        assertEquals(null, actualObject.textDocument)
        assertEquals(null, actualObject.window)
        assertEquals(null, actualObject.general)
        assertEquals(null, actualObject.experimental)
    }
}
