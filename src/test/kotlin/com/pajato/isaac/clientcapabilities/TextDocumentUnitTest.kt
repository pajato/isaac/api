package com.pajato.isaac.clientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.ClientCapabilities
import com.pajato.isaac.api.ClientCapabilitiesSerializer
import com.pajato.isaac.api.IsaacClientCapabilities
import com.pajato.isaac.api.IsaacTextDocumentClientCapabilities
import com.pajato.isaac.api.IsaacTextDocumentSyncClientCapabilities
import com.pajato.isaac.api.TextDocumentClientCapabilities
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TextDocumentUnitTest : ApiTestProfiler() {
    private val textDocument: TextDocumentClientCapabilities = IsaacTextDocumentClientCapabilities(
        synchronization = IsaacTextDocumentSyncClientCapabilities(dynamicRegistration = true)
    )
    private val capabilities: ClientCapabilities = IsaacClientCapabilities(textDocument = textDocument)
    private val json = """{"textDocument":{"synchronization":{"dynamicRegistration":true}}}"""

    @Test
    fun `When encoding a client capabilities object with a text document property, verify the result()`() {
        val actualJson = Json.encodeToString(ClientCapabilitiesSerializer, capabilities)

        assertEquals(json, actualJson)
    }

    @Test
    fun `When decoding a client capabilities object with a tex dDocument property, verify the result()`() {
        val actualObject = Json.decodeFromString(ClientCapabilitiesSerializer, json)
        val textDocument = actualObject.textDocument
        val synchronization = textDocument!!.synchronization

        assertTrue(actualObject is IsaacClientCapabilities)
        assertTrue(textDocument is IsaacTextDocumentClientCapabilities)
        assertTrue(synchronization is IsaacTextDocumentSyncClientCapabilities)
        assertEquals(true, synchronization.dynamicRegistration)
    }
}
