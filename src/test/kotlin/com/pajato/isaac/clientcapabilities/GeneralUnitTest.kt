package com.pajato.isaac.clientcapabilities

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.ClientCapabilities
import com.pajato.isaac.api.ClientCapabilitiesSerializer
import com.pajato.isaac.api.GeneralClientCapabilities
import com.pajato.isaac.api.RegularExpressionsClientCapabilities
import com.pajato.isaac.api.SerializerTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class GeneralUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing with a general client capabilities object, verify a correct result`() {
        val theEngine = "Some RE Engine"
        val engine = RegularExpressionsClientCapabilities(engine = theEngine)
        val general = GeneralClientCapabilities(regularExpressions = engine)
        val data = ClientCapabilities(general = general)
        val json = """{"general":{"regularExpressions":{"engine":"$theEngine"}}}"""

        testEncode(json, ClientCapabilitiesSerializer, data)
        testDecode(json, ClientCapabilitiesSerializer) { actualObject ->
            val actualGeneral = actualObject.general

            assertTrue(actualGeneral != null)
            assertTrue(actualGeneral.regularExpressions is RegularExpressionsClientCapabilities)
            assertEquals(theEngine, actualGeneral.regularExpressions!!.engine)
        }
    }
}
