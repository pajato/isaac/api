package com.pajato.isaac.workspace

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.WorkspaceSymbolOptions
import com.pajato.isaac.api.WorkspaceSymbolOptionsSerializer
import kotlin.test.Test
import kotlin.test.assertEquals

class WorkspaceSymbolOptionsUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a default workspace symbol options object, verify a correct result`() {
        val data = WorkspaceSymbolOptions()
        val json = """{}"""

        testEncode(json, WorkspaceSymbolOptionsSerializer, data)
        testDecode(json, WorkspaceSymbolOptionsSerializer) { actualObject ->
            assertEquals(null, actualObject.workDoneProgress)
        }
    }

    @Test fun `When serializing a non-default workspace symbol options object, verify a correct result`() {
        val data = WorkspaceSymbolOptions(true)
        val json = """{"workDoneProgress":true}"""

        testEncode(json, WorkspaceSymbolOptionsSerializer, data)
        testDecode(json, WorkspaceSymbolOptionsSerializer) { actualObject ->
            assertEquals(true, actualObject.workDoneProgress)
        }
    }
}
