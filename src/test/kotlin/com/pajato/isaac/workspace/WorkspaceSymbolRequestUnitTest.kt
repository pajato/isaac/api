package com.pajato.isaac.workspace

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.Id
import com.pajato.isaac.api.IntegerProgressToken
import com.pajato.isaac.api.Params
import com.pajato.isaac.api.ProgressToken
import com.pajato.isaac.api.RequestMessage
import com.pajato.isaac.api.RequestMessageSerializer
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.WorkspaceSymbolParams
import com.pajato.isaac.api.jsonrpcVersion
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

class WorkspaceSymbolRequestUnitTest : SerializerTest, ApiTestProfiler() {

    private val requestId = 2
    private val method = "workspace/symbol"

    @Test fun `When serializing a default workspace symbol request message, verify a correct result()`() {
        val data = RequestMessage(Id.NumberId(requestId), method)
        val json = """{"jsonrpc":"$jsonrpcVersion","id":$requestId,"method":"$method"}"""

        testEncode(json, RequestMessageSerializer, data)
        testDecode(json, RequestMessageSerializer) { actualObject -> assertMessage(actualObject) }
    }

    @Test fun `When serializing a workspace symbol request with default params, verify a correct result()`() {
        val params = WorkspaceSymbolParams()
        val data = RequestMessage(Id.NumberId(requestId), method, params)
        val json = """{"jsonrpc":"$jsonrpcVersion","id":$requestId,"method":"$method","params":{"query":""}}"""

        testEncode(json, RequestMessageSerializer, data)
        testDecode(json, RequestMessageSerializer) { actualObject -> assertMessage(actualObject, params) }
    }

    @Test fun `When serializing a workspace symbol request message, verify the correct result()`() {
        val token = IntegerProgressToken(0)
        val params = WorkspaceSymbolParams((token))
        val data = RequestMessage(Id.NumberId(requestId), method, params)
        val json = """{"jsonrpc":"$jsonrpcVersion","id":$requestId,"method":"$method",""" +
            """"params":{"workDoneToken":0,"query":""}}"""

        testEncode(json, RequestMessageSerializer, data)
        testDecode(json, RequestMessageSerializer) { actualObject -> assertMessage(actualObject, params) }
    }

    private fun assertMessage(message: RequestMessage, expectedParams: WorkspaceSymbolParams? = null) {
        fun assertMessageWithoutParams(message: RequestMessage) {
            val numberId = message.id

            assertEquals(jsonrpcVersion, message.jsonrpc, "Wrong JSON RPC version!")
            assertTrue(numberId is Id.NumberId, "Wrong id type! Should be Id.NumberId")
            assertEquals(requestId, numberId.id, "Wrong request id value!")
            assertEquals(method, message.method, "Wrong request method name!")
        }

        fun assertParams(params: Params?) {
            fun assertToken(token: ProgressToken?) {
                if (token == null) return
                assertTrue(token is IntegerProgressToken, "Token should be integer type!")
                assertEquals(0, token.intToken, "Token value is wrong!")
            }

            if (expectedParams == null && params != null) fail("Expected null params but got $params!")
            if (expectedParams == null && params == null) return
            assertTrue(params is WorkspaceSymbolParams, "Params type is wrong! Expected WorkspaceSymbolParams!")
            assertEquals("", params.query, "Query values is wrong!")
            assertToken(params.workDoneToken)
        }

        assertMessageWithoutParams(message)
        assertParams(message.params)
    }
}
