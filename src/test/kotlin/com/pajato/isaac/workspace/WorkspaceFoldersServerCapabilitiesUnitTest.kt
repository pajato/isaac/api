package com.pajato.isaac.workspace

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanChangeNotifications
import com.pajato.isaac.api.ChangeNotifications
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.StringChangeNotifications
import com.pajato.isaac.api.WorkspaceFoldersSC
import com.pajato.isaac.api.WorkspaceFoldersServerCapabilities
import com.pajato.isaac.api.WorkspaceFoldersServerCapabilitiesSerializer
import com.pajato.isaac.assertChangeNotifications
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals

class WorkspaceFoldersServerCapabilitiesUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertCapabilities(expected: WorkspaceFoldersSC, actual: WorkspaceFoldersSC) {
        assertEquals(expected.supported, actual.supported, getDiagnostic("supported"))
        assertChangeNotifications(expected.changeNotifications, actual.changeNotifications)
    }

    @Test fun `When serializing a default workspace folders server capabilities object, verify result`() {
        val data = WorkspaceFoldersServerCapabilities()
        val json = """{}"""

        testEncode(json, WorkspaceFoldersServerCapabilitiesSerializer, data)
        testDecode(json, WorkspaceFoldersServerCapabilitiesSerializer) { actual -> assertCapabilities(data, actual) }
    }

    @Test fun `When serializing a boolean change notification object, verify result`() {
        val changeNotifications: ChangeNotifications = BooleanChangeNotifications(true)
        val data = WorkspaceFoldersServerCapabilities(true, changeNotifications)
        val json = """{"supported":true,"changeNotifications":true}"""

        testEncode(json, WorkspaceFoldersServerCapabilitiesSerializer, data)
        testDecode(json, WorkspaceFoldersServerCapabilitiesSerializer) { actual -> assertCapabilities(data, actual) }
    }

    @Test fun `When serializing a string change notification object, verify result`() {
        val id = "alpha23"
        val changeNotifications: ChangeNotifications = StringChangeNotifications(id)
        val data = WorkspaceFoldersServerCapabilities(false, changeNotifications)
        val json = """{"supported":false,"changeNotifications":"$id"}"""

        testEncode(json, WorkspaceFoldersServerCapabilitiesSerializer, data)
        testDecode(json, WorkspaceFoldersServerCapabilitiesSerializer) { actual -> assertCapabilities(data, actual) }
    }
}
