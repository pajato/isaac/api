package com.pajato.isaac.workspace

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.DocumentUri
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.WorkspaceFolder
import com.pajato.isaac.api.WorkspaceFolderSerializer
import com.pajato.isaac.getTestProject
import kotlin.test.Test
import kotlin.test.assertEquals

class WorkspaceFolderUnitTest : SerializerTest, ApiTestProfiler() {

    @Test fun `When serializing a workspace folder, verify results`() {
        fun assertFolder(folder: WorkspaceFolder, uri: DocumentUri, name: String) {
            fun getDiagnostic(foo: String) = "The $foo is wrong!"

            assertEquals(uri, folder.uri, getDiagnostic("uri"))
            assertEquals(name, folder.name, getDiagnostic("name"))
        }

        val uri = this::class.java.classLoader.getTestProject("kotlin_gradle", "hello_world")
        val name = "Default Workspace Folder"
        val json = """{"uri":"$uri","name":"$name"}"""
        val data = WorkspaceFolder(uri, name)

        testEncode(json, WorkspaceFolderSerializer, data)
        testDecode(json, WorkspaceFolderSerializer) { actualFolder -> assertFolder(actualFolder, uri, name) }
    }
}
