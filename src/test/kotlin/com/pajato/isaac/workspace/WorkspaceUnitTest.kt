package com.pajato.isaac.workspace

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanChangeNotifications
import com.pajato.isaac.api.ChangeNotifications
import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.api.Workspace
import com.pajato.isaac.api.WorkspaceFoldersServerCapabilities
import com.pajato.isaac.api.WorkspaceSerializer
import com.pajato.isaac.assertChangeNotifications
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class WorkspaceUnitTest : SerializerTest, ApiTestProfiler() {

    private fun assertWorkspace(expected: Workspace, actual: Workspace) {
        fun assertFolder() {
            val expectedFolders = expected.workspaceFolders!!
            val actualFolders = actual.workspaceFolders!!

            assertEquals(expectedFolders.supported, actualFolders.supported, getDiagnostic("supported"))
            assertChangeNotifications(expectedFolders.changeNotifications, actualFolders.changeNotifications)
        }

        when {
            expected.workspaceFolders == null && actual.workspaceFolders != null -> fail("non null workspace folders")
            expected.workspaceFolders != null && actual.workspaceFolders == null -> fail("null workspace folder")
            expected.workspaceFolders != null && actual.workspaceFolders != null -> assertFolder()
        }
    }

    @Test fun `When serializing a workspace default object, verify result`() {
        val data = Workspace()
        val json = """{}"""

        testEncode(json, WorkspaceSerializer, data)
        testDecode(json, WorkspaceSerializer) { actual -> assertWorkspace(data, actual) }
    }

    @Test fun `When serializing a workspace non-default object, verify result`() {
        val changeNotifications: ChangeNotifications = BooleanChangeNotifications(true)
        val workspaceFolders = WorkspaceFoldersServerCapabilities(true, changeNotifications)
        val data = Workspace(workspaceFolders)
        val json = """{"workspaceFolders":{"supported":true,"changeNotifications":true}}"""

        testEncode(json, WorkspaceSerializer, data)
        testDecode(json, WorkspaceSerializer) { actual -> assertWorkspace(data, actual) }
    }
}
