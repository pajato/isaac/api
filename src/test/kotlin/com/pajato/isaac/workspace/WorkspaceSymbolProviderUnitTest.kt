package com.pajato.isaac.workspace

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.api.BooleanWorkspaceSymbolProvider
import com.pajato.isaac.api.IsaacWorkspaceSymbolOptions
import com.pajato.isaac.api.OptionsWorkspaceSymbolProvider
import com.pajato.isaac.api.WorkspaceSymbolProvider
import com.pajato.isaac.api.WorkspaceSymbolProviderSerializer
import kotlinx.serialization.json.Json
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class WorkspaceSymbolProviderUnitTest : ApiTestProfiler() {

    @Test
    fun `When encoding a boolean workspace symbol provider object, verify a correct result`() {
        val provider: WorkspaceSymbolProvider = BooleanWorkspaceSymbolProvider(true)
        val actualJson = Json.encodeToString(WorkspaceSymbolProviderSerializer, provider)
        val expectedJson = """{"state":true}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding a boolean workspace symbol provider object, verify a correct result`() {
        val incomingJson = """{"state":true}"""
        val actualObject = Json.decodeFromString(WorkspaceSymbolProviderSerializer, incomingJson)

        assertTrue(actualObject is BooleanWorkspaceSymbolProvider)
        assertEquals(true, actualObject.state)
    }

    @Test
    fun `When encoding an options workspace symbol provider object, verify a correct result`() {
        val provider: WorkspaceSymbolProvider =
            OptionsWorkspaceSymbolProvider(options = IsaacWorkspaceSymbolOptions(true))
        val actualJson = Json.encodeToString(WorkspaceSymbolProviderSerializer, provider)
        val expectedJson = """{"options":{"workDoneProgress":true}}"""

        assertEquals(expectedJson, actualJson)
    }

    @Test
    fun `When decoding an options workspace symbol provider object, verify a correct result`() {
        val incomingJson = """{"options":{"workDoneProgress":true}}"""
        val actualObject = Json.decodeFromString(WorkspaceSymbolProviderSerializer, incomingJson)

        assertTrue(actualObject is OptionsWorkspaceSymbolProvider)
        assertEquals(true, actualObject.options.workDoneProgress)
    }

    @Test
    fun `When invalid JSON is used, verify an illegal state exception is thrown`() {
        val incomingJson = """{"xyzzy":true}"""

        assertFailsWith<IllegalStateException> {
            Json.decodeFromString(WorkspaceSymbolProviderSerializer, incomingJson)
        }
    }
}
