@file:Suppress("unused", "MemberVisibilityCanBePrivate")

package com.pajato.isaac

import org.junit.jupiter.api.TestInfo
import java.time.Duration
import java.time.Instant.now
import java.time.temporal.ChronoUnit
import kotlin.test.AfterTest
import kotlin.test.BeforeTest

var reportElapsedThreshold = 100L

abstract class ApiTestProfiler {
    private lateinit var testName: String
    private val testReporterMap: MutableMap<String, TestReporter> = mutableMapOf()
    val testLogMap: MutableMap<String, MutableList<String>> = mutableMapOf()

    fun getKey() = "${this::class.simpleName}#$testName"

    fun setLog(log: MutableList<String>) = testLogMap.put(getKey(), log)

    @BeforeTest fun setUp(info: TestInfo) {
        testName = info.displayName
        showTestMessage(Phase.Starting, getKey())
    }

    @AfterTest fun tearDown() = showTestMessage(Phase.Stopping, getKey())

    private fun showTestMessage(phase: Phase, key: String) {
        fun start() = TestReporter(now().toEpochMilli(), key).apply { testReporterMap[key] = this }

        fun stop(duration: Long) {
            fun printLogMaybe() {
                val logEntries = testLogMap[key] ?: return

                if (logEntries.isEmpty()) return
                logEntries.forEach { entry -> println(entry) }
            }

            val testReporter = testReporterMap[key]

            check(testReporter != null)
            testReporter.printStopMessage(duration)
            printLogMaybe()
            testReporterMap.remove(key)
        }

        when (phase) {
            Phase.Starting -> start()
            Phase.Stopping -> stop(now().toEpochMilli())
        }
    }

    enum class Phase { Starting, Stopping }

    inner class TestReporter(private val startTimeMillis: Long, private val key: String) {
        fun printStopMessage(stopTimeMillis: Long) {
            val amount: Long = stopTimeMillis - startTimeMillis
            val duration: Long = Duration.of(amount, ChronoUnit.MILLIS).toMillis()
            if (duration > reportElapsedThreshold) println("Elapsed time for test $key: ${duration}ms")
        }
    }
}
