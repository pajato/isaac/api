package sample

class HelloWorld {

    fun helloWorld() {
        println("Hello, World!")
    }

    private fun hello() = println("Hello from the LSP4K SDK")

    private fun hello2() {
        fun hello1(name: String) {
            val message = "Hello $name!"
            println(message)
        }

        hello1("fred")
    }

    init {
        hello()
        hello2()
        helloWorld()
    }
}
