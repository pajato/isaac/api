package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.JsonTransformingSerializer
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put

@Serializable sealed class HoverProvider

@Serializable class BooleanHoverProvider(val data: Boolean) : HoverProvider()

@Serializable class OptionsHoverProvider(
    @Serializable(with = HoverOptionsSerializer::class) val data: HoverOptions
) : HoverProvider()

/**
 * Use a transforming serializer to remove (encoding) or add (decoding) the class discriminant (type field) and
 * transform elements during encoding and decoding.
 */
object TransformingHoverProviderSerializer : JsonTransformingSerializer<HoverProvider>(HoverProvider.serializer()) {
    private const val dataKey = "data"
    private const val typeKey = "type"

    /**
     * Transform (flatten) the element to remove the type element strip the data key.
     */
    override fun transformSerialize(element: JsonElement): JsonElement {
        fun getPrimitive(element: JsonObject) = element[dataKey] as JsonPrimitive
        fun getObject(element: JsonObject) = element[dataKey] as JsonObject

        fun isValid(element: JsonObject) = element.size == 2 && element.containsKey(dataKey) &&
            ((element[dataKey] is JsonPrimitive) || (element[dataKey] is JsonObject))

        check(element is JsonObject && isValid(element)) { "Invalid element content: $element." }
        return if (element[dataKey] is JsonPrimitive) getPrimitive(element) else getObject(element)
    }

    /**
     * Transform (expand) the element to add the type and a data key.
     */
    override fun transformDeserialize(element: JsonElement): JsonElement {
        fun isValidBoolean() = element is JsonPrimitive && (element.content == "true" || element.content == "false")
        fun isValidOptions() = element is JsonObject && element.size > 0

        fun transformToBoolean(data: Boolean) = buildJsonObject {
            put(typeKey, BooleanHoverProvider::class.java.name)
            put(dataKey, data)
        }

        fun transformToOptions(options: JsonElement) = buildJsonObject {
            put(typeKey, OptionsHoverProvider::class.java.name)
            put(dataKey, options)
        }

        check(isValidBoolean() || isValidOptions()) { "Invalid element: $element" }
        return when {
            isValidBoolean() -> transformToBoolean((element as JsonPrimitive).content.toBoolean())
            else -> transformToOptions(element)
        }
    }
}
