package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface PublishDiagnosticsClientCapabilities {
    /**
     * Whether the clients accept diagnostics with related information.
     */
    val relatedInformation: Boolean?

    /**
     * Client supports the tag property to provide metadata about a diagnostic.
     * Clients supporting tags have to handle unknown tags gracefully.
     *
     * @since 3.15.0
     */
    val tagSupport: DiagnosticTagSupport?

    /**
     * Whether the client interprets the version property of the
     * `textDocument/publishDiagnostics` notification's parameter.
     *
     * @since 3.15.0
     */
    val versionSupport: Boolean?

    /**
     * Client supports a codeDescription property
     *
     * @since 3.16.0
     */
    val codeDescriptionSupport: Boolean?

    /**
     * Whether code action supports the `data` property which is
     * preserved between a `textDocument/publishDiagnostics` and
     * `textDocument/codeAction` request.
     *
     * @since 3.16.0
     */
    val dataSupport: Boolean?
}

fun PublishDiagnosticsClientCapabilities(
    relatedInformation: Boolean? = null,
    tagSupport: DiagnosticTagSupport? = null,
    versionSupport: Boolean? = null,
    codeDescriptionSupport: Boolean? = null,
    dataSupport: Boolean? = null,
): PublishDiagnosticsClientCapabilities = IsaacPublishDiagnosticsClientCapabilities(
    relatedInformation, tagSupport, versionSupport, codeDescriptionSupport, dataSupport
)

@Serializable class IsaacPublishDiagnosticsClientCapabilities(
    override val relatedInformation: Boolean? = null,

    @Serializable(with = DiagnosticTagSupportSerializer::class)
    override val tagSupport: DiagnosticTagSupport? = null,

    override val versionSupport: Boolean? = null,
    override val codeDescriptionSupport: Boolean? = null,
    override val dataSupport: Boolean? = null,
) : PublishDiagnosticsClientCapabilities

object PublishDiagnosticsClientSerializer : JsonContentPolymorphicSerializer<PublishDiagnosticsClientCapabilities>(
    PublishDiagnosticsClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacPublishDiagnosticsClientCapabilities.serializer() }
