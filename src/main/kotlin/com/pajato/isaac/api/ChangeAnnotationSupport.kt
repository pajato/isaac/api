package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface ChangeAnnotationSupport {
    /**
     * Whether the client groups edits with equal labels into tree nodes,
     * for instance all edits labelled with "Changes in Strings" would
     * be a tree node.
     */
    val groupsOnLabel: Boolean?
}

fun ChangeAnnotationSupport(groupsOnLabel: Boolean? = null): ChangeAnnotationSupport =
    IsaacChangeAnnotationSupport(groupsOnLabel)

@Serializable class IsaacChangeAnnotationSupport(override val groupsOnLabel: Boolean? = null) : ChangeAnnotationSupport

object ChangeAnnotationSupportSerializer : JsonContentPolymorphicSerializer<ChangeAnnotationSupport>(
    ChangeAnnotationSupport::class
) { override fun selectDeserializer(element: JsonElement) = IsaacChangeAnnotationSupport.serializer() }
