package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface CallHierarchyOptions : WorkDoneProgressOptions

fun CallHierarchyOptions(workDoneProgress: Boolean? = null): CallHierarchyOptions =
    IsaacCallHierarchyOptions(workDoneProgress)

@Serializable class IsaacCallHierarchyOptions(override val workDoneProgress: Boolean? = null) : CallHierarchyOptions

object CallHierarchyOptionsSerializer : JsonContentPolymorphicSerializer<CallHierarchyOptions>(
    CallHierarchyOptions::class
) {
    override fun selectDeserializer(element: JsonElement) = IsaacCallHierarchyOptions.serializer()
}
