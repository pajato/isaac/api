package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DefinitionOptions : WorkDoneProgressOptions

fun DefinitionOptions(workDoneProgress: Boolean? = null): DefinitionOptions = IsaacDefinitionOptions(workDoneProgress)

@Serializable class IsaacDefinitionOptions(override val workDoneProgress: Boolean? = null) : DefinitionOptions

object ContentDefinitionOptionsSerializer : JsonContentPolymorphicSerializer<DefinitionOptions>(
    DefinitionOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDefinitionOptions.serializer() }
