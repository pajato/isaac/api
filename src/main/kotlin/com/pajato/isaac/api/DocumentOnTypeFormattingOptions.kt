@file:Suppress("GrazieInspection")

package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DocumentOnTypeFormattingOptions {
    /**
     * A character on which formatting should be triggered, like `}`.
     */
    val firstTriggerCharacter: String

    /**
     * More trigger characters.
     */
    val moreTriggerCharacter: Array<String>?
}

fun DocumentOnTypeFormattingOptions(
    firstTriggerCharacter: String,
    moreTriggerCharacter: Array<String>? = null
): DocumentOnTypeFormattingOptions = IsaacDocumentOnTypeFormattingOptions(firstTriggerCharacter, moreTriggerCharacter)

@Serializable class IsaacDocumentOnTypeFormattingOptions(
    override val firstTriggerCharacter: String,
    override val moreTriggerCharacter: Array<String>? = null
) : DocumentOnTypeFormattingOptions

object DocumentOnTypeFormattingOptionsSerializer : JsonContentPolymorphicSerializer<DocumentOnTypeFormattingOptions>(
    DocumentOnTypeFormattingOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDocumentOnTypeFormattingOptions.serializer() }
