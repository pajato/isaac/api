package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface MonikerRegistrationOptions :
    TextDocumentRegistrationOptions, MonikerOptions, StaticRegistrationOptions

fun MonikerRegistrationOptions(
    documentSelector: List<DocumentFilter>? = null,
    workDoneProgress: Boolean? = null,
    id: String? = null,
): MonikerRegistrationOptions = IsaacMonikerRegistrationOptions(documentSelector, workDoneProgress, id)

@Serializable class IsaacMonikerRegistrationOptions(
    override val documentSelector: List<DocumentFilter>? = null,
    override val workDoneProgress: Boolean? = null,
    override val id: String? = null,
) : MonikerRegistrationOptions

object MonikerRegistrationOptionsSerializer : JsonContentPolymorphicSerializer<MonikerRegistrationOptions>(
    MonikerRegistrationOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacMonikerRegistrationOptions.serializer() }
