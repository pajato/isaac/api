package com.pajato.isaac.api

enum class MarkupKind { PlainText, Markdown }
