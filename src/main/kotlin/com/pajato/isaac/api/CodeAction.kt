package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

enum class CodeActionKind {
    Empty, QuickFix, Refactor, RefactorExtract, RefactorInline, RefactorRewrite, Source, SourceOrganizeImports
}

interface CodeAction {
    /**
     * The code action kind values the client supports. When this
     * property exists the client also guarantees that it will
     * handle values outside its set gracefully and falls back
     * to a default value when unknown.
     */
    val valueSet: Array<CodeActionKind>
}

fun CodeAction(valueSet: Array<CodeActionKind>): CodeAction = IsaacCodeAction(valueSet)

@Serializable class IsaacCodeAction(
    override val valueSet: Array<CodeActionKind>,
) : CodeAction

object CodeActionKindSerializer : JsonContentPolymorphicSerializer<CodeAction>(CodeAction::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacCodeAction.serializer()
}
