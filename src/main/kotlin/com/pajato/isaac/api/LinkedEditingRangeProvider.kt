package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.JsonTransformingSerializer
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put

@Serializable sealed class LinkedEditingRangeProvider

@Serializable class BooleanLinkedEditingRangeProvider(val state: Boolean) : LinkedEditingRangeProvider()

@Serializable class OptionsLinkedEditingRangeProvider(
    @Serializable(with = LinkedEditingRangeOptionsSerializer::class) val options: LinkedEditingRangeOptions
) : LinkedEditingRangeProvider()

@Serializable class RegistrationOptionsLinkedEditingRangeProvider(
    @Serializable(with = LinkedEditingRangeRegistrationOptionsSerializer::class)
    val registrationOptions: LinkedEditingRangeRegistrationOptions
) : LinkedEditingRangeProvider()

object LinkedEditingRangeProviderSerializer : JsonContentPolymorphicSerializer<LinkedEditingRangeProvider>(
    LinkedEditingRangeProvider::class
) {
    override fun selectDeserializer(element: JsonElement) = when {
        element is JsonObject && element.containsKey("state") -> BooleanLinkedEditingRangeProvider.serializer()
        element is JsonObject && element.containsKey("options") -> OptionsLinkedEditingRangeProvider.serializer()
        element is JsonObject && element.containsKey("registrationOptions") ->
            RegistrationOptionsLinkedEditingRangeProvider.serializer()
        else -> throw IllegalStateException("Invalid element: $element")
    }
}

object LinkedEditingRangeProviderTransformingSerializer :
    JsonTransformingSerializer<LinkedEditingRangeProvider>(LinkedEditingRangeProvider.serializer()) {
    private const val booleanKey = "state"
    private const val optionsKey = "options"
    private const val registrationOptionsKey = "registrationOptions"
    private const val typeKey = "type"

    override fun transformSerialize(element: JsonElement): JsonElement {
        fun getPrimitive(element: JsonObject) = element[booleanKey] as JsonPrimitive
        fun isBoolean(element: JsonObject) = element.containsKey(booleanKey) && element[booleanKey] is JsonPrimitive
        fun isOptions(element: JsonObject) = element.containsKey(optionsKey) && element[optionsKey] is JsonObject
        fun isRegistrationOptions(element: JsonObject) = element.containsKey(registrationOptionsKey) &&
            element[registrationOptionsKey] is JsonObject
        fun isValid(element: JsonObject) = element.size == 2 &&
            (isBoolean(element) || isOptions(element) || isRegistrationOptions(element))
        fun getObject(element: JsonObject) = when {
            isOptions(element) -> element[optionsKey] as JsonObject
            else -> element[registrationOptionsKey] as JsonObject
        }

        check(element is JsonObject && isValid(element)) { "Invalid element content: $element." }
        return if (element[booleanKey] is JsonPrimitive) getPrimitive(element) else getObject(element)
    }

    override fun transformDeserialize(element: JsonElement): JsonElement {
        fun isValidBoolean() = element is JsonPrimitive && (element.content == "true" || element.content == "false")
        fun isValidOptions() = element is JsonObject && element.size == 1
        fun isValidRegistrationOptions() = element is JsonObject && element.size == 3
        fun isValid() = isValidBoolean() || isValidOptions() || isValidRegistrationOptions()

        fun transformToBoolean(data: Boolean) = buildJsonObject {
            put(typeKey, BooleanLinkedEditingRangeProvider::class.java.name)
            put(booleanKey, data)
        }

        fun transformToOptions(options: JsonElement) = buildJsonObject {
            put(typeKey, OptionsLinkedEditingRangeProvider::class.java.name)
            put(optionsKey, options)
        }

        fun transformToRegistrationOptions(options: JsonElement) = buildJsonObject {
            put(typeKey, RegistrationOptionsLinkedEditingRangeProvider::class.java.name)
            put(registrationOptionsKey, options)
        }

        check(isValid()) { "Invalid element: $element" }
        return when {
            isValidBoolean() -> transformToBoolean((element as JsonPrimitive).content.toBoolean())
            isValidOptions() -> transformToOptions(element)
            else -> transformToRegistrationOptions(element)
        }
    }
}
