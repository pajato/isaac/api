package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DocumentRangeFormattingOptions : WorkDoneProgressOptions

fun DocumentRangeFormattingOptions(workDoneProgress: Boolean? = null): DocumentRangeFormattingOptions =
    IsaacDocumentRangeFormattingOptions(workDoneProgress)

@Serializable class IsaacDocumentRangeFormattingOptions(
    override val workDoneProgress: Boolean? = null
) : DocumentRangeFormattingOptions

object DocumentRangeFormattingOptionsSerializer : JsonContentPolymorphicSerializer<DocumentRangeFormattingOptions>(
    DocumentRangeFormattingOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDocumentRangeFormattingOptions.serializer() }
