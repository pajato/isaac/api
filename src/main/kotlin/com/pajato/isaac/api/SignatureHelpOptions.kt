package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface SignatureHelpOptions : WorkDoneProgressOptions {
    /**
     * The characters that trigger signature help
     * automatically.
     */
    val triggerCharacters: Array<String>?

    /**
     * List of characters that re-trigger signature help.
     *
     * These trigger characters are only active when signature help is already
     * showing. All trigger characters are also counted as re-trigger
     * characters.
     *
     * @since 3.15.0
     */
    val retriggerCharacters: Array<String>?
}

fun SignatureHelpOptions(
    workDoneProgress: Boolean? = null,
    triggerCharacters: Array<String>? = null,
    retriggerCharacters: Array<String>? = null
): SignatureHelpOptions = IsaacSignatureHelpOptions(workDoneProgress, triggerCharacters, retriggerCharacters)

@Serializable class IsaacSignatureHelpOptions(
    override val workDoneProgress: Boolean? = null,
    override val triggerCharacters: Array<String>? = null,
    override val retriggerCharacters: Array<String>? = null,
) : SignatureHelpOptions

object SignatureHelpOptionsSerializer : JsonContentPolymorphicSerializer<SignatureHelpOptions>(
    SignatureHelpOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacSignatureHelpOptions.serializer() }
