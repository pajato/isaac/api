package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface Position {
    val line: Int
    val character: Int
}

fun Position(line: Int = 0, character: Int = 0): Position = IsaacPosition(line, character)

@Serializable class IsaacPosition(override val line: Int, override val character: Int) : Position

object PositionSerializer : JsonContentPolymorphicSerializer<Position>(Position::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacPosition.serializer()
}
