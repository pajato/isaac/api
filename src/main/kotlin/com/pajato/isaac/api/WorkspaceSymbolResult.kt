package com.pajato.isaac.api

import kotlinx.serialization.Serializable

interface WorkspaceSymbolResult {
    /**
     * A list (possibly empty) of symbol information.
     */
    val list: List<SymbolInformation>
}

fun WorkspaceSymbolResult(list: List<SymbolInformation>): WorkspaceSymbolResult = IsaacWorkspaceSymbolResult(list)

@Serializable class IsaacWorkspaceSymbolResult(
    @Serializable(with = SymbolInformationListSerializer::class)
    override val list: List<SymbolInformation>
) : WorkspaceSymbolResult, Result()
