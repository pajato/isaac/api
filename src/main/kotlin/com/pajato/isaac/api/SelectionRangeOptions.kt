package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface SelectionRangeOptions : WorkDoneProgressOptions

fun SelectionRangeOptions(workDoneProgress: Boolean? = null): SelectionRangeOptions =
    IsaacSelectionRangeOptions(workDoneProgress)

@Serializable class IsaacSelectionRangeOptions(override val workDoneProgress: Boolean? = null) : SelectionRangeOptions

object SelectionRangeOptionsSerializer : JsonContentPolymorphicSerializer<SelectionRangeOptions>(
    SelectionRangeOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacSelectionRangeOptions.serializer() }
