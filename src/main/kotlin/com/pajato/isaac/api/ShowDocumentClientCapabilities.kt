package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

/**
 * Client capabilities for the show document request.
 *
 * @since 3.16.0
 */
interface ShowDocumentClientCapabilities {
    /**
     * The client has support for the show document
     * request.
     */
    val support: Boolean
}

fun ShowDocumentClientCapabilities(support: Boolean): ShowDocumentClientCapabilities =
    IsaacShowDocumentClientCapabilities(support)

@Serializable class IsaacShowDocumentClientCapabilities(
    override val support: Boolean
) : ShowDocumentClientCapabilities

private typealias ShowDocument = ShowDocumentClientCapabilities

object ShowDocumentSerializer : JsonContentPolymorphicSerializer<ShowDocument>(ShowDocument::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacShowDocumentClientCapabilities.serializer()
}
