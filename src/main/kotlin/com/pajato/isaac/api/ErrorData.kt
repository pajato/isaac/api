package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.JsonTransformingSerializer
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put

@Serializable sealed class ErrorData

// @Serializable class ListErrorData<T>(val list: List<T>) : ErrorData()

@Serializable class BooleanErrorData(val data: Boolean) : ErrorData()

@Serializable class NumberErrorData(val data: Int) : ErrorData()

// @Serializable class ObjectErrorData(val objectErrorData: Any) : ErrorData()

@Serializable class StringErrorData(val data: String) : ErrorData()

/**
 * Use a transforming serializer to strip the type discriminant and flatten the data field from an object to a primitive
 * when encoding.
 */
object ErrorDataTransformingSerializer : JsonTransformingSerializer<ErrorData>(ErrorData.serializer()) {

    override fun transformSerialize(element: JsonElement): JsonElement {
        check(element is JsonObject && element["data"] != null) { "Invalid element content: $element." }
        return element["data"] as JsonPrimitive
    }

    override fun transformDeserialize(element: JsonElement): JsonElement {
        fun isValidNumber(number: String) = number.toIntOrNull() != null
        fun isValidBoolean(boolean: String) = boolean == "true" || boolean == "false"

        fun transform(boolean: Boolean) = buildJsonObject {
            put("type", BooleanErrorData::class.java.name)
            put("data", boolean)
        }

        fun transform(data: Int) = buildJsonObject {
            put("type", NumberErrorData::class.java.name)
            put("data", data)
        }

        fun transform(data: String) = buildJsonObject {
            put("type", StringErrorData::class.java.name)
            put("data", data)
        }

        return when {
            element is JsonPrimitive && isValidNumber(element.content) -> transform(element.content.toInt())
            element is JsonPrimitive && isValidBoolean(element.content) -> transform(element.content.toBoolean())
            else -> transform((element as JsonPrimitive).content)
        }
    }
}
