package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface LinkedEditingRangeClientCapabilities {
    /**
     * Whether implementation supports dynamic registration.
     * If this is set to `true` the client supports the new
     * `(TextDocumentRegistrationOptions & StaticRegistrationOptions)`
     * return value for the corresponding server capability as well.
     */
    val dynamicRegistration: Boolean?
}

fun LinkedEditingRangeClientCapabilities(dynamicRegistration: Boolean? = null): LinkedEditingRangeClientCapabilities =
    IsaacLinkedEditingRangeClientCapabilities(dynamicRegistration)

@Serializable class IsaacLinkedEditingRangeClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
) : LinkedEditingRangeClientCapabilities

typealias LinkedEditingRangeCC = LinkedEditingRangeClientCapabilities

object LinkedEditingRangeClientCapabilitiesSerializer : JsonContentPolymorphicSerializer<LinkedEditingRangeCC>(
    LinkedEditingRangeClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacLinkedEditingRangeClientCapabilities.serializer() }
