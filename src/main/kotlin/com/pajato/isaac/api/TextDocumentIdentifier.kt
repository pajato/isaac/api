package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface TextDocumentIdentifier {
    /**
     * The text document's URI.
     */
    val uri: DocumentUri
}

fun TextDocumentIdentifier(uri: DocumentUri): TextDocumentIdentifier = IsaacTextDocumentIdentifier(uri)

@Serializable class IsaacTextDocumentIdentifier(
    override val uri: DocumentUri
) : TextDocumentIdentifier

object TextDocumentIdentifierSerializer : JsonContentPolymorphicSerializer<TextDocumentIdentifier>(
    TextDocumentIdentifier::class
) { override fun selectDeserializer(element: JsonElement) = IsaacTextDocumentIdentifier.serializer() }
