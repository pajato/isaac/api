package com.pajato.isaac.api

import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject

/**
 * A general message as defined by JSON-RPC. The language server protocol always uses "2.0" as the jsonrpc version.
 */
interface Message {
    val jsonrpc: String
}

object MessageSerializer : JsonContentPolymorphicSerializer<Message>(Message::class) {

    override fun selectDeserializer(element: JsonElement): KSerializer<out Message> {
        fun isRequest() = element is JsonObject && element.containsKey("id") && element.containsKey("method")
        fun isResponse() = element is JsonObject && element.containsKey("id")
        fun isNotification() = element is JsonObject && element.containsKey("method")
        fun isLog() = element is JsonObject && element.containsKey("text")

        return when {
            isRequest() -> IsaacRequestMessage.serializer()
            isResponse() -> IsaacResponseMessage.serializer()
            isNotification() -> IsaacNotificationMessage.serializer()
            isLog() -> IsaacLogMessage.serializer()
            else -> throw IllegalStateException("Invalid element detected: $element")
        }
    }
}
