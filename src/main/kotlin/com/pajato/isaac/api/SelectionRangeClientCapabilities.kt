package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface SelectionRangeClientCapabilities {
    /**
     * Whether implementation supports dynamic registration for selection range
     * providers. If this is set to `true` the client supports the new
     * `SelectionRangeRegistrationOptions` return value for the corresponding
     * server capability as well.
     */
    val dynamicRegistration: Boolean?
}

fun SelectionRangeClientCapabilities(dynamicRegistration: Boolean? = null): SelectionRangeClientCapabilities =
    IsaacSelectionRangeClientCapabilities(dynamicRegistration)

@Serializable class IsaacSelectionRangeClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
) : SelectionRangeClientCapabilities

object SelectionRangeClientCapabilitiesSerializer : JsonContentPolymorphicSerializer<SelectionRangeClientCapabilities>(
    SelectionRangeClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacSelectionRangeClientCapabilities.serializer() }
