package com.pajato.isaac.api

import kotlinx.serialization.Polymorphic
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface ClientCapabilities {
    /**
     * Workspace specific client capabilities.
     */
    val workspace: WorkspaceClientCapabilities?

    /**
     * Text document specific client capabilities.
     */
    val textDocument: TextDocumentClientCapabilities?

    /**
     * Window specific client capabilities.
     */
    val window: WindowClientCapabilities?

    /**
     * General client capabilities.
     *
     * @since 3.16.0
     */
    val general: GeneralClientCapabilities?

    /**
     * Experimental client capabilities.
     */
    val experimental: Any?
}

fun ClientCapabilities(
    workspace: WorkspaceClientCapabilities? = null,
    textDocument: TextDocumentClientCapabilities? = null,
    window: WindowClientCapabilities? = null,
    general: GeneralClientCapabilities? = null,
    experimental: Any? = null,
): ClientCapabilities = IsaacClientCapabilities(workspace, textDocument, window, general, experimental)

@Serializable
class IsaacClientCapabilities(
    @Serializable(with = WorkspaceClientSerializer::class)
    override val workspace: WorkspaceClientCapabilities? = null,

    @Serializable(with = TextDocumentClientCapabilitiesSerializer::class)
    override val textDocument: TextDocumentClientCapabilities? = null,

    @Serializable(with = WindowClientCapabilitiesSerializer::class)
    override val window: WindowClientCapabilities? = null,

    @Serializable(with = GeneralClientCapabilitiesSerializer::class)
    override val general: GeneralClientCapabilities? = null,

    @Polymorphic
    override val experimental: Any? = null
) : ClientCapabilities

object ClientCapabilitiesSerializer : JsonContentPolymorphicSerializer<ClientCapabilities>(
    ClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacClientCapabilities.serializer() }
