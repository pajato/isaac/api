package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DocumentHighlightClientCapabilities {
    /**
     * Whether document highlight supports dynamic registration.
     */
    val dynamicRegistration: Boolean?
}

fun DocumentHighlightClientCapabilities(dynamicRegistration: Boolean? = null): DocumentHighlightClientCapabilities =
    IsaacDocumentHighlightClientCapabilities(dynamicRegistration)

@Serializable class IsaacDocumentHighlightClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
) : DocumentHighlightClientCapabilities

object DocumentHighlightSerializer : JsonContentPolymorphicSerializer<DocumentHighlightClientCapabilities>(
    DocumentHighlightClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDocumentHighlightClientCapabilities.serializer() }
