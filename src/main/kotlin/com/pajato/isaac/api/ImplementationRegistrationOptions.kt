package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface ImplementationRegistrationOptions :
    TextDocumentRegistrationOptions, ImplementationOptions, StaticRegistrationOptions

fun ImplementationRegistrationOptions(
    documentSelector: List<DocumentFilter>? = null,
    workDoneProgress: Boolean? = null,
    id: String? = null
): ImplementationRegistrationOptions = IsaacImplementationRegistrationOptions(documentSelector, workDoneProgress, id)

@Serializable class IsaacImplementationRegistrationOptions(
    override val documentSelector: List<DocumentFilter>? = null,
    override val workDoneProgress: Boolean? = null,
    override val id: String? = null,
) : ImplementationRegistrationOptions

object ImplementationRegistrationOptionsSerializer :
    JsonContentPolymorphicSerializer<ImplementationRegistrationOptions>(ImplementationRegistrationOptions::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacImplementationRegistrationOptions.serializer()
}
