package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface CodeActionOptions : WorkDoneProgressOptions

fun CodeActionOptions(workDoneProgress: Boolean? = null): CodeActionOptions = IsaacCodeActionOptions(workDoneProgress)

@Serializable class IsaacCodeActionOptions(override val workDoneProgress: Boolean? = null) : CodeActionOptions

object CodeActionOptionsSerializer : JsonContentPolymorphicSerializer<CodeActionOptions>(CodeActionOptions::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacCodeActionOptions.serializer()
}
