package com.pajato.isaac.api

typealias DocumentUri = String

const val jsonrpcVersion = "2.0"
