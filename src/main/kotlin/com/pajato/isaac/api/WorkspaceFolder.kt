package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface WorkspaceFolder {
    /**
     * The associated URI for this workspace folder.
     */
    val uri: DocumentUri

    /**
     * The name of the workspace folder. Used to refer to this
     * workspace folder in the user interface.
     */
    val name: String
}

fun WorkspaceFolder(uri: DocumentUri, name: String): WorkspaceFolder = IsaacWorkspaceFolder(uri, name)

@Serializable class IsaacWorkspaceFolder(
    override val uri: DocumentUri,
    override val name: String,
) : WorkspaceFolder

object WorkspaceFolderSerializer : JsonContentPolymorphicSerializer<WorkspaceFolder>(WorkspaceFolder::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacWorkspaceFolder.serializer()
}
