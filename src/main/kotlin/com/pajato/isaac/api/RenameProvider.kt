package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.JsonTransformingSerializer
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put

@Serializable sealed class RenameProvider

@Serializable class BooleanRenameProvider(val state: Boolean) : RenameProvider()

@Serializable class OptionsRenameProvider(
    @Serializable(with = RenameOptionsSerializer::class) val options: RenameOptions
) : RenameProvider()

object RenameProviderSerializer : JsonContentPolymorphicSerializer<RenameProvider>(RenameProvider::class) {
    override fun selectDeserializer(element: JsonElement) = when {
        element is JsonObject && element.containsKey("state") -> BooleanRenameProvider.serializer()
        element is JsonObject && element.containsKey("options") -> OptionsRenameProvider.serializer()
        else -> throw IllegalStateException("Invalid element: $element")
    }
}

object RenameProviderTransformingSerializer : JsonTransformingSerializer<RenameProvider>(RenameProvider.serializer()) {
    private const val booleanKey = "state"
    private const val optionsKey = "options"
    private const val typeKey = "type"

    /**
     * Transform (flatten) the element to remove the type element and strip the data key.
     */
    override fun transformSerialize(element: JsonElement): JsonElement {
        fun getPrimitive(element: JsonObject) = element[booleanKey] as JsonPrimitive
        fun getObject(element: JsonObject) = element[optionsKey] as JsonObject
        fun isBoolean(element: JsonObject) = element.containsKey(booleanKey) && element[booleanKey] is JsonPrimitive
        fun isOptions(element: JsonObject) = element.containsKey(optionsKey) && element[optionsKey] is JsonObject
        fun isValid(element: JsonObject) = element.size == 2 && (isBoolean(element) || isOptions(element))

        check(element is JsonObject && isValid(element)) { "Invalid element content: $element." }
        return if (element[booleanKey] is JsonPrimitive) getPrimitive(element) else getObject(element)
    }

    /**
     * Transform (expand) the element to add a type and a data key
     */
    override fun transformDeserialize(element: JsonElement): JsonElement {
        fun isValidBoolean() = element is JsonPrimitive && (element.content == "true" || element.content == "false")
        fun isValidOptions() = element is JsonObject && element.size > 0

        fun transformToBoolean(data: Boolean) = buildJsonObject {
            put(typeKey, BooleanRenameProvider::class.java.name)
            put(booleanKey, data)
        }

        fun transformToOptions(options: JsonElement) = buildJsonObject {
            put(typeKey, OptionsRenameProvider::class.java.name)
            put(optionsKey, options)
        }

        check(isValidBoolean() || isValidOptions()) { "Invalid element: $element" }
        return when {
            isValidBoolean() -> transformToBoolean((element as JsonPrimitive).content.toBoolean())
            else -> transformToOptions(element)
        }
    }
}
