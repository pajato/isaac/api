package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface SemanticTokensLegend {
    /**
     * The token types a server uses.
     */
    val tokenTypes: Array<String>

    /**
     * The token modifiers a server uses.
     */
    val tokenModifiers: Array<String>
}

fun SemanticTokensLegend(tokenTypes: Array<String>, tokenModifiers: Array<String>): SemanticTokensLegend =
    IsaacSemanticTokensLegend(tokenTypes, tokenModifiers)

@Serializable class IsaacSemanticTokensLegend(
    override val tokenTypes: Array<String>,
    override val tokenModifiers: Array<String>,
) : SemanticTokensLegend

object SemanticTokensLegendSerializer : JsonContentPolymorphicSerializer<SemanticTokensLegend>(
    SemanticTokensLegend::class
) { override fun selectDeserializer(element: JsonElement) = IsaacSemanticTokensLegend.serializer() }
