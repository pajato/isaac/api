package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface SemanticTokensOptions : WorkDoneProgressOptions {
    /**
     * The legend used by the server
     */
    val legend: SemanticTokensLegend

    /**
     * Server supports providing semantic tokens for a specific range
     * of a document.
     */
    val range: Boolean?

    /**
     * Server supports providing semantic tokens for a full document, or
     * the server supports deltas for full documents.
     */
    val full: Full?
}

fun SemanticTokensOptions(
    workDoneProgress: Boolean? = null,
    legend: SemanticTokensLegend,
    range: Boolean? = null,
    full: Full? = null
): SemanticTokensOptions = IsaacSemanticTokensOptions(workDoneProgress, legend, range, full)

@Serializable class IsaacSemanticTokensOptions(
    override val workDoneProgress: Boolean? = null,

    @Serializable(with = SemanticTokensLegendSerializer::class)
    override val legend: SemanticTokensLegend,

    override val range: Boolean? = null,

    @Serializable(with = TransformingFullSerializer::class)
    override val full: Full? = null,
) : SemanticTokensOptions

object SemanticTokensOptionsSerializer : JsonContentPolymorphicSerializer<SemanticTokensOptions>(
    SemanticTokensOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacSemanticTokensOptions.serializer() }
