package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DeclarationOptions : WorkDoneProgressOptions

fun DeclarationOptions(workDoneProgress: Boolean? = null): DeclarationOptions =
    IsaacDeclarationOptions(workDoneProgress)

@Serializable class IsaacDeclarationOptions(override val workDoneProgress: Boolean? = null) : DeclarationOptions

object DeclarationOptionsSerializer : JsonContentPolymorphicSerializer<DeclarationOptions>(DeclarationOptions::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacDeclarationOptions.serializer()
}
