package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.JsonTransformingSerializer
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put

/**
 * The following provides a Kotlin sealed class implementation for the following typescript interface snippet:
 *
 * boolean | {delta?: boolean;}
 */
@Serializable sealed class Full

@Serializable class BooleanFullOptions(
    /**
     * Server supports providing semantic tokens for a full document.
     */
    val state: Boolean
) : Full()

@Serializable class DeltaFullOptions(
    /**
     * The server supports deltas for full documents.
     */
    val delta: Boolean? = null
) : Full()

/**
 * Use a content serializer to remove (encode) or add (decode) the class discriminant (type field) to register the
 * implementations in the context of the sealed class.
 */
object ContentFullSerializer : JsonContentPolymorphicSerializer<Full>(Full::class) {
    override fun selectDeserializer(element: JsonElement) = when {
        element is JsonObject && element.containsKey("state") -> BooleanFullOptions.serializer()
        element is JsonObject && element.containsKey("delta") -> DeltaFullOptions.serializer()
        else -> throw IllegalStateException("Invalid element: $element")
    }
}

/**
 * Use a transforming serializer to remove (encoding) or add (decoding) the class discriminant (type field) and
 * transform elements during encoding and decoding.
 */
object TransformingFullSerializer : JsonTransformingSerializer<Full>(Full.serializer()) {
    private const val booleanKey = "state"
    private const val optionsKey = "delta"
    private const val typeKey = "type"

    /**
     * Transform (flatten) the element to remove the type element and strip the data key.
     */
    override fun transformSerialize(element: JsonElement): JsonElement {
        fun getPrimitive(element: JsonObject, key: String) = element[key] as JsonPrimitive

        fun getObject(jsonElement: JsonElement) = buildJsonObject {
            put(optionsKey, jsonElement)
        }

        fun isValid(element: JsonObject) = element.size == 2 &&
            (element.containsKey(booleanKey) || element.containsKey(optionsKey))

        check(element is JsonObject && isValid(element)) { "Invalid element content: $element." }
        val key = element.keys.filterNot { it == typeKey }[0]
        return if (element.containsKey(booleanKey)) getPrimitive(element, key) else getObject(element[optionsKey]!!)
    }

    /**
     * Transform (expand) the element to add a type and a data key
     */
    override fun transformDeserialize(element: JsonElement): JsonElement {
        fun isValidBoolean(element: JsonElement?) = element != null && element is JsonPrimitive &&
            (element.content == "true" || element.content == "false")

        fun isValidDelta() = element is JsonObject && element.containsKey(optionsKey) &&
            isValidBoolean(element[optionsKey])

        fun transformToBoolean() = buildJsonObject {
            val data = (element as JsonPrimitive).content.toBoolean()
            put(typeKey, BooleanFullOptions::class.java.name)
            put(booleanKey, data)
        }

        fun transformToDelta() = buildJsonObject {
            val data = ((element as JsonObject)[optionsKey] as JsonPrimitive).content.toBoolean()
            put(typeKey, DeltaFullOptions::class.java.name)
            put(optionsKey, data)
        }

        return when {
            isValidBoolean(element) -> transformToBoolean()
            isValidDelta() -> transformToDelta()
            else -> throw java.lang.IllegalStateException("Unsupported element: $element!")
        }
    }
}
