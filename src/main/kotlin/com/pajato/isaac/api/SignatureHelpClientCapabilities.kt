package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface SignatureHelpClientCapabilities {
    /**
     * Whether signature help supports dynamic registration.
     */
    val dynamicRegistration: Boolean?

    /**
     * The client supports the following `SignatureInformation`
     * specific properties.
     */
    val signatureInformation: SignatureInformation?

    /**
     * The client supports to send additional context information for a
     * `textDocument/signatureHelp` request. A client that opts into
     * contextSupport will also support the `retriggerCharacters` on
     * `SignatureHelpOptions`.
     *
     * @since 3.15.0
     */
    val contextSupport: Boolean?
}

fun SignatureHelpClientCapabilities(
    dynamicRegistration: Boolean? = null,
    signatureInformation: SignatureInformation? = null,
    contextSupport: Boolean? = null,
): SignatureHelpClientCapabilities =
    IsaacSignatureHelpClientCapabilities(dynamicRegistration, signatureInformation, contextSupport)

@Serializable class IsaacSignatureHelpClientCapabilities(
    override val dynamicRegistration: Boolean? = null,

    @Serializable(with = SignatureInformationSerializer::class)
    override val signatureInformation: SignatureInformation? = null,

    override val contextSupport: Boolean? = null,
) : SignatureHelpClientCapabilities

object SignatureHelpClientCapabilitiesSerializer : JsonContentPolymorphicSerializer<SignatureHelpClientCapabilities>(
    SignatureHelpClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacSignatureHelpClientCapabilities.serializer() }
