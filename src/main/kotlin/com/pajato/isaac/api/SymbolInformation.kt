package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface SymbolInformation {
    /**
     * The name of this symbol.
     */
    val name: String

    /**
     * The kind of this symbol.
     */
    val kind: Int // SymbolKind

    /**
     * Tags for this completion item.
     *
     * @since 3.16.0
     */
    val tags: Array<SymbolTag>?

    /**
     * Indicates if this symbol is deprecated.
     *
     * @deprecated Use tags instead
     */
    val deprecated: Boolean?

    /**
     * The location of this symbol. The location's range is used by a tool
     * to reveal the location in the editor. If the symbol is selected in the
     * tool the range's start information is used to position the cursor. So
     * the range usually spans more than the actual symbol's name and does
     * normally include things like visibility modifiers.
     *
     * The range doesn't have to denote a node range in the sense of an abstract
     * syntax tree. It can therefore not be used to re-construct a hierarchy of
     * the symbols.
     */
    val location: Location

    /**
     * The name of the symbol containing this symbol. This information is for
     * user interface purposes (e.g. to render a qualifier in the user interface
     * if necessary). It can't be used to re-infer a hierarchy for the document
     * symbols.
     */
    val containerName: String?
}

fun SymbolInformation(
    name: String,
    kind: SymbolKind,
    tags: Array<SymbolTag>? = null,
    deprecated: Boolean? = null,
    location: Location,
    containerName: String? = null,
): SymbolInformation = IsaacSymbolInformation(name, kind.ordinal, tags, deprecated, location, containerName)

@Serializable class IsaacSymbolInformation(
    override val name: String,
    override val kind: Int, // SymbolKind,
    override val tags: Array<SymbolTag>? = null,
    override val deprecated: Boolean? = null,

    @Serializable(with = LocationSerializer::class)
    override val location: Location,

    override val containerName: String? = null,
) : SymbolInformation

object SymbolInformationSerializer : JsonContentPolymorphicSerializer<SymbolInformation>(SymbolInformation::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacSymbolInformation.serializer()
}
