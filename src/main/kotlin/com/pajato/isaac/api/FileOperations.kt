package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface FileOperations {
    /**
     * Whether the client supports dynamic registration for file
     * requests/notifications.
     */
    val dynamicRegistration: Boolean?

    /**
     * The client has support for sending didCreateFiles notifications.
     */
    val didCreate: Boolean?

    /**
     * The client has support for sending willCreateFiles requests.
     */
    val willCreate: Boolean?

    /**
     * The client has support for sending didRenameFiles notifications.
     */
    val didRename: Boolean?

    /**
     * The client has support for sending willRenameFiles requests.
     */
    val willRename: Boolean?

    /**
     * The client has support for sending didDeleteFiles notifications.
     */
    val didDelete: Boolean?

    /**
     * The client has support for sending willDeleteFiles requests.
     */
    val willDelete: Boolean?
}

fun FileOperations(
    dynamicRegistration: Boolean? = null,
    didCreate: Boolean? = null,
    willCreate: Boolean? = null,
    didRename: Boolean? = null,
    willRename: Boolean? = null,
    didDelete: Boolean? = null,
    willDelete: Boolean? = null
): FileOperations = IsaacFileOperations(
    dynamicRegistration, didCreate, willCreate, didRename, willRename, didDelete, willDelete
)

@Serializable class IsaacFileOperations(
    override val dynamicRegistration: Boolean? = null,
    override val didCreate: Boolean? = null,
    override val willCreate: Boolean? = null,
    override val didRename: Boolean? = null,
    override val willRename: Boolean? = null,
    override val didDelete: Boolean? = null,
    override val willDelete: Boolean? = null,
) : FileOperations

object FileOperationsSerializer : JsonContentPolymorphicSerializer<FileOperations>(FileOperations::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacFileOperations.serializer()
}
