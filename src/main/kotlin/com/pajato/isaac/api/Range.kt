package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface Range {
    val start: Position
    val end: Position
}

fun Range(start: Position = Position(), end: Position = Position()): Range = IsaacRange(start, end)

@Serializable class IsaacRange(
    @Serializable(with = PositionSerializer::class)
    override val start: Position,

    @Serializable(with = PositionSerializer::class)
    override val end: Position
) : Range

object RangeSerializer : JsonContentPolymorphicSerializer<Range>(Range::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacRange.serializer()
}
