package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface ShowMessageResult {
    /**
     * The selected action item or null if no action item is selected.
     */
    val action: MessageActionItem?
}

fun ShowMessageResult(action: MessageActionItem? = null): ShowMessageResult = IsaacShowMessageResult(action)

@Serializable class IsaacShowMessageResult(
    @Serializable(with = MessageActionItemSerializer::class)
    override val action: MessageActionItem? = null
) : ShowMessageResult

object ShowMessageResultSerializer : JsonContentPolymorphicSerializer<ShowMessageResult>(ShowMessageResult::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacShowMessageResult.serializer()
}
