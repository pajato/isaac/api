package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DidChangeWatchedFilesClientCapabilities {
    /**
     * Did change watched files notification supports dynamic registration.
     * Please note that the current protocol doesn't support static
     * configuration for file changes from the server side.
     */
    val dynamicRegistration: Boolean?
}

fun DidChangeWatchedFilesClientCapabilities(dynamicRegistration: Boolean? = null):
    DidChangeWatchedFilesClientCapabilities = IsaacDidChangeWatchedFilesClientCapabilities(dynamicRegistration)

@Serializable class IsaacDidChangeWatchedFilesClientCapabilities(
    override val dynamicRegistration: Boolean? = null
) : DidChangeWatchedFilesClientCapabilities

private typealias DidChangeWatchedFiles = DidChangeWatchedFilesClientCapabilities

object DidChangeWatchedFilesSerializer : JsonContentPolymorphicSerializer<DidChangeWatchedFiles>(
    DidChangeWatchedFiles::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDidChangeWatchedFilesClientCapabilities.serializer() }
