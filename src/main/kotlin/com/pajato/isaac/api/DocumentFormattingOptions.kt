package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DocumentFormattingOptions : WorkDoneProgressOptions

fun DocumentFormattingOptions(workDoneProgress: Boolean? = null): DocumentFormattingOptions =
    IsaacDocumentFormattingOptions(workDoneProgress)

@Serializable class IsaacDocumentFormattingOptions(override val workDoneProgress: Boolean? = null) : DocumentFormattingOptions

object DocumentFormattingOptionsSerializer : JsonContentPolymorphicSerializer<DocumentFormattingOptions>(
    DocumentFormattingOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDocumentFormattingOptions.serializer() }
