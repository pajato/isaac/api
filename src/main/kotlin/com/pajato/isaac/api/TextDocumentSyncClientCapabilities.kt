package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface TextDocumentSyncClientCapabilities {
    /**
     * Whether text document synchronization supports dynamic registration.
     */
    val dynamicRegistration: Boolean?

    /**
     * The client supports sending will save notifications.
     */
    val willSave: Boolean?

    /**
     * The client supports sending a will save request and
     * waits for a response providing text edits which will
     * be applied to the document before it is saved.
     */
    val willSaveWaitUntil: Boolean?

    /**
     * The client supports did save notifications.
     */
    val didSave: Boolean?
}

fun TextDocumentSyncClientCapabilities(
    dynamicRegistration: Boolean? = null,
    willSave: Boolean? = null,
    willSaveWaitUntil: Boolean? = null,
    didSave: Boolean? = null,
): TextDocumentSyncClientCapabilities =
    IsaacTextDocumentSyncClientCapabilities(dynamicRegistration, willSave, willSaveWaitUntil, didSave)

@Serializable class IsaacTextDocumentSyncClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
    override val willSave: Boolean? = null,
    override val willSaveWaitUntil: Boolean? = null,
    override val didSave: Boolean? = null,
) : TextDocumentSyncClientCapabilities

internal typealias TextDocumentSyncCC = TextDocumentSyncClientCapabilities

object TextDocumentSyncClientCapabilitiesSerializer : JsonContentPolymorphicSerializer<TextDocumentSyncCC>(
    TextDocumentSyncClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacTextDocumentSyncClientCapabilities.serializer() }
