package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DocumentFormattingClientCapabilities {
    /**
     * Whether formatting supports dynamic registration.
     */
    val dynamicRegistration: Boolean?
}

fun DocumentFormattingClientCapabilities(dynamicRegistration: Boolean? = null): DocumentFormattingClientCapabilities =
    IsaacDocumentFormattingClientCapabilities(dynamicRegistration)

@Serializable class IsaacDocumentFormattingClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
) : DocumentFormattingClientCapabilities

object DocumentFormattingSerializer : JsonContentPolymorphicSerializer<DocumentFormattingClientCapabilities>(
    DocumentFormattingClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDocumentFormattingClientCapabilities.serializer() }
