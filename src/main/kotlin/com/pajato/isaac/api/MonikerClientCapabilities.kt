package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface MonikerClientCapabilities {
    /**
     * Whether implementation supports dynamic registration. If this is set to
     * `true` the client supports the new `(TextDocumentRegistrationOptions &
     * StaticRegistrationOptions)` return value for the corresponding server
     * capability as well.
     */
    val dynamicRegistration: Boolean?
}

fun MonikerClientCapabilities(dynamicRegistration: Boolean? = null): MonikerClientCapabilities =
    IsaacMonikerClientCapabilities(dynamicRegistration)

@Serializable class IsaacMonikerClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
) : MonikerClientCapabilities

object MonikerClientCapabilitiesSerializer : JsonContentPolymorphicSerializer<MonikerClientCapabilities>(
    MonikerClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacMonikerClientCapabilities.serializer() }
