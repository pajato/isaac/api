package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface WorkspaceSymbolClientCapabilities {
    /**
     * Symbol request supports dynamic registration.
     */
    val dynamicRegistration: Boolean?

    /**
     * Specific capabilities for the `SymbolKind` in the `workspace/symbol`
     * request.
     */
    val symbolKind: SymbolKind?

    /**
     * The client supports tags on `SymbolInformation`.
     * Clients supporting tags have to handle unknown tags gracefully.
     *
     * @since 3.16.0
     */
    val tagSupport: SymbolTagSupport?
}

fun WorkspaceSymbolClientCapabilities(
    dynamicRegistration: Boolean? = null,
    symbolKind: SymbolKind? = null,
    tagSupport: SymbolTagSupport? = null,
): WorkspaceSymbolClientCapabilities = IsaacWorkspaceSymbolClientCapabilities(
    dynamicRegistration, symbolKind, tagSupport
)

@Serializable class IsaacWorkspaceSymbolClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
    override val symbolKind: SymbolKind? = null,

    @Serializable(with = SymbolTagSupportSerializer::class)
    override val tagSupport: SymbolTagSupport? = null,
) : WorkspaceSymbolClientCapabilities

object WorkspaceSymbolClientSerializer : JsonContentPolymorphicSerializer<WorkspaceSymbolClientCapabilities>(
    WorkspaceSymbolClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacWorkspaceSymbolClientCapabilities.serializer() }
