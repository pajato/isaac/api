package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface ResponseMessage : Message {
    /**
     * The request id.
     */
    val id: Id? // string | integer | null

    /**
     * The result of a request. This member is REQUIRED on success.
     * This member MUST NOT exist if there was an error invoking the method.
     */
    val result: Result? // string | number | boolean | object | null;

    /**
     * The error object in case a request fails.
     */
    val error: ResponseError?
}

fun ResponseMessage(
    id: Id? = null,
    result: Result? = null,
    error: ResponseError? = null,
    jsonrpc: String = jsonrpcVersion,
): ResponseMessage = IsaacResponseMessage(jsonrpc, id, result, error)

@Serializable class IsaacResponseMessage(
    override val jsonrpc: String,

    @Serializable(with = IdTransformingSerializer::class)
    override val id: Id? = null,

    @Serializable(with = ResultTransformingSerializer::class)
    override val result: Result? = null,

    @Serializable(with = ResponseErrorSerializer::class)
    override val error: ResponseError? = null,
) : ResponseMessage

object ResponseMessageSerializer : JsonContentPolymorphicSerializer<ResponseMessage>(ResponseMessage::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacResponseMessage.serializer()
}
