package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.JsonTransformingSerializer
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put

object NumberSerializer : JsonTransformingSerializer<SealedNumber>(SealedNumber.serializer()) {

    override fun transformSerialize(element: JsonElement): JsonElement {
        check(element is JsonObject && element.size == 3) { "Invalid element content: $element." }
        return when {
            element.containsKey("byteValue") -> element["byteValue"] as JsonPrimitive
            element.containsKey("doubleValue") -> element["doubleValue"] as JsonPrimitive
            element.containsKey("floatValue") -> element["floatValue"] as JsonPrimitive
            element.containsKey("intValue") -> element["intValue"] as JsonPrimitive
            element.containsKey("longValue") -> element["longValue"] as JsonPrimitive
            else /* element.containsKey("shortValue") */ -> element["shortValue"] as JsonPrimitive
        }
    }

    override fun transformDeserialize(element: JsonElement): JsonElement {
        fun transform(className: String, value: String, fieldName: String, number: Number) = buildJsonObject {
            put("type", "com.pajato.isaac.api.$className")
            put("number", value)
            put(fieldName, number)
        }

        check(element is JsonPrimitive) { "Invalid element content: $element." }
        val number = element.content
        return when {
            number.toByteOrNull() != null -> transform("ByteNumber", number, "byteValue", number.toByte())
            number.toShortOrNull() != null -> transform("ShortNumber", number, "shortValue", number.toShort())
            number.toIntOrNull() != null -> transform("IntNumber", number, "intValue", number.toInt())
            number.toLongOrNull() != null -> transform("LongNumber", number, "longValue", number.toLong())
            else -> transform("DoubleNumber", number, "doubleValue", number.toDouble())
        }
    }
}

@Serializable sealed class SealedNumber(val number: String) {
    fun isByte() = number.toByteOrNull() != null
    fun isDouble() = number.toDoubleOrNull() != null
    fun isFloat() = number.toFloatOrNull() != null
    fun isInt() = number.toIntOrNull() != null
    fun isLong() = number.toLongOrNull() != null
    fun isShort() = number.toShortOrNull() != null
}

@Serializable class ByteNumber(internal val byteValue: Byte) : SealedNumber("$byteValue")
@Serializable class DoubleNumber(internal val doubleValue: Double) : SealedNumber("$doubleValue")
@Serializable class FloatNumber(internal val floatValue: Float) : SealedNumber("$floatValue")
@Serializable class IntNumber(internal val intValue: Int) : SealedNumber("$intValue")
@Serializable class LongNumber(internal val longValue: Long) : SealedNumber("$longValue")
@Serializable class ShortNumber(internal val shortValue: Short) : SealedNumber("$shortValue")
