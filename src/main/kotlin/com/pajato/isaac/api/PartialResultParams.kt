package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface PartialResultParams {
    /**
     * An optional token that a server can use to report partial results (e.g.
     * streaming) to the client.
     */
    val partialResultToken: ProgressToken?
}

fun PartialResultParams(partialResultToken: ProgressToken? = null): PartialResultParams =
    IsaacPartialResultParams(partialResultToken)

@Serializable class IsaacPartialResultParams(
    @Serializable(with = ProgressTokenSerializer::class) override val partialResultToken: ProgressToken? = null
) : PartialResultParams

object PartialResultParamsSerializer : JsonContentPolymorphicSerializer<PartialResultParams>(
    PartialResultParams::class
) { override fun selectDeserializer(element: JsonElement) = IsaacPartialResultParams.serializer() }
