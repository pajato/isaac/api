package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface MonikerOptions : WorkDoneProgressOptions

fun MonikerOptions(workDoneProgress: Boolean? = null): MonikerOptions = IsaacMonikerOptions(workDoneProgress)

@Serializable class IsaacMonikerOptions(override val workDoneProgress: Boolean? = null) : MonikerOptions

object MonikerOptionsSerializer :
    JsonContentPolymorphicSerializer<MonikerOptions>(MonikerOptions::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacMonikerOptions.serializer()
}
