package com.pajato.isaac.api

/**
 * Defines how the host (editor) should sync document changes to the language
 * server.
 */
object TextDocumentSyncKind {
    /**
     * Documents should not be synced at all.
     */
    const val None: Int = 0

    /**
     * Documents are synced by always sending the full content
     * of the document.
     */
    const val Full: Int = 1

    /**
     * Documents are synced by sending the full content on open.
     * After that only incremental updates to the document are
     * sent.
     */
    const val Incremental: Int = 2
}
