package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface HoverClientCapabilities {
    /**
     * Whether hover supports dynamic registration.
     */
    val dynamicRegistration: Boolean?

    /**
     * Client supports the following content formats if the content
     * property refers to a `literal of type MarkupContent`.
     * The order describes the preferred format of the client.
     */
    val contentFormat: Array<MarkupKind>?
}

fun HoverClientCapabilities(
    dynamicRegistration: Boolean? = null,
    contentFormat: Array<MarkupKind>? = null,
): HoverClientCapabilities = IsaacHoverClientCapabilities(dynamicRegistration, contentFormat)

@Serializable class IsaacHoverClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
    override val contentFormat: Array<MarkupKind>? = null,
) : HoverClientCapabilities

object HoverClientSerializer : JsonContentPolymorphicSerializer<HoverClientCapabilities>(
    HoverClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacHoverClientCapabilities.serializer() }
