package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface MessageActionItemCapability {
    /**
     * Whether the client supports additional attributes which
     * are preserved and sent back to the server in the
     * request's response.
     */
    val additionalPropertiesSupport: Boolean?
}

fun MessageActionItemCapability(additionalPropertiesSupport: Boolean? = null): MessageActionItemCapability =
    IsaacMessageActionItemCapability(additionalPropertiesSupport)

@Serializable class IsaacMessageActionItemCapability(
    override val additionalPropertiesSupport: Boolean? = null
) : MessageActionItemCapability

object MessageActionItemCapabilitySerializer : JsonContentPolymorphicSerializer<MessageActionItemCapability>(
    MessageActionItemCapability::class
) { override fun selectDeserializer(element: JsonElement) = IsaacMessageActionItemCapability.serializer() }
