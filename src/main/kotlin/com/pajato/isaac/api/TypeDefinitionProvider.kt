package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.JsonTransformingSerializer
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put

@Serializable sealed class TypeDefinitionProvider

@Serializable class BooleanTypeDefinitionProvider(val state: Boolean) : TypeDefinitionProvider()

@Serializable class OptionsTypeDefinitionProvider(
    @Serializable(with = TypeDefinitionOptionsSerializer::class) val options: TypeDefinitionOptions
) : TypeDefinitionProvider()

@Serializable class RegistrationOptionsTypeDefinitionProvider(
    @Serializable(with = TypeDefinitionRegistrationOptionsSerializer::class)
    val registrationOptions: TypeDefinitionRegistrationOptions
) : TypeDefinitionProvider()

object TypeDefinitionProviderSerializer : JsonContentPolymorphicSerializer<TypeDefinitionProvider>(
    TypeDefinitionProvider::class
) {
    override fun selectDeserializer(element: JsonElement) = when {
        element is JsonObject && element.containsKey("state") -> BooleanTypeDefinitionProvider.serializer()
        element is JsonObject && element.containsKey("options") -> OptionsTypeDefinitionProvider.serializer()
        element is JsonObject && element.containsKey("registrationOptions") ->
            RegistrationOptionsTypeDefinitionProvider.serializer()
        else -> throw IllegalStateException("Invalid element: $element")
    }
}

object TypeDefinitionProviderTransformingSerializer : JsonTransformingSerializer<TypeDefinitionProvider>(
    TypeDefinitionProvider.serializer()
) {
    private const val booleanKey = "state"
    private const val optionsKey = "options"
    private const val registrationOptionsKey = "registrationOptions"
    private const val typeKey = "type"

    override fun transformSerialize(element: JsonElement): JsonElement {
        fun getPrimitive(element: JsonObject) = element[booleanKey] as JsonPrimitive
        fun isBoolean(element: JsonObject) = element.containsKey(booleanKey) && element[booleanKey] is JsonPrimitive
        fun isOptions(element: JsonObject) = element.containsKey(optionsKey) && element[optionsKey] is JsonObject
        fun isRegistrationOptions(element: JsonObject) = element.containsKey(registrationOptionsKey) &&
            element[registrationOptionsKey] is JsonObject
        fun isValid(element: JsonObject) = element.size == 2 &&
            (isBoolean(element) || isOptions(element) || isRegistrationOptions(element))
        fun getObject(element: JsonObject) = when {
            isOptions(element) -> element[optionsKey] as JsonObject
            else -> element[registrationOptionsKey] as JsonObject
        }

        check(element is JsonObject && isValid(element)) { "Invalid element content: $element." }
        return if (element[booleanKey] is JsonPrimitive) getPrimitive(element) else getObject(element)
    }

    override fun transformDeserialize(element: JsonElement): JsonElement {
        fun isValidBoolean() = element is JsonPrimitive && (element.content == "true" || element.content == "false")
        fun isValidOptions() = element is JsonObject && element.size == 1
        fun isValidRegistrationOptions() = element is JsonObject && element.size == 3
        fun isValid() = isValidBoolean() || isValidOptions() || isValidRegistrationOptions()

        fun transformToBoolean(data: Boolean) = buildJsonObject {
            put(typeKey, BooleanTypeDefinitionProvider::class.java.name)
            put(booleanKey, data)
        }

        fun transformToOptions(options: JsonElement) = buildJsonObject {
            put(typeKey, OptionsTypeDefinitionProvider::class.java.name)
            put(optionsKey, options)
        }

        fun transformToRegistrationOptions(options: JsonElement) = buildJsonObject {
            put(typeKey, RegistrationOptionsTypeDefinitionProvider::class.java.name)
            put(registrationOptionsKey, options)
        }

        check(isValid()) { "Invalid element: $element" }
        return when {
            isValidBoolean() -> transformToBoolean((element as JsonPrimitive).content.toBoolean())
            isValidOptions() -> transformToOptions(element)
            else -> transformToRegistrationOptions(element)
        }
    }
}
