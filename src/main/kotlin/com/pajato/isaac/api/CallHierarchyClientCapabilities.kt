package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface CallHierarchyClientCapabilities {
    /**
     * Whether implementation supports dynamic registration.
     * If this is set to `true` the client supports the new
     * `(TextDocumentRegistrationOptions & StaticRegistrationOptions)`
     * return value for the corresponding server capability as well.
     */
    val dynamicRegistration: Boolean?
}
fun CallHierarchyClientCapabilities(dynamicRegistration: Boolean? = null): CallHierarchyClientCapabilities =
    IsaacCallHierarchyClientCapabilities(dynamicRegistration)

@Serializable class IsaacCallHierarchyClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
) : CallHierarchyClientCapabilities

object CallHierarchyClientCapabilitiesSerializer : JsonContentPolymorphicSerializer<CallHierarchyClientCapabilities>(
    CallHierarchyClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacCallHierarchyClientCapabilities.serializer() }
