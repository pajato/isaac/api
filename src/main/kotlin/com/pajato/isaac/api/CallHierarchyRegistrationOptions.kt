package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface CallHierarchyRegistrationOptions :
    TextDocumentRegistrationOptions, CallHierarchyOptions, StaticRegistrationOptions

fun CallHierarchyRegistrationOptions(
    documentSelector: List<DocumentFilter>? = null,
    workDoneProgress: Boolean? = null,
    id: String? = null
): CallHierarchyRegistrationOptions = IsaacCallHierarchyRegistrationOptions(documentSelector, workDoneProgress, id)

@Serializable class IsaacCallHierarchyRegistrationOptions(
    override val documentSelector: List<DocumentFilter>? = null,
    override val workDoneProgress: Boolean? = null,
    override val id: String? = null,
) : CallHierarchyRegistrationOptions

object CallHierarchyRegistrationOptionsSerializer :
    JsonContentPolymorphicSerializer<CallHierarchyRegistrationOptions>(
        CallHierarchyRegistrationOptions::class
    ) {
    override fun selectDeserializer(element: JsonElement) = IsaacCallHierarchyRegistrationOptions.serializer()
}
