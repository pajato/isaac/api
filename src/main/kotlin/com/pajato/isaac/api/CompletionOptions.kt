package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface CompletionOptions : WorkDoneProgressOptions {
    /**
     * Most tools trigger completion request automatically without explicitly
     * requesting it using a keyboard shortcut (e.g. Ctrl+Space). Typically, they
     * do so when the user starts to type an identifier. For example if the user
     * types `c` in a JavaScript file code complete will automatically pop up
     * present `console` besides others as a completion item. Characters that
     * make up identifiers don't need to be listed here.
     *
     * If code complete should automatically be trigger on characters not being
     * valid inside an identifier (for example `.` in JavaScript) list them in
     * `triggerCharacters`.
     */
    val triggerCharacters: Array<String>?

    /**
     * The list of all possible characters that commit a completion. This field
     * can be used if clients don't support individual commit characters per
     * completion item. See client capability
     * `completion.completionItem.commitCharactersSupport`.
     *
     * If a server provides both `allCommitCharacters` and commit characters on
     * an individual completion item the ones on the completion item win.
     *
     * @since 3.2.0
     */
    val allCommitCharacters: Array<String>?

    /**
     * The server provides support to resolve additional
     * information for a completion item.
     */
    val resolveProvider: Boolean?
}

fun CompletionOptions(
    workDoneProgress: Boolean? = null,
    triggerCharacters: Array<String>? = null,
    allCommitCharacters: Array<String>? = null,
    resolveProvider: Boolean? = null
): CompletionOptions = IsaacCompletionOptions(
    workDoneProgress, triggerCharacters, allCommitCharacters, resolveProvider
)

@Serializable class IsaacCompletionOptions(
    override val workDoneProgress: Boolean? = null,
    override val triggerCharacters: Array<String>? = null,
    override val allCommitCharacters: Array<String>? = null,
    override val resolveProvider: Boolean? = null,
) : CompletionOptions

object CompletionOptionsSerializer : JsonContentPolymorphicSerializer<CompletionOptions>(CompletionOptions::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacCompletionOptions.serializer()
}
