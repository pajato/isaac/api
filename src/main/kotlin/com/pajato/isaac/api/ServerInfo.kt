package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface ServerInfo {
    /**
     * The name of the server as defined by the server.
     */
    val name: String

    /**
     * The server's version as defined by the server.
     */
    val version: String?
}

fun ServerInfo(name: String, version: String?): ServerInfo = IsaacServerInfo(name, version)

@Serializable class IsaacServerInfo(override val name: String, override val version: String?) : ServerInfo

object ServerInfoSerializer : JsonContentPolymorphicSerializer<ServerInfo>(ServerInfo::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacServerInfo.serializer()
}
