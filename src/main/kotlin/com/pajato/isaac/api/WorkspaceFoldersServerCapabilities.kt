package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface WorkspaceFoldersServerCapabilities {
    val supported: Boolean?
    val changeNotifications: ChangeNotifications?
}

fun WorkspaceFoldersServerCapabilities(
    supported: Boolean? = null,
    changeNotifications: ChangeNotifications? = null,
): WorkspaceFoldersServerCapabilities = IsaacWorkspaceFoldersServerCapabilities(supported, changeNotifications)

@Serializable class IsaacWorkspaceFoldersServerCapabilities(
    override val supported: Boolean? = null,
    @Serializable(with = ChangeNotificationsTransformingSerializer::class)
    override val changeNotifications: ChangeNotifications? = null,
) : WorkspaceFoldersServerCapabilities

internal typealias WorkspaceFoldersSC = WorkspaceFoldersServerCapabilities

object WorkspaceFoldersServerCapabilitiesSerializer : JsonContentPolymorphicSerializer<WorkspaceFoldersSC>(
    WorkspaceFoldersServerCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacWorkspaceFoldersServerCapabilities.serializer() }
