package com.pajato.isaac.api

import kotlinx.serialization.Serializable

interface ShowMessageParams : Params {
    /**
     * The message type. See {@link MessageType}.
     */
    val type: Int // See MessageType.

    /**
     * The actual message.
     */
    val message: String
}

fun ShowMessageParams(type: Int, message: String): ShowMessageParams = IsaacShowMessageParams(type, message)

@Serializable class IsaacShowMessageParams(
    override val type: Int,
    override val message: String
) : ShowMessageParams
