package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface ImplementationOptions : WorkDoneProgressOptions

fun ImplementationOptions(workDoneProgress: Boolean? = null): ImplementationOptions =
    IsaacImplementationOptions(workDoneProgress)

@Serializable class IsaacImplementationOptions(override val workDoneProgress: Boolean? = null) : ImplementationOptions

object ImplementationOptionsSerializer : JsonContentPolymorphicSerializer<ImplementationOptions>(
    ImplementationOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacImplementationOptions.serializer() }
