package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface LinkedEditingRangeRegistrationOptions :
    TextDocumentRegistrationOptions, LinkedEditingRangeOptions, StaticRegistrationOptions

@Serializable class IsaacLinkedEditingRangeRegistrationOptions(
    override val documentSelector: List<DocumentFilter>? = null,
    override val workDoneProgress: Boolean? = null,
    override val id: String? = null,
) : LinkedEditingRangeRegistrationOptions

fun LinkedEditingRangeRegistrationOptions(
    documentSelector: List<DocumentFilter>? = null,
    workDoneProgress: Boolean? = null,
    id: String? = null,
): LinkedEditingRangeRegistrationOptions =
    IsaacLinkedEditingRangeRegistrationOptions(documentSelector, workDoneProgress, id)

object LinkedEditingRangeRegistrationOptionsSerializer :
    JsonContentPolymorphicSerializer<LinkedEditingRangeRegistrationOptions>(
        LinkedEditingRangeRegistrationOptions::class
    ) {
    override fun selectDeserializer(element: JsonElement) = IsaacLinkedEditingRangeRegistrationOptions.serializer()
}
