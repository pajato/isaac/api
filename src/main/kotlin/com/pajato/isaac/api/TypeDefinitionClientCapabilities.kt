package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface TypeDefinitionClientCapabilities {
    /**
     * Whether implementation supports dynamic registration. If this is set to
     * `true` the client supports the new `TypeDefinitionRegistrationOptions`
     * return value for the corresponding server capability as well.
     */
    val dynamicRegistration: Boolean?

    /**
     * The client supports additional metadata in the form of definition links.
     *
     * @since 3.14.0
     */
    val linkSupport: Boolean?
}

fun TypeDefinitionClientCapabilities(
    dynamicRegistration: Boolean? = null,
    linkSupport: Boolean? = null
): TypeDefinitionClientCapabilities = IsaacTypeDefinitionClientCapabilities(dynamicRegistration, linkSupport)

@Serializable class IsaacTypeDefinitionClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
    override val linkSupport: Boolean? = null,
) : TypeDefinitionClientCapabilities

object TypeDefinitionClientCapabilitiesSerializer : JsonContentPolymorphicSerializer<TypeDefinitionClientCapabilities>(
    TypeDefinitionClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacTypeDefinitionClientCapabilities.serializer() }
