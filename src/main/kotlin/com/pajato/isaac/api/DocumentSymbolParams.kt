package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DocumentSymbolParams : WorkDoneProgressParams, PartialResultParams, Params {
    /**
     * The text document.
     */
    val textDocument: TextDocumentIdentifier
}

fun DocumentSymbolParams(
    workDoneToken: ProgressToken? = null,
    partialResultToken: ProgressToken? = null,
    textDocument: TextDocumentIdentifier
): DocumentSymbolParams =
    IsaacDocumentSymbolParams(workDoneToken, partialResultToken, textDocument)

@Serializable class IsaacDocumentSymbolParams(
    @Serializable(with = ProgressTokenTransformingSerializer::class)
    override val workDoneToken: ProgressToken? = null,

    @Serializable(with = ProgressTokenTransformingSerializer::class)
    override val partialResultToken: ProgressToken? = null,

    @Serializable(with = TextDocumentIdentifierSerializer::class)
    override val textDocument: TextDocumentIdentifier
) : DocumentSymbolParams

object DocumentSymbolParamsSerializer : JsonContentPolymorphicSerializer<DocumentSymbolParams>(
    DocumentSymbolParams::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDocumentSymbolParams.serializer() }
