package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface NotificationMessage : Message {
    /**
     * The method to be invoked.
     */
    val method: String

    /**
     * The notification's params.
     */
    val params: Params?
}

fun NotificationMessage(method: String, params: Params? = null, jsonrpc: String = jsonrpcVersion): NotificationMessage =
    IsaacNotificationMessage(jsonrpc, method, params)

@Serializable class IsaacNotificationMessage(
    override val jsonrpc: String,
    override val method: String,

    @Serializable(with = ParamsSerializer::class)
    override val params: Params? = null,
) : NotificationMessage

object NotificationMessageSerializer : JsonContentPolymorphicSerializer<NotificationMessage>(
    NotificationMessage::class
) { override fun selectDeserializer(element: JsonElement) = IsaacNotificationMessage.serializer() }
