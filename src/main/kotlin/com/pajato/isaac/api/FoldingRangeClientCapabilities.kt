package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface FoldingRangeClientCapabilities {
    /**
     * Whether implementation supports dynamic registration for folding range
     * providers. If this is set to `true` the client supports the new
     * `FoldingRangeRegistrationOptions` return value for the corresponding
     * server capability as well.
     */
    val dynamicRegistration: Boolean?

    /**
     * The maximum number of folding ranges that the client prefers to receive
     * per document. The value serves as a hint, servers are free to follow the
     * limit.
     */
    val rangeLimit: /* Todo: should be UInt -- see note below! */ Int?

    /**
     * If set, the client signals that it only supports folding complete lines.
     * If set, client will ignore specified `startCharacter` and `endCharacter`
     * properties in a FoldingRange.
     */
    val lineFoldingOnly: Boolean?

    /** Note on UInt support.
     *
     * Kotlin as of LSP 3.16 supports unsigned integers but the serialization library does not. As a temporary
     * work-around signed integer support will be used as it is temporary and not a serious impediment.
     */
}

fun FoldingRangeClientCapabilities(
    dynamicRegistration: Boolean? = null,
    rangeLimit: Int? = null,
    lineFoldingOnly: Boolean? = null,
): FoldingRangeClientCapabilities =
    IsaacFoldingRangeClientCapabilities(dynamicRegistration, rangeLimit, lineFoldingOnly)

@Serializable class IsaacFoldingRangeClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
    override val rangeLimit: Int? = null,
    override val lineFoldingOnly: Boolean? = null,
) : FoldingRangeClientCapabilities

object FoldingRangeClientSerializer : JsonContentPolymorphicSerializer<FoldingRangeClientCapabilities>(
    FoldingRangeClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacFoldingRangeClientCapabilities.serializer() }
