package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface CodeLensOptions : WorkDoneProgressOptions {
    /**
     * Code lens has a resolve provider as well.
     */
    val resolveProvider: Boolean?
}

fun CodeLensOptions(workDoneProgress: Boolean? = null, resolveProvider: Boolean? = null): CodeLensOptions =
    IsaacCodeLensOptions(workDoneProgress, resolveProvider)

@Serializable class IsaacCodeLensOptions(
    override val workDoneProgress: Boolean? = null,
    override val resolveProvider: Boolean? = null,
) : CodeLensOptions

object CodeLensOptionsSerializer : JsonContentPolymorphicSerializer<CodeLensOptions>(CodeLensOptions::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacCodeLensOptions.serializer()
}
