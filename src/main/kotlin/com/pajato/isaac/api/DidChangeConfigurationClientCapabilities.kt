package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DidChangeConfigurationClientCapabilities {
    /**
     * Did change configuration notification supports dynamic registration.
     */
    val dynamicRegistration: Boolean?
}

fun DidChangeConfigurationClientCapabilities(
    dynamicRegistration: Boolean? = null
): DidChangeConfigurationClientCapabilities = IsaacDidChangeConfigurationClientCapabilities(dynamicRegistration)

@Serializable class IsaacDidChangeConfigurationClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
) : DidChangeConfigurationClientCapabilities

object DidChangeConfigurationSerializer : JsonContentPolymorphicSerializer<DidChangeConfigurationClientCapabilities>(
    DidChangeConfigurationClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDidChangeConfigurationClientCapabilities.serializer() }
