package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface FoldingRangeOptions : WorkDoneProgressOptions

fun FoldingRangeOptions(workDoneProgress: Boolean? = null): FoldingRangeOptions =
    IsaacFoldingRangeOptions(workDoneProgress)

@Serializable class IsaacFoldingRangeOptions(override val workDoneProgress: Boolean? = null) : FoldingRangeOptions

object FoldingRangeOptionsSerializer : JsonContentPolymorphicSerializer<FoldingRangeOptions>(
    FoldingRangeOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacFoldingRangeOptions.serializer() }
