package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.JsonTransformingSerializer
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put

@Serializable sealed class TextDocumentSync

@Serializable class OptionsTextDocumentSync(
    @Serializable(with = TextDocumentSyncOptionsSerializer::class) val options: TextDocumentSyncOptions
) : TextDocumentSync()

@Serializable class KindTextDocumentSync(val kind: Int) : TextDocumentSync()

/**
 * Use a content serializer to choose between the types of decoding to be done as well as to avoid class discriminants.
 */
object ContentTextDocumentSyncSerializer :
    JsonContentPolymorphicSerializer<TextDocumentSync>(TextDocumentSync::class) {

    override fun selectDeserializer(element: JsonElement) = when {
        element is JsonPrimitive && element.content.toIntOrNull() != null -> KindTextDocumentSync.serializer()
        element is JsonObject && element.containsKey("options") -> OptionsTextDocumentSync.serializer()
        element is JsonObject && element.containsKey("kind") -> KindTextDocumentSync.serializer()
        else -> throw IllegalStateException("Unsupported text document sync element: $element!")
    }
}

/**
 * Use a transforming serializer to remove (encoding) or add (decoding) the class discriminant (type field) and flatten
 * the kind element from an object to a primitive when encoding.
 */
object TransformingTextDocumentSyncSerializer :
    JsonTransformingSerializer<TextDocumentSync>(TextDocumentSync.serializer()) {

    override fun transformSerialize(element: JsonElement): JsonElement {
        fun getPrimitive(element: JsonObject) = element["kind"] as JsonPrimitive
        fun getObject(element: JsonObject) = element["options"] as JsonObject

        fun isValidKindOrObjects(element: JsonObject) =
            (element.size == 2 && element.containsKey("kind") && element["kind"] is JsonPrimitive) ||
                (element.size == 2 && element.containsKey("options") && element["options"] is JsonObject)

        check(element is JsonObject && isValidKindOrObjects(element)) { "Invalid element content: $element." }
        return if (element["kind"] is JsonPrimitive) getPrimitive(element) else getObject(element)
    }

    override fun transformDeserialize(element: JsonElement): JsonElement {
        fun isValidKind() = element is JsonPrimitive && element.content.toIntOrNull() != null
        fun isValidOptions() = element is JsonObject && element.size > 0

        fun transformToKind(kind: Int) = buildJsonObject {
            put("type", KindTextDocumentSync::class.java.name)
            put("kind", kind)
        }

        fun transformToOptions(options: JsonElement) = buildJsonObject {
            put("type", OptionsTextDocumentSync::class.java.name)
            put("options", options)
        }

        return when {
            isValidKind() -> transformToKind((element as JsonPrimitive).content.toInt())
            isValidOptions() -> transformToOptions(element)
            else -> throw IllegalStateException("Invalid element: $element")
        }
    }
}
