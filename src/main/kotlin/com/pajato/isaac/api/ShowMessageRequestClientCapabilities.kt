package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface ShowMessageRequestClientCapabilities {
    /**
     * Capabilities specific to the `MessageActionItem` type.
     */
    val messageActionItem: MessageActionItemCapability?
}

fun ShowMessageRequestClientCapabilities(
    messageActionItem: MessageActionItemCapability? = null
): ShowMessageRequestClientCapabilities = IsaacShowMessageRequestClientCapabilities(messageActionItem)

@Serializable class IsaacShowMessageRequestClientCapabilities(
    @Serializable(with = MessageActionItemCapabilitySerializer::class)
    override val messageActionItem: MessageActionItemCapability? = null
) : ShowMessageRequestClientCapabilities

typealias ShowMessageRequestCC = ShowMessageRequestClientCapabilities

object ShowMessageRequestClientCapabilitiesSerializer : JsonContentPolymorphicSerializer<ShowMessageRequestCC>(
    ShowMessageRequestCC::class
) { override fun selectDeserializer(element: JsonElement) = IsaacShowMessageRequestClientCapabilities.serializer() }
