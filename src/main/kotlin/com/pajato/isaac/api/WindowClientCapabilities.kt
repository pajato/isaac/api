package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface WindowClientCapabilities {
    /**
     * Whether client supports handling progress notifications. If set
     * servers are allowed to report in `workDoneProgress` property in the
     * request specific server capabilities.
     *
     * @since 3.15.0
     */
    val workDoneProgress: Boolean?

    /**
     * Capabilities specific to the showMessage request
     *
     * @since 3.16.0
     */
    val showMessage: ShowMessageRequestClientCapabilities?

    /**
     * Client capabilities for the show document request.
     *
     * @since 3.16.0
     */
    val showDocument: ShowDocumentClientCapabilities?
}

fun WindowClientCapabilities(
    workDoneProgress: Boolean? = null,
    showMessage: ShowMessageRequestClientCapabilities? = null,
    showDocument: ShowDocumentClientCapabilities? = null,
): WindowClientCapabilities = IsaacWindowClientCapabilities(workDoneProgress, showMessage, showDocument)

@Serializable class IsaacWindowClientCapabilities(
    override val workDoneProgress: Boolean? = null,

    @Serializable(with = ShowMessageRequestClientCapabilitiesSerializer::class)
    override val showMessage: ShowMessageRequestClientCapabilities? = null,

    @Serializable(with = ShowDocumentSerializer::class)
    override val showDocument: ShowDocumentClientCapabilities? = null,
) : WindowClientCapabilities

object WindowClientCapabilitiesSerializer : JsonContentPolymorphicSerializer<WindowClientCapabilities>(
    WindowClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacWindowClientCapabilities.serializer() }
