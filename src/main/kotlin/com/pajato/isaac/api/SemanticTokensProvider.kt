package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonTransformingSerializer
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put

@Serializable sealed class SemanticTokensProvider

@Serializable class OptionsSemanticTokensProvider(
    @Serializable(with = SemanticTokensOptionsSerializer::class)
    val options: SemanticTokensOptions
) : SemanticTokensProvider()

@Serializable class RegistrationOptionsSemanticTokensProvider(
    @Serializable(with = SemanticTokensRegistrationOptionsSerializer::class)
    val registrationOptions: SemanticTokensRegistrationOptions
) : SemanticTokensProvider()

object SemanticTokensProviderSerializer : JsonContentPolymorphicSerializer<SemanticTokensProvider>(
    SemanticTokensProvider::class
) {
    override fun selectDeserializer(element: JsonElement) = when {
        element is JsonObject && element.containsKey("options") -> OptionsSemanticTokensProvider.serializer()
        element is JsonObject && element.containsKey("registrationOptions") ->
            RegistrationOptionsSemanticTokensProvider.serializer()
        else -> throw IllegalStateException("Invalid element: $element")
    }
}

object SemanticTokensProviderTransformingSerializer : JsonTransformingSerializer<SemanticTokensProvider>(
    SemanticTokensProvider.serializer()
) {
    private const val optionsKey = "options"
    private const val registrationOptionsKey = "registrationOptions"
    private const val typeKey = "type"
    private const val idKey = "id"
    private const val documentSelectorKey = "documentSelector"

    /**
     * Transform (flatten) the element to remove the type element and strip the data key.
     */
    override fun transformSerialize(element: JsonElement): JsonElement {
        fun isOptions(element: JsonObject) = element.containsKey(optionsKey) && element[optionsKey] is JsonObject
        fun isRegistrationOptions(element: JsonObject) = element.containsKey(registrationOptionsKey) &&
            element[registrationOptionsKey] is JsonObject
        fun isValid(element: JsonObject) = element.size == 2 && (isOptions(element) || isRegistrationOptions(element))
        fun getObject(element: JsonObject) = when {
            isOptions(element) -> element[optionsKey] as JsonObject
            else -> element[registrationOptionsKey] as JsonObject
        }

        check(element is JsonObject && isValid(element)) { "Invalid element content: $element." }
        return getObject(element)
    }

    /**
     * Transform (expand) the element to add a type and a data key
     */
    override fun transformDeserialize(element: JsonElement): JsonElement {
        fun isValid() = element is JsonObject && element.containsKey("legend")
        fun isRegistrationOptions() = isValid() && element is JsonObject &&
            (element.containsKey(documentSelectorKey) || element.containsKey(idKey))

        fun transformToOptions(options: JsonElement) = buildJsonObject {
            put(typeKey, OptionsSemanticTokensProvider::class.java.name)
            put(optionsKey, options)
        }

        fun transformToRegistrationOptions(options: JsonElement) = buildJsonObject {
            put(typeKey, RegistrationOptionsSemanticTokensProvider::class.java.name)
            put(registrationOptionsKey, options)
        }

        check(isValid()) { "Invalid element: $element" }
        return if (isRegistrationOptions()) transformToRegistrationOptions(element) else transformToOptions(element)
    }
}
