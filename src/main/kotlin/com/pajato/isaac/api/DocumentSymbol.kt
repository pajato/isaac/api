package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

/**
 * Represents programming constructs like variables, classes, interfaces etc.
 * that appear in a document. Document symbols can be hierarchical, and they
 * have two ranges: one that encloses its definition and one that points to its
 * most interesting range, e.g. the range of an identifier.
 */
interface DocumentSymbol {

    /**
     * The name of this symbol. Will be displayed in the user interface and
     * therefore must not be an empty string or a string only consisting of
     * white spaces.
     */
    val name: String

    /**
     * More detail for this symbol, e.g. the signature of a function.
     */
    val detail: String?

    /**
     * The kind of this symbol.
     */
    val kind: Int // SymbolKind

    /**
     * Tags for this document symbol.
     *
     * @since 3.16.0
     */
    val tags: Array<SymbolTag>?

    /**
     * Indicates if this symbol is deprecated.
     *
     * @deprecated Use tags instead
     */
    val deprecated: Boolean?

    /**
     * The range enclosing this symbol not including leading/trailing whitespace
     * but everything else like comments. This information is typically used to
     * determine if the client's cursor is inside the symbol to reveal in the
     * symbol in the UI.
     */
    val range: Range

    /**
     * The range that should be selected and revealed when this symbol is being
     * picked, e.g. the name of a function. Must be contained by the `range`.
     */
    val selectionRange: Range

    /**
     * Children of this symbol, e.g. properties of a class.
     */
    val children: List<DocumentSymbol>?
}

fun DocumentSymbol(
    name: String,
    kind: SymbolKind,
    tags: Array<SymbolTag>? = null,
    deprecated: Boolean? = null,
    detail: String? = null,
    range: Range,
    selectionRange: Range = range,
    children: List<DocumentSymbol>? = null
): DocumentSymbol = IsaacDocumentSymbol(name, kind.ordinal, tags, deprecated, detail, range, selectionRange, children)

@Serializable class IsaacDocumentSymbol(
    override val name: String,
    override val kind: Int, // SymbolKind.ordinal
    override val tags: Array<SymbolTag>? = null,
    override val deprecated: Boolean? = null,
    override val detail: String? = null,

    @Serializable(with = RangeSerializer::class) override val range: Range,

    @Serializable(with = RangeSerializer::class) override val selectionRange: Range,

    @Serializable(with = DocumentSymbolListSerializer::class) override val children: List<DocumentSymbol>? = null,
) : DocumentSymbol

object DocumentSymbolSerializer : JsonContentPolymorphicSerializer<DocumentSymbol>(DocumentSymbol::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacDocumentSymbol.serializer()
}
