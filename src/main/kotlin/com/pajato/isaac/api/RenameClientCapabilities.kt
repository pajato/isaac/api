package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

enum class PrepareSupportDefaultBehavior { Identifier }

interface RenameClientCapabilities {
    /**
     * Whether rename supports dynamic registration.
     */
    val dynamicRegistration: Boolean?

    /**
     * Client supports testing for validity of rename operations
     * before execution.
     *
     * @since 3.12.0
     */
    val prepareSupport: Boolean?

    /**
     * Client supports the default behavior result
     * (`{ defaultBehavior: boolean }`).
     *
     * The value indicates the default behavior used by the
     * client.
     *
     * @since 3.16.0
     */
    val prepareSupportDefaultBehavior: PrepareSupportDefaultBehavior?

    /**
     * Whether th client honors the change annotations in
     * text edits and resource operations returned via the
     * rename request's workspace edit by for example presenting
     * the workspace edit in the user interface and asking
     * for confirmation.
     *
     * @since 3.16.0
     */
    val honorsChangeAnnotations: Boolean?
}

fun RenameClientCapabilities(
    dynamicRegistration: Boolean? = null,
    prepareSupport: Boolean? = null,
    prepareSupportDefaultBehavior: PrepareSupportDefaultBehavior? = null,
    honorsChangeAnnotations: Boolean? = null,
): RenameClientCapabilities = IsaacRenameClientCapabilities(
    dynamicRegistration, prepareSupport, prepareSupportDefaultBehavior, honorsChangeAnnotations
)

@Serializable class IsaacRenameClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
    override val prepareSupport: Boolean? = null,
    override val prepareSupportDefaultBehavior: PrepareSupportDefaultBehavior? = null,
    override val honorsChangeAnnotations: Boolean? = null,
) : RenameClientCapabilities

object RenameClientSerializer : JsonContentPolymorphicSerializer<RenameClientCapabilities>(
    RenameClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacRenameClientCapabilities.serializer() }
