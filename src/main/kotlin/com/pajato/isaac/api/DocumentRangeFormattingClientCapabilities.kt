package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DocumentRangeFormattingClientCapabilities {
    /**
     * Whether formatting supports dynamic registration.
     */
    val dynamicRegistration: Boolean?
}

fun DocumentRangeFormattingClientCapabilities(
    dynamicRegistration: Boolean? = null
): DocumentRangeFormattingClientCapabilities = IsaacDocumentRangeFormattingClientCapabilities(dynamicRegistration)

@Serializable class IsaacDocumentRangeFormattingClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
) : DocumentRangeFormattingClientCapabilities

private typealias DocumentRangeFormattingCC = DocumentRangeFormattingClientCapabilities

object DocumentRangeFormattingClientCapabilitiesSerializer :
    JsonContentPolymorphicSerializer<DocumentRangeFormattingCC>(DocumentRangeFormattingCC::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacDocumentRangeFormattingClientCapabilities.serializer()
}
