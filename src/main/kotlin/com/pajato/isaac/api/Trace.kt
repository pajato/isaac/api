@file:Suppress("EnumEntryName")

package com.pajato.isaac.api

enum class Trace { off, messages, verbose }
