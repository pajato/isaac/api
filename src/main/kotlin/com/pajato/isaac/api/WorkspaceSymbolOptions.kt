package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface WorkspaceSymbolOptions : WorkDoneProgressOptions

fun WorkspaceSymbolOptions(workDoneProgress: Boolean? = null): WorkspaceSymbolOptions =
    IsaacWorkspaceSymbolOptions(workDoneProgress)

@Serializable class IsaacWorkspaceSymbolOptions(override val workDoneProgress: Boolean? = null) : WorkspaceSymbolOptions

object WorkspaceSymbolOptionsSerializer : JsonContentPolymorphicSerializer<WorkspaceSymbolOptions>(
    WorkspaceSymbolOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacWorkspaceSymbolOptions.serializer() }
