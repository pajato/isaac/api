package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface WorkDoneProgressParams {
    /**
     * An optional token that a server can use to report work done progress.
     */
    val workDoneToken: ProgressToken?
}

fun WorkDoneProgressParams(workDoneProgress: ProgressToken? = null): WorkDoneProgressParams =
    IsaacWorkDoneProgressParams(workDoneProgress)

@Serializable class IsaacWorkDoneProgressParams(
    @Serializable(with = ProgressTokenSerializer::class) override val workDoneToken: ProgressToken? = null
) : WorkDoneProgressParams

object WorkDoneProgressParamsSerializer : JsonContentPolymorphicSerializer<WorkDoneProgressParams>(
    WorkDoneProgressParams::class
) { override fun selectDeserializer(element: JsonElement) = IsaacWorkDoneProgressParams.serializer() }
