package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface FoldingRangeRegistrationOptions :
    TextDocumentRegistrationOptions, FoldingRangeOptions, StaticRegistrationOptions

fun FoldingRangeRegistrationOptions(
    documentSelector: List<DocumentFilter>? = null,
    workDoneProgress: Boolean? = null,
    id: String? = null
): FoldingRangeRegistrationOptions = IsaacFoldingRangeRegistrationOptions(documentSelector, workDoneProgress, id)

@Serializable class IsaacFoldingRangeRegistrationOptions(
    override val documentSelector: List<DocumentFilter>? = null,
    override val workDoneProgress: Boolean? = null,
    override val id: String? = null,
) : FoldingRangeRegistrationOptions

object FoldingRangeRegistrationOptionsSerializer : JsonContentPolymorphicSerializer<FoldingRangeRegistrationOptions>(
    FoldingRangeRegistrationOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacFoldingRangeRegistrationOptions.serializer() }
