package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface ParameterInformation {
    /**
     * The client supports processing label offsets instead of a
     * simple label string.
     *
     * @since 3.14.0
     */
    val labelOffsetSupport: Boolean?
}

fun ParameterInformation(labelOffsetSupport: Boolean? = null): ParameterInformation =
    IsaacParameterInformation(labelOffsetSupport)

@Serializable class IsaacParameterInformation(
    override val labelOffsetSupport: Boolean? = null,
) : ParameterInformation

object ParameterInformationSerializer : JsonContentPolymorphicSerializer<ParameterInformation>(
    ParameterInformation::class
) { override fun selectDeserializer(element: JsonElement) = IsaacParameterInformation.serializer() }
