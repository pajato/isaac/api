package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DocumentColorClientCapabilities {
    /**
     * Whether document color supports dynamic registration.
     */
    val dynamicRegistration: Boolean?
}

fun DocumentColorClientCapabilities(dynamicRegistration: Boolean? = null): DocumentColorClientCapabilities =
    IsaacDocumentColorClientCapabilities(dynamicRegistration)

@Serializable class IsaacDocumentColorClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
) : DocumentColorClientCapabilities

object DocumentColorSerializer : JsonContentPolymorphicSerializer<DocumentColorClientCapabilities>(
    DocumentColorClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDocumentColorClientCapabilities.serializer() }
