package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

enum class DiagnosticTag { Deprecated }

interface DiagnosticTagSupport {
    /**
     * The tags supported by the client.
     */
    val valueSet: Array<DiagnosticTag>
}

fun DiagnosticTagSupport(valueSet: Array<DiagnosticTag>): DiagnosticTagSupport = IsaacDiagnosticTagSupport(valueSet)

@Serializable class IsaacDiagnosticTagSupport(
    override val valueSet: Array<DiagnosticTag>

) : DiagnosticTagSupport

object DiagnosticTagSupportSerializer : JsonContentPolymorphicSerializer<DiagnosticTagSupport>(
    DiagnosticTagSupport::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDiagnosticTagSupport.serializer() }
