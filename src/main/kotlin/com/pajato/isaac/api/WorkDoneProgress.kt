package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface WorkDoneProgress {
    /**
     * Whether client supports handling progress notifications. If set
     * servers are allowed to report in `workDoneProgress` property in the
     * request specific server capabilities.
     *
     * @since 3.15.0
     */
    val workDoneProgress: Boolean?
}

fun WorkDoneProgress(workDoneProgress: Boolean? = null): WorkDoneProgress = IsaacWorkDoneProgress(workDoneProgress)

@Serializable class IsaacWorkDoneProgress(override val workDoneProgress: Boolean? = null) : WorkDoneProgress

object WorkDoneProgressSerializer : JsonContentPolymorphicSerializer<WorkDoneProgress>(WorkDoneProgress::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacWorkDoneProgress.serializer()
}
