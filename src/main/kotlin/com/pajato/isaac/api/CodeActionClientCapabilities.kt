package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface CodeActionClientCapabilities {
    /**
     * Whether code action supports dynamic registration.
     */
    val dynamicRegistration: Boolean?

    /**
     * The client supports code action literals as a valid
     * response of the `textDocument/codeAction` request.
     *
     * @since 3.8.0
     */
    val codeActionLiteralSupport: CodeActionLiteralSupport?

    /**
     * Whether code action supports the `isPreferred` property.
     *
     * @since 3.15.0
     */
    val isPreferredSupport: Boolean?

    /**
     * Whether code action supports the `disabled` property.
     *
     * @since 3.16.0
     */
    val disabledSupport: Boolean?

    /**
     * Whether code action supports the `data` property which is
     * preserved between a `textDocument/codeAction` and a
     * `codeAction/resolve` request.
     *
     * @since 3.16.0
     */
    val dataSupport: Boolean?

    /**
     * Whether the client supports resolving additional code action
     * properties via a separate `codeAction/resolve` request.
     *
     * @since 3.16.0
     */
    val resolveSupport: ResolveSupport?

    /**
     * Whether the client honors the change annotations in
     * text edits and resource operations returned via the
     * `CodeAction#edit` property by for example presenting
     * the workspace edit in the user interface and asking
     * for confirmation.
     *
     * @since 3.16.0
     */
    val honorsChangeAnnotations: Boolean?
}

fun CodeActionClientCapabilities(
    dynamicRegistration: Boolean? = null,
    codeActionLiteralSupport: CodeActionLiteralSupport? = null,
    isPreferredSupport: Boolean? = null,
    disabledSupport: Boolean? = null,
    dataSupport: Boolean? = null,
    resolveSupport: ResolveSupport? = null,
    honorsChangeAnnotations: Boolean? = null,
): CodeActionClientCapabilities = IsaacCodeActionClientCapabilities(
    dynamicRegistration, codeActionLiteralSupport, isPreferredSupport, disabledSupport, dataSupport, resolveSupport,
    honorsChangeAnnotations
)
@Serializable class IsaacCodeActionClientCapabilities(
    override val dynamicRegistration: Boolean? = null,

    @Serializable(with = CodeActionLiteralSupportSerializer::class)
    override val codeActionLiteralSupport: CodeActionLiteralSupport? = null,

    override val isPreferredSupport: Boolean? = null,
    override val disabledSupport: Boolean? = null,
    override val dataSupport: Boolean? = null,

    @Serializable(with = ResolveSupportSerializer::class)
    override val resolveSupport: ResolveSupport? = null,

    override val honorsChangeAnnotations: Boolean? = null,
) : CodeActionClientCapabilities

object CodeActionClientSerializer : JsonContentPolymorphicSerializer<CodeActionClientCapabilities>(
    CodeActionClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacCodeActionClientCapabilities.serializer() }
