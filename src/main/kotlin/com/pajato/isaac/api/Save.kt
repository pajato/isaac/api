package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.JsonTransformingSerializer
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put

@Serializable sealed class Save

@Serializable class BooleanSave(val value: Boolean) : Save()

@Serializable class OptionsSave(@Serializable(with = SaveOptionsSerializer::class) val value: SaveOptions) : Save()

object SaveTransformingSerializer : JsonTransformingSerializer<Save>(Save.serializer()) {

    override fun transformSerialize(element: JsonElement): JsonElement {
        fun compressElement(state: String) = buildJsonObject { put("includeText", state.toBoolean()) }

        fun getElementAsString(item: JsonElement?): String {
            check(item != null) { "Invalid element: (item is null)" }
            return when {
                item is JsonObject && item.containsKey("includeText") -> getElementAsString(item["includeText"])
                else -> (item as JsonPrimitive).content
            }
        }

        check(element is JsonObject && element.size == 2) { "Invalid element content: $element." }
        val type = getElementAsString(element["type"])
        val state = getElementAsString(element["value"])
        return if (type.endsWith("BooleanSave")) element["value"] as JsonPrimitive else compressElement(state)
    }

    override fun transformDeserialize(element: JsonElement): JsonElement {
        return when (element) {
            is JsonPrimitive -> buildJsonObject {
                put("type", "com.pajato.isaac.api.BooleanSave")
                put("value", element.content.toBoolean())
            }
            is JsonObject -> buildJsonObject {
                put("type", "com.pajato.isaac.api.OptionsSave")
                put("value", element)
            }
            else -> throw IllegalStateException("Unsupported element content: $element")
        }
    }
}
