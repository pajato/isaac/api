package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.JsonTransformingSerializer
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put

@Serializable sealed class ProgressToken

@Serializable class StringProgressToken(val stringToken: String) : ProgressToken()

@Serializable class IntegerProgressToken(val intToken: Int) : ProgressToken()

object ProgressTokenSerializer : JsonContentPolymorphicSerializer<ProgressToken>(ProgressToken::class) {
    private const val key = "stringToken"

    override fun selectDeserializer(element: JsonElement) = when {
        element is JsonObject && element.containsKey(key) -> StringProgressToken.serializer()
        else -> IntegerProgressToken.serializer()
    }
}

object ProgressTokenTransformingSerializer : JsonTransformingSerializer<ProgressToken>(ProgressToken.serializer()) {
    private const val stringKey = "stringToken"
    private const val intKey = "intToken"
    private const val typeKey = "type"

    override fun transformSerialize(element: JsonElement): JsonElement {
        fun isValid() = element is JsonObject && element.size == 2 &&
            (element.containsKey(intKey) || element.containsKey(stringKey))
        fun transform(element: JsonObject) = when {
            element.containsKey(stringKey) -> element[stringKey] as JsonPrimitive
            else -> element[intKey] as JsonPrimitive
        }

        check(isValid()) { "Invalid element: $element" }
        return transform(element as JsonObject)
    }

    override fun transformDeserialize(element: JsonElement): JsonElement {
        fun transformToString(element: JsonPrimitive) = buildJsonObject {
            put(typeKey, StringProgressToken::class.java.name)
            put(stringKey, element)
        }

        fun transformToInteger(element: JsonPrimitive) = buildJsonObject {
            put(typeKey, IntegerProgressToken::class.java.name)
            put(intKey, element)
        }

        return when {
            element is JsonPrimitive && element.isString -> transformToString(element)
            element is JsonPrimitive && element.content.toIntOrNull() != null -> transformToInteger(element)
            else -> throw IllegalStateException("Invalid element: $element!")
        }
    }
}
