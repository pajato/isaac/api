package com.pajato.isaac.api

import kotlinx.serialization.Serializable

interface WorkspaceSymbolParams : WorkDoneProgressParams, PartialResultParams, Params {
    /**
     * A query string to filter symbols by. Clients may send an empty
     * string here to request all symbols.
     */
    val query: String
}

fun WorkspaceSymbolParams(
    workDoneToken: ProgressToken? = null,
    partialResultToken: ProgressToken? = null,
    query: String = "",
): WorkspaceSymbolParams = IsaacWorkspaceSymbolParams(workDoneToken, partialResultToken, query)

@Serializable class IsaacWorkspaceSymbolParams(
    @Serializable(with = ProgressTokenTransformingSerializer::class)
    override val workDoneToken: ProgressToken? = null,

    @Serializable(with = ProgressTokenTransformingSerializer::class)
    override val partialResultToken: ProgressToken? = null,

    override val query: String,
) : WorkspaceSymbolParams
