package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface Location {
    val uri: DocumentUri
    val range: Range
}

fun Location(uri: DocumentUri, range: Range): Location = IsaacLocation(uri, range)

@Serializable class IsaacLocation(
    override val uri: DocumentUri,

    @Serializable(with = RangeSerializer::class)
    override val range: Range
) : Location

object LocationSerializer : JsonContentPolymorphicSerializer<Location>(Location::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacLocation.serializer()
}
