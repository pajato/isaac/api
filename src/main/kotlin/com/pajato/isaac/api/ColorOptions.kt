package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface ColorOptions : WorkDoneProgressOptions

fun ColorOptions(workDoneProgress: Boolean? = null): ColorOptions = IsaacColorOptions(workDoneProgress)

@Serializable class IsaacColorOptions(override val workDoneProgress: Boolean? = null) : ColorOptions

object ColorOptionsSerializer : JsonContentPolymorphicSerializer<ColorOptions>(ColorOptions::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacColorOptions.serializer()
}
