package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface SemanticTokensWorkspaceClientCapabilities {
    /**
     * Whether the client implementation supports a refresh request sent from
     * the server to the client.
     *
     * Note that this event is global and will force the client to refresh all
     * semantic tokens currently shown. It should be used with absolute care
     * and is useful for situation where a server for example detect a project
     * wide change that requires such a calculation.
     */
    val refreshSupport: Boolean?
}

fun SemanticTokensWorkspaceClientCapabilities(
    refreshSupport: Boolean? = null
): SemanticTokensWorkspaceClientCapabilities = IsaacSemanticTokensWorkspaceClientCapabilities(refreshSupport)

@Serializable class IsaacSemanticTokensWorkspaceClientCapabilities(
    override val refreshSupport: Boolean? = null,
) : SemanticTokensWorkspaceClientCapabilities

typealias SemanticTokensWorkspaceCC = SemanticTokensWorkspaceClientCapabilities

object SemanticTokensWorkspaceCCSerializer : JsonContentPolymorphicSerializer<SemanticTokensWorkspaceCC>(
    SemanticTokensWorkspaceCC::class
) {
    override fun selectDeserializer(element: JsonElement) =
        IsaacSemanticTokensWorkspaceClientCapabilities.serializer()
}
