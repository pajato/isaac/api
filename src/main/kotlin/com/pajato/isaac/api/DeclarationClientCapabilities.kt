package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DeclarationClientCapabilities {
    /**
     * Whether declaration supports dynamic registration. If this is set to
     * `true` the client supports the new `DeclarationRegistrationOptions`
     * return value for the corresponding server capability as well.
     */
    val dynamicRegistration: Boolean?

    /**
     * The client supports additional metadata in the form of declaration links.
     */
    val linkSupport: Boolean?
}

fun DeclarationClientCapabilities(
    dynamicRegistration: Boolean? = null,
    linkSupport: Boolean? = null,
): DeclarationClientCapabilities = IsaacDeclarationClientCapabilities(dynamicRegistration, linkSupport)

@Serializable class IsaacDeclarationClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
    override val linkSupport: Boolean? = null,
) : DeclarationClientCapabilities

object DeclarationClientSerializer : JsonContentPolymorphicSerializer<DeclarationClientCapabilities>(
    DeclarationClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDeclarationClientCapabilities.serializer() }
