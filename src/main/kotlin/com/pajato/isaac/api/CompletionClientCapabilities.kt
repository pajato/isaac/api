package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface CompletionClientCapabilities {
    /**
     * Whether code lens supports dynamic registration.
     */
    val dynamicRegistration: Boolean?
}

fun CompletionClientCapabilities(dynamicRegistration: Boolean? = null): CompletionClientCapabilities =
    IsaacCompletionClientCapabilities(dynamicRegistration)

@Serializable class IsaacCompletionClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
) : CompletionClientCapabilities

object CompletionClientCapabilitiesSerializer : JsonContentPolymorphicSerializer<CompletionClientCapabilities>(
    CompletionClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacCompletionClientCapabilities.serializer() }
