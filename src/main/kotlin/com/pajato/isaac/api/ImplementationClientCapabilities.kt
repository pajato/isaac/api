package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface ImplementationClientCapabilities {
    /**
     * Whether implementation supports dynamic registration. If this is set to
     * `true` the client supports the new `ImplementationRegistrationOptions`
     * return value for the corresponding server capability as well.
     */
    val dynamicRegistration: Boolean?

    /**
     * The client supports additional metadata in the form of definition links.
     *
     * @since 3.14.0
     */
    val linkSupport: Boolean?
}

fun ImplementationClientCapabilities(
    dynamicRegistration: Boolean? = null,
    linkSupport: Boolean? = null,
): ImplementationClientCapabilities = IsaacImplementationClientCapabilities(dynamicRegistration, linkSupport)

@Serializable class IsaacImplementationClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
    override val linkSupport: Boolean? = null,
) : ImplementationClientCapabilities

object ImplementationClientSerializer : JsonContentPolymorphicSerializer<ImplementationClientCapabilities>(
    ImplementationClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacImplementationClientCapabilities.serializer() }
