package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface TextDocumentSyncOptions {
    /**
     * Open and close notifications are sent to the server. If omitted open
     * close notification should not be sent.
     */
    val openClose: Boolean?

    /**
     * Change notifications are sent to the server. See
     * TextDocumentSyncKind.None, TextDocumentSyncKind.Full and
     * TextDocumentSyncKind.Incremental. If omitted it defaults to
     * TextDocumentSyncKind.None.
     */
    val change: Int?

    /**
     * If present will save notifications are sent to the server. If omitted
     * the notification should not be sent.
     */
    val willSave: Boolean?

    /**
     * If present will save wait until requests are sent to the server. If
     * omitted the request should not be sent.
     */
    val willSaveWaitUntil: Boolean?

    /**
     * If present save notifications are sent to the server. If omitted the
     * notification should not be sent.
     */
    val save: Save?
}

fun TextDocumentSyncOptions(
    openClose: Boolean? = null,
    change: Int? = null,
    willSave: Boolean? = null,
    willSaveWaitUntil: Boolean? = null,
    save: Save? = null,
): TextDocumentSyncOptions = IsaacTextDocumentSyncOptions(openClose, change, willSave, willSaveWaitUntil, save)

@Serializable class IsaacTextDocumentSyncOptions(
    override val openClose: Boolean? = null,
    override val change: Int? = null,
    override val willSave: Boolean? = null,
    override val willSaveWaitUntil: Boolean? = null,
    @Serializable(with = SaveTransformingSerializer::class) override val save: Save? = null,
) : TextDocumentSyncOptions

object TextDocumentSyncOptionsSerializer : JsonContentPolymorphicSerializer<TextDocumentSyncOptions>(
    TextDocumentSyncOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacTextDocumentSyncOptions.serializer() }
