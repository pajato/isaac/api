package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface ResponseError {
    /**
     * A number indicating the error type that occurred.
     */
    val code: Int

    /**
     * A string providing a short description of the error.
     */
    val message: String

    /**
     * A primitive or structured value that contains additional
     * information about the error. Can be omitted.
     */
    val data: ErrorData?
}

fun ResponseError(
    code: Int,
    message: String,
    data: ErrorData? = null
): ResponseError = if (data == null) IsaacResponseError(code, message) else IsaacResponseError(code, message, data)

@Serializable class IsaacResponseError(
    override val code: Int,
    override val message: String,

    @Serializable(with = ErrorDataTransformingSerializer::class)
    override val data: ErrorData? = null,
) : ResponseError

object ResponseErrorSerializer : JsonContentPolymorphicSerializer<ResponseError>(ResponseError::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacResponseError.serializer()
}
