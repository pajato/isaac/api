@file:Suppress("GrazieInspection")

package com.pajato.isaac.api

import kotlinx.serialization.Contextual
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

interface InitializeParams : WorkDoneProgressParams, Params {
    /**
     * The process Id of the parent process that started the server. Is null if
     * the process has not been started by another process. If the parent
     * process is not alive then the server should exit (see exit notification)
     * its process.
     */
    val processId: Int

    /**
     * Information about the client
     *
     * @since 3.15.0
     */
    val clientInfo: ClientInfo?

    /**
     * The locale the client is currently showing the user interface
     * in. This must not necessarily be the locale of the operating
     * system.
     *
     * Uses IETF language tags as the value's syntax
     * (See https://en.wikipedia.org/wiki/IETF_language_tag)
     *
     * @since 3.16.0
     */
    val locale: String?

    /**
     * The rootPath of the workspace. Is null
     * if no folder is open.
     *
     * @deprecated in favour of `rootUri`.
     */
    val rootPath: String?

    /**
     * The rootUri of the workspace. Is null if no
     * folder is open. If both `rootPath` and `rootUri` are set
     * `rootUri` wins.
     *
     * @deprecated in favour of `workspaceFolders`
     */
    val rootUri: DocumentUri?

    /**
     * User provided initialization options.
     */
    val initializationOptions: Any?

    /**
     * The capabilities provided by the client (editor or tool)
     */
    val capabilities: ClientCapabilities

    /**
     * The initial trace setting. If omitted trace is disabled ('off').
     */
    val trace: Trace?

    /**
     * The workspace folders configured in the client when the server starts.
     * This property is only available if the client supports workspace folders.
     * It can be `null` if the client supports workspace folders but none are
     * configured.
     *
     * @since 3.6.0
     */
    val workspaceFolders: List<WorkspaceFolder>?
}

fun InitializeParams(
    workDoneToken: ProgressToken? = null,
    processId: Int = 0,
    clientInfo: ClientInfo? = null,
    locale: String? = null,
    rootPath: String? = null,
    rootUri: String? = null,
    initializationOptions: Any? = null,
    capabilities: ClientCapabilities,
    trace: Trace? = null,
    workspaceFolders: List<WorkspaceFolder>? = null,
): InitializeParams = IsaacInitializeParams(
    workDoneToken, processId, clientInfo, locale, rootPath, rootUri, initializationOptions, capabilities, trace,
    workspaceFolders
)

@Serializable
class IsaacInitializeParams(
    @Serializable(with = ProgressTokenTransformingSerializer::class)
    override val workDoneToken: ProgressToken? = null,

    override val processId: Int,

    @Serializable(with = ClientInfoSerializer::class)
    override val clientInfo: ClientInfo? = null,

    override val locale: String? = null,
    override val rootPath: String? = null,
    override val rootUri: DocumentUri? = null,

    @Contextual
    override val initializationOptions: Any? = null,

    @Serializable(with = ClientCapabilitiesSerializer::class)
    override val capabilities: ClientCapabilities,

    override val trace: Trace? = null,

    @Serializable(with = WorkspaceFolderListSerializer::class)
    override val workspaceFolders: List<WorkspaceFolder>? = null,
) : InitializeParams

object WorkspaceFolderListSerializer : KSerializer<List<WorkspaceFolder>> {
    private val builtIn: KSerializer<List<WorkspaceFolder>> = ListSerializer(WorkspaceFolderSerializer)

    override fun deserialize(decoder: Decoder): List<WorkspaceFolder> {
        return builtIn.deserialize(decoder)
    }

    override val descriptor: SerialDescriptor = builtIn.descriptor

    override fun serialize(encoder: Encoder, value: List<WorkspaceFolder>) {
        builtIn.serialize(encoder, value)
    }
}
