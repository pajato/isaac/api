package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DefinitionClientCapabilities {
    /**
     * Whether definition supports dynamic registration.
     */
    val dynamicRegistration: Boolean?

    /**
     * The client supports additional metadata in the form of definition links.
     *
     * @since 3.14.0
     */
    val linkSupport: Boolean?
}

fun DefinitionClientCapabilities(
    dynamicRegistration: Boolean? = null,
    linkSupport: Boolean? = null
): DefinitionClientCapabilities = IsaacDefinitionClientCapabilities(dynamicRegistration, linkSupport)

@Serializable class IsaacDefinitionClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
    override val linkSupport: Boolean? = null,
) : DefinitionClientCapabilities

object DefinitionClientCapabilitiesSerializer : JsonContentPolymorphicSerializer<DefinitionClientCapabilities>(
    DefinitionClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDefinitionClientCapabilities.serializer() }
