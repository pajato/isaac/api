package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface SignatureInformation {
    /**
     * Client supports the following content formats for the documentation
     * property. The order describes the preferred format of the client.
     */
    val documentationFormat: Array<MarkupKind>?

    /**
     * Client capabilities specific to parameter information.
     */
    val parameterInformation: ParameterInformation?

    /**
     * The client supports the `activeParameter` property on
     * `SignatureInformation` literal.
     *
     * @since 3.16.0
     */
    val activeParameterSupport: Boolean?
}

fun SignatureInformation(
    documentationFormat: Array<MarkupKind>? = null,
    parameterInformation: ParameterInformation? = null,
    activeParameterSupport: Boolean? = null
): SignatureInformation = IsaacSignatureInformation(documentationFormat, parameterInformation, activeParameterSupport)

@Serializable class IsaacSignatureInformation(
    override val documentationFormat: Array<MarkupKind>? = null,

    @Serializable(with = ParameterInformationSerializer::class)
    override val parameterInformation: ParameterInformation? = null,

    override val activeParameterSupport: Boolean? = null,
) : SignatureInformation

object SignatureInformationSerializer : JsonContentPolymorphicSerializer<SignatureInformation>(
    SignatureInformation::class
) { override fun selectDeserializer(element: JsonElement) = IsaacSignatureInformation.serializer() }
