package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

enum class TokenFormat { Relative }

interface SemanticTokensClientCapabilities {
    /**
     * Whether implementation supports dynamic registration. If this is set to
     * `true` the client supports the new `(TextDocumentRegistrationOptions &
     * StaticRegistrationOptions)` return value for the corresponding server
     * capability as well.
     */
    val dynamicRegistration: Boolean?

    /**
     * Which requests the client supports and might send to the server
     * depending on the server's capability. Please note that clients might not
     * show semantic tokens or degrade some user experience if a range
     * or full request is advertised by the client but not provided by the
     * server. If for example the client capability `requests.full` and
     * `request.range` are both set to true but the server only provides a
     * range provider the client might not render a minimap correctly or might
     * even decide to not show any semantic tokens at all.
     */
    val requests: SemanticTokensRequests?

    /**
     * The token types that the client supports.
     */
    val tokenTypes: Array<String>

    /**
     * The token modifiers that the client supports.
     */
    val tokenModifiers: Array<String>

    /**
     * The formats the clients supports.
     */
    val formats: Array<TokenFormat>

    /**
     * Whether the client supports tokens that can overlap each other.
     */
    val overlappingTokenSupport: Boolean?

    /**
     * Whether the client supports tokens that can span multiple lines.
     */
    val multilineTokenSupport: Boolean?
}

fun SemanticTokensClientCapabilities(
    dynamicRegistration: Boolean? = null,
    requests: SemanticTokensRequests? = null,
    tokenTypes: Array<String>,
    tokenModifiers: Array<String>,
    formats: Array<TokenFormat>,
    overlappingTokenSupport: Boolean? = null,
    multilineTokenSupport: Boolean? = null
): SemanticTokensClientCapabilities = IsaacSemanticTokensClientCapabilities(
    dynamicRegistration, requests, tokenTypes, tokenModifiers, formats, overlappingTokenSupport, multilineTokenSupport
)

@Serializable class IsaacSemanticTokensClientCapabilities(
    override val dynamicRegistration: Boolean? = null,

    @Serializable(with = SemanticTokensRequestsSerializer::class)
    override val requests: SemanticTokensRequests? = null,

    override val tokenTypes: Array<String>,
    override val tokenModifiers: Array<String>,
    override val formats: Array<TokenFormat>,
    override val overlappingTokenSupport: Boolean? = null,
    override val multilineTokenSupport: Boolean? = null,
) : SemanticTokensClientCapabilities

typealias SemanticTokensCC = SemanticTokensClientCapabilities

object SemanticTokensClientCapabilitiesSerializer : JsonContentPolymorphicSerializer<SemanticTokensCC>(
    SemanticTokensClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacSemanticTokensClientCapabilities.serializer() }
