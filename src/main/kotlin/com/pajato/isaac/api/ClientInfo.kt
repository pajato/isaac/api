package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

/**
 * A synthesized interface supporting client information: name and version.
 */
interface ClientInfo {
    val name: String
    val version: String?
}

fun ClientInfo(name: String, version: String? = null): ClientInfo =
    if (version == null) IsaacClientInfo(name) else IsaacClientInfo(name, version)

@Serializable class IsaacClientInfo(
    override val name: String,
    override val version: String? = null
) : ClientInfo

object ClientInfoSerializer : JsonContentPolymorphicSerializer<ClientInfo>(ClientInfo::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacClientInfo.serializer()
}
