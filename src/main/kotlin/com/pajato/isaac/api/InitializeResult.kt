package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface InitializeResult {
    /**
     * The capabilities the language server provides.
     */
    val capabilities: ServerCapabilities

    /**
     * Information about the server.
     *
     * @since 3.15.0
     */
    val serverInfo: ServerInfo?
}

fun InitializeResult(capabilities: ServerCapabilities, serverInfo: ServerInfo? = null): InitializeResult =
    IsaacInitializeResult(capabilities, serverInfo)

@Serializable class IsaacInitializeResult(
    @Serializable(with = ServerCapabilitiesSerializer::class)
    override val capabilities: ServerCapabilities,

    @Serializable(with = ServerInfoSerializer::class)
    override val serverInfo: ServerInfo? = null,
) : InitializeResult, Result()

object InitializeResultSerializer : JsonContentPolymorphicSerializer<InitializeResult>(InitializeResult::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacInitializeResult.serializer()
}
