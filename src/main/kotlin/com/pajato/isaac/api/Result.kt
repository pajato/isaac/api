package com.pajato.isaac.api

import com.pajato.isaac.stripType
import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.JsonTransformingSerializer
import kotlinx.serialization.json.buildJsonObject

@Serializable sealed class Result

// @Serializable class IsaacShutdownResult(val value: String?) : Result()

object DocumentSymbolListSerializer : KSerializer<List<DocumentSymbol>> {
    private val builtIn: KSerializer<List<DocumentSymbol>> = ListSerializer(DocumentSymbolSerializer)

    override fun deserialize(decoder: Decoder): List<DocumentSymbol> {
        return builtIn.deserialize(decoder)
    }

    override val descriptor: SerialDescriptor = builtIn.descriptor

    override fun serialize(encoder: Encoder, value: List<DocumentSymbol>) {
        builtIn.serialize(encoder, value)
    }
}

object SymbolInformationListSerializer : KSerializer<List<SymbolInformation>> {
    private val builtIn: KSerializer<List<SymbolInformation>> = ListSerializer(SymbolInformationSerializer)

    override fun deserialize(decoder: Decoder): List<SymbolInformation> {
        return builtIn.deserialize(decoder)
    }

    override val descriptor: SerialDescriptor = builtIn.descriptor

    override fun serialize(encoder: Encoder, value: List<SymbolInformation>) {
        builtIn.serialize(encoder, value)
    }
}

private const val listKey = "list"
private const val typeKey = "type"
private const val capabilitiesKey = "capabilities"
private const val serverInfoKey = "serverInfo"

object ResultTransformingSerializer : JsonTransformingSerializer<Result>(Result.serializer()) {
    override fun transformSerialize(element: JsonElement): JsonElement {
        fun transformToList(list: JsonArray): JsonElement = list
        fun isWorkspaceSymbolList(element: JsonObject) = element.containsKey(listKey) && element[listKey] is JsonArray

        check(element is JsonObject)
        return when {
            isWorkspaceSymbolList(element) -> transformToList(element[listKey] as JsonArray)
            else -> stripType(element)
        }
    }

    override fun transformDeserialize(element: JsonElement): JsonElement {
        fun containsKey(element: JsonArray, key: String): Boolean {
            assert(element.size > 0)
            val item = element[0]

            return item is JsonObject && item.containsKey(key)
        }

        fun transformToList(element: JsonArray, listName: String): JsonElement = buildJsonObject {
            put(typeKey, JsonPrimitive("com.pajato.isaac.api.Isaac${listName}Result"))
            put(listKey, element)
        }

        fun transformToInitialize(element: JsonObject): JsonElement = buildJsonObject {
            put(typeKey, JsonPrimitive("com.pajato.isaac.api.IsaacInitializeResult"))
            put(capabilitiesKey, element[capabilitiesKey]!!)
            if (element.containsKey(serverInfoKey)) put(serverInfoKey, element[serverInfoKey]!!)
        }

        /* fun transformToNullForShutdown(): JsonElement = buildJsonObject {
            put(typeKey, JsonPrimitive(IsaacShutdownResult::class.java.name))
            put(valueKey, JsonNull)
        } */

        return when {
            element is JsonArray && element.size == 0 -> transformToList(element, "WorkspaceSymbol")
            element is JsonArray && containsKey(element, "range") -> transformToList(element, "DocumentSymbol")
            element is JsonArray && containsKey(element, "location") -> transformToList(element, "WorkspaceSymbol")
            element is JsonObject && element.containsKey(capabilitiesKey) -> transformToInitialize(element)
            else -> throw IllegalStateException("Invalid element: $element!")
        }
    }
}
