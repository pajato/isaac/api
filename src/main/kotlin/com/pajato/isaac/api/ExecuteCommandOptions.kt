package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface ExecuteCommandOptions : WorkDoneProgressOptions {
    /**
     * The commands to be executed on the server
     */
    val commands: List<String>
}

fun ExecuteCommandOptions(
    workDoneProgress: Boolean? = null,
    commands: List<String>,
): ExecuteCommandOptions = IsaacExecuteCommandOptions(workDoneProgress, commands)

@Serializable class IsaacExecuteCommandOptions(
    override val workDoneProgress: Boolean? = null,
    override val commands: List<String>,
) : ExecuteCommandOptions

object ExecuteCommandOptionsSerializer : JsonContentPolymorphicSerializer<ExecuteCommandOptions>(
    ExecuteCommandOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacExecuteCommandOptions.serializer() }
