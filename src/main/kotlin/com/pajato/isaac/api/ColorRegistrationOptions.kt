package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface ColorRegistrationOptions :
    TextDocumentRegistrationOptions, ColorOptions, StaticRegistrationOptions

fun ColorRegistrationOptions(
    documentSelector: List<DocumentFilter>? = null,
    workDoneProgress: Boolean? = null,
    id: String? = null
): ColorRegistrationOptions = IsaacColorRegistrationOptions(documentSelector, workDoneProgress, id)

@Serializable class IsaacColorRegistrationOptions(
    override val documentSelector: List<DocumentFilter>? = null,
    override val workDoneProgress: Boolean? = null,
    override val id: String? = null,
) : ColorRegistrationOptions

object ColorRegistrationOptionsSerializer : JsonContentPolymorphicSerializer<ColorRegistrationOptions>(
    ColorRegistrationOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacColorRegistrationOptions.serializer() }
