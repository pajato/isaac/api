package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

/**
 * General text document registration options.
 */
interface TextDocumentRegistrationOptions {
    /**
     * A document selector to identify the scope of the registration. If set to
     * null the document selector provided on the client side will be used.
     */
    val documentSelector: DocumentSelector?
}

fun TextDocumentRegistrationOptions(documentSelector: List<DocumentFilter>? = null): TextDocumentRegistrationOptions =
    IsaacTextDocumentRegistrationOptions(documentSelector)

@Serializable class IsaacTextDocumentRegistrationOptions(override val documentSelector: List<DocumentFilter>? = null) :
    TextDocumentRegistrationOptions

object TextDocumentRegistrationOptionsSerializer :
    JsonContentPolymorphicSerializer<TextDocumentRegistrationOptions>(TextDocumentRegistrationOptions::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacTextDocumentRegistrationOptions.serializer()
}
