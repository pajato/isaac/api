package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface TypeDefinitionRegistrationOptions :
    TextDocumentRegistrationOptions, TypeDefinitionOptions, StaticRegistrationOptions

fun TypeDefinitionRegistrationOptions(
    documentSelector: List<DocumentFilter>? = null,
    workDoneProgress: Boolean? = null,
    id: String? = null,
): TypeDefinitionRegistrationOptions = IsaacTypeDefinitionRegistrationOptions(documentSelector, workDoneProgress, id)

@Serializable class IsaacTypeDefinitionRegistrationOptions(
    override val documentSelector: List<DocumentFilter>? = null,
    override val workDoneProgress: Boolean? = null,
    override val id: String? = null,
) : TypeDefinitionRegistrationOptions

private typealias TypeDefRegOptions = TypeDefinitionRegistrationOptions

object TypeDefinitionRegistrationOptionsSerializer : JsonContentPolymorphicSerializer<TypeDefRegOptions>(
    TypeDefRegOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacTypeDefinitionRegistrationOptions.serializer() }
