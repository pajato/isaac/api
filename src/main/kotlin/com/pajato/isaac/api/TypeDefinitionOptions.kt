package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface TypeDefinitionOptions : WorkDoneProgressOptions

fun TypeDefinitionOptions(workDoneProgress: Boolean? = null): TypeDefinitionOptions =
    IsaacTypeDefinitionOptions(workDoneProgress)

@Serializable class IsaacTypeDefinitionOptions(override val workDoneProgress: Boolean? = null) : TypeDefinitionOptions

object TypeDefinitionOptionsSerializer : JsonContentPolymorphicSerializer<TypeDefinitionOptions>(
    TypeDefinitionOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacTypeDefinitionOptions.serializer() }
