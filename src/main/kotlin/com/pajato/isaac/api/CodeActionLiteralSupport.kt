package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface CodeActionLiteralSupport {
    /**
     * The code action kind is supported with the following value
     * set.
     */
    val codeActionKind: CodeAction
}

fun CodeActionLiteralSupport(codeActionKind: CodeAction): CodeActionLiteralSupport =
    IsaacCodeActionLiteralSupport(codeActionKind)

@Serializable class IsaacCodeActionLiteralSupport(
    @Serializable(with = CodeActionKindSerializer::class)
    override val codeActionKind: CodeAction,
) : CodeActionLiteralSupport

object CodeActionLiteralSupportSerializer : JsonContentPolymorphicSerializer<CodeActionLiteralSupport>(
    CodeActionLiteralSupport::class
) { override fun selectDeserializer(element: JsonElement) = IsaacCodeActionLiteralSupport.serializer() }
