package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

/**
 * Static registration options returned by the initialize request.
 */
interface StaticRegistrationOptions {
    /**
     * The id used to register the request. The id can be used to deregister
     * the request again. See also Registration#id.
     */
    val id: String?
}

fun StaticRegistrationOptions(id: String? = null): StaticRegistrationOptions = IsaacStaticRegistrationOptions(id)

@Serializable class IsaacStaticRegistrationOptions(override val id: String? = null) : StaticRegistrationOptions

object StaticRegistrationOptionsSerializer : JsonContentPolymorphicSerializer<StaticRegistrationOptions>(
    StaticRegistrationOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacStaticRegistrationOptions.serializer() }
