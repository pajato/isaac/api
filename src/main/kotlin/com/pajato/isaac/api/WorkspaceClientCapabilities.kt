package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface WorkspaceClientCapabilities {
    /**
     * The client supports applying batch edits
     * to the workspace by supporting the request
     * 'workspace/applyEdit'
     */
    val applyEdit: Boolean?

    /**
     * Capabilities specific to `WorkspaceEdit`s
     */
    val workspaceEdit: WorkspaceEditClientCapabilities?

    /**
     * Capabilities specific to the `workspace/didChangeConfiguration`
     * notification.
     */
    val didChangeConfiguration: DidChangeConfigurationClientCapabilities?

    /**
     * Capabilities specific to the `workspace/didChangeWatchedFiles`
     * notification.
     */
    val didChangeWatchedFiles: DidChangeWatchedFilesClientCapabilities?

    /**
     * Capabilities specific to the `workspace/symbol` request.
     */
    val symbol: WorkspaceSymbolClientCapabilities?

    /**
     * Capabilities specific to the `workspace/executeCommand` request.
     */
    val executeCommand: ExecuteCommandClientCapabilities?

    /**
     * The client has support for workspace folders.
     *
     * @since 3.6.0
     */
    val workspaceFolders: Boolean?

    /**
     * The client supports `workspace/configuration` requests.
     *
     * @since 3.6.0
     */
    val configuration: Boolean?

    /**
     * Capabilities specific to the semantic token requests scoped to the
     * workspace.
     *
     * @since 3.16.0
     */
    val semanticTokens: SemanticTokensWorkspaceClientCapabilities?

    /**
     * Capabilities specific to the code lens requests scoped to the
     * workspace.
     *
     * @since 3.16.0
     */
    val codeLens: CodeLensWorkspaceClientCapabilities?

    /**
     * The client has support for file requests/notifications.
     *
     * @since 3.16.0
     */
    val fileOperations: FileOperations?
}

fun WorkspaceClientCapabilities(
    applyEdit: Boolean? = null,
    workspaceEdit: WorkspaceEditClientCapabilities? = null,
    didChangeConfiguration: DidChangeConfigurationClientCapabilities? = null,
    didChangeWatchedFiles: DidChangeWatchedFilesClientCapabilities? = null,
    symbol: WorkspaceSymbolClientCapabilities? = null,
    executeCommand: ExecuteCommandClientCapabilities? = null,
    workspaceFolders: Boolean? = null,
    configuration: Boolean? = null,
    semanticTokens: SemanticTokensWorkspaceClientCapabilities? = null,
    codeLens: CodeLensWorkspaceClientCapabilities? = null,
    fileOperations: FileOperations? = null,
): WorkspaceClientCapabilities = IsaacWorkspaceClientCapabilities(
    applyEdit, workspaceEdit, didChangeConfiguration, didChangeWatchedFiles, symbol, executeCommand, workspaceFolders,
    configuration, semanticTokens, codeLens, fileOperations
)

@Serializable
class IsaacWorkspaceClientCapabilities(
    override val applyEdit: Boolean? = null,

    @Serializable(with = WorkspaceEditClientSerializer::class)
    override val workspaceEdit: WorkspaceEditClientCapabilities? = null,

    @Serializable(with = DidChangeConfigurationSerializer::class)
    override val didChangeConfiguration: DidChangeConfigurationClientCapabilities? = null,

    @Serializable(with = DidChangeWatchedFilesSerializer::class)
    override val didChangeWatchedFiles: DidChangeWatchedFilesClientCapabilities? = null,

    @Serializable(with = WorkspaceSymbolClientSerializer::class)
    override val symbol: WorkspaceSymbolClientCapabilities? = null,

    @Serializable(with = ExecuteCommandClientCapabilitiesSerializer::class)
    override val executeCommand: ExecuteCommandClientCapabilities? = null,

    override val workspaceFolders: Boolean? = null,
    override val configuration: Boolean? = null,

    @Serializable(with = SemanticTokensWorkspaceCCSerializer::class)
    override val semanticTokens: SemanticTokensWorkspaceClientCapabilities? = null,

    @Serializable(with = CodeLensWorkspaceClientCapabilitiesSerializer::class)
    override val codeLens: CodeLensWorkspaceClientCapabilities? = null,

    @Serializable(with = FileOperationsSerializer::class)
    override val fileOperations: FileOperations? = null,
) : WorkspaceClientCapabilities

private typealias WorkspaceClient = WorkspaceClientCapabilities

object WorkspaceClientSerializer : JsonContentPolymorphicSerializer<WorkspaceClient>(WorkspaceClient::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacWorkspaceClientCapabilities.serializer()
}
