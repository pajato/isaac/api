package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DocumentSymbolResult {
    /**
     * A list (possibly empty) of document symbol objects.
     */
    val list: List<DocumentSymbol>
}

fun DocumentSymbolResult(list: List<DocumentSymbol>): DocumentSymbolResult = IsaacDocumentSymbolResult(list)

@Serializable class IsaacDocumentSymbolResult(
    @Serializable(with = DocumentSymbolListSerializer::class)
    override val list: List<DocumentSymbol>
) : DocumentSymbolResult, Result()

object DocumentSymbolResultSerializer : JsonContentPolymorphicSerializer<DocumentSymbolResult>(
    DocumentSymbolResult::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDocumentSymbolResult.serializer() }
