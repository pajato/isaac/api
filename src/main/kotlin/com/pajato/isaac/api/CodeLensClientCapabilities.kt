package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface CodeLensClientCapabilities {
    /**
     * Whether code lens supports dynamic registration.
     */
    val dynamicRegistration: Boolean?
}

fun CodeLensClientCapabilities(dynamicRegistration: Boolean? = null): CodeLensClientCapabilities =
    IsaacCodeLensClientCapabilities(dynamicRegistration)

@Serializable class IsaacCodeLensClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
) : CodeLensClientCapabilities

object CodeLensClientCapabilitiesSerializer : JsonContentPolymorphicSerializer<CodeLensClientCapabilities>(
    CodeLensClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacCodeLensClientCapabilities.serializer() }
