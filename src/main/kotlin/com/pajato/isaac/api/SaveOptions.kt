package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.JsonTransformingSerializer
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put

interface SaveOptions {
    /**
     * The client is supposed to include the content on save.
     */
    val includeText: Boolean?
}

fun SaveOptions(includeText: Boolean? = null): SaveOptions = IsaacSaveOptions(includeText)

@Serializable class IsaacSaveOptions(override val includeText: Boolean? = null) : SaveOptions

object SaveOptionsSerializer : JsonContentPolymorphicSerializer<SaveOptions>(SaveOptions::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacSaveOptions.serializer()
}

object SaveOptionsTransformingSerializer : JsonTransformingSerializer<IsaacSaveOptions>(IsaacSaveOptions.serializer()) {

    override fun transformSerialize(element: JsonElement): JsonElement {
        check(element is JsonObject && element.size == 1) { "Invalid element content: $element." }
        val key = "includeText"

        return element[key] as JsonPrimitive
    }

    override fun transformDeserialize(element: JsonElement): JsonElement {
        check(element is JsonPrimitive) { "Invalid element content: $element." }
        return buildJsonObject { put("includeText", element.content.toBoolean()) }
    }
}
