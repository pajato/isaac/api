package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DeclarationRegistrationOptions :
    TextDocumentRegistrationOptions, DeclarationOptions, StaticRegistrationOptions

fun DeclarationRegistrationOptions(
    documentSelector: List<DocumentFilter>? = null,
    workDoneProgress: Boolean? = null,
    id: String? = null
): DeclarationRegistrationOptions = IsaacDeclarationRegistrationOptions(documentSelector, workDoneProgress, id)

@Serializable class IsaacDeclarationRegistrationOptions(
    override val documentSelector: List<DocumentFilter>? = null,
    override val workDoneProgress: Boolean? = null,
    override val id: String? = null,
) : DeclarationRegistrationOptions

object DeclarationRegistrationOptionsSerializer : JsonContentPolymorphicSerializer<DeclarationRegistrationOptions>(
    DeclarationRegistrationOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDeclarationRegistrationOptions.serializer() }
