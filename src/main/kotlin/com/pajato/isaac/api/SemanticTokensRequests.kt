package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface SemanticTokensRequests {
    /**
     * The client will send the `textDocument/semanticTokens/range` request
     * if the server provides a corresponding handler.
     */
    val range: Boolean?

    /**
     * The client will send the `textDocument/semanticTokens/full` request
     * if the server provides a corresponding handler.
     */
    val full: Full?
}

fun SemanticTokensRequests(
    range: Boolean? = null,
    full: Full? = null,
): SemanticTokensRequests = IsaacSemanticTokensRequests(range, full)

@Serializable class IsaacSemanticTokensRequests(
    override val range: Boolean? = null,

    @Serializable(with = ContentFullSerializer::class)
    override val full: Full? = null,
) : SemanticTokensRequests

object SemanticTokensRequestsSerializer : JsonContentPolymorphicSerializer<SemanticTokensRequests>(
    SemanticTokensRequests::class
) { override fun selectDeserializer(element: JsonElement) = IsaacSemanticTokensRequests.serializer() }
