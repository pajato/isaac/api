package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface ReferencesOptions : WorkDoneProgressOptions

fun ReferencesOptions(workDoneProgress: Boolean? = null): ReferencesOptions = IsaacReferencesOptions(workDoneProgress)

@Serializable class IsaacReferencesOptions(override val workDoneProgress: Boolean? = null) : ReferencesOptions

object ReferencesOptionsSerializer : JsonContentPolymorphicSerializer<ReferencesOptions>(ReferencesOptions::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacReferencesOptions.serializer()
}
