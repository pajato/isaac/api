package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface ReferenceClientCapabilities {
    /**
     * Whether references supports dynamic registration.
     */
    val dynamicRegistration: Boolean?
}

fun ReferenceClientCapabilities(dynamicRegistration: Boolean? = null): ReferenceClientCapabilities =
    IsaacReferenceClientCapabilities(dynamicRegistration)

@Serializable class IsaacReferenceClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
) : ReferenceClientCapabilities

object ReferenceClientCapabilitiesSerializer : JsonContentPolymorphicSerializer<ReferenceClientCapabilities>(
    ReferenceClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacReferenceClientCapabilities.serializer() }
