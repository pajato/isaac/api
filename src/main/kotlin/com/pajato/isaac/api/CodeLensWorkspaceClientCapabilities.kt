package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface CodeLensWorkspaceClientCapabilities {
    /**
     * Whether the client implementation supports a refresh request sent from the
     * server to the client.
     *
     * Note that this event is global and will force the client to refresh all
     * code lenses currently shown. It should be used with absolute care and is
     * useful for situation where a server for example detect a project wide
     * change that requires such a calculation.
     */
    val refreshSupport: Boolean?
}

fun CodeLensWorkspaceClientCapabilities(refreshSupport: Boolean? = null): CodeLensWorkspaceClientCapabilities =
    IsaacCodeLensWorkspaceClientCapabilities(refreshSupport)

@Serializable class IsaacCodeLensWorkspaceClientCapabilities(
    override val refreshSupport: Boolean? = null,
) : CodeLensWorkspaceClientCapabilities

typealias CodeLensWorkspaceCC = CodeLensWorkspaceClientCapabilities

object CodeLensWorkspaceClientCapabilitiesSerializer : JsonContentPolymorphicSerializer<CodeLensWorkspaceCC>(
    CodeLensWorkspaceClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacCodeLensWorkspaceClientCapabilities.serializer() }
