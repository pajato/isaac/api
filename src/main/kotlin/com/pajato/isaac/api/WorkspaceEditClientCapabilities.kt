package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

enum class ResourceOperationKind { Create, Rename, Delete }
enum class FailureHandlingKind { Abort, Transactional, Undo, TextOnlyTransactional }

interface WorkspaceEditClientCapabilities {
    /**
     * The client supports versioned document changes in `WorkspaceEdit`s
     */
    val documentChanges: Boolean?

    /**
     * The resource operations the client supports. Clients should at least
     * support 'create', 'rename' and 'delete' files and folders.
     *
     * @since 3.13.0
     */
    val resourceOperations: Array<ResourceOperationKind>?

    /**
     * The failure handling strategy of a client if applying the workspace edit
     * fails.
     *
     * @since 3.13.0
     */
    val failureHandling: FailureHandlingKind?

    /**
     * Whether the client normalizes line endings to the client specific
     * setting.
     * If set to `true` the client will normalize line ending characters
     * in a workspace edit to the client specific new line character(s).
     *
     * @since 3.16.0
     */
    val normalizesLineEndings: Boolean?

    /**
     * Whether the client in general supports change annotations on text edits,
     * create file, rename file and delete file changes.
     *
     * @since 3.16.0
     */
    val changeAnnotationSupport: ChangeAnnotationSupport?
}

fun WorkspaceEditClientCapabilities(
    documentChanges: Boolean? = null,
    resourceOperations: Array<ResourceOperationKind>? = null,
    failureHandling: FailureHandlingKind? = null,
    normalizesLineEndings: Boolean? = null,
    changeAnnotationSupport: ChangeAnnotationSupport? = null,
): WorkspaceEditClientCapabilities = IsaacWorkspaceEditClientCapabilities(
    documentChanges, resourceOperations, failureHandling, normalizesLineEndings, changeAnnotationSupport
)

@Serializable class IsaacWorkspaceEditClientCapabilities(
    override val documentChanges: Boolean? = null,
    override val resourceOperations: Array<ResourceOperationKind>? = null,
    override val failureHandling: FailureHandlingKind? = null,
    override val normalizesLineEndings: Boolean? = null,

    @Serializable(with = ChangeAnnotationSupportSerializer::class)
    override val changeAnnotationSupport: ChangeAnnotationSupport? = null,
) : WorkspaceEditClientCapabilities

object WorkspaceEditClientSerializer : JsonContentPolymorphicSerializer<WorkspaceEditClientCapabilities>(
    WorkspaceEditClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacWorkspaceEditClientCapabilities.serializer() }
