package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DocumentSymbolClientCapabilities {
    /**
     * Whether document symbol supports dynamic registration.
     */
    val dynamicRegistration: Boolean?

    /**
     * Specific capabilities for the `SymbolKind` in the
     * `textDocument/documentSymbol` request.
     */
    val symbolKind: SymbolKind?

    /**
     * The client supports hierarchical document symbols.
     */
    val hierarchicalDocumentSymbolSupport: Boolean?

    /**
     * The client supports tags on `SymbolInformation`. Tags are supported on
     * `DocumentSymbol` if `hierarchicalDocumentSymbolSupport` is set to true.
     * Clients supporting tags have to handle unknown tags gracefully.
     *
     * @since 3.16.0
     */
    val tagSupport: SymbolTagSupport?

    /**
     * The client supports an additional label presented in the UI when
     * registering a document symbol provider.
     *
     * @since 3.16.0
     */
    val labelSupport: Boolean?
}

fun DocumentSymbolClientCapabilities(
    dynamicRegistration: Boolean? = null,
    symbolKind: SymbolKind? = null,
    hierarchicalDocumentSymbolSupport: Boolean? = null,
    tagSupport: SymbolTagSupport? = null,
    labelSupport: Boolean? = null,
): DocumentSymbolClientCapabilities = IsaacDocumentSymbolClientCapabilities(
    dynamicRegistration, symbolKind, hierarchicalDocumentSymbolSupport, tagSupport, labelSupport
)

@Serializable class IsaacDocumentSymbolClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
    override val symbolKind: SymbolKind? = null,
    override val hierarchicalDocumentSymbolSupport: Boolean? = null,

    @Serializable(with = SymbolTagSupportSerializer::class)
    override val tagSupport: SymbolTagSupport? = null,

    override val labelSupport: Boolean? = null,
) : DocumentSymbolClientCapabilities

object DocumentSymbolClientSerializer : JsonContentPolymorphicSerializer<DocumentSymbolClientCapabilities>(
    DocumentSymbolClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDocumentSymbolClientCapabilities.serializer() }
