package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

@Serializable(with = DocumentFilterSerializer::class)
interface DocumentFilter {
    /**
     * A language id, like `typescript`.
     */
    val language: String?

    /**
     * A Uri [scheme](#Uri.scheme), like `file` or `untitled`.
     */
    val scheme: String?

    /**
     * A glob pattern, like `*.{ts,js}`.
     *
     * Glob patterns can have the following syntax:
     * - `*` to match one or more characters in a path segment
     * - `?` to match on one character in a path segment
     * - `**` to match any number of path segments, including none
     * - `{}` to group conditions (e.g. `**​\/\*.{ts,js}` matches all TypeScript
     *   and JavaScript files)
     * - `[]` to declare a range of characters to match in a path segment
     *   (e.g., `example.[0-9]` to match on `example.0`, `example.1`, …)
     * - `[!...]` to negate a range of characters to match in a path segment
     *   (e.g., `example.[!0-9]` to match on `example.a`, `example.b`, but
     *   not `example.0`)
     */
    val pattern: String?
}

fun DocumentFilter(
    language: String? = null,
    scheme: String? = null,
    pattern: String? = null,
): DocumentFilter = IsaacDocumentFilter(language, scheme, pattern)

@Serializable class IsaacDocumentFilter(
    override val language: String? = null,
    override val scheme: String? = null,
    override val pattern: String? = null,
) : DocumentFilter

object DocumentFilterSerializer : JsonContentPolymorphicSerializer<DocumentFilter>(DocumentFilter::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacDocumentFilter.serializer()
}
