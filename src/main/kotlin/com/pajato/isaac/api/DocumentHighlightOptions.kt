package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DocumentHighlightOptions : WorkDoneProgressOptions

fun DocumentHighlightOptions(workDoneProgress: Boolean? = null): DocumentHighlightOptions =
    IsaacDocumentHighlightOptions(workDoneProgress)

@Serializable internal class IsaacDocumentHighlightOptions(override val workDoneProgress: Boolean? = null) :
    DocumentHighlightOptions

internal object DocumentHighlightOptionsSerializer : JsonContentPolymorphicSerializer<DocumentHighlightOptions>(
    DocumentHighlightOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDocumentHighlightOptions.serializer() }
