package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

/**
 * Workspace specific server capabilities
 */
interface Workspace {
    /**
     * The server supports workspace folder.
     *
     * @since 3.6.0
     */
    val workspaceFolders: WorkspaceFoldersServerCapabilities?
}

fun Workspace(workspaceFolders: WorkspaceFoldersServerCapabilities? = null): Workspace =
    IsaacWorkspace(workspaceFolders)

@Serializable class IsaacWorkspace(
    @Serializable(with = WorkspaceFoldersServerCapabilitiesSerializer::class)
    override val workspaceFolders: WorkspaceFoldersServerCapabilities? = null
) : Workspace

object WorkspaceSerializer : JsonContentPolymorphicSerializer<Workspace>(Workspace::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacWorkspace.serializer()
}
