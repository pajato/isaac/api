package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.JsonTransformingSerializer
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put

@Serializable sealed class Id {
    abstract fun toInt(): Int

    @Serializable class NumberId(val id: Int) : Id() {
        override fun toInt() = id
    }

    @Serializable class StringId(val id: String) : Id() {
        override fun toInt() = id.toInt()
    }
}

object IdSerializer : JsonContentPolymorphicSerializer<Id>(Id::class) {
    private const val key = "id"
    private fun isString(element: JsonPrimitive) = element.isString

    override fun selectDeserializer(element: JsonElement) = when {
        element is JsonObject && isString(element[key] as JsonPrimitive) -> Id.StringId.serializer()
        else -> Id.NumberId.serializer()
    }
}

object IdTransformingSerializer : JsonTransformingSerializer<Id>(Id.serializer()) {
    private const val key = "id"
    private const val typeKey = "type"

    override fun transformSerialize(element: JsonElement): JsonElement {
        fun isValid() = element is JsonObject && element.size == 2 && element.containsKey(key)
        fun transform(element: JsonObject) = element[key] as JsonPrimitive

        check(isValid()) { "Invalid element: $element" }
        return transform(element as JsonObject)
    }

    override fun transformDeserialize(element: JsonElement): JsonElement {
        fun transformToString(element: JsonPrimitive) = buildJsonObject {
            put(typeKey, Id::class.java.name + ".StringId")
            put(key, element)
        }

        fun transformToInteger(element: JsonPrimitive) = buildJsonObject {
            put(typeKey, Id::class.java.name + ".NumberId")
            put(key, element)
        }

        return when {
            element is JsonPrimitive && element.isString -> transformToString(element)
            element is JsonPrimitive && element.content.toIntOrNull() != null -> transformToInteger(element)
            else -> throw IllegalStateException("Invalid element: $element!")
        }
    }
}
