package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DocumentOnTypeFormattingClientCapabilities {
    /**
     * Whether on type formatting supports dynamic registration.
     */
    val dynamicRegistration: Boolean?
}

fun DocumentOnTypeFormattingClientCapabilities(dynamicRegistration: Boolean? = null):
    DocumentOnTypeFormattingClientCapabilities =
    IsaacDocumentOnTypeFormattingClientCapabilities(dynamicRegistration)

@Serializable class IsaacDocumentOnTypeFormattingClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
) : DocumentOnTypeFormattingClientCapabilities

private typealias DocumentOnTypeFormattingCC = DocumentOnTypeFormattingClientCapabilities

object DocumentOnTypeFormattingCCSerializer : JsonContentPolymorphicSerializer<DocumentOnTypeFormattingCC>(
    DocumentOnTypeFormattingCC::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDocumentOnTypeFormattingClientCapabilities.serializer() }
