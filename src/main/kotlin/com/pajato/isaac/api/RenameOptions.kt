package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface RenameOptions : WorkDoneProgressOptions

fun RenameOptions(workDoneProgress: Boolean? = null): RenameOptions = IsaacRenameOptions(workDoneProgress)

@Serializable class IsaacRenameOptions(override val workDoneProgress: Boolean? = null) : RenameOptions

object RenameOptionsSerializer : JsonContentPolymorphicSerializer<RenameOptions>(RenameOptions::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacRenameOptions.serializer()
}
