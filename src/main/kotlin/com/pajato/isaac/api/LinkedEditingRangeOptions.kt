package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface LinkedEditingRangeOptions : WorkDoneProgressOptions

fun LinkedEditingRangeOptions(workDoneProgress: Boolean? = null): LinkedEditingRangeOptions =
    IsaacLinkedEditingRangeOptions(workDoneProgress)

@Serializable class IsaacLinkedEditingRangeOptions(override val workDoneProgress: Boolean? = null) :
    LinkedEditingRangeOptions

object LinkedEditingRangeOptionsSerializer :
    JsonContentPolymorphicSerializer<LinkedEditingRangeOptions>(LinkedEditingRangeOptions::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacLinkedEditingRangeOptions.serializer()
}
