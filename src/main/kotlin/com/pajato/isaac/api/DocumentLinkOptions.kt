package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DocumentLinkOptions : WorkDoneProgressOptions {
    /**
     * Document link has a resolve provider as well.
     */
    val resolveProvider: Boolean?
}

fun DocumentLinkOptions(workDoneProgress: Boolean? = null, resolveProvider: Boolean? = null): DocumentLinkOptions =
    IsaacDocumentLinkOptions(workDoneProgress, resolveProvider)

@Serializable class IsaacDocumentLinkOptions(
    override val workDoneProgress: Boolean? = null,
    override val resolveProvider: Boolean? = null,
) : DocumentLinkOptions

object DocumentLinkOptionsSerializer : JsonContentPolymorphicSerializer<DocumentLinkOptions>(
    DocumentLinkOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDocumentLinkOptions.serializer() }
