package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface GeneralClientCapabilities {
    /**
     * Client capabilities specific to regular expressions.
     *
     * @since 3.16.0
     */
    val regularExpressions: RegularExpressionsClientCapabilities?

    /**
     * Client capabilities specific to the clients' Markdown parser.
     *
     * @since 3.16.0
     */
    val markdown: MarkdownClientCapabilities?
}

fun GeneralClientCapabilities(
    regularExpressions: RegularExpressionsClientCapabilities? = null,
    markdown: MarkdownClientCapabilities? = null,
): GeneralClientCapabilities = IsaacGeneralClientCapabilities(regularExpressions, markdown)

@Serializable class IsaacGeneralClientCapabilities(
    @Serializable(with = RegularExpressionsClientCapabilitiesSerializer::class)
    override val regularExpressions: RegularExpressionsClientCapabilities? = null,

    @Serializable(with = MarkdownClientCapabilitiesSerializer::class)
    override val markdown: MarkdownClientCapabilities? = null,
) : GeneralClientCapabilities

object GeneralClientCapabilitiesSerializer :
    JsonContentPolymorphicSerializer<GeneralClientCapabilities>(GeneralClientCapabilities::class) {

    override fun selectDeserializer(element: JsonElement) = IsaacGeneralClientCapabilities.serializer()
}
