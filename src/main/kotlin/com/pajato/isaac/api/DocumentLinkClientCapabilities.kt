package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DocumentLinkClientCapabilities {
    /**
     * Whether document link supports dynamic registration.
     */
    val dynamicRegistration: Boolean?

    /**
     * Whether the client supports the `tooltip` property on `DocumentLink`.
     *
     * @since 3.15.0
     */
    val tooltipSupport: Boolean?
}

fun DocumentLinkClientCapabilities(
    dynamicRegistration: Boolean? = null,
    tooltipSupport: Boolean? = null,
): DocumentLinkClientCapabilities = IsaacDocumentLinkClientCapabilities(dynamicRegistration, tooltipSupport)

@Serializable class IsaacDocumentLinkClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
    override val tooltipSupport: Boolean? = null,
) : DocumentLinkClientCapabilities

object DocumentLinkClientSerializer : JsonContentPolymorphicSerializer<DocumentLinkClientCapabilities>(
    DocumentLinkClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDocumentLinkClientCapabilities.serializer() }
