package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface HoverOptions : WorkDoneProgressOptions

fun HoverOptions(workDoneProgress: Boolean? = null): HoverOptions = IsaacHoverOptions(workDoneProgress)

@Serializable class IsaacHoverOptions(override val workDoneProgress: Boolean? = null) : HoverOptions

object HoverOptionsSerializer : JsonContentPolymorphicSerializer<HoverOptions>(HoverOptions::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacHoverOptions.serializer()
}
