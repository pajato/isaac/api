package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface ExecuteCommandClientCapabilities {
    /**
     * Execute command supports dynamic registration.
     */
    val dynamicRegistration: Boolean?
}

fun ExecuteCommandClientCapabilities(dynamicRegistration: Boolean? = null): ExecuteCommandClientCapabilities =
    IsaacExecuteCommandClientCapabilities(dynamicRegistration)

@Serializable class IsaacExecuteCommandClientCapabilities(
    override val dynamicRegistration: Boolean? = null,
) : ExecuteCommandClientCapabilities

object ExecuteCommandClientCapabilitiesSerializer : JsonContentPolymorphicSerializer<ExecuteCommandClientCapabilities>(
    ExecuteCommandClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacExecuteCommandClientCapabilities.serializer() }
