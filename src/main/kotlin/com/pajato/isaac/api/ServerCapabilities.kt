package com.pajato.isaac.api

import kotlinx.serialization.Polymorphic
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface ServerCapabilities {
    /**
     * Defines how text documents are synced. Is either a detailed structure
     * defining each notification or for backwards compatibility the
     * TextDocumentSyncKind number. If omitted it defaults to
     * `TextDocumentSyncKind.None`.
     */
    val textDocumentSync: TextDocumentSync?

    /**
     * The server provides completion support.
     */
    val completionProvider: CompletionOptions?

    /**
     * The server provides hover support.
     */
    val hoverProvider: HoverProvider?

    /**
     * The server provides signature help support.
     */
    val signatureHelpProvider: SignatureHelpOptions?

    /**
     * The server provides go to declaration support.
     *
     * @since 3.14.0
     */
    val declarationProvider: DeclarationProvider?

    /**
     * The server provides goto definition support.
     */
    val definitionProvider: DefinitionProvider?

    /**
     * The server provides goto type definition support.
     *
     * @since 3.6.0
     */
    val typeDefinitionProvider: TypeDefinitionProvider?

    /**
     * The server provides goto implementation support.
     *
     * @since 3.6.0
     */
    val implementationProvider: ImplementationProvider?

    /**
     * The server provides find references support.
     */
    val referencesProvider: ReferencesProvider?

    /**
     * The server provides document highlight support.
     */
    val documentHighlightProvider: DocumentHighlightProvider?

    /**
     * The server provides document symbol support.
     */
    val documentSymbolProvider: DocumentSymbolProvider?

    /**
     * The server provides code actions. The `CodeActionOptions` return type is
     * only valid if the client signals code action literal support via the
     * property `textDocument.codeAction.codeActionLiteralSupport`.
     */
    val codeActionProvider: CodeActionProvider?

    /**
     * The server provides code lens.
     */
    val codeLensProvider: CodeLensOptions?

    /**
     * The server provides document link support.
     */
    val documentLinkProvider: DocumentLinkOptions?

    /**
     * The server provides color provider support.
     *
     * @since 3.6.0
     */
    val colorProvider: ColorProvider?

    /**
     * The server provides document formatting.
     */
    val documentFormattingProvider: DocumentFormattingProvider?

    /**
     * The server provides document range formatting.
     */
    val documentRangeFormattingProvider: DocumentRangeFormattingProvider?

    /**
     * The server provides document formatting on typing.
     */
    val documentOnTypeFormattingProvider: DocumentOnTypeFormattingOptions?

    /**
     * The server provides rename support. RenameOptions may only be
     * specified if the client states that it supports
     * `prepareSupport` in its initial `initialize` request.
     */
    val renameProvider: RenameProvider?

    /**
     * The server provides folding provider support.
     *
     * @since 3.10.0
     */
    val foldingRangeProvider: FoldingRangeProvider?

    /**
     * The server provides execute command support.
     */
    val executeCommandProvider: ExecuteCommandOptions?

    /**
     * The server provides selection range support.
     *
     * @since 3.15.0
     */
    val selectionRangeProvider: SelectionRangeProvider?

    /**
     * The server provides linked editing range support.
     *
     * @since 3.16.0
     */
    val linkedEditingRangeProvider: LinkedEditingRangeProvider?

    /**
     * The server provides call hierarchy support.
     *
     * @since 3.16.0
     */
    val callHierarchyProvider: CallHierarchyProvider?

    /**
     * The server provides semantic tokens support.
     *
     * @since 3.16.0
     */
    val semanticTokensProvider: SemanticTokensProvider?

    /**
     * Whether server provides moniker support.
     *
     * @since 3.16.0
     */
    val monikerProvider: MonikerProvider?

    /**
     * The server provides workspace symbol support.
     */
    val workspaceSymbolProvider: WorkspaceSymbolProvider?

    /**
     * Workspace specific server capabilities
     */
    val workspace: Workspace?

    /**
     * Experimental server capabilities.
     */
    val experimental: Any?
}

fun ServerCapabilities(
    textDocumentSync: TextDocumentSync? = null,
    completionProvider: CompletionOptions? = null,
    hoverProvider: HoverProvider? = null,
    signatureHelpProvider: SignatureHelpOptions? = null,
    declarationProvider: DeclarationProvider? = null,
    definitionProvider: BooleanDefinitionProvider? = null,
    implementationProvider: ImplementationProvider? = null,
    referencesProvider: ReferencesProvider? = null,
    documentHighlightProvider: DocumentHighlightProvider? = null,
    documentSymbolProvider: DocumentSymbolProvider? = null,
    codeActionProvider: CodeActionProvider? = null,
    codeLensProvider: CodeLensOptions? = null,
    documentLinkProvider: DocumentLinkOptions? = null,
    colorProvider: ColorProvider? = null,
    documentFormattingProvider: DocumentFormattingProvider? = null,
    documentRangeFormattingProvider: DocumentRangeFormattingProvider? = null,
    documentOnTypeFormattingProvider: DocumentOnTypeFormattingOptions? = null,
    renameProvider: RenameProvider? = null,
    foldingRangeProvider: FoldingRangeProvider? = null,
    executeCommandProvider: ExecuteCommandOptions? = null,
    selectionRangeProvider: SelectionRangeProvider? = null,
    linkedEditingRangeProvider: LinkedEditingRangeProvider? = null,
    callHierarchyProvider: CallHierarchyProvider? = null,
    typeDefinitionProvider: TypeDefinitionProvider? = null,
    workspaceSymbolProvider: WorkspaceSymbolProvider? = null,
    semanticTokensProvider: SemanticTokensProvider? = null,
    monikerProvider: MonikerProvider? = null,
    workspace: Workspace? = null,
    experimental: Any? = null,
): ServerCapabilities = IsaacServerCapabilities(
    textDocumentSync, completionProvider, hoverProvider,
    signatureHelpProvider, declarationProvider, definitionProvider, implementationProvider, referencesProvider,
    documentHighlightProvider, documentSymbolProvider, codeActionProvider, codeLensProvider, documentLinkProvider,
    colorProvider, documentFormattingProvider, documentRangeFormattingProvider, documentOnTypeFormattingProvider,
    renameProvider, foldingRangeProvider, executeCommandProvider, selectionRangeProvider, linkedEditingRangeProvider,
    callHierarchyProvider, typeDefinitionProvider, workspaceSymbolProvider, semanticTokensProvider, monikerProvider,
    workspace, experimental
)

@Serializable class IsaacServerCapabilities(
    @Serializable(with = TransformingTextDocumentSyncSerializer::class)
    override val textDocumentSync: TextDocumentSync? = null,

    @Serializable(with = CompletionOptionsSerializer::class)
    override val completionProvider: CompletionOptions? = null,

    @Serializable(with = TransformingHoverProviderSerializer::class)
    override val hoverProvider: HoverProvider? = null,

    @Serializable(with = SignatureHelpOptionsSerializer::class)
    override val signatureHelpProvider: SignatureHelpOptions? = null,

    @Serializable(with = DeclarationProviderTransformingSerializer::class)
    override val declarationProvider: DeclarationProvider? = null,

    @Serializable(with = DefinitionProviderTransformingSerializer::class)
    override val definitionProvider: DefinitionProvider? = null,

    @Serializable(with = ImplementationProviderTransformingSerializer::class)
    override val implementationProvider: ImplementationProvider? = null,

    @Serializable(with = ReferencesProviderTransformingSerializer::class)
    override val referencesProvider: ReferencesProvider? = null,

    @Serializable(with = DocumentHighlightProviderTransformingSerializer::class)
    override val documentHighlightProvider: DocumentHighlightProvider? = null,

    @Serializable(with = DocumentSymbolProviderTransformingSerializer::class)
    override val documentSymbolProvider: DocumentSymbolProvider? = null,

    @Serializable(with = CodeActionProviderTransformingSerializer::class)
    override val codeActionProvider: CodeActionProvider? = null,

    @Serializable(with = CodeLensOptionsSerializer::class)
    override val codeLensProvider: CodeLensOptions? = null,

    @Serializable(with = DocumentLinkOptionsSerializer::class)
    override val documentLinkProvider: DocumentLinkOptions? = null,

    @Serializable(with = ColorProviderTransformingSerializer::class)
    override val colorProvider: ColorProvider? = null,

    @Serializable(with = DocumentFormattingProviderTransformingSerializer::class)
    override val documentFormattingProvider: DocumentFormattingProvider? = null,

    @Serializable(with = DocumentRangeFormattingProviderTransformingSerializer::class)
    override val documentRangeFormattingProvider: DocumentRangeFormattingProvider? = null,

    @Serializable(with = DocumentOnTypeFormattingOptionsSerializer::class)
    override val documentOnTypeFormattingProvider: DocumentOnTypeFormattingOptions? = null,

    @Serializable(with = RenameProviderTransformingSerializer::class)
    override val renameProvider: RenameProvider? = null,

    @Serializable(with = FoldingRangeProviderTransformingSerializer::class)
    override val foldingRangeProvider: FoldingRangeProvider? = null,

    @Serializable(with = ExecuteCommandOptionsSerializer::class)
    override val executeCommandProvider: ExecuteCommandOptions? = null,

    @Serializable(with = SelectionRangeProviderTransformingSerializer::class)
    override val selectionRangeProvider: SelectionRangeProvider? = null,

    @Serializable(with = LinkedEditingRangeProviderTransformingSerializer::class)
    override val linkedEditingRangeProvider: LinkedEditingRangeProvider? = null,

    @Serializable(with = CallHierarchyProviderTransformingSerializer::class)
    override val callHierarchyProvider: CallHierarchyProvider? = null,

    @Serializable(with = TypeDefinitionProviderTransformingSerializer::class)
    override val typeDefinitionProvider: TypeDefinitionProvider? = null,

    @Serializable(with = WorkspaceSymbolProviderTransformingSerializer::class)
    override val workspaceSymbolProvider: WorkspaceSymbolProvider? = null,

    @Serializable(with = SemanticTokensProviderTransformingSerializer::class)
    override val semanticTokensProvider: SemanticTokensProvider? = null,

    @Serializable(with = MonikerProviderTransformingSerializer::class)
    override val monikerProvider: MonikerProvider? = null,

    @Serializable(with = WorkspaceSerializer::class)
    override val workspace: Workspace? = null,

    @Polymorphic
    override val experimental: Any? = null,
) : ServerCapabilities

/**
 * Use a content serializer to avoid a type discriminant. Properties will require transformations.
 */
object ServerCapabilitiesSerializer : JsonContentPolymorphicSerializer<ServerCapabilities>(ServerCapabilities::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacServerCapabilities.serializer()
}
