package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface DocumentSymbolOptions : WorkDoneProgressOptions

fun DocumentSymbolOptions(workDoneProgress: Boolean? = null): DocumentSymbolOptions =
    IsaacDocumentSymbolOptions(workDoneProgress)

@Serializable class IsaacDocumentSymbolOptions(override val workDoneProgress: Boolean? = null) : DocumentSymbolOptions

object DocumentSymbolOptionsSerializer : JsonContentPolymorphicSerializer<DocumentSymbolOptions>(
    DocumentSymbolOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacDocumentSymbolOptions.serializer() }
