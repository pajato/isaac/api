package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface SemanticTokensRegistrationOptions :
    TextDocumentRegistrationOptions, SemanticTokensOptions, StaticRegistrationOptions

fun SemanticTokensRegistrationOptions(
    documentSelector: List<DocumentFilter>? = null,
    workDoneProgress: Boolean? = null,
    legend: SemanticTokensLegend,
    range: Boolean? = null,
    full: Full? = null,
    id: String? = null,
): SemanticTokensRegistrationOptions = IsaacSemanticTokensRegistrationOptions(
    documentSelector, workDoneProgress, legend, range, full, id
)

@Serializable class IsaacSemanticTokensRegistrationOptions(
    override val documentSelector: List<DocumentFilter>? = null,

    override val workDoneProgress: Boolean? = null,

    @Serializable(with = SemanticTokensLegendSerializer::class)
    override val legend: SemanticTokensLegend,

    override val range: Boolean? = null,

    @Serializable(with = ContentFullSerializer::class)
    override val full: Full? = null,

    override val id: String? = null,
) : SemanticTokensRegistrationOptions

object SemanticTokensRegistrationOptionsSerializer :
    JsonContentPolymorphicSerializer<SemanticTokensRegistrationOptions>(SemanticTokensRegistrationOptions::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacSemanticTokensRegistrationOptions.serializer()
}
