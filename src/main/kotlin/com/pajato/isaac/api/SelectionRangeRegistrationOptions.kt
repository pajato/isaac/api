package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface SelectionRangeRegistrationOptions :
    TextDocumentRegistrationOptions, SelectionRangeOptions, StaticRegistrationOptions

fun SelectionRangeRegistrationOptions(
    documentSelector: List<DocumentFilter>? = null,
    workDoneProgress: Boolean? = null,
    id: String? = null,
): SelectionRangeRegistrationOptions = IsaacSelectionRangeRegistrationOptions(documentSelector, workDoneProgress, id)

@Serializable class IsaacSelectionRangeRegistrationOptions(
    override val documentSelector: List<DocumentFilter>? = null,
    override val workDoneProgress: Boolean? = null,
    override val id: String? = null,
) : SelectionRangeRegistrationOptions

object SelectionRangeRegistrationOptionsSerializer :
    JsonContentPolymorphicSerializer<SelectionRangeRegistrationOptions>(SelectionRangeRegistrationOptions::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacSelectionRangeRegistrationOptions.serializer()
}
