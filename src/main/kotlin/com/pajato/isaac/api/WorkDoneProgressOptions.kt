package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface WorkDoneProgressOptions {
    val workDoneProgress: Boolean?
}

fun WorkDoneProgressOptions(workDoneProgress: Boolean? = null): WorkDoneProgressOptions =
    IsaacWorkDoneProgressOptions(workDoneProgress)

@Serializable class IsaacWorkDoneProgressOptions(
    override val workDoneProgress: Boolean? = null
) : WorkDoneProgressOptions

object WorkDoneProgressOptionsSerializer : JsonContentPolymorphicSerializer<WorkDoneProgressOptions>(
    WorkDoneProgressOptions::class
) { override fun selectDeserializer(element: JsonElement) = IsaacWorkDoneProgressOptions.serializer() }
