package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

/**
 * A request message to describe a request between the client and the server. Every processed request must send a
 * response back to the sender of the request.
 */
interface RequestMessage : Message {
    /**
     * The request id.
     */
    val id: Id

    /**
     * The method to be invoked.
     */
    val method: String

    /**
     * The method's params.
     */
    val params: Params?
}

fun RequestMessage(
    id: Id,
    method: String,
    params: Params? = null,
    jsonrpc: String = jsonrpcVersion,
): RequestMessage = IsaacRequestMessage(jsonrpc, id, method, params)

@Serializable class IsaacRequestMessage(
    override val jsonrpc: String,

    @Serializable(with = IdTransformingSerializer::class)
    override val id: Id,

    override val method: String,

    @Serializable(with = ParamsSerializer::class)
    override val params: Params? = null,
) : RequestMessage

object RequestMessageSerializer : JsonContentPolymorphicSerializer<RequestMessage>(RequestMessage::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacRequestMessage.serializer()
}
