package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface MessageActionItem {
    /**
     * A short title like 'Retry', 'Open Log' etc.
     */
    val title: String
}

fun MessageActionItem(title: String): MessageActionItem = IsaacMessageActionItem(title)

@Serializable class IsaacMessageActionItem(override val title: String) : MessageActionItem

object MessageActionItemSerializer : JsonContentPolymorphicSerializer<MessageActionItem>(MessageActionItem::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacMessageActionItem.serializer()
}
