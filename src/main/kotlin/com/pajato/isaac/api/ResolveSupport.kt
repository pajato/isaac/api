package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

interface ResolveSupport {
    /**
     * The properties that a client can resolve lazily.
     */
    val properties: Array<String>
}

fun ResolveSupport(properties: Array<String>): ResolveSupport = IsaacResolveSupport(properties)

@Serializable class IsaacResolveSupport(override val properties: Array<String>) : ResolveSupport

object ResolveSupportSerializer : JsonContentPolymorphicSerializer<ResolveSupport>(ResolveSupport::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacResolveSupport.serializer()
}
