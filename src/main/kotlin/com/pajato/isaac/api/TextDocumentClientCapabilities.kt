package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

/**
 * Text document specific client capabilities.
 */
interface TextDocumentClientCapabilities {
    val synchronization: TextDocumentSyncClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/completion` request.
     */
    val completion: CompletionClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/hover` request.
     */
    val hover: HoverClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/signatureHelp` request.
     */
    val signatureHelp: SignatureHelpClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/declaration` request.
     *
     * @since 3.14.0
     */
    val declaration: DeclarationClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/definition` request.
     */
    val definition: DefinitionClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/typeDefinition` request.
     *
     * @since 3.6.0
     */
    val typeDefinition: TypeDefinitionClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/implementation` request.
     *
     * @since 3.6.0
     */
    val implementation: ImplementationClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/references` request.
     */
    val references: ReferenceClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/documentHighlight` request.
     */
    val documentHighlight: DocumentHighlightClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/documentSymbol` request.
     */
    val documentSymbol: DocumentSymbolClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/codeAction` request.
     */
    val codeAction: CodeActionClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/codeLens` request.
     */
    val codeLens: CodeLensClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/documentLink` request.
     */
    val documentLink: DocumentLinkClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/documentColor` and the
     * `textDocument/colorPresentation` request.
     *
     * @since 3.6.0
     */
    val colorProvider: DocumentColorClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/formatting` request.
     */
    val formatting: DocumentFormattingClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/rangeFormatting` request.
     */
    val rangeFormatting: DocumentRangeFormattingClientCapabilities?

    /** request.
     * Capabilities specific to the `textDocument/onTypeFormatting` request.
     */
    val onTypeFormatting: DocumentOnTypeFormattingClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/rename` request.
     */
    val rename: RenameClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/publishDiagnostics`
     * notification.
     */
    val publishDiagnostics: PublishDiagnosticsClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/foldingRange` request.
     *
     * @since 3.10.0
     */
    val foldingRange: FoldingRangeClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/selectionRange` request.
     *
     * @since 3.15.0
     */
    val selectionRange: SelectionRangeClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/linkedEditingRange` request.
     *
     * @since 3.16.0
     */
    val linkedEditingRange: LinkedEditingRangeClientCapabilities?

    /**
     * Capabilities specific to the various call hierarchy requests.
     *
     * @since 3.16.0
     */
    val callHierarchy: CallHierarchyClientCapabilities?

    /**
     * Capabilities specific to the various semantic token requests.
     *
     * @since 3.16.0
     */
    val semanticTokens: SemanticTokensClientCapabilities?

    /**
     * Capabilities specific to the `textDocument/moniker` request.
     *
     * @since 3.16.0
     */
    val moniker: MonikerClientCapabilities?
}

fun TextDocumentClientCapabilities(
    synchronization: TextDocumentSyncClientCapabilities? = null,
    completion: CompletionClientCapabilities? = null,
    hover: HoverClientCapabilities? = null,
    signatureHelp: SignatureHelpClientCapabilities? = null,
    declaration: DeclarationClientCapabilities? = null,
    definition: DefinitionClientCapabilities? = null,
    typeDefinition: TypeDefinitionClientCapabilities? = null,
    implementation: ImplementationClientCapabilities? = null,
    references: ReferenceClientCapabilities? = null,
    documentHighlight: DocumentHighlightClientCapabilities? = null,
    documentSymbol: DocumentSymbolClientCapabilities? = null,
    codeAction: CodeActionClientCapabilities? = null,
    codeLens: CodeLensClientCapabilities? = null,
    documentLink: DocumentLinkClientCapabilities? = null,
    colorProvider: DocumentColorClientCapabilities? = null,
    formatting: DocumentFormattingClientCapabilities? = null,
    rangeFormatting: DocumentRangeFormattingClientCapabilities? = null,
    onTypeFormatting: DocumentOnTypeFormattingClientCapabilities? = null,
    rename: RenameClientCapabilities? = null,
    publishDiagnostics: PublishDiagnosticsClientCapabilities? = null,
    foldingRange: FoldingRangeClientCapabilities? = null,
    selectionRange: SelectionRangeClientCapabilities? = null,
    linkedEditingRange: LinkedEditingRangeClientCapabilities? = null,
    callHierarchy: CallHierarchyClientCapabilities? = null,
    semanticTokens: SemanticTokensClientCapabilities? = null,
    moniker: MonikerClientCapabilities? = null,
): TextDocumentClientCapabilities =
    IsaacTextDocumentClientCapabilities(
        synchronization, completion, hover, signatureHelp, declaration, definition, typeDefinition, implementation,
        references, documentHighlight, documentSymbol, codeAction, codeLens, documentLink, colorProvider, formatting,
        rangeFormatting, onTypeFormatting, rename, publishDiagnostics, foldingRange, selectionRange,
        linkedEditingRange, callHierarchy, semanticTokens, moniker
    )

@Serializable class IsaacTextDocumentClientCapabilities(
    @Serializable(with = TextDocumentSyncClientCapabilitiesSerializer::class)
    override val synchronization: TextDocumentSyncClientCapabilities? = null,

    @Serializable(with = CompletionClientCapabilitiesSerializer::class)
    override val completion: CompletionClientCapabilities? = null,

    @Serializable(with = HoverClientSerializer::class)
    override val hover: HoverClientCapabilities? = null,

    @Serializable(with = SignatureHelpClientCapabilitiesSerializer::class)
    override val signatureHelp: SignatureHelpClientCapabilities? = null,

    @Serializable(with = DeclarationClientSerializer::class)
    override val declaration: DeclarationClientCapabilities? = null,

    @Serializable(with = DefinitionClientCapabilitiesSerializer::class)
    override val definition: DefinitionClientCapabilities? = null,

    @Serializable(with = TypeDefinitionClientCapabilitiesSerializer::class)
    override val typeDefinition: TypeDefinitionClientCapabilities? = null,

    @Serializable(with = ImplementationClientSerializer::class)
    override val implementation: ImplementationClientCapabilities? = null,

    @Serializable(with = ReferenceClientCapabilitiesSerializer::class)
    override val references: ReferenceClientCapabilities? = null,

    @Serializable(with = DocumentHighlightSerializer::class)
    override val documentHighlight: DocumentHighlightClientCapabilities? = null,

    @Serializable(with = DocumentSymbolClientSerializer::class)
    override val documentSymbol: DocumentSymbolClientCapabilities? = null,

    @Serializable(with = CodeActionClientSerializer::class)
    override val codeAction: CodeActionClientCapabilities? = null,

    @Serializable(with = CodeLensClientCapabilitiesSerializer::class)
    override val codeLens: CodeLensClientCapabilities? = null,

    @Serializable(with = DocumentLinkClientSerializer::class)
    override val documentLink: DocumentLinkClientCapabilities? = null,

    @Serializable(with = DocumentColorSerializer::class)
    override val colorProvider: DocumentColorClientCapabilities? = null,

    @Serializable(with = DocumentFormattingSerializer::class)
    override val formatting: DocumentFormattingClientCapabilities? = null,

    @Serializable(with = DocumentRangeFormattingClientCapabilitiesSerializer::class)
    override val rangeFormatting: DocumentRangeFormattingClientCapabilities? = null,

    @Serializable(with = DocumentOnTypeFormattingCCSerializer::class)
    override val onTypeFormatting: DocumentOnTypeFormattingClientCapabilities? = null,

    @Serializable(with = RenameClientSerializer::class)
    override val rename: RenameClientCapabilities? = null,

    @Serializable(with = PublishDiagnosticsClientSerializer::class)
    override val publishDiagnostics: PublishDiagnosticsClientCapabilities? = null,

    @Serializable(with = FoldingRangeClientSerializer::class)
    override val foldingRange: FoldingRangeClientCapabilities? = null,

    @Serializable(with = SelectionRangeClientCapabilitiesSerializer::class)
    override val selectionRange: SelectionRangeClientCapabilities? = null,

    @Serializable(with = LinkedEditingRangeClientCapabilitiesSerializer::class)
    override val linkedEditingRange: LinkedEditingRangeClientCapabilities? = null,

    @Serializable(with = CallHierarchyClientCapabilitiesSerializer::class)
    override val callHierarchy: CallHierarchyClientCapabilities? = null,

    @Serializable(with = SemanticTokensClientCapabilitiesSerializer::class)
    override val semanticTokens: SemanticTokensClientCapabilities? = null,

    @Serializable(with = MonikerClientCapabilitiesSerializer::class)
    override val moniker: MonikerClientCapabilities? = null,
) : TextDocumentClientCapabilities

object TextDocumentClientCapabilitiesSerializer : JsonContentPolymorphicSerializer<TextDocumentClientCapabilities>(
    TextDocumentClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacTextDocumentClientCapabilities.serializer() }
