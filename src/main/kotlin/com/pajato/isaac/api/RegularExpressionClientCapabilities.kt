package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

/**
 * Client capabilities specific to regular expressions.
 */
interface RegularExpressionsClientCapabilities {
    /**
     * The engine's name.
     */
    val engine: String

    /**
     * The engine's version.
     */
    val version: String?
}

fun RegularExpressionsClientCapabilities(
    engine: String,
    version: String? = null
): RegularExpressionsClientCapabilities = IsaacRegularExpressionsClientCapabilities(engine, version)

@Serializable class IsaacRegularExpressionsClientCapabilities(
    override val engine: String,
    override val version: String? = null,
) : RegularExpressionsClientCapabilities

private typealias RegularExpressionsCC = RegularExpressionsClientCapabilities

object RegularExpressionsClientCapabilitiesSerializer : JsonContentPolymorphicSerializer<RegularExpressionsCC>(
    RegularExpressionsClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacRegularExpressionsClientCapabilities.serializer() }
