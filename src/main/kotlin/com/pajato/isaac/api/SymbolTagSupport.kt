package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

enum class SymbolTag { Deprecated }

interface SymbolTagSupport {
    /**
     * The tags supported by the client.
     */
    val valueSet: Array<SymbolTag>
}

fun SymbolTagSupport(valueSet: Array<SymbolTag>): SymbolTagSupport = IsaacSymbolTagSupport(valueSet)

@Serializable class IsaacSymbolTagSupport(override val valueSet: Array<SymbolTag>) : SymbolTagSupport

object SymbolTagSupportSerializer : JsonContentPolymorphicSerializer<SymbolTagSupport>(SymbolTagSupport::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacSymbolTagSupport.serializer()
}
