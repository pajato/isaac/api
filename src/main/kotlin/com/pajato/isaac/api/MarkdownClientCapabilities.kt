package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

/**
 * Client capabilities specific to the used Markdown parser.
 *
 * @since 3.16.0
 */
interface MarkdownClientCapabilities {
    /**
     * The name of the parser.
     */
    val parser: String

    /**
     * The version of the parser.
     */
    val version: String?
}

fun MarkdownClientCapabilities(parser: String, version: String? = null): MarkdownClientCapabilities =
    IsaacMarkdownClientCapabilities(parser, version)

@Serializable class IsaacMarkdownClientCapabilities(
    override val parser: String,
    override val version: String? = null,
) : MarkdownClientCapabilities

object MarkdownClientCapabilitiesSerializer : JsonContentPolymorphicSerializer<MarkdownClientCapabilities>(
    MarkdownClientCapabilities::class
) { override fun selectDeserializer(element: JsonElement) = IsaacMarkdownClientCapabilities.serializer() }
