package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement

/**
 * A log message augments request, notification and response messages to allow for free form text.
 */
interface LogMessage : Message {
    /**
     * The free form text.
     */
    val text: String
}

fun LogMessage(text: String, jsonrpc: String = jsonrpcVersion): LogMessage = IsaacLogMessage(jsonrpc, text)

@Serializable class IsaacLogMessage(override val jsonrpc: String, override val text: String) : LogMessage

object LogMessageSerializer : JsonContentPolymorphicSerializer<LogMessage>(LogMessage::class) {
    override fun selectDeserializer(element: JsonElement) = IsaacLogMessage.serializer()
}
