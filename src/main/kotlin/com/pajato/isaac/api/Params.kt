package com.pajato.isaac.api

import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive

/**
 * A pseudo spec for generic params?: array | object
 *
 * The instances of Params are identified as "XyzParams" for Request and Notification messages.
 */
sealed interface Params

object ParamsSerializer : JsonContentPolymorphicSerializer<Params>(Params::class) {
    private const val queryKey = "query"
    private const val processIdKey = "processId"
    private const val valueKey = "value"
    private const val messageKey = "message"
    private const val idKey = "textDocument"

    override fun selectDeserializer(element: JsonElement): DeserializationStrategy<out Params> {
        fun isStringValue(element: JsonObject) = element[valueKey] is JsonPrimitive &&
            (element[valueKey] as JsonPrimitive).isString

        fun isNumberValue(element: JsonObject) = element[valueKey] is JsonPrimitive &&
            !(element[valueKey] as JsonPrimitive).isString

        fun isArrayValue(element: JsonObject) = element.containsKey(valueKey) && element[valueKey] is JsonArray

        return when {
            element is JsonObject && element.containsKey(processIdKey) -> IsaacInitializeParams.serializer()
            element is JsonObject && element.containsKey(queryKey) -> IsaacWorkspaceSymbolParams.serializer()
            element is JsonObject && element.size == 0 -> ShutdownParams.serializer()
            element is JsonObject && isStringValue(element) -> StringParams.serializer()
            element is JsonObject && isNumberValue(element) -> NumberParams.serializer()
            element is JsonObject && isArrayValue(element) -> ListParams.serializer()
            element is JsonObject && element.containsKey(messageKey) -> IsaacShowMessageParams.serializer()
            element is JsonObject && element.containsKey(idKey) -> IsaacDocumentSymbolParams.serializer()
            else -> throw IllegalStateException("Invalid element: $element!")
        }
    }
}

@Serializable
object ShutdownParams : Params

@Serializable
class ListParams(val value: List<String>) : Params

@Serializable
class NumberParams(val value: Int) : Params

@Serializable
class StringParams(val value: String) : Params
