package com.pajato.isaac.api

import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.JsonTransformingSerializer
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.put

@Serializable sealed class ChangeNotifications

@Serializable class BooleanChangeNotifications(val flag: Boolean) : ChangeNotifications()
@Serializable class StringChangeNotifications(val id: String) : ChangeNotifications()

object ChangeNotificationsTransformingSerializer : JsonTransformingSerializer<ChangeNotifications>(
    ChangeNotifications.serializer()
) {
    override fun transformSerialize(element: JsonElement): JsonElement {
        val flag = element.jsonObject["flag"]
        val id = element.jsonObject["id"]
        return if (flag != null) flag as JsonPrimitive else id as JsonPrimitive
    }

    override fun transformDeserialize(element: JsonElement): JsonElement {
        check(element is JsonPrimitive) { "The element is not a supported type (JsonPrimitive)!" }
        fun getIdObject(): JsonObject = buildJsonObject {
            put("type", "com.pajato.isaac.api.StringChangeNotifications")
            put("id", element.content)
        }
        fun getFlagObject() = buildJsonObject {
            put("type", "com.pajato.isaac.api.BooleanChangeNotifications")
            put("flag", element.content.toBoolean())
        }

        return if (element.isString) getIdObject() else getFlagObject()
    }
}
