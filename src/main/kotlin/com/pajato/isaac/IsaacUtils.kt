package com.pajato.isaac

import com.pajato.isaac.api.Id
import com.pajato.isaac.api.InitializeParams
import com.pajato.isaac.api.Params
import com.pajato.isaac.api.RequestMessage
import com.pajato.isaac.api.ShutdownParams
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.buildJsonObject
import java.lang.management.ManagementFactory

const val clientErrorTag = "ClientError"
const val clientInfoTag = "ClientInfo/Debug"
const val clientNotificationTag = "ClientNotification"
const val clientRequestTag = "ClientRequest"
const val clientResponseTag = "ClientResponse"
const val serverResponseTag = "ServerResponse"
const val serverErrorTag = "ServerError"
const val serverInfoTag = "ServerInfo/Debug"
const val serverNotificationTag = "ServerNotification"

fun getProcessIdFromFactory() = ManagementFactory.getRuntimeMXBean().name.split("@")[0].toInt()

fun stripType(element: JsonObject): JsonElement = buildJsonObject {
    val typeKey = "type"
    val elements = element.filterNot { it.key == typeKey }
    for (item in elements) put(item.key, item.value)
}

fun getInitializeRequestMessage(params: InitializeParams) = RequestMessage(
    Id.NumberId(1), "initialize", params as Params
)

fun getShutdownRequestMessage(id: Int) = RequestMessage(Id.NumberId(id), "shutdown", ShutdownParams)
